#! /usr/bin/python

def ReturnTTLightList(Generator):
    PP6radHi_List  = ["410001_ttlight"]
    PP6radLo_List  = ["410002_ttlight"]
    MCNLO_Hpp_List = ["410003_ttlight"]
    Pow_Hpp_List   = ["410004_ttlight"]
    PP8radHi_List  = ["410511_ttlight"]
    PP8radLo_List  = ["410512_ttlight"]

    if Generator == "PP6radHi":
        return PP6radHi_List
    if Generator == "PP6radLo":
        return PP6radLo_List
    if Generator == "MCNLO_Hpp":
        return MCNLO_Hpp_List
    if Generator == "Pow_Hpp":
        return Pow_Hpp_List
    if Generator == "PP8radHi":
        return PP8radHi_List
    if Generator == "PP8radLo":
        return PP8radLo_List
    

def ReturnTTBBList(Generator):
    PP6radHi_List  = ["410001_ttbb"]
    PP6radLo_List  = ["410002_ttbb"]
    MCNLO_Hpp_List = ["410003_ttbb"]
    Pow_Hpp_List   = ["410004_ttbb"]
    PP8radHi_List  = ["410511_ttbb"]
    PP8radLo_List  = ["410512_ttbb"]

    if Generator == "PP6radHi":
        return PP6radHi_List
    if Generator == "PP6radLo":
        return PP6radLo_List
    if Generator == "MCNLO_Hpp":
        return MCNLO_Hpp_List
    if Generator == "Pow_Hpp":
        return Pow_Hpp_List
    if Generator == "PP8radHi":
        return PP8radHi_List
    if Generator == "PP8radLo":
        return PP8radLo_List


def ReturnTTCCList(Generator):
    PP6radHi_List  = ["410001_ttcc"]
    PP6radLo_List  = ["410002_ttcc"]
    MCNLO_Hpp_List = ["410003_ttcc"]
    Pow_Hpp_List   = ["410004_ttcc"]
    PP8radHi_List  = ["410511_ttcc"]
    PP8radLo_List  = ["410512_ttcc"]

    if Generator == "PP6radHi":
        return PP6radHi_List
    if Generator == "PP6radLo":
        return PP6radLo_List
    if Generator == "MCNLO_Hpp":
        return MCNLO_Hpp_List
    if Generator == "Pow_Hpp":
        return Pow_Hpp_List
    if Generator == "PP8radHi":
        return PP8radHi_List
    if Generator == "PP8radLo":
        return PP8radLo_List

def ReturnTTbarList(Generator):
    PP6radHi_List  = ["410001"]
    PP6radLo_List  = ["410002"]
    MCNLO_Hpp_List = ["410003"]
    Pow_Hpp_List   = ["410004"]
    PP8radHi_List  = ["410511"]
    PP8radLo_List  = ["410512"]

    if Generator == "PP6radHi":
        return PP6radHi_List
    if Generator == "PP6radLo":
        return PP6radLo_List
    if Generator == "MCNLO_Hpp":
        return MCNLO_Hpp_List
    if Generator == "Pow_Hpp":
        return Pow_Hpp_List
    if Generator == "PP8radHi":
        return PP8radHi_List
    if Generator == "PP8radLo":
        return PP8radLo_List

def ReturnTTLight4TTHList(Generator):
    PowPy6List = [
        "410000_ttlight",
        "410120_ttlight",
    ]
    PowPy8List = [
        "410501_ttlight",
        "410504_ttlight",
    ]
    PowH7List = [
        "410525_ttlight",
        "410528_ttlight",
    ]
    MCNLO_Py8List = [
        "410225_ttlight",
        "410274_ttlight",
    ]

    if Generator == "PP6":
        return PowPy6List
    if Generator == "PP8":
        return PowPy8List
    if Generator == "PH7":
        return PowH7List
    if Generator == "MCNLO_P8":
        return MCNLO_Py8List


def ReturnTTCC4TTHList(Generator):
    PowPy6List = [
        "410000_ttcc",
        "410120_ttcc",
    ]
    PowPy8List = [
        "410501_ttcc",
        "410504_ttcc",
    ]
    PowH7List = [
        "410525_ttcc",
        "410528_ttcc",
    ]
    MCNLO_Py8List = [
        "410225_ttcc",
        "410274_ttcc",
    ]

    if Generator == "PP6":
        return PowPy6List
    if Generator == "PP8":
        return PowPy8List
    if Generator == "PH7":
        return PowH7List
    if Generator == "MCNLO_P8":
        return MCNLO_Py8List

def ReturnTTBB4TTHList(Generator):
    PowPy6List = [
        "410000_ttbb",
        "410120_ttbb",
    ]
    PowPy8List = [
        "410501_ttbb",
        "410504_ttbb",
    ]
    PowH7List = [
        "410525_ttbb",
        "410528_ttbb",
    ]
    MCNLO_Py8List = [
        "410225_ttbb",
        "410274_ttbb",
    ]

    if Generator == "PP6":
        return PowPy6List
    if Generator == "PP8":
        return PowPy8List
    if Generator == "PH7":
        return PowH7List
    if Generator == "MCNLO_P8":
        return MCNLO_Py8List

def ReturnTTbar4TTHList(Generator):
    PowPy6List = [
        "410000",
        "410120",
    ]
    PowPy8List = [
        "410501",
        "410504",
    ]
    PowH7List = [
        "410525",
        "410528",
    ]
    MCNLO_Py8List = [
        "410225",
        "410274",
    ]
    # Sherpa ttbar samples, l+jets + and - and dilepton
    Sherpa_List = [
        "410250",
        "410251",
        "410252"
        ]
    Sherpa_AF2_List = [
        "410250",
        "410251",
        "410252",
        "410281",
        "410282",
        "410283",
        ]
    # Sherpa tt+bb open-loops sample
    Sherpa_ttbb_List = [
        "410323",
        "410324",
        "310325"
        ]

    PP8radHi_withBFilt_List = ReturnTTbarList('PP8radHi')
    PP8radHi_withBFilt_List.append("410517")

    PP8radLo_withBFilt_List = ReturnTTbarList('PP8radLo')
    PP8radLo_withBFilt_List.append("410518")

    if Generator == "PP6":
        return PowPy6List
    if Generator == "PP8":
        return PowPy8List
    if Generator == "PP8radLo":
        return PP8radLo_withBFilt_List
    if Generator == "PP8radHi":
        return PP8radHi_withBFilt_List
    if Generator == "PH7":
        return PowH7List
    if Generator == "MCNLO_P8":
        return MCNLO_Py8List
    if Generator == "Sherpa":
        return Sherpa_List
    if Generator == "SherpaAF2":
        return Sherpa_AF2_List
    if Generator == "Sherpa_ttbb":
        return Sherpa_ttbb_List

def ReturnSingleTopList_Dilepton(Generator):
    PowhegPy6List = [
        "410011",
        "410012",
        "410013",
        "410014",
        "410025",
        "410026"
    ]

    if Generator == "PowhegPy6":
        return PowhegPy6List

def ReturnSingleTopList_tChan(Generator):
    PowPy6List = [
        "410011",
        "410012"
        ]
    PowPy6RadHiList = [
        "410018",
        "410020"
        ]
    PowPy6RadLoList = [
        "410017",
        "410019"
        ]
    PowHppList = [
        "410047",
        "410048"
        ]
    if Generator == "PowhegPy6":
        return PowPy6List
    if Generator == "PowhegHpp":
        return PowHppList
    if Generator == "PP6_radHi":
        return PowPy6RadHiList
    if Generator == "PP6_radLo":
        return PowPy6RadLoList


def ReturnSingleTopList_sChan(Generator):
    PowPy6List = [
        "410025",
        "410026"
        ]

    if Generator == "PowhegPy6":
        return PowPy6List

def ReturnSingleTopList_WtChan(Generator):
    PowPy6List = [
        "410013",
        "410014"
    ]

    PowHppList = [
        "410147",
        "410148"
    ]
    
    DSList = [
        "410062",
        "410063"
    ]

    radHiList = [
        "410099",
        "410101"
    ]

    radLoList = [
        "410100",
        "410102"
    ]
    
    if Generator == "PowhegPy6":
        return PowPy6List
    elif Generator =="PowhegHpp":
        return PowHppList
    elif Generator == "PP6_DS":
        return DSList
    elif Generator == "PP6_radHi":
        return radHiList
    elif Generator == "PP6_radLo":
        return radLoList
    



def ReturnTTVList(Generator):
    
    MadgraphList = [
        "410050","410215",
        "410155","410156","410157",
        "410218","410219","410220",
        "410080","410081"
    ]
    
    return MadgraphList


def ReturnTTWList(Generator):
    MadgraphList = [
        "410155"
    ]
    SherpaList = [
        "410144"
        ]
    
    if Generator=='Madgraph' or Generator=='':
        return MadgraphList
    if Generator=='Sherpa':
        return SherpaList
            
def ReturnTZList(Generator):

    MadgraphList = [ "410050"]

    return MadgraphList

def ReturnTWZList(Generator):

    MadgraphList = [ "410215"]

    return MadgraphList
                 
def Return4TopList(Generator):

    MadgraphList = ["410080"]

    return MadgraphList

def ReturnTTWWList(Generator):

    MadgraphList = ["410081"]

    return MadgraphList
                                   
def ReturnOtherTTVList(Generator):
    MadgraphList = ReturnTZList(Generator)
    MadgraphList.extend( ReturnTWZList(Generator) )
    MadgraphList.extend( Return4TopList(Generator) )
    MadgraphList.extend( ReturnTTWWList(Generator) )
    return MadgraphList

def ReturnTTZList(Generator):
    MadgraphList = [
        "410156","410157",
        "410218","410219","410220"
    ]
    SherpaList = [
        "410142", "410143"
        ]

    if Generator=='Madgraph' or Generator=='':
        return MadgraphList  
    if Generator=='Sherpa':
        return SherpaList

def ReturnDibosonList(Generator):

    SherpaList = [
        "361063","361064","361065","361066","361067",
        "361068","361070","361071","361072","361073",
        "361077","361091","361092","361093","361094",
        "361095","361096", "361097"
    ]

    return SherpaList

def ReturnZjetsList(Generator):

    Sherpa221List = [
        "364100",        "364101",        "364102",        "364103",
        "364104",        "364105",        "364106",        "364107",
        "364108",        "364109",        "364110",        "364111",
        "364112",        "364113",        "364114",        "364115",
        "364116",        "364117",        "364118",        "364119",
        "364120",        "364121",        "364122",        "364123",
        "364124",        "364125",        "364126",        "364127",
        "364128",        "364129",
        "364130",        "364131",        "364132",        "364133",
        "364134",        "364135",        "364136",        "364137",
        "364138",        "364139",        "364140",        "364141",
        
        "364198",        "364199",        "364200",        "364201",
        "364202",        "364203",        "364204",        "364205",
        "364206",        "364207",        "364208",        "364209",
        "364210",        "364211",        "364212",        "364213",
        "364214",        "364215"
        ]    
    
    SherpaList = [
        "361372",        "361373",        "361374",        "361375",        "361376",        "361377",        "361378",        "361379",
        "361380",        "361381",        "361382",        "361383",        "361384",        "361385",        "361386",        "361387",
        "361388",        "361389",        "361390",        "361391",        "361392",        "361393",        "361394",        "361395",
        "361396",        "361397",        "361398",        "361399",        "361400",        "361401",        "361402",        "361403",
        "361404",        "361405",        "361406",        "361407",        "361408",        "361409",        "361410",        "361411",
        "361412",        "361413",        "361414",        "361415",        "361416",        "361417",        "361418",        "361419",
        "361420",        "361421",        "361422",        "361423",        "361424",        "361425",        "361426",        "361427",
        "361428",        "361429",        "361430",        "361431",        "361432",        "361433",        "361434",        "361435",
        "361436",        "361437",        "361438",        "361439",        "361440",        "361441",        "361442",        "361443",
        "361468",        "361469",        "361470",        "361471",        "361472",        "361473",        "361474",        "361475",
        "361476",        "361477",        "361478",        "361479",        "361480",        "361481",        "361482",        "361483",
        "361484",        "361485",        "361486",        "361487",        "361488",        "361489",        "361490",        "361491"
    ]
    
    MadgraphList = [
        "361500",
        "361501",
        "361502",
        "361503",
        "361504",
        "361505",
        "361506",
        "361507",
        "361508",
        "361509",
        "361510",
        "361511",
        "361512",
        "361513",
        "361514",
        "361520",
        "361521",
        "361522",
        "361523",
        "361524",
        "361525",
        "361526",
        "361527",
        "361528",
        "361529",
        "361530",
        "361531",
        "361532",
        "361533",
        "361534"
    ]

    List = [
        "361106",
        "361107",
        "361108",
        ]

    
    return Sherpa221List


    if Generator == "Sherpa":
        return SherpaList
    elif Generator == "Madgraph":
        return MadgraphList

def ReturnWjetsList(Generator):


    Sherpa221List = [
        "364156",        "364157",
        "364158",        "364159",        "364160",        "364161",
        "364162",        "364163",        "364164",        "364165",
        "364166",        "364167",        "364168",        "364169",
        
        "364170",        "364171",        "364172",        "364173",
        "364174",        "364175",        "364176",        "364177",
        "364178",        "364179",        "364180",        "364181",
        "364182",        "364183",

        "364184",        "364185",        "364186",        "364187",
        "364188",        "364189",        "364190",        "364191",
        "364192",        "364193",        "364194",        "364195",
        "364196",        "364197",
        ]


    SherpaList = [
        "361300",        "361301",        "361302",        "361303",        "361304",        "361305",        "361306",        "361307",
        "361308",        "361309",        "361310",        "361311",        "361312",        "361313",        "361314",        "361315",
        "361316",        "361317",        "361318",        "361319",        "361320",        "361321",        "361322",        "361323",
        "361324",        "361325",        "361326",        "361327",        "361328",        "361329",        "361330",        "361331",
        "361332",        "361333",        "361334",        "361335",        "361336",        "361337",        "361338",        "361339",
        "361340",        "361341",        "361342",        "361343",        "361344",        "361345",        "361346",        "361347",
        "361348",        "361349",        "361350",        "361351",        "361352",        "361353",        "361354",        "361355",
        "361356",        "361357",        "361358",        "361359",        "361360",        "361361",        "361362",        "361363",
        "361364",        "361365",        "361366",        "361367",        "361368",        "361369",        "361370",        "361371"
    ]

    List = [
        "361100",
        "361101",
        "361102",
        "361103",
        "361104",
        "361105",
        ]


    return Sherpa221List


def ReturnTTHList(Generator):
    if Generator == "aMCatNLO_HerwigPP":
        List = [
            "341177",
            "341270",
            "341271"
        ]
    elif Generator == "aMCatNLO_Pythia8":
        List = [
            "343367",
            "343366",
            "343365"
        ]
    else:
        List = []

    return List

def ReturnTHJBList():
    List = [
        "343267",
        "343270",
        "343273"
        ]
    
    return List


def ReturnTWHList():
    List = [
        "341998",
        "342001",
        "342004"
        ]
    
    return List



def ReturnDataList2016():

    DataList = [
        "periodA",
        "periodB",
        "periodC",
        "periodD",
        "periodE",
        "periodF",
        "periodG",
        "periodI",
        "periodK",
        "periodL",
        ]

    return DataList

def ReturnDataList2015():

    DataList = [
        "periodD",
        "periodE",
        "periodF",
        "periodG",
        "periodH",
        "periodJ",
        ]
    
    return DataList
  

def ReturnTreeList():
    List = [
        "nominal",  #                                           "nominal_Loose",
        "MET_SoftTrk_ResoPara", 
        "MET_SoftTrk_ResoPerp", 
        "MET_SoftTrk_ScaleUp",                                 "MET_SoftTrk_ScaleDown", 
        "MUON_SAGITTA_RESBIAS__1up",                           "MUON_SAGITTA_RESBIAS__1down",
        "MUON_SAGITTA_RHO__1up",                               "MUON_SAGITTA_RHO__1up",   
        "MUON_ID__1up",                                        "MUON_ID__1down", 
        "MUON_MS__1up",                                        "MUON_MS__1down", 
        "MUON_SCALE__1up",                                     "MUON_SCALE__1down",
        "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up",               "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down"
        "TAUS_TRUEHADTAU_SME_TES_INSITU__1up",                 "TAUS_TRUEHADTAU_SME_TES_INSITU__1down"
        "TAUS_TRUEHADTAU_SME_TES_MODEL__1up",                  "TAUS_TRUEHADTAU_SME_TES_MODEL__1down"
        "EG_SCALE_ALL__1up",                                   "EG_SCALE_ALL__1down", 
        "EG_RESOLUTION_ALL__1up",                              "EG_RESOLUTION_ALL__1down",
        "JET_JER_SINGLE_NP__1up",  
        "JET_21NP_JET_BJES_Response__1up",                     "JET_21NP_JET_BJES_Response__1down",
        "JET_21NP_JET_EffectiveNP_1__1up",                     "JET_21NP_JET_EffectiveNP_1__1down",
        "JET_21NP_JET_EffectiveNP_2__1up",                     "JET_21NP_JET_EffectiveNP_2__1down",
        "JET_21NP_JET_EffectiveNP_3__1up",                     "JET_21NP_JET_EffectiveNP_3__1down",
        "JET_21NP_JET_EffectiveNP_4__1up",                     "JET_21NP_JET_EffectiveNP_4__1down",
        "JET_21NP_JET_EffectiveNP_5__1up",                     "JET_21NP_JET_EffectiveNP_5__1down",
        "JET_21NP_JET_EffectiveNP_6__1up",                     "JET_21NP_JET_EffectiveNP_6__1down",
        "JET_21NP_JET_EffectiveNP_7__1up",                     "JET_21NP_JET_EffectiveNP_7__1down",
        "JET_21NP_JET_EffectiveNP_8restTerm__1up",             "JET_21NP_JET_EffectiveNP_8restTerm__1down",
        "JET_21NP_JET_EtaIntercalibration_Modelling__1up",     "JET_21NP_JET_EtaIntercalibration_Modelling__1down",
        "JET_21NP_JET_EtaIntercalibration_NonClosure__1up",    "JET_21NP_JET_EtaIntercalibration_NonClosure__1down",
        "JET_21NP_JET_EtaIntercalibration_TotalStat__1up",     "JET_21NP_JET_EtaIntercalibration_TotalStat__1down",
        "JET_21NP_JET_Flavor_Response__1up",                   "JET_21NP_JET_Flavor_Response__1down", 
        "JET_21NP_JET_Flavor_Composition__1up",                "JET_21NP_JET_Flavor_Composition__1down", 
        "JET_21NP_JET_PunchThrough_MC15__1up",                 "JET_21NP_JET_PunchThrough_MC15__1down", 
        "JET_21NP_JET_Pileup_PtTerm__1up",                     "JET_21NP_JET_Pileup_PtTerm__1down", 
        "JET_21NP_JET_Pileup_OffsetNPV__1up",                  "JET_21NP_JET_Pileup_OffsetNPV__1down", 
        "JET_21NP_JET_Pileup_OffsetMu__1up",                   "JET_21NP_JET_Pileup_OffsetMu__1down", 
        "JET_21NP_JET_Pileup_RhoTopology__1up",                "JET_21NP_JET_Pileup_RhoTopology__1down",
        "JET_21NP_JET_SingleParticle_HighPt__1up",             "JET_21NP_JET_SingleParticle_HighPt__1down", 
                                                            
    ]

    return List

def ReturnOtherList():
    List = [
        "EG_SCALE_ALL__1up",                               "EG_SCALE_ALL__1down",
        "EG_RESOLUTION_ALL__1up",                          "EG_RESOLUTION_ALL__1down",
        "MUON_ID__1up",                                    "MUON_ID__1down",
        "MUON_MS__1up",                                    "MUON_MS__1down",
        "MUON_SCALE__1up",                                 "MUON_SCALE__1down"
        "MUON_SAGITTA_RESBIAS__1up",                       "MUON_SAGITTA_RESBIAS__1down"
        "MUON_SAGITTA_RHO__1up",                           "MUON_SAGITTA_RHO__1down"
        "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up",           "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down"
        "TAUS_TRUEHADTAU_SME_TES_INSITU__1up",             "TAUS_TRUEHADTAU_SME_TES_INSITU__1down"
        "TAUS_TRUEHADTAU_SME_TES_MODEL__1up",              "TAUS_TRUEHADTAU_SME_TES_MODEL__1down"
        ]

    return List


def ReturnLargeRList():
    List = [
        "LARGERJET_Weak_JET_Rtrk_Baseline_mass__1up",          "LARGERJET_Weak_JET_Rtrk_Baseline_mass__1down",
        "LARGERJET_Weak_JET_Rtrk_Modelling_mass__1up",         "LARGERJET_Weak_JET_Rtrk_Modelling_mass__1down",
        "LARGERJET_Weak_JET_Rtrk_Tracking_mass__1up",          "LARGERJET_Weak_JET_Rtrk_Tracking_mass__1down",
        "LARGERJET_Weak_JET_Rtrk_TotalStat_mass__1up",         "LARGERJET_Weak_JET_Rtrk_TotalStat_mass__1down",
        "LARGERJET_Weak_JET_Rtrk_Baseline_pT__1up",            "LARGERJET_Weak_JET_Rtrk_Baseline_pT__1down",
        "LARGERJET_Weak_JET_Rtrk_Modelling_pT__1up",           "LARGERJET_Weak_JET_Rtrk_Modelling_pT__1down",
        "LARGERJET_Weak_JET_Rtrk_Tracking_pT__1up",            "LARGERJET_Weak_JET_Rtrk_Tracking_pT__1down",
        "LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1up",           "LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1down",
        "LARGERJET_Weak_JET_Rtrk_Baseline_Tau32__1up",         "LARGERJET_Weak_JET_Rtrk_Baseline_Tau32__1down",
        "LARGERJET_Weak_JET_Rtrk_Modelling_Tau32__1up",        "LARGERJET_Weak_JET_Rtrk_Modelling_Tau32__1down",
        "LARGERJET_Weak_JET_Rtrk_Tracking_Tau32__1up",         "LARGERJET_Weak_JET_Rtrk_Tracking_Tau32__1down",
        "LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32__1up",        "LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32__1down",
        "LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1up",         "LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1down",                                                    
        "LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1up",         "LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1down",                                                    
        "LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1up",        "LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1down",                                                    
        "LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1up",        "LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1down",
        "LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1up",        "LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1down",                                                    
        "LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1up",        "LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1down",                                                    
        "LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1up",         "LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1down",                                                    
        "LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1up",         "LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1down",                                                    
        "LARGERJET_Strong_JET_Rtrk_Baseline_All__1up",         "LARGERJET_Strong_JET_Rtrk_Baseline_All__1down",                                                    
        "LARGERJET_Strong_JET_Rtrk_Modelling_All__1u",         "LARGERJET_Strong_JET_Rtrk_Modelling_All__1down",                                                    
        "LARGERJET_Strong_JET_Rtrk_TotalStat_All__1up",        "LARGERJET_Strong_JET_Rtrk_TotalStat_All__1down",                                                    
        "LARGERJET_Strong_JET_Rtrk_Tracking_All__1up",         "LARGERJET_Strong_JET_Rtrk_Tracking_All__1down",
    ]

    return List

def ReturnSmallRList():
    List = [
        "JET_JER_SINGLE_NP__1up",
        "JET_21NP_JET_BJES_Response__1up",                  "JET_21NP_JET_BJES_Response__1down",
        "JET_21NP_JET_EffectiveNP_1__1up",                  "JET_21NP_JET_EffectiveNP_1__1down",
        "JET_21NP_JET_EffectiveNP_2__1up",                  "JET_21NP_JET_EffectiveNP_2__1down",
        "JET_21NP_JET_EffectiveNP_3__1up",                  "JET_21NP_JET_EffectiveNP_3__1down",
        "JET_21NP_JET_EffectiveNP_4__1up",                  "JET_21NP_JET_EffectiveNP_4__1down",
        "JET_21NP_JET_EffectiveNP_5__1up",                  "JET_21NP_JET_EffectiveNP_5__1down",
        "JET_21NP_JET_EffectiveNP_6__1up",                  "JET_21NP_JET_EffectiveNP_6__1down",
        "JET_21NP_JET_EffectiveNP_7__1up",                  "JET_21NP_JET_EffectiveNP_7__1down",
        "JET_21NP_JET_EffectiveNP_8restTerm__1up",          "JET_21NP_JET_EffectiveNP_8restTerm__1down",
        "JET_21NP_JET_EtaIntercalibration_Modelling__1up",  "JET_21NP_JET_EtaIntercalibration_Modelling__1down",
        "JET_21NP_JET_EtaIntercalibration_NonClosure__1up", "JET_21NP_JET_EtaIntercalibration_NonClosure__1down",
        "JET_21NP_JET_EtaIntercalibration_TotalStat__1up",  "JET_21NP_JET_EtaIntercalibration_TotalStat__1down",
        "JET_21NP_JET_Flavor_Response__1up",                "JET_21NP_JET_Flavor_Response__1down",
        "JET_21NP_JET_Flavor_Composition__1up",             "JET_21NP_JET_Flavor_Composition__1down",
        "JET_21NP_JET_PunchThrough_MC15__1up",              "JET_21NP_JET_PunchThrough_MC15__1down",
        "JET_21NP_JET_Pileup_PtTerm__1up",                  "JET_21NP_JET_Pileup_PtTerm__1down",
        "JET_21NP_JET_Pileup_OffsetNPV__1up",               "JET_21NP_JET_Pileup_OffsetNPV__1down",
        "JET_21NP_JET_Pileup_OffsetMu__1up",                "JET_21NP_JET_Pileup_OffsetMu__1down",
        "JET_21NP_JET_Pileup_RhoTopology__1up",             "JET_21NP_JET_Pileup_RhoTopology__1down",
        "JET_21NP_JET_SingleParticle_HighPt__1up",          "JET_21NP_JET_SingleParticle_HighPt__1down",
    ]

    return List

def ReturnSFSystList():
    List = [
        "weight_pileup_UP", "weight_pileup_DOWN",
        "leptonSF_EL_SF_Trigger_UP",             "leptonSF_EL_SF_Trigger_DOWN", 
        "leptonSF_EL_SF_Reco_UP",                "leptonSF_EL_SF_Reco_DOWN",
        "leptonSF_EL_SF_ID_UP",                  "leptonSF_EL_SF_ID_DOWN", 
        "leptonSF_EL_SF_Isol_UP",                "leptonSF_EL_SF_Isol_DOWN",
        "leptonSF_MU_SF_Trigger_STAT_UP",        "leptonSF_MU_SF_Trigger_STAT_DOWN", 
        "leptonSF_MU_SF_Trigger_SYST_UP",        "leptonSF_MU_SF_Trigger_SYST_DOWN",
        "leptonSF_MU_SF_ID_STAT_UP",             "leptonSF_MU_SF_ID_STAT_DOWN",
        "leptonSF_MU_SF_ID_SYST_UP",             "leptonSF_MU_SF_ID_SYST_DOWN",
        "leptonSF_MU_SF_Isol_STAT_UP",           "leptonSF_MU_SF_Isol_STAT_DOWN", 
        "leptonSF_MU_SF_Isol_SYST_UP",           "leptonSF_MU_SF_Isol_SYST_DOWN",
        "leptonSF_MU_SF_TTVA_STAT_UP",           "leptonSF_MU_SF_TTVA_STAT_DOWN", 
        "leptonSF_MU_SF_TTVA_SYST_UP",           "leptonSF_MU_SF_TTVA_SYST_DOWN",

        "bTagSF_77_extrapolation_up",            "bTagSF_77_extrapolation_down", 
        "bTagSF_77_extrapolation_from_charm_up", "bTagSF_77_extrapolation_from_charm_down",
        "jvt_UP", "jvt_DOWN",

        "bTagSF_77_eigenvars_B_up_0",            "bTagSF_77_eigenvars_B_up_1",
        "bTagSF_77_eigenvars_B_up_2",            "bTagSF_77_eigenvars_B_up_3",
        "bTagSF_77_eigenvars_B_up_4",            "bTagSF_77_eigenvars_B_down_0",
        "bTagSF_77_eigenvars_B_down_1",          "bTagSF_77_eigenvars_B_down_2",
        "bTagSF_77_eigenvars_B_down_3",          "bTagSF_77_eigenvars_B_down_4",

        "bTagSF_77_eigenvars_C_up_0",            "bTagSF_77_eigenvars_C_up_1",
        "bTagSF_77_eigenvars_C_up_2",            "bTagSF_77_eigenvars_C_up_3",
        "bTagSF_77_eigenvars_C_down_0",          "bTagSF_77_eigenvars_C_down_1",
        "bTagSF_77_eigenvars_C_down_2",          "bTagSF_77_eigenvars_C_down_3",

        "bTagSF_77_eigenvars_Light_up_0",        "bTagSF_77_eigenvars_Light_up_1",
        "bTagSF_77_eigenvars_Light_up_2",        "bTagSF_77_eigenvars_Light_up_3",
        "bTagSF_77_eigenvars_Light_up_4",        "bTagSF_77_eigenvars_Light_up_5",
        "bTagSF_77_eigenvars_Light_up_6",        "bTagSF_77_eigenvars_Light_up_7",
        "bTagSF_77_eigenvars_Light_up_8",        "bTagSF_77_eigenvars_Light_up_9",
        "bTagSF_77_eigenvars_Light_up_10",       "bTagSF_77_eigenvars_Light_up_11",
        "bTagSF_77_eigenvars_Light_down_0",      "bTagSF_77_eigenvars_Light_down_1",
        "bTagSF_77_eigenvars_Light_down_2",      "bTagSF_77_eigenvars_Light_down_3",
        "bTagSF_77_eigenvars_Light_down_4",      "bTagSF_77_eigenvars_Light_down_5",
        "bTagSF_77_eigenvars_Light_down_6",      "bTagSF_77_eigenvars_Light_down_7",
        "bTagSF_77_eigenvars_Light_down_8",      "bTagSF_77_eigenvars_Light_down_9",
        "bTagSF_77_eigenvars_Light_down_10",     "bTagSF_77_eigenvars_Light_down_11",
    ]

    return List
