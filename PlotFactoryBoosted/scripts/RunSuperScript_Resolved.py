#! /usr/bin/python

import os
import sys
import glob

import DefineSamples
import SuperScript

RootCoreDir = os.environ["ROOTCOREBIN"] + "/data/PlotFactoryBoosted/"
CodeFolder  = os.environ["ROOTCOREBIN"]
CodeFolder  = CodeFolder.replace("RootCoreBin", "")

###############################################################################
########################## To be specified by user ############################
###############################################################################

#SampleFolder = "/nfs/atlas/ttH02/BoostedSamples_CentralProduction_July2016/"
#SampleFolder = "/data/atlas07/aalshehri/production_v43/"
SampleFolder = "/data/atlas07/aalshehri/production_v44/" #systematic


#ttH mainfolder
#MainFolder   = "/afs/phas.gla.ac.uk/user/s/sboutle/PlotFactoryOutput/"
#MainFolder   = "/data/atlas06/aalshehri/PlotFactory_KLFitter_20_7_916"
MainFolder   = "/data/atlas06/aalshehri/PlotFactory_KLFitter_20_7_916_sys"

os.system("mkdir -p "+MainFolder)

#TreeList           = DefineSamples.ReturnLargeRList()
#TreeList           = DefineSamples.ReturnOtherList()
#TreeList           = DefineSamples.ReturnTreeList()
#TreeList           = ["nominal","EG_RESOLUTION_ALL__1down"]
#TreeList           = ["nominal"]

TreeList           = [
        "nominal",  #                                           "nominal_Loose",                                                       
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_ScaleUp",                                 "MET_SoftTrk_ScaleDown",
        "MUON_SAGITTA_RESBIAS__1up",                           "MUON_SAGITTA_RESBIAS__1down",
        "MUON_SAGITTA_RHO__1up",                               "MUON_SAGITTA_RHO__1up",
        "MUON_ID__1up",                                        "MUON_ID__1down",
        "MUON_MS__1up",                                        "MUON_MS__1down",
        "MUON_SCALE__1up",                                     "MUON_SCALE__1down",
        "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up",               "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down"
        "TAUS_TRUEHADTAU_SME_TES_INSITU__1up",                 "TAUS_TRUEHADTAU_SME_TES_INSITU__1down"
        "TAUS_TRUEHADTAU_SME_TES_MODEL__1up",                  "TAUS_TRUEHADTAU_SME_TES_MODEL__1down"
        "EG_SCALE_ALL__1up",                                   "EG_SCALE_ALL__1down",
        "EG_RESOLUTION_ALL__1up",                              "EG_RESOLUTION_ALL__1down",
        "JET_JER_SINGLE_NP__1up",
        "JET_21NP_JET_BJES_Response__1up",                     "JET_21NP_JET_BJES_Response__1down",
        "JET_21NP_JET_EffectiveNP_1__1up",                     "JET_21NP_JET_EffectiveNP_1__1down",
        "JET_21NP_JET_EffectiveNP_2__1up",                     "JET_21NP_JET_EffectiveNP_2__1down",
        "JET_21NP_JET_EffectiveNP_3__1up",                     "JET_21NP_JET_EffectiveNP_3__1down",
        "JET_21NP_JET_EffectiveNP_4__1up",                     "JET_21NP_JET_EffectiveNP_4__1down",
        "JET_21NP_JET_EffectiveNP_5__1up",                     "JET_21NP_JET_EffectiveNP_5__1down",
        "JET_21NP_JET_EffectiveNP_6__1up",                     "JET_21NP_JET_EffectiveNP_6__1down",
        "JET_21NP_JET_EffectiveNP_7__1up",                     "JET_21NP_JET_EffectiveNP_7__1down",
        "JET_21NP_JET_EffectiveNP_8restTerm__1up",             "JET_21NP_JET_EffectiveNP_8restTerm__1down",
        "JET_21NP_JET_EtaIntercalibration_Modelling__1up",     "JET_21NP_JET_EtaIntercalibration_Modelling__1down",
        "JET_21NP_JET_EtaIntercalibration_NonClosure__1up",    "JET_21NP_JET_EtaIntercalibration_NonClosure__1down",
        "JET_21NP_JET_EtaIntercalibration_TotalStat__1up",     "JET_21NP_JET_EtaIntercalibration_TotalStat__1down",
        "JET_21NP_JET_Flavor_Response__1up",                   "JET_21NP_JET_Flavor_Response__1down",
        "JET_21NP_JET_Flavor_Composition__1up",                "JET_21NP_JET_Flavor_Composition__1down",
        "JET_21NP_JET_PunchThrough_MC15__1up",                 "JET_21NP_JET_PunchThrough_MC15__1down",
        "JET_21NP_JET_Pileup_PtTerm__1up",                     "JET_21NP_JET_Pileup_PtTerm__1down",
        "JET_21NP_JET_Pileup_OffsetNPV__1up",                  "JET_21NP_JET_Pileup_OffsetNPV__1down",
        "JET_21NP_JET_Pileup_OffsetMu__1up",                   "JET_21NP_JET_Pileup_OffsetMu__1down",
        "JET_21NP_JET_Pileup_RhoTopology__1up",                "JET_21NP_JET_Pileup_RhoTopology__1down",
        "JET_21NP_JET_SingleParticle_HighPt__1up",             "JET_21NP_JET_SingleParticle_HighPt__1down",
        
    ]

ListFolder         = MainFolder   + "/ListFolder/"
InputFolder        = SampleFolder + "/Downloads/"
HistogramFolder    = MainFolder   + "/HistogramFolder/"
#ListFolder         = MainFolder   + "/ListFolder_ljet/"
#InputFolder        = SampleFolder + "/Downloads_ljet/"

#ListFolder         = MainFolder   + "/ListFolder_QCD/"
#InputFolder        = SampleFolder + "/Downloads_QCD/"
#HistogramFolder    = MainFolder   + "/HistogramFolder_QCD/"

DoMakeFileLists  = False
DoMakeHistos     = False
DoRunSystematics = False
DoMergeHistos    = False
DoMakeCP         = True
DoRunOnBatch     = False
DoApplyWeights   = False
PlotErrorBand    = False
AnalysisType     = "TTH" 
QCDType          = "QCD_Nedaa" 
DataLabel        = "2016" # 2015, 2016 or 2015+2016
ConfigFileList   = []
ConfigFileList.append(RootCoreDir + "MainConfigFileTTH2016Res")
ConfigFileList.append(RootCoreDir + "MainConfigFileTTH2015Res")

JetBTagLargeRList = []
#JetBTagLargeRList.append(["4incl", "2incl", "0incl", "0incl", "0incl"])
JetBTagLargeRList.append(["6incl", "4incl", "0incl", "0incl","0incl"])

VetoChannelList   = []
VetoChannelList.append(["none", "none", "none", "none", "none"])
#VetoChannelList.append(["3incl", "3incl", "1incl", "1incl"])

###############################################################################
###############################################################################

#PathToTrainWeights = "/afs/ipp-garching.mpg.de/home/k/knand/AnalysisTop-2.4.19_TTH/PlotFactoryBoosted/Training_TMVA_3211_30Sept/TMVA_3211_2016_Vanilla/"
#PathToTrainWeights = "/afs/ipp-garching.mpg.de/home/k/knand/AnalysisTop-2.4.19_TTH/PlotFactoryBoosted/Training_TMVA_3311_30Sept/TMVA_3311_2016_Vanilla_noWjets/"
PathToTrainWeights = "/afs/phas.gla.ac.uk/user/a/aalshehri/TMVA-CERN-ROOT6-10-04-FILES-modified/dataset/"

if not os.path.exists(ListFolder):
    os.system("mkdir " + MainFolder)
    os.system("mkdir " + ListFolder)
    os.system("mkdir " + HistogramFolder)

if DoMakeFileLists:
    SuperScript.MakeFileLists(InputFolder, ListFolder, SampleFolder)

if DoMakeHistos:
    DataList    = glob.glob(ListFolder + "*Data.txt")
    MCList      = glob.glob(ListFolder + "*MC.txt")
    #TreeList    = ["nominal_Loose"]
    for TreeName in TreeList:
        TreeName = TreeName+"_Loose"
        if TreeName != "nominal" and TreeName != "nominal_Loose" and not DoRunSystematics:
            continue

        #if TreeName == "nominal_Loose" and DoRunSystematics: # QCD Systs
            #QCDSysts = DefineSamples.ReturnQCDSystList()
            #print QCDSysts
            #for QCDTypeSyst in QCDSysts:
            #    SuperScript.MakeHistos(ListFolder, HistogramFolder, DataList, MCList, JetBTagLargeRList, ConfigFileList, VetoChannelList, CodeFolder, TreeName, DoRunOnBatch, QCDTypeSyst)

        #elif TreeName == "nominal" and DoRunSystematics: #other bg norm systs
        #    SuperScript.MakeHistos(ListFolder, HistogramFolder, DataList, MCList, JetBTagLargeRList, ConfigFileList, VetoChannelList, CodeFolder, TreeName, DoRunOnBatch, "MCNorm") #using QCD flag 


        SuperScript.MakeHistos(
            ListFolder,
            HistogramFolder,
            DataList,
            MCList,
            JetBTagLargeRList,
            ConfigFileList,
            VetoChannelList,
            CodeFolder,
            TreeName,
            DoRunOnBatch,
            QCDType
        )
    if DoRunOnBatch:
        SuperScript.SubmitMakeHistoJobs()

if DoMergeHistos:
    SuperScript.MergeHistos(
        MainFolder,
        HistogramFolder,
        AnalysisType
    )

if DoMakeCP:
    SuperScript.MakeCP(
        HistogramFolder,
        PlotErrorBand,
        DataLabel
    )

if DoApplyWeights:
    #TreeList    = ["nominal_Loose"]
    for TreeName in TreeList:
        if TreeName != "nominal" and TreeName != "nominal_Loose" and not DoRunSystematics:
            continue

        TreeName = TreeName+"_Loose"

        SuperScript.ApplyTMVAWeights(
            HistogramFolder,
            JetBTagLargeRList,
            ConfigFileList,
            PathToTrainWeights,
            TreeName
            )
