#! /usr/bin/python

import glob
import os
import sys

import DefineSamples
from ROOT import *


def GetFileEntry(EntryList, EntryType):
    Status = ""
    for Entry in EntryList:
        if EntryType + " " in Entry:
            Status = Entry.replace(EntryType + " ", "")
            Status = Status.replace("\n", "")
            return Status
    if Status == "":
        print("ERROR(GetFileEntry): EntryType={EntryType} is not defined!!! ---> EXIT.".format(EntryType = EntryType))
        Status = "Entry not defined!!!"
        return Status

class ConfigFileReader(object):
    def __init__(self, ConfigFile):
        self.__ConfigFile   = ConfigFile
        fInFile     = open(self.__ConfigFile, 'r')
        fInputLines = fInFile.readlines()
        fInFile.close()
        self.AllLines          = fInputLines
        self.MinLargeRJetPt    = GetFileEntry(self.AllLines, "MinLargeRJetPt")
        self.MatchingDR        = GetFileEntry(self.AllLines, "MatchingDR")
        self.BTaggingDisc      = GetFileEntry(self.AllLines, "BTaggingDisc")
        self.BTaggingDiscLoose = GetFileEntry(self.AllLines, "BTaggingDiscLoose")
        self.LeadLeptonPt      = GetFileEntry(self.AllLines, "LeadLeptonPt")
        self.UseLooseBTagging  = GetFileEntry(self.AllLines, "UseLooseBTagging")
        self.UseHF             = GetFileEntry(self.AllLines, "UseHF")
        self.AnalysisType      = GetFileEntry(self.AllLines, "AnalysisType")
        self.Lumi              = GetFileEntry(self.AllLines, "Lumi")
        self.VariablesList     = GetFileEntry(self.AllLines, "VariablesList")
        self.LeptonChannel     = GetFileEntry(self.AllLines, "LeptonChannel")
        self.MissingEt         = GetFileEntry(self.AllLines, "MissingEt")
        self.TriangularCut     = GetFileEntry(self.AllLines, "TriangularCut")


#def ConfigFileWriter(object, OutputFolder):
#    StatusFileName = OutputFolder          + "/ConfigFilePlotFactory.txt"
#    StatusFile = open(StatusFileName, 'w')
#    StatusFile.write("MinLargeRJetPt "     + self.__MinLargeRJetPt + "\n")
#    StatusFile.write("MatchingDR "         + self.__MatchingDR + "\n")
#    StatusFile.write("BTaggingDisc "       + self.__BTaggingDisc + "\n")
#    StatusFile.write("BTaggingDiscLoose "  + self.__BTaggingDiscLoose + "\n")
#    StatusFile.write("LeadLeptonPt "       + self.__LeadLeptonPt + "\n")
#    StatusFile.write("UseLooseBTagging "   + self.__UseLooseBTagging + "\n")
#    StatusFile.write("UseHF "+self.__UseHF + "\n")
#    StatusFile.write("AnalysisType "       + self.__AnalysisType + "\n")
#    StatusFile.write("Lumi "+self.__Lumi   + "\n")
#    StatusFile.write("VariablesList "      + self.__VariablesList + "\n")
#    StatusFile.write("LeptonChannel "      + self.__LeptonChannel + "\n")
#    StatusFile.write("MissingEt "          + self.__MissingEt + "\n")
#    StatusFile.write("TriangularCut "      + self.TriangularCut + "\n")

def MakeFileLists(InputFolder, FolderForLists, SampleFolder):
    FolderList    = os.listdir(InputFolder)

    FileTags      = []

    MergeList = []
    #MergeList.append(DefineSamples.ReturnTTbarList("PP6radHi"))
    #MergeList.append(DefineSamples.ReturnTTbarList("PP6radLo"))
    #MergeList.append(DefineSamples.ReturnTTbarList("MCNLO_Hpp"))
    #MergeList.append(DefineSamples.ReturnTTbarList("Pow_Hpp"))
    #MergeList.append(DefineSamples.ReturnTTbarList("PP8radHi"))
    #MergeList.append(DefineSamples.ReturnTTbarList("PP8radLo"))
    MergeList.append(DefineSamples.ReturnOtherTTVList(""))
    MergeList.append(DefineSamples.ReturnTTWList(""))
    MergeList.append(DefineSamples.ReturnTTZList(""))
    MergeList.append(DefineSamples.ReturnSingleTopList_Dilepton("PowhegPy6"))
    MergeList.append(DefineSamples.ReturnDibosonList(""))
    MergeList.append(DefineSamples.ReturnZjetsList(""))
    MergeList.append(DefineSamples.ReturnWjetsList(""))
    MergeList.append(DefineSamples.ReturnDataList2015())
    MergeList.append(DefineSamples.ReturnDataList2016())
    #MergeList.append(DefineSamples.ReturnTTHList("aMCatNLO_HerwigPP"))
    MergeList.append(DefineSamples.ReturnTTHList("aMCatNLO_Pythia8"))
    #MergeList.append(DefineSamples.ReturnTTbar4TTHList("PP6"))
    MergeList.append(DefineSamples.ReturnTTbar4TTHList("PP8"))
    #MergeList.append(DefineSamples.ReturnTTbar4TTHList("PH7"))
    #MergeList.append(DefineSamples.ReturnTTbar4TTHList("MCNLO_P8"))

    #MergeList.append(SystsList)
    NameList = [
        "sboutle",
        "aknue",
        "scrawley",
        "wbm",
        "wbreaden",
        "mfenton",
        "tpelzer",
        "sbiondi",
        "rsoualah",
        "nasbah",
        "jwebster",
        "misacson",
        "aalshehr",
        "phys-higgs"
    ]
    UserList = []
    # Loop over all folder names and find all possible prefixes.
    usertype = "user"

    for Folder in FolderList:
        #print Folder
        if not "user." and not "group." in Folder:
            continue
        #print "again: "+Folder
        nameExists = False
        for name in NameList:
            if name in Folder:
                nameExists = True
        if nameExists == False:
            print("No correct username present in Folder: {folder}".format(folder = Folder))
            #continue
        parts  = Folder.split(".")
        #print(parts)
        usertype = parts[0]
        if parts[2] == "data15_13TeV" or parts[2] == "mc15_13TeV" or parts[2] == "data16_13TeV":
            ID = parts[3]
            #print(ID)
        else:
            ID = parts[2]
        user   = parts[1]
        tagExists = False

        for tag in FileTags:
            #print(tag)
            if tag == ID:
                tagExists = True

        if not tagExists:
            FileTags.append(ID)
            UserList.append(user)

    GoodTag = True

    for List in MergeList:
        for ID in List:
            tagInID = False
            for Tag in FileTags:
                if Tag == ID:
                    tagInID = True

            if tagInID == False:
                GoodTag = False
                print("ERROR: Missing sample with ID {sampleID}. Please include sample or edit DefineSample.py.".format(sampleID = ID))

    #if GoodTag == False:
    #    sys.exit()

    # make a txt file for each prefix, replace the "user.username." and use the number to produce the txt file
    counter = 0

    if not os.path.exists(FolderForLists):
        os.system("mkdir " + FolderForLists)

    for tag in FileTags:
        OutputFile = FolderForLists
        if len(tag) == 6:
            OutputFile += "list_" + str(tag) + "_MC.txt"
        elif len(tag) == 8 or len(tag) == 7:
            OutputFile += "list_" + str(tag) + "_Data.txt"
        else:
            print("ERROR: The number {tag} could not be sorted! ---> EXIT.".format(tag = tag))
            sys.exit()
        userName = UserList[counter]
        fileList = glob.glob(InputFolder + "/" + usertype + "." + userName + "*" + tag + "*/*.root*")

        f = open(OutputFile, "w")
        for file in fileList:
            f.write(file + "\n")
        f.close()
        counter = counter + 1

    f1 = open("Samples.html", "w")
    f1.write("<link href=\"txtstyle.css\" rel=\"stylesheet\" type=\"text/css\" />\n")
    for Folder in FolderList:
        if not os.path.isdir(Folder):
            continue
        FileList = os.listdir(InputFolder + "/" + Folder)
        FileNumber = len(FileList)
        f1.write(Folder + "<br>\n")
        f1.write("Number of Files in Sample: " + str(FileNumber) + "<br>\n")
    f1.close()

    f2 = open("txtstyle.css", "w")
    f2.write("html, body {font-family:Helvetica, Arial, sans-serif}\n")
    f2.close()

def ApplyTMVAWeights(
    HistogramFolder,
    JetBTagLargeRList,
    ConfigFileList,
    PathToTrainWeights,
    TreeName
    ):

    for JetBTag in JetBTagLargeRList:
        JetBin    = JetBTag[0]
        BTagBin   = JetBTag[1]
        LargeRBin = JetBTag[2]
        TopTagBin = JetBTag[3]
        HiggsTagBin = JetBTag[4]

        for ConfigFileName in ConfigFileList:
            ConfigObj    = ConfigFileReader(ConfigFileName)
            Channel      = ConfigObj.LeptonChannel

            InputFolder = "{main_output_directory}/Merge_{channel}_{jet_bin}_{b_tag_bin}_{large_R_bin}_{top_tag_bin}_{higgs_tag_bin}_{treeName}/".format(
                main_output_directory = HistogramFolder,
                channel               = Channel,
                jet_bin               = JetBin,
                b_tag_bin             = BTagBin,
                large_R_bin           = LargeRBin,
                top_tag_bin           = TopTagBin,
                higgs_tag_bin         = HiggsTagBin,
                treeName              = TreeName
            )
            OutputFolder = "{main_output_directory}/Merge_{channel}_{jet_bin}_{b_tag_bin}_{large_R_bin}_{top_tag_bin}_{treeName}_Trained/".format(
                main_output_directory = HistogramFolder,
                channel               = Channel,
                jet_bin               = JetBin,
                b_tag_bin             = BTagBin,
                large_R_bin           = LargeRBin,
                top_tag_bin           = TopTagBin,
                higgs_tag_bin         = HiggsTagBin,
                treeName              = TreeName
            )

            FileList = glob.glob(InputFolder + "/*Tree*.root*")

            VariableList = glob.glob(PathToTrainWeights + "/*.root")

            VariableFile = PathToTrainWeights + "/TMVAVariables.txt"

            for TMVAFile in VariableList:
                a = TChain("TestTree")
                a.Add(TMVAFile)
                names = [b.GetName() for b in a.GetListOfBranches()]
                size = len(names)
                f = open(VariableFile, 'w')
                for x in range(0, size):
                    if x == 0 or x == 1:
                        continue;
                    if x == (size-2) or x == (size-1):
                        continue;
                    f.write(names[x] + "\n")
                f.close()

            os.system("mkdir " + OutputFolder)

            for InputFile in FileList:

                if "TMVA" in InputFile:
                    continue

                OutputFile = InputFile.replace(InputFolder, OutputFolder)
                print("Make output file {OutputFile}".format(
                    OutputFile = OutputFile
                ))

                if os.path.exists(OutputFile):
                    continue


                if "nominal" in TreeName:
                    os.system("./RunApplyTMVAWeightsNominal.sh {InputFile} {OutputFile} {PathToTrainWeights} {VariableFile}".format(
                            InputFile          = InputFile,
                            OutputFile         = OutputFile,
                            PathToTrainWeights = PathToTrainWeights,
                            VariableFile       = VariableFile
                            ))
                else:
                    os.system("./RunApplyTMVAWeights.sh {InputFile} {OutputFile} {PathToTrainWeights} {VariableFile}".format(
                            InputFile          = InputFile,
                            OutputFile         = OutputFile,
                            PathToTrainWeights = PathToTrainWeights,
                            VariableFile       = VariableFile
                            ))



def MakeHistos(
    ListFolder,
    HistogramFolder,
    DataList,
    MCList,
    JetBTagLargeRList,
    ConfigFileList,
    VetoChannelList,
    CodeFolder,
    TreeName,
    DoRunOnBatch,
    QCDType
    ):
    for ConfigFileName in ConfigFileList:        
        # Get information from the configuration file.                                                                                                                                                                                    
        ConfigObj    = ConfigFileReader(ConfigFileName)
        AnalysisType = ConfigObj.AnalysisType
        
    #TTLight4TTHMergeList_PP6      = DefineSamples.ReturnTTLight4TTHList("PP6")
    #TTBB4TTHMergeList_PP6         = DefineSamples.ReturnTTBB4TTHList("PP6")
    #TTCC4TTHMergeList_PP6         = DefineSamples.ReturnTTCC4TTHList("PP6")
    TTLight4TTHMergeList_PP8      = DefineSamples.ReturnTTLight4TTHList("PP8")
    TTBB4TTHMergeList_PP8         = DefineSamples.ReturnTTBB4TTHList("PP8")
    TTCC4TTHMergeList_PP8         = DefineSamples.ReturnTTCC4TTHList("PP8")
    #TTLight4TTHMergeList_PH7      = DefineSamples.ReturnTTLight4TTHList("PH7")
    #TTBB4TTHMergeList_PH7         = DefineSamples.ReturnTTBB4TTHList("PH7")
    #TTCC4TTHMergeList_PH7         = DefineSamples.ReturnTTCC4TTHList("PH7")
    #TTLight4TTHMergeList_MCNLO_P8 = DefineSamples.ReturnTTLight4TTHList("MCNLO_P8")
    #TTBB4TTHMergeList_MCNLO_P8    = DefineSamples.ReturnTTBB4TTHList("MCNLO_P8")
    #TTCC4TTHMergeList_MCNLO_P8    = DefineSamples.ReturnTTCC4TTHList("MCNLO_P8")
    #TTLightMergeList_PP6radHi     = DefineSamples.ReturnTTLightList("PP6radHi")
    #TTBBMergeList_PP6radHi        = DefineSamples.ReturnTTBBList("PP6radHi")
    #TTCCMergeList_PP6radHi        = DefineSamples.ReturnTTCCList("PP6radHi")
    #TTLightMergeList_PP6radLo     = DefineSamples.ReturnTTLightList("PP6radLo")
    #TTBBMergeList_PP6radLo        = DefineSamples.ReturnTTBBList("PP6radLo")
    #TTCCMergeList_PP6radLo        = DefineSamples.ReturnTTCCList("PP6radLo")
    #TTLightMergeList_MCNLO_Hpp    = DefineSamples.ReturnTTLightList("MCNLO_Hpp")
    #TTBBMergeList_MCNLO_Hpp       = DefineSamples.ReturnTTBBList("MCNLO_Hpp")
    #TTCCMergeList_MCNLO_Hpp       = DefineSamples.ReturnTTCCList("MCNLO_Hpp")
    #TTLightMergeList_Pow_Hpp      = DefineSamples.ReturnTTLightList("Pow_Hpp")
    #TTBBMergeList_Pow_Hpp         = DefineSamples.ReturnTTBBList("Pow_Hpp")
    #TTCCMergeList_Pow_Hpp         = DefineSamples.ReturnTTCCList("Pow_Hpp")
    #TTLightMergeList_PP8radHi     = DefineSamples.ReturnTTLightList("PP8radHi")
    #TTBBMergeList_PP8radHi        = DefineSamples.ReturnTTBBList("PP8radHi")
    #TTCCMergeList_PP8radHi        = DefineSamples.ReturnTTCCList("PP8radHi")
    #TTLightMergeList_PP8radLo     = DefineSamples.ReturnTTLightList("PP8radLo")
    #TTBBMergeList_PP8radLo        = DefineSamples.ReturnTTBBList("PP8radLo")
    #TTCCMergeList_PP8radLo        = DefineSamples.ReturnTTCCList("PP8radLo")


    TTZMergeList         = DefineSamples.ReturnTTZList("")
    TTWMergeList         = DefineSamples.ReturnTTWList("")
    OtherTTVMergeList    = DefineSamples.ReturnOtherTTVList("")    
    SingleTopMergeList   = DefineSamples.ReturnSingleTopList_Dilepton("PowhegPy6")
    DibosonMergeList     = DefineSamples.ReturnDibosonList("")
    ZjetsMergeList       = DefineSamples.ReturnZjetsList("")
    #ZjetsMGMergeList     = DefineSamples.ReturnZjetsList("")
    WjetsMergeList       = DefineSamples.ReturnWjetsList("")
    DataMergeList2015    = DefineSamples.ReturnDataList2015()
    DataMergeList2016    = DefineSamples.ReturnDataList2016()
    QCDMergeList2015     = DefineSamples.ReturnDataList2015()
    QCDMergeList2016     = DefineSamples.ReturnDataList2016()
    #TTHMergeList1        = DefineSamples.ReturnTTHList("aMCatNLO_HerwigPP")
    TTHMergeList2        = DefineSamples.ReturnTTHList("aMCatNLO_Pythia8")

    SubMergeList = []

    #SubMergeList.append([TTLight4TTHMergeList_PP6,      "ttlight"])
    #SubMergeList.append([TTBB4TTHMergeList_PP6,         "ttbb"])
    #SubMergeList.append([TTCC4TTHMergeList_PP6,         "ttcc"])

    SubMergeList.append([TTLight4TTHMergeList_PP8,      "ttlight_PP8"])
    SubMergeList.append([TTBB4TTHMergeList_PP8,         "ttbb_PP8"])
    SubMergeList.append([TTCC4TTHMergeList_PP8,         "ttcc_PP8"])

    #SubMergeList.append([TTLight4TTHMergeList_PH7,      "ttlight_PH7"])
    #SubMergeList.append([TTBB4TTHMergeList_PH7,         "ttbb_PH7"])
    #SubMergeList.append([TTCC4TTHMergeList_PH7,         "ttcc_PH7"])

    #SubMergeList.append([TTLight4TTHMergeList_MCNLO_P8, "ttlight_MCNLO_P8"])
    #SubMergeList.append([TTBB4TTHMergeList_MCNLO_P8,    "ttbb_MCNLO_P8"])
    #SubMergeList.append([TTCC4TTHMergeList_MCNLO_P8,    "ttcc_MCNLO_P8"])

    #SubMergeList.append([TTLightMergeList_PP6radHi,     "ttlight_PP6radHi"])
    #SubMergeList.append([TTBBMergeList_PP6radHi,        "ttbb_PP6radHi"])
    #SubMergeList.append([TTCCMergeList_PP6radHi,        "ttcc_PP6radHi"])

    #SubMergeList.append([TTLightMergeList_PP6radLo,     "ttlight_PP6radLo"])
    #SubMergeList.append([TTBBMergeList_PP6radLo,        "ttbb_PP6radLo"])
    #SubMergeList.append([TTCCMergeList_PP6radLo,        "ttcc_PP6radLo"])

    #SubMergeList.append([TTLightMergeList_PP8radHi,     "ttlight_PP8radHi"])
    #SubMergeList.append([TTBBMergeList_PP8radHi,        "ttbb_PP8radHi"])
    #SubMergeList.append([TTCCMergeList_PP8radHi,        "ttcc_PP8radHi"])

    #SubMergeList.append([TTLightMergeList_PP8radLo,     "ttlight_PP8radLo"])
    #SubMergeList.append([TTBBMergeList_PP8radLo,        "ttbb_PP8radLo"])
    #SubMergeList.append([TTCCMergeList_PP8radLo,        "ttcc_PP8radLo"])

    #SubMergeList.append([TTLightMergeList_MCNLO_Hpp,    "ttlight_MCNLO_Hpp"])
    #SubMergeList.append([TTBBMergeList_MCNLO_Hpp,       "ttbb_MCNLO_Hpp"])
    #SubMergeList.append([TTCCMergeList_MCNLO_Hpp,       "ttcc_MCNLO_Hpp"])

    #SubMergeList.append([TTLightMergeList_Pow_Hpp,      "ttlight_Pow_Hpp"])
    #SubMergeList.append([TTBBMergeList_Pow_Hpp,         "ttbb_Pow_Hpp"])
    #SubMergeList.append([TTCCMergeList_Pow_Hpp,         "ttcc_Pow_Hpp"])

    #SubMergeList.append([TTHMergeList1,        "ttH"])
    SubMergeList.append([TTHMergeList2,        "ttH_Py8"])
    SubMergeList.append([SingleTopMergeList, "singleTop"])
    SubMergeList.append([DibosonMergeList,   "diboson"])
    SubMergeList.append([ZjetsMergeList,     "zjets"])
    SubMergeList.append([WjetsMergeList,     "wjets"])
    SubMergeList.append([DataMergeList2015,  "data_2015"])
    SubMergeList.append([QCDMergeList2015,   "qcd_2015"])
    SubMergeList.append([DataMergeList2016,  "data_2016"])
    SubMergeList.append([QCDMergeList2016,   "qcd_2016"])
    SubMergeList.append([TTZMergeList,       "ttZ"])
    SubMergeList.append([TTWMergeList,       "ttW"])
    SubMergeList.append([OtherTTVMergeList,  "OtherttV"])

    Executable  = "MakePlots"
    AllLists = []
    AllLists.append([MCList,   "MC"])
    if not "QCD" in ListFolder:
        AllLists.append([DataList, "Data"])
    else:
        AllLists.append([DataList, "QCD"])

    HFList = []
    HFList.append("Inclusive")
    HFList.append("ttlight")
    HFList.append("ttcc")
    HFList.append("ttbb")

    print "Make scripts for ", TreeName

    for Lists in AllLists:
        #print(Lists)
        for entry in Lists[0]:
            #print(entry)
            parts = entry.replace(ListFolder, "")
            parts = parts.split("_")
            ID    = parts[1]

            #if not "410000" in ID:
            #    continue

            SampleType = Lists[1]

            #print SampleType

            if not TreeName == "nominal_Loose" and not TreeName == "nominal":
                if "Data" in entry:
                    continue

            #if TreeName == "nominal_Loose":
                #if not "Data" in entry:
                #    continue
            #    if not "Data" in SampleType:
            #                    continue

            #if ID in AltTTbarMergeList and not TreeName == "nominal":
            #    continue
            #print(entry)

            #if "Data" in entry:
            #    continue

            #if not "002" in ID:
            #    continue

            InputFileList = entry

            for JetBTag in JetBTagLargeRList:
                JetBin    = JetBTag[0]
                BTagBin   = JetBTag[1]
                LargeRBin = JetBTag[2]
                TopTagBin = JetBTag[3]
                HiggsTagBin = JetBTag[4]

                for VetoChannel in VetoChannelList:
                    VetoJetBin    = VetoChannel[0]
                    VetoBTagBin   = VetoChannel[1]
                    VetoLargeRBin = VetoChannel[2]
                    VetoTopTagBin = VetoChannel[3]
                    VetoHiggsTagBin = VetoChannel[4]

                    for ConfigFileName in ConfigFileList:

                        # Get information from the configuration file.
                        ConfigObj    = ConfigFileReader(ConfigFileName)
                        Channel      = ConfigObj.LeptonChannel
                        fRunHF       = int(ConfigObj.UseHF)
                        AnalysisType = ConfigObj.AnalysisType


                        #print("===================================================================================")
                        #print("========================= Values from Config file =================================")
                        #print(Channel)
                        #print(fRunHF)
                        #print(AnalysisType)
                        #print("===================================================================================")
                        #print("===================================================================================")

                        if VetoJetBin == "none":
                            OutputFolder = "{main_output_directory}/Channel_{channel}_{jet_bin}_{b_tag_bin}_{large_R_bin}_{top_tag_bin}_{higgs_tag_bin}_{treeName}/".format(
                                main_output_directory = HistogramFolder,
                                channel               = Channel,
                                jet_bin               = JetBin,
                                b_tag_bin             = BTagBin,
                                large_R_bin           = LargeRBin,
                                top_tag_bin           = TopTagBin,
                                higgs_tag_bin         = HiggsTagBin,
                                treeName              = TreeName
                                )
                        else:
                            OutputFolder = "{main_output_directory}/Channel_{channel}_{jet_bin}_{b_tag_bin}_{large_R_bin}_{top_tag_bin}_minus_{veto_jet_bin}_{veto_b_tag_bin}_{veto_large_R_bin}_{veto_top_tag_bin}_{veto_higgs_tag_bin}_{treeName}/".format(
                                main_output_directory = HistogramFolder,
                                channel               = Channel,
                                jet_bin               = JetBin,
                                b_tag_bin             = BTagBin,
                                large_R_bin           = LargeRBin,
                                top_tag_bin           = TopTagBin,
                                higgs_tag_bin         = HiggsTagBin,
                                veto_jet_bin          = VetoJetBin,
                                veto_b_tag_bin        = VetoBTagBin,
                                veto_large_R_bin      = VetoLargeRBin,
                                veto_top_tag_bin      = VetoTopTagBin,
                                veto_higgs_tag_bin    = VetoHiggsTagBin,
                                treeName              = TreeName
                                )

                        if not os.path.exists(OutputFolder):
                            os.system("mkdir -p {directory}".format(
                                    directory = OutputFolder
                            ))

                        os.system(
                            "cp {ConfigFileName} {HistogramFolder}".format(
                                ConfigFileName = ConfigFileName,
                                HistogramFolder = HistogramFolder
                        ))
                        for HFType in HFList:

                            if fRunHF == 0 and HFType != "Inclusive":
                                continue

                            HF_DSID  = ["410000", "410120", "410001", "410002", "410003", "410004", "410225", "410274", "410501", "410504", "410525", "410528", "410511", "410512"]

                            IsInList = False

                            for helpDSID in HF_DSID:
                                if helpDSID in str(ID):
                                    IsInList = True

                            if not IsInList and HFType != "Inclusive":
                                continue

                            if IsInList and HFType == "Inclusive":
                                continue

                            IDhelp = ID

                            if HFType != "Inclusive":
                                IDhelp = ID + "_" + HFType

                            if TreeName == "nominal_Loose" and Lists[1] == "Data":
                                #SampleType = QCDType
                                #if "QCD_Rafal" in SampleType and SampleType != "QCD_Rafal": # ie if running a syst of QCD
                                #    QCDSyst = SampleType.replace("QCD_Rafal_", "")
                                #    OutputFolder = OutputFolder.replace("nominal_Loose", QCDSyst)
                                if not os.path.exists(OutputFolder):
                                    os.system("mkdir -p {directory}".format(
                                            directory = OutputFolder
                                            ))
  
                            if TreeName == "nominal_Loose" and Lists[1] == "QCD":
                                SampleType = QCDType                                                                                                                                                                                                                              
                                if not os.path.exists(OutputFolder):
                                    os.system("mkdir -p {directory}".format(
                                            directory = OutputFolder
                                            ))


                            # a QCD type without "QCD" in it indicates a bkg norm syst
                            if TreeName == "nominal" and Lists[1] == "MC" and QCDType=="MCNorm":
                                SampleType = QCDType
                                OutputFolder = OutputFolder.replace("nominal", QCDType)
                                if not os.path.exists(OutputFolder):
                                        os.system("mkdir -p {directory}".format(
                                            directory = OutputFolder
                                        ))
                                

                            OutputFile = "{output_directory}/Histogram_{ID}.root".format(
                                output_directory = OutputFolder,
                                ID               = IDhelp
                            )
                            OutputTreeFile = "{output_directory}/Tree_{ID}.root".format(
                                output_directory = OutputFolder,
                                ID               = IDhelp
                            )

                            if not "SF" in TreeName and not "jvt" in TreeName:

                                cmd =\
                                    Executable     + " " +\
                                    InputFileList  + " " +\
                                    OutputFile     + " " +\
                                    OutputTreeFile + " " +\
                                    SampleType     + " " +\
                                    JetBin         + " " +\
                                    BTagBin        + " " +\
                                    LargeRBin      + " " +\
                                    TopTagBin      + " " +\
                                    HiggsTagBin    + " " +\
                                    VetoJetBin     + " " +\
                                    VetoBTagBin    + " " +\
                                    VetoLargeRBin  + " " +\
                                    VetoTopTagBin  + " " +\
                                    VetoHiggsTagBin+ " " +\
                                    HFType         + " " +\
                                    ConfigFileName + " " +\
                                    TreeName
                            else:

                                cmd =\
                                    Executable     + " " +\
                                    InputFileList  + " " +\
                                    OutputFile     + " " +\
                                    OutputTreeFile + " " +\
                                    SampleType     + " " +\
                                    JetBin         + " " +\
                                    BTagBin        + " " +\
                                    LargeRBin      + " " +\
                                    TopTagBin      + " " +\
                                    HiggsTagBin    + " " +\
                                    VetoJetBin     + " " +\
                                    VetoBTagBin    + " " +\
                                    VetoLargeRBin  + " " +\
                                    VetoTopTagBin  + " " +\
                                    VetoHiggsTagBin+ " " +\
                                    HFType         + " " +\
                                    ConfigFileName + " " +\
                                    TreeName       + " " +\
                                    TreeName

                            # Run e and mu separately for QCD due to negative bins.
                            if (SampleType == QCDType and "emujets" in Channel) and not SampleType == "MCNorm":
                                ChanList = []
                                if Channel == "boosted_emujets_2015":
                                    ChanList = ["boosted_emujets_2015"]
                                if Channel == "emujets_2015":
                                    ChanList = ["emujets_2015"]
                                if Channel == "boosted_emujets_2016":
                                    ChanList = ["boosted_emujets_2015"]
                                if Channel == "emujets_2016":
                                    ChanList = ["emujets_2015"]

                                for Channel_sep in ChanList:
                                    if Channel == "boosted_emujets_2015":
                                        Chan = Channel_sep.split("_")
                                        chan = Chan[1]
                                    elif Channel == "boosted_emujets_2016":
                                        Chan = Channel_sep.split("_")
                                        chan = Chan[1]
                                    else:
                                        chan = Channel_sep

                                    OutputFile     = "{output_directory}/Histogram_{ID}_{channel}.root".format(
                                        output_directory = OutputFolder,
                                        ID               = IDhelp,
                                        channel = chan
                                        )
                                    OutputTreeFile = "{output_directory}/Tree_{ID}_{channel}.root".format(
                                        output_directory = OutputFolder,
                                        ID               = IDhelp,
                                        channel = chan
                                        )

                                    cmd = ""

                                    if not "SF" in TreeName and not "jvt" in TreeName:
                                        cmd =\
                                            Executable     + " " +\
                                            InputFileList  + " " +\
                                            OutputFile     + " " +\
                                            OutputTreeFile + " " +\
                                            SampleType     + " " +\
                                            JetBin         + " " +\
                                            BTagBin        + " " +\
                                            LargeRBin      + " " +\
                                            TopTagBin      + " " +\
                                            HiggsTagBin    + " " +\
                                            VetoJetBin     + " " +\
                                            VetoBTagBin    + " " +\
                                            VetoLargeRBin  + " " +\
                                            VetoTopTagBin  + " " +\
                                            VetoHiggsTagBin+ " " +\
                                            HFType         + " " +\
                                            ConfigFileName + " " +\
                                            TreeName

                                    else:
                                        cmd =\
                                            Executable     + " " +\
                                            InputFileList  + " " +\
                                            OutputFile     + " " +\
                                            OutputTreeFile + " " +\
                                            SampleType     + " " +\
                                            JetBin         + " " +\
                                            BTagBin        + " " +\
                                            LargeRBin      + " " +\
                                            TopTagBin      + " " +\
                                            HiggsTagBin    + " " +\
                                            VetoJetBin     + " " +\
                                            VetoBTagBin    + " " +\
                                            VetoLargeRBin  + " " +\
                                            VetoTopTagBin  + " " +\
                                            VetoHiggsTagBin  + " " +\
                                            HFType         + " " +\
                                            ConfigFileName + " " +\
                                            TreeName       + " " +\
                                            TreeName

                                    if DoRunOnBatch:

                                        for dataListboutle in SubMergeList:
                                            if IDhelp in dataListboutle[0]:
                                                SubmissionScript = os.getcwd() + "/Submit_" + Channel_sep + "_" + JetBin + "_" + BTagBin + "_" + LargeRBin + "_" + TopTagBin + "_" + HiggsTagBin + "_" + dataListboutle[1] + "_" + TreeName + ".sh"
                                                break

                                            
                                        if not os.path.exists(OutputFile):
                                                
                                            if not os.path.exists(SubmissionScript):
                                                
                                                script = open(SubmissionScript, "w")
                                                script.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/ \n")
                                                script.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh \n")
                                                script.write("cd " + CodeFolder + "\n")
                                                script.write("rcSetup \n")
                                                script.write("cd PlotFactoryBoosted/scripts/ \n")
                                                script.write("sleep 300 \n")
                                                script.write("ulimit -n 4000 \n")
                                                script.write(cmd + " \n")
                                                script.write("cd " + os.getcwd() + "\n")
                                                script.close()
                                                
                                            else:
                                                script   = open(SubmissionScript, "a")
                                                script.write(cmd + " \n")
                                                script.write("cd " + os.getcwd() + "\n")
                                                script.close()
                                                    

                                                    
                                    else:
                                        if not os.path.exists(OutputFile):
                                            os.system(cmd)


                            else:
                                if DoRunOnBatch:


                                    SubmissionScript = os.getcwd() + "/Submit_" + Channel + "_" + JetBin + "_" + BTagBin + "_" + LargeRBin + "_" + TopTagBin + "_" + HiggsTagBin + "_" + IDhelp + "_" + TreeName + ".sh"


                                    for dataListboutle in SubMergeList:
                                        if IDhelp in dataListboutle[0]:
                                            SubmissionScript = os.getcwd() + "/Submit_" + Channel + "_" + JetBin + "_" + BTagBin + "_" + LargeRBin + "_" + TopTagBin + "_" + HiggsTagBin + "_" + dataListboutle[1] + "_" + TreeName + ".sh"
                                            break

                                            
                                    if not os.path.exists(OutputFile):

                                        if not os.path.exists(SubmissionScript):
                                            
                                            script = open(SubmissionScript, "w")
                                            script.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/ \n")
                                            script.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh \n")
                                            script.write("cd " + CodeFolder + "\n")
                                            script.write("rcSetup \n")
                                            script.write("cd PlotFactoryBoosted/scripts/ \n")
                                            script.write("ulimit -n 4000 \n")
                                            script.write(cmd + " \n")
                                            script.write("cd " + os.getcwd() + "\n")
                                            script.close()
                                            
                                        else:
                                            script   = open(SubmissionScript, "a")
                                            script.write(cmd + " \n")
                                            script.write("cd " + os.getcwd() + "\n")
                                            script.close()
                                            
                                else:
                                    if not os.path.exists(OutputFile):
                                        os.system(cmd)

def SubmitMakeHistoJobs():
    FilesToSubmit = glob.glob("Submit_*.sh")                                    
    nr_files = len(FilesToSubmit)
    for i in range(0, nr_files):
        script   = open(FilesToSubmit[i], "a")
        script.write("rm " + FilesToSubmit[i] + "\n")
        script.close()
        cmd2 = "chmod u+x " + FilesToSubmit[i]
        os.system(cmd2)

        CURRENTUSER=os.environ.get('USER')

        #print CURRENTUSER

        if CURRENTUSER =="knand" or CURRENTUSER == "aknue":  # batch queue works differently on Munich cluster
            cmd3 = "qsub " + FilesToSubmit[i]
        else:
            cmd3 = "qsub -q medium6 " + FilesToSubmit[i]
        os.system(cmd3)

def MergeHistos(
    MainFolder,
    HistogramFolder,
    Analysis
    ):
    
    #TTLight4TTHMergeList_PP6      = DefineSamples.ReturnTTLight4TTHList("PP6")
    #TTBB4TTHMergeList_PP6         = DefineSamples.ReturnTTBB4TTHList("PP6")
    #TTCC4TTHMergeList_PP6         = DefineSamples.ReturnTTCC4TTHList("PP6")
    TTLight4TTHMergeList_PP8      = DefineSamples.ReturnTTLight4TTHList("PP8")
    TTBB4TTHMergeList_PP8         = DefineSamples.ReturnTTBB4TTHList("PP8")
    TTCC4TTHMergeList_PP8         = DefineSamples.ReturnTTCC4TTHList("PP8")
    #TTLight4TTHMergeList_PH7      = DefineSamples.ReturnTTLight4TTHList("PH7")
    #TTBB4TTHMergeList_PH7         = DefineSamples.ReturnTTBB4TTHList("PH7")
    #TTCC4TTHMergeList_PH7         = DefineSamples.ReturnTTCC4TTHList("PH7")
    #TTLight4TTHMergeList_MCNLO_P8 = DefineSamples.ReturnTTLight4TTHList("MCNLO_P8")
    #TTBB4TTHMergeList_MCNLO_P8    = DefineSamples.ReturnTTBB4TTHList("MCNLO_P8")
    #TTCC4TTHMergeList_MCNLO_P8    = DefineSamples.ReturnTTCC4TTHList("MCNLO_P8")
    #TTLightMergeList_PP6radHi     = DefineSamples.ReturnTTLightList("PP6radHi")
    #TTBBMergeList_PP6radHi        = DefineSamples.ReturnTTBBList("PP6radHi")
    #TTCCMergeList_PP6radHi        = DefineSamples.ReturnTTCCList("PP6radHi")
    #TTLightMergeList_PP6radLo     = DefineSamples.ReturnTTLightList("PP6radLo")
    #TTBBMergeList_PP6radLo        = DefineSamples.ReturnTTBBList("PP6radLo")
    #TTCCMergeList_PP6radLo        = DefineSamples.ReturnTTCCList("PP6radLo")
    #TTLightMergeList_MCNLO_Hpp    = DefineSamples.ReturnTTLightList("MCNLO_Hpp")
    #TTBBMergeList_MCNLO_Hpp       = DefineSamples.ReturnTTBBList("MCNLO_Hpp")
    #TTCCMergeList_MCNLO_Hpp       = DefineSamples.ReturnTTCCList("MCNLO_Hpp")
    #TTLightMergeList_Pow_Hpp      = DefineSamples.ReturnTTLightList("Pow_Hpp")
    #TTBBMergeList_Pow_Hpp         = DefineSamples.ReturnTTBBList("Pow_Hpp")
    #TTCCMergeList_Pow_Hpp         = DefineSamples.ReturnTTCCList("Pow_Hpp")
    #TTLightMergeList_PP8radHi     = DefineSamples.ReturnTTLightList("PP8radHi")
    #TTBBMergeList_PP8radHi        = DefineSamples.ReturnTTBBList("PP8radHi")
    #TTCCMergeList_PP8radHi        = DefineSamples.ReturnTTCCList("PP8radHi")
    #TTLightMergeList_PP8radLo     = DefineSamples.ReturnTTLightList("PP8radLo")
    #TTBBMergeList_PP8radLo        = DefineSamples.ReturnTTBBList("PP8radLo")
    #TTCCMergeList_PP8radLo        = DefineSamples.ReturnTTCCList("PP8radLo")

    TTZList         = DefineSamples.ReturnTTZList("")
    TTWList         = DefineSamples.ReturnTTWList("")
    OtherTTVList    = DefineSamples.ReturnOtherTTVList("")
    SingleTopList   = DefineSamples.ReturnSingleTopList_Dilepton("PowhegPy6")
    DibosonList     = DefineSamples.ReturnDibosonList("")
    ZjetsList       = DefineSamples.ReturnZjetsList("")
    WjetsList       = DefineSamples.ReturnWjetsList("")
    DataList2015    = DefineSamples.ReturnDataList2015()
    QCDList2015     = DefineSamples.ReturnDataList2015()
    DataList2016    = DefineSamples.ReturnDataList2016()
    QCDList2016     = DefineSamples.ReturnDataList2016()
    #TTHList1        = DefineSamples.ReturnTTHList("aMCatNLO_HerwigPP")
    TTHList2        = DefineSamples.ReturnTTHList("aMCatNLO_Pythia8")

    MergeList = []

    #MergeList.append([TTLight4TTHMergeList_PP6,      "ttlight"])
    #MergeList.append([TTBB4TTHMergeList_PP6,         "ttbb"])
    #MergeList.append([TTCC4TTHMergeList_PP6,         "ttcc"])

    MergeList.append([TTLight4TTHMergeList_PP8,      "ttlight_PP8"])
    MergeList.append([TTBB4TTHMergeList_PP8,         "ttbb_PP8"])
    MergeList.append([TTCC4TTHMergeList_PP8,         "ttcc_PP8"])

    #MergeList.append([TTLight4TTHMergeList_PH7,      "ttlight_PH7"])
    #MergeList.append([TTBB4TTHMergeList_PH7,         "ttbb_PH7"])
    #MergeList.append([TTCC4TTHMergeList_PH7,         "ttcc_PH7"])

    #MergeList.append([TTLight4TTHMergeList_MCNLO_P8, "ttlight_MCNLO_P8"])
    #MergeList.append([TTBB4TTHMergeList_MCNLO_P8,    "ttbb_MCNLO_P8"])
    #MergeList.append([TTCC4TTHMergeList_MCNLO_P8,    "ttcc_MCNLO_P8"])

    #MergeList.append([TTLightMergeList_PP6radHi,     "ttlight_PP6radHi"])
    #MergeList.append([TTBBMergeList_PP6radHi,        "ttbb_PP6radHi"])
    #MergeList.append([TTCCMergeList_PP6radHi,        "ttcc_PP6radHi"])

    #MergeList.append([TTLightMergeList_PP6radLo,     "ttlight_PP6radLo"])
    #MergeList.append([TTBBMergeList_PP6radLo,        "ttbb_PP6radLo"])
    #MergeList.append([TTCCMergeList_PP6radLo,        "ttcc_PP6radLo"])

    #MergeList.append([TTLightMergeList_PP8radHi,     "ttlight_PP8radHi"])
    #MergeList.append([TTBBMergeList_PP8radHi,        "ttbb_PP8radHi"])
    #MergeList.append([TTCCMergeList_PP8radHi,        "ttcc_PP8radHi"])

    #MergeList.append([TTLightMergeList_PP8radLo,     "ttlight_PP8radLo"])
    #MergeList.append([TTBBMergeList_PP8radLo,        "ttbb_PP8radLo"])
    #MergeList.append([TTCCMergeList_PP8radLo,        "ttcc_PP8radLo"])

    #MergeList.append([TTLightMergeList_MCNLO_Hpp,    "ttlight_MCNLO_Hpp"])
    #MergeList.append([TTBBMergeList_MCNLO_Hpp,       "ttbb_MCNLO_Hpp"])
    #MergeList.append([TTCCMergeList_MCNLO_Hpp,       "ttcc_MCNLO_Hpp"])

    #MergeList.append([TTLightMergeList_Pow_Hpp,      "ttlight_Pow_Hpp"])
    #MergeList.append([TTBBMergeList_Pow_Hpp,         "ttbb_Pow_Hpp"])
    #MergeList.append([TTCCMergeList_Pow_Hpp,         "ttcc_Pow_Hpp"])

    MergeList.append([SingleTopList, "singleTop"])
    MergeList.append([DibosonList,   "diboson"])
    MergeList.append([ZjetsList,     "zjets"])
    MergeList.append([WjetsList,     "wjets"])
    MergeList.append([DataList2016,  "data_2016"])
    MergeList.append([QCDList2016,   "qcd_2016"])
    MergeList.append([DataList2015,  "data_2015"])
    MergeList.append([QCDList2015,   "qcd_2015"])
    MergeList.append([TTZList,       "ttZ"])
    MergeList.append([TTWList,       "ttW"])
    MergeList.append([OtherTTVList,  "OtherttV"])
    #MergeList.append([TTHList1,      "ttH"])
    MergeList.append([TTHList2,      "ttH_Py8"])

    
    print "MergeHistos function..."

    # Get a list of directories for channels.
    ChannelFolders = os.listdir(HistogramFolder)

    MissingFileListAll = []

    for ChannelFolder in ChannelFolders:

        #if not os.path.isdir(ChannelFolder):
        #    continue

        if not "Channel" in ChannelFolder:
            continue

        ChannelFolder = HistogramFolder + "/" + ChannelFolder
        MergeFolder   = ChannelFolder.replace("Channel", "Merge")

        if not os.path.exists(MergeFolder) and not "MCNorm" in MergeFolder:
            os.system("mkdir " + MergeFolder)

        for entry in MergeList:

            ProcessList = entry[0]
            ProcessName = entry[1]
            
            if "MCNorm" in MergeFolder:

                if "ttbar" in ProcessName or "data" in ProcessName or "qcd" in ProcessName:
                    continue

                MergeFolder   = ChannelFolder.replace("Channel", "Merge")
                procFolderName = "MCNorm_"+ProcessName
                MergeFolder = MergeFolder.replace("MCNorm", procFolderName)
                
                if not os.path.exists(MergeFolder):
                    os.system("mkdir " + MergeFolder)
            
            OutputFile  = MergeFolder + "/Merged_" + ProcessName + ".root"
            OutputTreeFile  = MergeFolder + "/Merged_Tree_" + ProcessName + ".root"

            #if os.path.exists(OutputFile):
            #    continue

            #if os.path.exists(OutputTreeFile):
            #    continue

            MergeString = ""
            MergeTreeString = ""

            AllFilesComplete = True

            MissingFileList  = []

            #if "Loose" in ChannelFolder or "fakeEff" in ChannelFolder or "realEff" in ChannelFolder or "fakes_MTWModelling" in ChannelFolder:
            #    if not "qcd" in ProcessName:
            #        continue

                

            if "qcd" in ProcessName and "emu" in ChannelFolder :
                # For emu QCD, we need to merge el and mu separately, then zero them, then merge the channels
                if not ("Loose" in ChannelFolder or "fakeEff" in ChannelFolder or "realEff" in ChannelFolder or "MTWModelling" in ChannelFolder):
                    continue

                #for Chan in "ejets_2015", "mujets_2015", "ejets_2016", "mujets_2016":
                for Chan in "emujets_2015", "boosted_emujets_2015":

                    if "boosted" in ChannelFolder and not "boosted" in Chan:
                        continue
                    if not "boosted" in ChannelFolder and "boosted" in Chan:
                        continue

                    MergeString = ""
                    MergeTreeString = ""

                    for Process in ProcessList:
                        helpFile = ChannelFolder + "/Histogram_" + Process + "_" + Chan + ".root"
                        helpTreeFile = ChannelFolder + "/Tree_" + Process + "_" + Chan + ".root"
                        if os.path.exists(helpFile):
                            MergeString += " " + helpFile
                        else:
                            AllFilesComplete = False
                            #print helpFile, "   ", os.path.exists(helpFile)
                            #sys.exit()
                            MissingFileList.append(helpFile)
                            #MissingFileListAll.append(helpFile)

                        if os.path.exists(helpTreeFile):
                            MergeTreeString += " " + helpTreeFile
                        else:
                            AllFilesComplete = False
                            MissingFileList.append(helpTreeFile)
                            #MissingFileListAll.append(helpTreeFile)

                    if not AllFilesComplete and (not "data" in ProcessName  and not "qcd" in ProcessName):
                        print("\n==========================================================================================================")
                        print("The process " + ProcessName + " is missing the following files:")
                        print(MissingFileList)
                        continue

                    #print("Before merging ...")
                    # make output file only if at least one input file exists!!!
                    # instead of merging to OutputFile, we want to merge to a channel file, then zero it, then merge to OutputFile

                    TmpChanFile = MergeFolder + "/AllFakes_" + Chan + ".root"
                    TmpChanTreeFile =MergeFolder + "/AllFakes_" + Chan + "_Tree.root"

                    if ".root" in MergeString:
                        if not os.path.exists(TmpChanFile):
                            os.system("hadd -f " + TmpChanFile + " " + MergeString)
                    if ".root" in MergeTreeString:
                        if not os.path.exists(TmpChanTreeFile):
                            os.system("hadd -f " + TmpChanTreeFile + " " + MergeTreeString)

                    # now zero the bins
                    print("Zeroing...")
                    # call my script?
                    ZeroStr = "root -q -b -l PlotFactoryBoosted/scripts/CombineQCD.C\(\\\"" + TmpChanFile + "\\\"\)"
                    os.system(ZeroStr)

                #now hadd the two channels
                #if not os.path.exists(OutputFile):
                #    os.system("hadd -f " + OutputFile + " " + MergeFolder + "/AllFakes_ejets.root " + MergeFolder +  "/AllFakes_mujets.root")
                #if not os.path.exists(OutputTreeFile):
                #    os.system("hadd -f " + OutputTreeFile + " " + MergeFolder + "/AllFakes_ejets_Tree.root " + MergeFolder + "/AllFakes_mujets_Tree.root"  )
                #os.system("rm " + MergeFolder + "/AllFakes*.root")

                # if qcd, then copy final trees from "nominal_Loose" to "nominal" folder
                if "qcd" in OutputFile and "_Loose" in ChannelFolder:
                    HelpFolder = MergeFolder.replace("_Loose", "")
                    #print HelpFolder
                    os.system("cp " + MergeFolder + "/Merged_qcd.root " + HelpFolder + "/")
                    os.system("cp " + MergeFolder + "/Merged_Tree_qcd.root " + HelpFolder + "/")
                    #print "cp " + ChannelFolder + "/Merged_qcd.root " + HelpFolder + "/"

            else:
                #print "========================================================================================================="                                                                                                                                             
                #print ProcessName
                for Process in ProcessList:
                    #print "========================================================================================================="
                    #print Process
                    helpFile = ChannelFolder + "/Histogram_" + Process + ".root"
                    helpTreeFile = ChannelFolder + "/Tree_" + Process + ".root"
                    if os.path.exists(helpFile):
                        MergeString += " " + helpFile
                    else:
                        AllFilesComplete = False
                        #print helpFile, "   ", os.path.exists(helpFile)
                       # sys.exit()
                        MissingFileList.append(helpFile)
                        MissingFileListAll.append(helpFile)

                    if os.path.exists(helpTreeFile):
                        MergeTreeString += " " + helpTreeFile
                    else:
                        AllFilesComplete = False
                        MissingFileList.append(helpTreeFile)
                        MissingFileListAll.append(helpTreeFile)

                if not AllFilesComplete and (not "data" in ProcessName  and not "qcd" in ProcessName):
                    print("\n==========================================================================================================")
                    print("The process " + ProcessName + " is missing the following files:")
                    print(MissingFileList)
                    #continue

                #print("Before merging...")

                # make output file only if at least one input file exists!!!
                #print OutputFile
                #print MergeString
                if ".root" in MergeString:
                    if not os.path.exists(OutputFile):
                        os.system("hadd -f " + OutputFile + " " + MergeString)
                if ".root" in MergeTreeString:
                    if not os.path.exists(OutputTreeFile):
                        os.system("hadd -f " + OutputTreeFile + " " + MergeTreeString)

                if "qcd" in OutputFile:
                    ZeroStr = "root -q -b -l PlotFactoryBoosted/scripts/CombineQCD.C\(\\\"" + OutputFile + "\\\"\)"
                    os.system( ZeroStr)

                # if qcd, then copy final trees from "nominal_Loose" to "nominal" folder
                if "qcd" in OutputFile and "_Loose" in ChannelFolder:
                    HelpFolder = MergeFolder.replace("_Loose", "")
                    #print(HelpFolder)
                    os.system("cp " + MergeFolder + "/Merged_qcd.root " + HelpFolder + "/")
                    os.system("cp " + MergeFolder + "/Merged_Tree_qcd.root " + HelpFolder + "/")
                    #print("cp " + ChannelFolder + "/Merged_qcd.root " + HelpFolder + "/")

                

    if not MissingFileListAll:
        print("Success!")
    #else:
    #    print("Uh-oh! Some files were missing! You better check these:")
    #    print(MissingFileListAll)

def MakeCP(MainFolder, PlotErrorBand, DataLabel):
    Executable = "RunControlPlots"
    TotalList  = os.listdir(MainFolder)

    print(MainFolder)
    print(TotalList)

    ConfigFileNames = glob.glob(MainFolder + "/*Config*")

    print(ConfigFileNames)

    ConfigFileName  = ConfigFileNames[0]

    for entry in TotalList:

        if not "Merge" in entry:
            continue

        #if "_Loose" in entry:
        #    continue

        if not "nominal" in entry:
            continue

        comp = entry.split("_")

        print("=============================================================")
        print(comp)

        Channel     = comp[1]+"_"+comp[2]
        #Channel     = comp[1]
        JetBin      = comp[3]
        BTagBin     = comp[4]
        LargeJetBin = comp[5]
        TopTagBin   = comp[6]
        HiggsTagBin   = comp[7]

        for config in ConfigFileNames:
            if "Main" in config:
                ConfigFileName = config

        for config in ConfigFileNames:
            if "Electron" in config and "boosted_ejets" in entry:
                ConfigFileName = config
                continue
            elif "Muon" in config and "boosted_mujets" in entry:
                ConfigFileName = config
                continue

        if "ejets" in entry or "mujets" in entry or "emujets" in entry :
             Channel     = comp[1]+"_"+comp[2]
             JetBin      = comp[3]
             BTagBin     = comp[4]
             LargeJetBin = comp[5]
             TopTagBin   = comp[6]
             HiggsTagBin = comp[7]

        if "boosted_ejets" in entry or "boosted_mujets" in entry or "boosted_emujets" in entry:
            Channel     = comp[1] + "_" + comp[2] + "_" + comp[3]
            JetBin      = comp[4]
            BTagBin     = comp[5]
            LargeJetBin = comp[6]
            TopTagBin   = comp[7]
            HiggsTagBin = comp[8]

        if "minus" in entry:
            VetoJetBin      = comp[9]
            VetoBTagBin     = comp[10]
            VetoLargeJetBin = comp[11]
            VetoTopTagBin   = comp[12]
            VetoHiggsTagBin   = comp[13]
        else:
            VetoJetBin      = "none"
            VetoBTagBin     = "none"
            VetoLargeJetBin = "none"
            VetoTopTagBin   = "none"
            VetoHiggsTagBin = "none"

        InputFolder = MainFolder + "/" + entry + "/"

        if VetoJetBin == "none":
            OutputFolder = "ControlPlots_" + Channel + "_" + JetBin + "_" + BTagBin + "_" + LargeJetBin + "_" + TopTagBin + "_" + HiggsTagBin
        else:
            OutputFolder = "ControlPlots_" + Channel + "_" + JetBin + "_" + BTagBin + "_" + LargeJetBin + "_" + TopTagBin + "_" + HiggsTagBin + "_minus_" + VetoJetBin + "_" + VetoBTagBin + "_" + VetoLargeJetBin + "_" + VetoTopTagBin + "_" + VetoHiggsTagBin

        if not os.path.exists(OutputFolder):
            os.system("mkdir " + OutputFolder)

        cmd = Executable + " " + InputFolder + " " + OutputFolder + " " + JetBin + " " + BTagBin + " " + LargeJetBin + " " + TopTagBin + " " + HiggsTagBin + " " + VetoJetBin + " " + VetoBTagBin + " " + VetoLargeJetBin + " " + VetoTopTagBin + " " + VetoHiggsTagBin + " " + ConfigFileName + " 0 "+DataLabel

        if PlotErrorBand:
            cmd = Executable + " " + InputFolder + " " + OutputFolder + " " + JetBin + " " + BTagBin + " " + LargeJetBin + " " + TopTagBin + " " + HiggsTagBin + " " + VetoJetBin + " " + VetoBTagBin + " " + VetoLargeJetBin + " " + VetoTopTagBin + " " + VetoHiggsTagBin + " " + ConfigFileName + " 1 "+DataLabel

        print(cmd)
        os.system(cmd)
