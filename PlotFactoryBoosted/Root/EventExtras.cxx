#include "PlotFactoryBoosted/EventExtras.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/HistoUtilities.h"
#include "TLorentzVector.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <stdio.h>

using namespace std;


EventExtras::EventExtras(int EventIndex, FlatTreeReader *FlatTree, string Analysis, std::shared_ptr<ConfigClass> Config){

  //  std::cout << "PRINT 1 " << std::endl;

  m_eventIndex=EventIndex;
  m_flattree=FlatTree;
  m_flattree -> fChain -> GetEntry(m_eventIndex);

  min_largejetpt  = Config -> GetMinLargeRJetPt();
  min_Toppt       = Config -> GetMinTopPt();
  min_Higgspt     = Config -> GetMinHiggsPt();
  matchingdR      = Config -> GetMatchingDR();
  btagdisc        = Config -> GetBTaggingCut();
  btagdisc_loose  = Config -> GetBTaggingCutLoose();
  leadleppt       = Config -> GetLeadingLeptonPt();
  loose_b_tagging = Config -> GetUseLooseBTagging();
  m_missingEtCut  = Config -> GetMissingEtCut();
  m_triangularCut = Config -> GetTriangularCut();
  nbInTop         = Config -> GetNbInTop();
  nNonbInTop      = Config -> GetNnonbInTop();
  nbInHiggs       = Config -> GetNbInHiggs();


  if(loose_b_tagging == 0) btagdisc_loose = btagdisc;

  int higgstags = Config -> GetHiggsTagBin();
  int toptags = Config -> GetTopTagBin();

  int nJetCount   = m_flattree -> jet_pt  -> size();
  int nLJetCount  = m_flattree -> ljet_pt -> size();


  // All Small Jets
  std::vector<TLorentzVector> GoodSmallJets;
  GoodSmallJets.clear();
  std::vector<TLorentzVector> GoodSmallBTags;
  GoodSmallBTags.clear();
  std::vector<TLorentzVector> GoodSmallAntiTags;
  GoodSmallAntiTags.clear();

  //counters
  int nTagCount = 0;
  int nNTagCount = 0;

  //std::cout << "PRINT 2 " << std::endl;

  for(int iJet = 0; iJet < nJetCount; ++iJet){

    // count number of good small jets
    TLorentzVector smallJet;
    smallJet.SetPtEtaPhiE(m_flattree -> jet_pt -> at(iJet)/1000.0, m_flattree -> jet_eta -> at(iJet), m_flattree -> jet_phi -> at(iJet), m_flattree -> jet_e -> at(iJet)/1000.0);
    GoodSmallJets.push_back(smallJet);

    // count number of b-tagged jets
    if(m_flattree -> jet_mv2c10 -> at(iJet) > btagdisc){
      GoodSmallBTags.push_back(GoodSmallJets[iJet]);
      nTagCount++;
    }

    // count number of non b-tagged jets
   if(m_flattree -> jet_mv2c10 -> at(iJet) < btagdisc){
      GoodSmallAntiTags.push_back(GoodSmallJets[iJet]);
      nNTagCount++;
    }
  }

  // Get electron to be used for overlap removal
  bool isEL_forOR = false;
  TLorentzVector goodLepton_forOR;
  for(unsigned int electron = 0; electron < m_flattree->el_pt->size(); ++electron){
    TLorentzVector el;
    el.SetPtEtaPhiE(m_flattree->el_pt-> at(electron)/1000., m_flattree->el_eta->at(electron), m_flattree->el_phi->at(electron), m_flattree->el_e-> at(electron)/1000.);
    if(el.Pt() > leadleppt){
      goodLepton_forOR = el;
      isEL_forOR = true;
    }
  }
  
  //std::cout << "PRINT 3 " << std::endl;

  // Get large-R jets and top-tags
  std::vector<int> GoodLJetIndex;
  GoodLJetIndex.clear();
  std::vector<TLorentzVector> GoodLJets;
  GoodLJets.clear();
  std::vector<int> TopTagJetIndex;
  TopTagJetIndex.clear();
  std::vector<TLorentzVector> TopTagJets;
  TopTagJets.clear();
  std::vector<int> HiggsTagJetIndex;
  HiggsTagJetIndex.clear();
  std::vector<TLorentzVector> HiggsTagJets;
  HiggsTagJets.clear();

  int nTopTagCount = 0;

  for(int iJet = 0; iJet < nLJetCount; ++iJet){
    if(m_flattree -> ljet_pt  -> at(iJet)/1000.0 < min_largejetpt)
      continue;
    if(fabs(m_flattree -> ljet_eta -> at(iJet)) > largejeteta)
      continue;
    if(m_flattree -> ljet_pt  -> at(iJet)/1000.0 > max_largejetpt)
      continue;
    if(m_flattree -> ljet_m  -> at(iJet)/1000.0 < largejetmass)
      continue;
    TLorentzVector largeJet;
    largeJet.SetPtEtaPhiE(m_flattree -> ljet_pt -> at(iJet)/1000.0, m_flattree -> ljet_eta -> at(iJet), m_flattree -> ljet_phi -> at(iJet), m_flattree -> ljet_e -> at(iJet)/1000.0);

    // save good large jets
    GoodLJets.push_back(largeJet);
    GoodLJetIndex.push_back(iJet);

    // count number of tagged and untagged jets near the large jet                                                                                                               
    int bTagClose = 0;
    int nbTagClose = 0;
    for(unsigned int iTag = 0; iTag < GoodSmallBTags.size(); ++iTag){
      if(GoodSmallBTags[iTag].DeltaR(largeJet) < 1.0){
	bTagClose++;
      }
    }
    for(unsigned int iNTag = 0; iNTag < GoodSmallAntiTags.size(); ++iNTag){
      if(GoodSmallAntiTags[iNTag].DeltaR(largeJet) < 1.0){
	nbTagClose++;
      } 
    }

    bool isLeadT = false;

    //std::cout << "PRINT 4 " << std::endl;

    // save and count top-tags
  /*
  if(((bool)m_flattree -> ljet_topTag_loose -> at(iJet)) && (passBin(bTagClose, nbInTop)) && (passBin(nbTagClose,  nNonbInTop)) && (m_flattree -> ljet_pt  -> at(iJet)/1000.0 > min_Toppt)){
      if(isEL_forOR){
	double dR = goodLepton_forOR.DeltaR(largeJet);
	if(dR > 1.0){ 
	  TopTagJets.push_back(largeJet);
	  TopTagJetIndex.push_back(iJet);
	  nTopTagCount++;
	  if(nTopTagCount==1) isLeadT=true;
	}
      }
      else{
	TopTagJets.push_back(largeJet);
	TopTagJetIndex.push_back(iJet);
	nTopTagCount++;
	if(nTopTagCount==1) isLeadT=true;
      }
    }
  */
    if(!isLeadT && passBin(bTagClose,  nbInHiggs) && (m_flattree -> ljet_pt  -> at(iJet)/1000.0 > min_Higgspt)){
      //if(!isLeadT && (bTagClose >= 2)){
      HiggsTagJets.push_back(largeJet);
      HiggsTagJetIndex.push_back(iJet);
    }    
  }
  
  // Get Additional Jets

  //std::cout << "PRINT 5 " << std::endl;


  std::vector<TLorentzVector> OutAllTJets;
  OutAllTJets.clear();
  std::vector<TLorentzVector> OutAllTBTags;
  OutAllTBTags.clear();
  std::vector<TLorentzVector> OutAllTAntiTags;
  OutAllTAntiTags.clear();

  std::vector<TLorentzVector> AdditionalJets;
  AdditionalJets.clear();
  std::vector<TLorentzVector> AdditionalTags;
  AdditionalTags.clear();
  std::vector<TLorentzVector> AdditionalAntiTags;
  AdditionalAntiTags.clear();

  for(int iJet = 0; iJet < nJetCount; ++iJet){
    TLorentzVector small;
    small.SetPtEtaPhiE(m_flattree -> jet_pt -> at(iJet)/1000.0, m_flattree -> jet_eta -> at(iJet), m_flattree -> jet_phi -> at(iJet), m_flattree -> jet_e -> at(iJet)/1000.0);
    bool notMatched_allt = true;
    bool notMatched_leadt = true;
    bool notMatched_allH = true;
    for(unsigned int tJet = 0; tJet < TopTagJets.size(); ++tJet){
      double dR = small.DeltaR(TopTagJets[tJet]);
      if(dR < matchingdR){
	notMatched_allt = false;
	if(tJet==0) notMatched_leadt=false;
      }
    }
    for(unsigned int tJet = 0; tJet < HiggsTagJets.size(); ++tJet){
      double dR = small.DeltaR(HiggsTagJets[tJet]);
      if(dR < matchingdR){
	notMatched_allH = false;
      }
    }
    if(notMatched_allt){
      OutAllTJets.push_back(small);
      if(m_flattree -> jet_mv2c10 -> at(iJet) > btagdisc) OutAllTBTags.push_back(small);
      else OutAllTAntiTags.push_back(small);
    }
    if(higgstags>0 && notMatched_allH && notMatched_leadt){
      AdditionalJets.push_back(small);
      if(m_flattree -> jet_mv2c10 -> at(iJet) > btagdisc) AdditionalTags.push_back(small);
      else AdditionalAntiTags.push_back(small);
    }
    else if(higgstags==0 && notMatched_allt){
      AdditionalJets.push_back(small);
      if(m_flattree -> jet_mv2c10 -> at(iJet) > btagdisc) AdditionalTags.push_back(small);
      else AdditionalAntiTags.push_back(small);
    }
  }

  //std::cout << "PRINT 6 " << std::endl;

  // count number of leptons
  int mu_n = m_flattree->mu_pt->size();
  int el_n = m_flattree->el_pt->size();

  // Define good lepton
  TLorentzVector goodLepton;
  int ngoodleptons = 0;
  bool isEl    = false;
  bool isMu    = false;
  char isTight = -1;
  float goodLeptonCharge = 0;

  for( int electron = 0; electron < el_n; ++electron){
    TLorentzVector el;
    el.SetPtEtaPhiE(m_flattree->el_pt-> at(electron)/1000., m_flattree->el_eta->at(electron), m_flattree->el_phi->at(electron), m_flattree->el_e-> at(electron)/1000.);
    float charge = m_flattree -> el_charge  -> at(electron);
    if(el.Pt() > leadleppt){
      goodLepton = el;
      ngoodleptons++;
      isEl = true;
      goodLeptonCharge = charge;
    }
  }
  for( int muon = 0; muon < mu_n; ++muon){
    TLorentzVector mu;
    mu.SetPtEtaPhiE(m_flattree->mu_pt-> at(muon)/1000., m_flattree->mu_eta->at(muon), m_flattree->mu_phi->at(muon), m_flattree->mu_e-> at(muon)/1000.);
    float charge = m_flattree -> mu_charge  -> at(muon);
    if (mu.Pt() > leadleppt){
      goodLepton = mu;
      ngoodleptons++;
      isMu = true;
      goodLeptonCharge = charge;
    }
  }

  //std::cout << "PRINT 7 " << std::endl;



  if(ngoodleptons > 1){ std::cout << "WARNING: More than one good lepton in the event!" << std::endl; exit(0);}

  m_nJets_all=GoodSmallJets.size();
  m_nBTags_all=GoodSmallBTags.size();
  m_nAntiTags_all=GoodSmallAntiTags.size();

  m_nJets_additional=AdditionalJets.size();
  m_nTags_additional=AdditionalTags.size();
  m_nAntiTags_additional=AdditionalAntiTags.size();

  m_nJets_outallT=OutAllTJets.size();
  m_nTags_outallT=OutAllTBTags.size();
  m_nAntiTags_outallT=OutAllTAntiTags.size();

  m_nJets_inTop = m_nJets_all-m_nJets_outallT;
  m_nTags_inTop = m_nBTags_all-m_nTags_outallT;
  m_nAntiTags_inTop = m_nAntiTags_all-m_nAntiTags_outallT;

  m_nLargeJets=GoodLJets.size();
  m_nTopTags=TopTagJets.size();
  m_nHiggsTags=HiggsTagJets.size();

  m_GoodLJets.clear();
  m_GoodLJets=GoodLJets;
  m_GoodLJetIndex.clear();
  m_GoodLJetIndex=GoodLJetIndex;

  m_TopTagJets.clear();
  m_TopTagJets=TopTagJets;
  m_TopTagJetIndex.clear();
  m_TopTagJetIndex=TopTagJetIndex;

  m_HiggsTagJets.clear();
  m_HiggsTagJets=HiggsTagJets;
  m_HiggsTagJetIndex.clear();
  m_HiggsTagJetIndex=HiggsTagJetIndex;

  m_GoodSmallJets.clear();
  m_GoodSmallJets=GoodSmallJets;
  m_GoodSmallBTags.clear();
  m_GoodSmallBTags=GoodSmallBTags;
  m_GoodSmallAntiTags.clear();
  m_GoodSmallAntiTags=GoodSmallAntiTags;

  m_OutAllTJets.clear();
  m_OutAllTJets=OutAllTJets;
  m_OutAllTBTags.clear();
  m_OutAllTBTags=OutAllTBTags;
  m_OutAllTAntiTags.clear();
  m_OutAllTAntiTags=OutAllTAntiTags;

  m_AdditionalJets.clear();
  m_AdditionalJets=AdditionalJets;
  m_AdditionalBTags.clear();
  m_AdditionalBTags=AdditionalTags;
  m_AdditionalAntiTags.clear();
  m_AdditionalAntiTags=AdditionalAntiTags;

  
  m_mu_n = mu_n;
  m_el_n = el_n;

  m_GoodLepton = goodLepton;
  m_isMu = isMu;
  m_isEl = isEl;
  m_LeptonIsTight    = isTight;
  m_GoodLeptonCharge = goodLeptonCharge;

}

EventExtras::EventExtras()
{

}

EventExtras::~EventExtras()
{

}
