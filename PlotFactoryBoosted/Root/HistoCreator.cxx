#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/StatusLogbook.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/BKUtilities.h"
#include "PlotFactoryBoosted/HistoUtilities.h"
#include "PlotFactoryBoosted/EventExtras.h"
#include "TopDataPreparation/SampleXsection.h"

#include "TChain.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TH2D.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif


HistoCreator::HistoCreator(std::string sampleList, std::string Channel, std::string AnalysisType, std::shared_ptr<ConfigClass> Config, std::string TreeName)
{

  gROOT        -> ProcessLine("#include<vector>");
  gInterpreter -> GenerateDictionary("vector<vector<int> >","vector");

  fSampleList = sampleList;

  gConfig     = Config;

  //  const char* const rc = getenv("ROOTCOREBIN");

  // chain all input trees
  TChain *HelpTree   = ChainFiles(TreeName.c_str(), fSampleList); // flat nominal tree

  std::vector<std::string> FileList = GetFileList(fSampleList);

  fAnalysisType = AnalysisType;

  // get here all values from your config file
  fLumi            = gConfig -> GetLumi();
  fLargeJetBin     = gConfig -> GetLargeJetBin();
  fTopTagBin       = gConfig -> GetTopTagBin();
  fHiggsTagBin     = gConfig -> GetHiggsTagBin();
  fJetBin          = gConfig -> GetJetBin();
  fBTagBin         = gConfig -> GetBTagBin();
  fVetoLargeJetBin = gConfig -> GetVetoLargeJetBin();
  fVetoTopTagBin   = gConfig -> GetVetoTopTagBin();
  fVetoJetBin      = gConfig -> GetVetoJetBin();
  fVetoBTagBin     = gConfig -> GetVetoBTagBin();
  fSampleType      = gConfig -> GetSampleType();
  fOutputFile      = gConfig -> GetOutputName();
  fOutputTreeFile  = gConfig -> GetOutputTreeName();
  fTTbarHF         = gConfig -> GetHFType();
  fMVATreesForResolved = gConfig -> GetMVATreesForResolved();
  fTreeName        = TreeName;

  if(fSampleType != "QCD_Nedaa"){

    // get total number of events for correct normalisation                                                                                                        

    TChain *HelpTreeBK = ChainFiles("sumWeights",     fSampleList); // Bookkeeping Tree

  
    fSampleInfo = CalculateTotalEventNumberHisto(FileList, HelpTreeBK, Channel);

    fDSID          = fSampleInfo.second;
    fTotalEventsMC = fSampleInfo.first;

  }


  gConfig -> SetDSID(fDSID);

  fChannel      = Channel;

  std::cout << "=======================================================================================> " << fChannel << "\t" << Channel << std::endl;


  fFlatTree        = new FlatTreeReader(HelpTree);

  std::cout << "fDSID == "        << fDSID        << std::endl;
  std::cout << "AnalysisType == " << AnalysisType << std::endl;
    
  fReweightTTbb = false;

  if(AnalysisType == "ttH"){

    int hfDSID = fDSID;

    if((hfDSID>410000 && hfDSID<=410004) || hfDSID == 410009 || hfDSID == 410120 || hfDSID == 410225 || hfDSID == 410274 || (hfDSID>=410500 && hfDSID<=410528)){
      if(TreeName == "nominal" || TreeName == "nominal_Loose")
	fReweightTTbb = true;
    }

  }


  fSplitHF = false; // false by default  

  if(Channel == "emujets_2015"){
    fNtupleCutflowHisto_e = MakeNtupleCutflow(FileList, "ejets_2015");
    fNtupleCutflowHisto_mu = MakeNtupleCutflow(FileList, "mujets_2015");
  }
  else if(Channel == "emujets_2016"){
    fNtupleCutflowHisto_e = MakeNtupleCutflow(FileList, "ejets_2016");
    fNtupleCutflowHisto_mu = MakeNtupleCutflow(FileList, "mujets_2016");
  }
  else if(Channel == "boosted_emujets_2015"){
    fNtupleCutflowHisto_e = MakeNtupleCutflow(FileList, "boosted_ejets_2015");
    fNtupleCutflowHisto_mu = MakeNtupleCutflow(FileList, "boosted_mujets_2015");
  }
  else if(Channel == "boosted_emujets_2016"){
    fNtupleCutflowHisto_e = MakeNtupleCutflow(FileList, "boosted_ejets_2016");
    fNtupleCutflowHisto_mu = MakeNtupleCutflow(FileList, "boosted_mujets_2016");
  }
  else if(Channel == "ejets_2015" || Channel == "boosted_ejets_2015" || Channel == "ejets_2016" || Channel == "boosted_ejets_2016" ){

    if(fSampleType != "QCD_Nedaa")
      fNtupleCutflowHisto_e = MakeNtupleCutflow(FileList, Channel);

  }
  else if(Channel == "mujets_2015" || Channel == "boosted_mujets_2015" || Channel == "mujets_2016" || Channel == "boosted_mujets_2016"){
    if(fSampleType != "QCD_Nedaa")
      fNtupleCutflowHisto_mu = MakeNtupleCutflow(FileList, Channel);
  }


}

HistoCreator::~HistoCreator()
{

  delete fFlatTree;

}

bool HistoCreator::InitializeHistograms(std::string InputFileName, std::string InputFileDir)
{
  
  WriteInfoStatus("HistoCreator", "InitializeHistograms...");

  ifstream InputListFile;
  InputListFile.open(InputFileName.c_str());

  int counter = 0;

  string fileline;
  string line;
  
  while (InputListFile.good()) {

    getline(InputListFile, fileline);
    std::istringstream input_fileline(fileline);

    string setname;
    string setindex;
    input_fileline >> setname;
    input_fileline >> setindex;

    int setindex_int = TString(setindex).Atoi();

    
    if(setindex_int == 0 && setname!="CoreHistos") continue;

    std::cout << "Adding Histogram set: " << setname << std::endl;    
    fSetList.push_back(setname);

    string setfilename = InputFileDir+setname+".txt";

    ifstream InputFile;
    InputFile.open(setfilename.c_str());

    while (InputFile.good()) {

      counter++;
      
      getline(InputFile, line);
    
      std::istringstream input_line(line);
      string name;
      string index;
      string bins;
      string lower;
      string upper;
      
      
      std::vector<double> binEdges;
      
      input_line >> name;
      input_line >> index;
      int   index_int = TString(index).Atoi();
      
      if(index_int == 1){

	TH1F helpHist;
	
       	input_line >> bins;

	// Variable Binning
	if (bins == "v" || bins == "V")
	  { 
	    string tmp;
	    input_line >> tmp;
	    int size = 0;
	    while (tmp != "v" && tmp != "V")
	      {
		binEdges.push_back(TString(tmp).Atof());
		size++;
		input_line >> tmp;
	      }
	    
	    double* edges_array = new double[ size ];
	    for ( int iii = 0; iii < size; ++iii)
	      {
		edges_array[iii] = binEdges[iii];
	      }
	    helpHist = TH1F(name.c_str(), name.c_str(), size-1, edges_array);
	    
	    delete edges_array;
	  }
	
	else
	  {
	    input_line >> lower;
	    input_line >> upper;
	    int   bins_int  = TString(bins).Atoi();
	    float lower_int = TString(lower).Atof();
	    float upper_int = TString(upper).Atof();
	    
	    helpHist = TH1F(name.c_str(), name.c_str(), bins_int, lower_int, upper_int);
	  }
	
	
	if(name.find( "weight_" ) != std::string::npos && fTreeName != "nominal_Loose")
          continue;
	
	helpHist.Sumw2();
	
	fHistoVector.push_back(helpHist);
	fHistoName.push_back("hist_"+name);
	fVariables.push_back(name);
	if(name.find( "weight_" ) != std::string::npos && fTreeName == "nominal_Loose") fWeights.push_back(name);
	
	float helpFloat;
	
	fVar.push_back(helpFloat);
	

	
      }
      
      // 2D Plots
      if(index_int == 2)
	{
	  TH2F helpHist_2D;
	  //std::cout << "Histogram: " << name << std::endl;

	  input_line >> bins;
	  if (bins == "v" || bins == "V")
	    {
	      string tmp;
	      input_line >> tmp;
	      int size = 0;
	      while (tmp != "v" && tmp != "V")
		{
		  binEdges.push_back(TString(tmp).Atof());
		  size++;
		  input_line >> tmp;
		}
	      double* edges_array = new double[ size ];
	      for ( int iii = 0; iii < size; ++iii)
		{
		  edges_array[iii] = binEdges[iii];
		}
	      helpHist_2D = TH2F(name.c_str(), name.c_str(), size-1, edges_array, size-1, edges_array);
	      
	      delete edges_array;
	      
	    }

	else {
	  
	  input_line >> lower;
	  input_line >> upper;
	  int   bins_int  = TString(bins).Atoi();
	  float lower_int = TString(lower).Atof();
	  float upper_int = TString(upper).Atof();
	  string bins_2, lower_2, upper_2;
	  input_line >> bins_2;
	  input_line >> lower_2;
	  input_line >> upper_2;
	  int   bins_2_int  = TString(bins_2).Atoi();
	  float lower_2_int = TString(lower_2).Atof();
	  float upper_2_int = TString(upper_2).Atof();
	  
	  helpHist_2D = TH2F(name.c_str(), name.c_str(), bins_int, lower_int, upper_int, bins_2_int, lower_2_int, upper_2_int);

	}

	helpHist_2D.Sumw2();
	
	fHistoVector_2D.push_back(helpHist_2D);
	fHistoName_2D.push_back("hist_"+name);
	fVariables_2D.push_back(name);

	float helpFloat_2D;
	fVar_2D.push_back(helpFloat_2D);
	  
	}
      // Vector Variables
      if(index_int == 3){
	
        fVariables_vec.push_back(name);
        
        float helpFloat_vec;
        fVar_vec.push_back(helpFloat_vec);
        
      }      
      

    }
  }
  string cutflowfilename = InputFileDir+"cutflows.txt";
  string cutflowline;

  ifstream InputCutflowFile;
  InputCutflowFile.open(cutflowfilename.c_str());
  vector<string> pass_labels;

  while (InputCutflowFile.good()) {
    getline(InputCutflowFile, cutflowline);

    std::istringstream input_cutflowline(cutflowline);
    
    string cfname;
    string cfindex;
    string cfbins;

    input_cutflowline >> cfname;
    input_cutflowline >> cfindex;
    input_cutflowline >> cfbins;
    int   cfindex_int = TString(cfindex).Atoi();
    TH2D helpHist_cf;
    int cfbins_int  = TString(cfbins).Atoi();
    
    if(cfindex_int == 1){
      helpHist_cf = TH2D(cfname.c_str(), cfname.c_str(), cfbins_int, 0.5, cfbins_int+0.5, 3, 0.5, 3.5);
      pass_labels.resize(cfbins_int);
      pass_labels.clear();
      if(cfname=="BoostedttH"){
	pass_labels.push_back("ALL");
	pass_labels.push_back("LJet_N >= 1");
	pass_labels.push_back("N_topTagLoose >= 1");
	pass_labels.push_back("Jet_N 25000 >= 3");
	pass_labels.push_back("Jet_N[dR(jet,toptags)>1.0] >= 3");
	pass_labels.push_back("BTag_N[dR(jet,toptags)>1.0] >= 2");
      }
      if(cfname=="BoostedttH_Eloi"){
	pass_labels.push_back("ALL");
	pass_labels.push_back("N_Higgs >= 1");
	pass_labels.push_back("N_topTagLoose >= 1");
	pass_labels.push_back("Jet_N[dR(jet,(H,t)tags)>1.0] >= 1");
	pass_labels.push_back("BTag_N[dR(jet,(H,t)tags)>1.0] >= 1");
      }
      if(cfname=="ResolvedttH"){
	pass_labels.push_back("ALL");
	pass_labels.push_back("Njets == 4");
	pass_labels.push_back("Njets == 4, NBtags ==2");
	pass_labels.push_back("Njets == 4, NBtags ==3");
	pass_labels.push_back("Njets == 4, NBtags >=4");
	pass_labels.push_back("Njets == 5");
	pass_labels.push_back("Njets == 5, NBtags ==2");
	pass_labels.push_back("Njets == 5, NBtags ==3");
	pass_labels.push_back("Njets == 5, NBtags >=4");
	pass_labels.push_back("Njets == 6");
	pass_labels.push_back("Njets == 6, NBtags ==2");
	pass_labels.push_back("Njets == 6, NBtags ==3");
	pass_labels.push_back("Njets == 6, NBtags >=4");
      }

      if(((int)pass_labels.size())!=cfbins_int) std::cout<<"WARNING: number of cuts does not match up to number of cut labels" << std::endl;
      else{
	for(int i = 0; i<cfbins_int;i++){
	  helpHist_cf.GetXaxis()->SetBinLabel(i+1,pass_labels[i].c_str());
	}
      }
      fCutflowHistoVector.push_back(helpHist_cf);
      fCutflowName.push_back(cfname);
      
    }

  }

  return true;
}


void HistoCreator::FillCPDistributions()
{
  float LumiWeight = 1.0;
  if(fSampleType == "MC" || fSampleType == "MCNorm") 
    LumiWeight = CalculateLumiWeight();

  
  WriteInfoStatus("HistoCreator", "Fill CP distributions...");

  std::string OutputFileTree;  
  int TotalEntries = fFlatTree -> fChain -> GetEntries();
  float FinalWeight = -100.0;
  float FinalWeightNoPileUp = FinalWeight;
  std::cout << "TotalEntries: " << TotalEntries << std::endl;
  std::cout << "fMVATreesForResolved " << fMVATreesForResolved << std::endl;
  bool isResolvedSignalRegion = (fJetBin==6 && fBTagBin==4) || (fJetBin==6 && fBTagBin==3) || (fJetBin==5 && fBTagBin==4);

  int Pass_4JE2BE = -1.0;
  int Pass_5JE2BE = -1.0;
  int Pass_6JI2BE = -1.0;
  int Pass_4JE3BE = -1.0;
  int Pass_5JE3BE = -1.0;
  int Pass_6JI3BE = -1.0;
  int Pass_4JE4BI = -1.0;
  int Pass_5JE4BI = -1.0;
  int Pass_6JI4BI = -1.0;

  TFile *fOutTreeFile = TFile::Open(fOutputTreeFile.c_str(), "RECREATE");
  TTree t2("t2","a new tree for MVA");
  TTree t3("t3","a new reduced tree for resolved analysis");

  t2.Branch("FinalWeight", &FinalWeight);
  t2.Branch("Pass_4JE2BE", &Pass_4JE2BE);
  t2.Branch("Pass_5JE2BE", &Pass_5JE2BE);
  t2.Branch("Pass_6JI2BE", &Pass_6JI2BE);
  t2.Branch("Pass_4JE3BE", &Pass_4JE3BE);
  t2.Branch("Pass_5JE3BE", &Pass_5JE3BE);
  t2.Branch("Pass_6JI3BE", &Pass_6JI3BE);
  t2.Branch("Pass_4JE4BI", &Pass_4JE4BI);
  t2.Branch("Pass_5JE4BI", &Pass_5JE4BI);
  t2.Branch("Pass_6JI4BI", &Pass_6JI4BI);

  t3.Branch("FinalWeight", &FinalWeight);
  t3.Branch("Pass_4JE2BE", &Pass_4JE2BE);
  t3.Branch("Pass_5JE2BE", &Pass_5JE2BE);
  t3.Branch("Pass_6JI2BE", &Pass_6JI2BE);
  t3.Branch("Pass_4JE3BE", &Pass_4JE3BE);
  t3.Branch("Pass_5JE3BE", &Pass_5JE3BE);
  t3.Branch("Pass_6JI3BE", &Pass_6JI3BE);
  t3.Branch("Pass_4JE4BI", &Pass_4JE4BI);
  t3.Branch("Pass_5JE4BI", &Pass_5JE4BI);
  t3.Branch("Pass_6JI4BI", &Pass_6JI4BI);

  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){
    fTreeVariables.push_back(0.);
  }
  for(unsigned iVar = 0; iVar < fVar_vec.size(); ++iVar){
    vector<float> tempvec;
    tempvec.push_back(0.0);
    fTreeVectorVariables.push_back(tempvec);
  }
  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){
    t2.Branch(fVariables[iVar].c_str(),&fTreeVariables[iVar]);
    if(fVariables[iVar] == "ClassifBDTOutput_withReco_basic" || fVariables[iVar] == "ClassifBDTOutput_basic" || fVariables[iVar] == "ClassifBDTOutput_withReco_6jsplit" || fVariables[iVar] == "ClassifBDTOutput_6jsplit" || fVariables[iVar].find( "SemilepRegions_" ) != std::string::npos || fVariables[iVar] == "HhadT_jets" || fVariables[iVar].find( "weight_" ) != std::string::npos){
      t3.Branch(fVariables[iVar].c_str(),&fTreeVariables[iVar]);
    }
  }
  for(unsigned iVar = 0; iVar < fVar_vec.size(); ++iVar){
    t2.Branch(fVariables_vec[iVar].c_str(),&fTreeVectorVariables[iVar]);
    if( fVariables_vec[iVar].find( "weight_" ) != std::string::npos){
      t3.Branch(fVariables_vec[iVar].c_str(),&fTreeVectorVariables[iVar]);
    }
  }



  /*
  for(unsigned iVar = 0; iVar < fWeights.size(); ++iVar){
    fTreeWeights.push_back(0.);
  }
  for(unsigned iVar = 0; iVar < fWeights.size(); ++iVar){
    t3.Branch(fWeights[iVar].c_str(),&fTreeWeights[iVar]);
  }
  */
  // check for duplicates
  std::set<std::pair<int, int> > pairDuplicates; 
  EventExtras *iEntryExtras = 0;

  // Loop events
  int HFvar = -10000;

  if(((fDSID>=410501 && fDSID<=410009) || fDSID==410504 || fDSID==410121 || (fDSID>=410225 && fDSID<=410275) || (fDSID>=410500 && fDSID<=410528)) && fTTbarHF != "Inclusive"){
    std::cout<<"AZ: I am in fTTbarHF!=Inclusive        and fTTBarHF="<<fTTbarHF<<std::endl;
    fSplitHF = true;

    if(fTTbarHF == "ttlight")
      HFvar = 0;
    else if(fTTbarHF == "ttbb")
      HFvar = 1;
    else if(fTTbarHF =="ttcc")
      HFvar = -1;
    else{
      std::cout << "ERROR::HF Flag " << fTTbarHF << "is not defined!!! ---> EXIT." << std::endl;
      exit(-1);
    }
    
  }

  double TestWeight      = 0;

  for(int iEntry = 0; iEntry < TotalEntries; ++iEntry){

    fFlatTree -> fChain -> GetEntry(iEntry);
    
    if((iEntry % 50000) == 0){
      std::cout << "Processing entry: " << iEntry << std::endl;
    }
    double fakesWeightTemp = fFlatTree -> weight_mm_ejets;
        //if(fakesWeightTemp>0) std::cout << fakesWeightTemp << std::endl; 

    if(fSampleType == "QCD_Nedaa"){

      if(fChannel == "ejets_2015"   && fFlatTree  ->  ejets_2015_Loose == 0)
	continue;
      if(fChannel == "mujets_2015"  && fFlatTree  -> mujets_2015_Loose == 0)
	continue;
      if(fChannel == "ejets_2016"   && fFlatTree  ->  ejets_2016_Loose == 0)
	continue;
      if(fChannel == "mujets_2016"  && fFlatTree  -> mujets_2016_Loose == 0)
	continue;
      if(fChannel == "emujets_2015" && (fFlatTree ->  ejets_2015_Loose == 0 && fFlatTree -> mujets_2015_Loose == 0))
	continue;
      if(fChannel == "emujets_2016" && (fFlatTree ->  ejets_2016_Loose == 0 && fFlatTree -> mujets_2016_Loose == 0))
	continue;
          
      if(fChannel == "boosted_ejets_2015"   && fFlatTree  -> boosted_ejets_2015_Loose  == 0)
	continue;
      if(fChannel == "boosted_mujets_2015"  && fFlatTree  -> boosted_mujets_2015_Loose == 0)
	continue;
      if(fChannel == "boosted_emujets_2015" && (fFlatTree -> boosted_ejets_2015_Loose  == 0 && fFlatTree -> boosted_mujets_2015_Loose == 0))
	continue;    
      if(fChannel == "boosted_ejets_2016"   && fFlatTree  -> boosted_ejets_2016_Loose  == 0)
	continue;
      if(fChannel == "boosted_mujets_2016"  && fFlatTree  -> boosted_mujets_2016_Loose == 0)
	continue;
      if(fChannel == "boosted_emujets_2016" && (fFlatTree -> boosted_ejets_2016_Loose  == 0 && fFlatTree -> boosted_mujets_2016_Loose == 0))
	continue;

    
    }

    else{

      if(fChannel == "ejets_2015"   && fFlatTree  ->  ejets_2015 == 0)
        continue;
      if(fChannel == "mujets_2015"  && fFlatTree  -> mujets_2015 == 0)
        continue;
      if(fChannel == "ejets_2016"   && fFlatTree  ->  ejets_2016 == 0)
        continue;
      if(fChannel == "mujets_2016"  && fFlatTree  -> mujets_2016 == 0)
        continue;
      if(fChannel == "emujets_2015" && (fFlatTree ->  ejets_2015 == 0 && fFlatTree -> mujets_2015 == 0))
        continue;
      if(fChannel == "emujets_2016" && (fFlatTree ->  ejets_2016 == 0 && fFlatTree -> mujets_2016 == 0))
        continue;
      
          
      if(fChannel == "boosted_ejets_2015"   && fFlatTree  -> boosted_ejets_2015  == 0)
        continue;
      if(fChannel == "boosted_mujets_2015"  && fFlatTree  -> boosted_mujets_2015 == 0)
        continue;
      if(fChannel == "boosted_emujets_2015" && (fFlatTree -> boosted_ejets_2015 == 0 && fFlatTree -> boosted_mujets_2015 == 0))
        continue;
      if(fChannel == "boosted_ejets_2016"   && fFlatTree  -> boosted_ejets_2016  == 0)
        continue;
      if(fChannel == "boosted_mujets_2016"  && fFlatTree  -> boosted_mujets_2016 == 0)
        continue;
      if(fChannel == "boosted_emujets_2016" && (fFlatTree -> boosted_ejets_2016 == 0 && fFlatTree -> boosted_mujets_2016 == 0))
        continue;

            std::cout << "Hier 1" << std::endl;
      
      // reject dilepton events                                                                                                                                                        
      
      if(fFlatTree  -> ee_2015  == 1 || fFlatTree  -> mumu_2015  == 1 || fFlatTree  -> emu_2015  == 1)
        continue;
      
      if(fFlatTree  -> ee_2016  == 1 || fFlatTree  -> mumu_2016  == 1 || fFlatTree  -> emu_2016  == 1)
        continue;
    
    }
    float HF_SimpleClassification;  //comment this line
        std::cout << "Hier 2" << std::endl;


    if(fSplitHF){
      
      //std::cout<< "AZ: HF_SimpleClassification"<<fFlatTree -> HF_SimpleClassification<<"     AND HFvar="<<HFvar<<std::endl;
      if(fFlatTree -> HF_SimpleClassification != HFvar)
	continue;

    }

        std::cout << "Hier 3" << "\t" << fFlatTree -> TopHeavyFlavorFilterFlag  << std::endl;
    //std::cout<< "WARNING!! THIS CODE WILL DOUBLE COUNT ttbb from 410501 and 410504"<< std::endl;
    if (fAnalysisType == "ttH" && fDSID==410501  && fFlatTree -> TopHeavyFlavorFilterFlag == 5) continue;
    //uncomment the line above to correct it!!! 

            std::cout << "Hier 4" << std::endl;


    // Create EventExtras
    if(iEntryExtras) delete iEntryExtras;
    iEntryExtras = new EventExtras(iEntry, fFlatTree, fAnalysisType, gConfig);


    // if data, check for double counting and take events that already exists out!!!
    if(fSampleType == "Data"){
      if(!pairDuplicates.insert(std::make_pair(fFlatTree->runNumber, fFlatTree->eventNumber)).second){
	std::cout << "WARNING Found duplicated data event in event: " << fFlatTree->eventNumber << "  run: " << fFlatTree->runNumber << " ===> event will be removed!" << std::endl;
	continue;
      }
    }


    // Set event weights
    FinalWeight = 1.0;
    FinalWeightNoPileUp = FinalWeight;
    
    //int nJets = iEntryExtras -> GetNJetsAll();
    //int nBJets = iEntryExtras-> GetNBTagsAll();



    if(fAnalysisType == "ttH"){

      if(fSampleType.find("MC") != std::string::npos && fBTagBin != v0incl){
	//FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_bTagSF_85*fFlatTree->weight_jvt*fFlatTree->weight_pileup;
	FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_bTagSF_77*fFlatTree->weight_jvt*fFlatTree->weight_pileup;
	//FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_bTagSF_Continuous*fFlatTree->weight_jvt*fFlatTree->weight_pileup;
      }
      else if(fSampleType.find("MC") != std::string::npos){
	FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_jvt*fFlatTree->weight_pileup;
      }
    }
    

    std::cout << "Hier 5 = " << FinalWeight << std::endl;

    //FinalWeight = FinalWeight/LumiWeight;

    if(fSampleType == "MC" && fSFType != "nominal"){

      if (fSFType.find("weight_pileup") != std::string::npos){
	FinalWeight = FinalWeightNoPileUp * GetSystWeights(fSFType);
      }
      else{
	FinalWeight *= GetSystWeights(fSFType);
      }
    }


    iEntryExtras -> SetEntryWeight(FinalWeight);
    iEntryExtras -> SetEntryLumiWeight(LumiWeight);
    
    //Choose the additional small R jets for event selection
    int jets_to_cut_on = iEntryExtras->GetNJetsAll();
    int tags_to_cut_on = iEntryExtras->GetNBTagsAll();

    int jets_all = iEntryExtras->GetNJetsAll();
    int tags_all = iEntryExtras->GetNBTagsAll();

    if((fChannel == "boosted_ejets_2015" || fChannel == "boosted_mujets_2015" || fChannel == "boosted_ejets_2016" || fChannel == "boosted_mujets_2016" || fChannel == "boosted_emujets_2015" || fChannel == "boosted_emujets_2016") && fAnalysisType=="ttH"){
      jets_to_cut_on = iEntryExtras->GetNJetsAdditional();
      tags_to_cut_on = iEntryExtras->GetNBTagsAdditional(); 


    }

    
    float tthffraction = 1.0;
    if(fReweightTTbb){
      tthffraction = fFlatTree -> weight_ttbar_FracRw;
    }

    std::cout << "Hier 11 " << FinalWeight << std::endl;


    if(fAnalysisType == "ttH"){

      if(fSampleType.find("MC") != std::string::npos){

	//std::cout<<"AZ: Weights:  "<<std::endl;
	//std::cout<<"LumiWeight                        "<<LumiWeight<<std::endl;
	//std::cout<<"fFlatTree->weight_mc              "<<fFlatTree->weight_mc<<std::endl;
	//std::cout<<"fFlatTree->weight_leptonSF        "<<fFlatTree->weight_leptonSF<<std::endl;
	//std::cout<<"fFlatTree->weight_bTagSF_77       "<<fFlatTree->weight_bTagSF_77<<std::endl;
	//std::cout<<"fFlatTree->weight_jvt             "<<fFlatTree->weight_jvt<<std::endl;
	//std::cout<<"fFlatTree->weight_pileup          "<<fFlatTree->weight_pileup<<std::endl;
	//std::cout<<"fFlatTree->weight_ttbb_Nominal    "<<fFlatTree->weight_ttbb_Nominal<<std::endl;
	//std::cout<<"fFlatTree->weight_ttbb_Norm    "<<fFlatTree->weight_ttbb_Norm<<std::endl;
	//std::cout<<"tthffraction                      "<<tthffraction<<std::endl;


        //FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_bTagSF_85*fFlatTree->weight_jvt*fFlatTree->weight_pileup*fFlatTree->weight_ttbb_Nominal*tthffraction;
	FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_bTagSF_77*fFlatTree->weight_jvt*fFlatTree->weight_pileup*fFlatTree->weight_ttbb_Norm*tthffraction;
	//FinalWeight = LumiWeight*fFlatTree->weight_mc*fFlatTree->weight_leptonSF*fFlatTree->weight_bTagSF_Continuous*fFlatTree->weight_jvt*fFlatTree->weight_pileup*fFlatTree->weight_ttbb_Norm*tthffraction;
      }
      //      std::cout << fFlatTree->weight_ttbb_Nominal << "\t" << tthffraction << std::endl;

    }

    std::cout << "Hier 12 " << FinalWeight << std::endl;


    iEntryExtras -> SetEntryWeight(FinalWeight);

    // Do cutflow challenge
    FillCutflowChallenge(iEntryExtras);


    std::vector<TLorentzVector> TopTaggedLJets = iEntryExtras -> GetTopTaggedJets();
    TLorentzVector Lepton = iEntryExtras -> GetGoodLepton();

    std::cout << "Hier 6 " <<  jets_to_cut_on << " " << tags_to_cut_on << " " << iEntryExtras->GetNLargeJets() << " " << iEntryExtras->GetNTopTags() << std::endl;

    if(!passJetAndBTagBin(jets_to_cut_on, tags_to_cut_on, iEntryExtras->GetNLargeJets(), iEntryExtras->GetNTopTags(), iEntryExtras->GetNHiggsTags(),fJetBin, fBTagBin, fLargeJetBin, fTopTagBin, fHiggsTagBin))
      continue;

        std::cout << "Hier 7" << std::endl;


    if(passJetAndBTagBin(iEntryExtras->GetNJetsOutAllT(), iEntryExtras->GetNBTagsOutAllT(), iEntryExtras->GetNLargeJets(), iEntryExtras->GetNTopTags(), iEntryExtras->GetNHiggsTags(), fVetoJetBin, fVetoBTagBin, fVetoLargeJetBin, fVetoTopTagBin, fHiggsTagBin))
      continue;

    TestWeight += FinalWeight;

        std::cout << "Hier 8    " << FinalWeight << "\t" << TestWeight << std::endl;


    if(jets_all == 4 && tags_all == 2) Pass_4JE2BE = 1;
    else Pass_4JE2BE = 0;
    if(jets_all == 4 && tags_all == 3) Pass_4JE3BE = 1;
    else Pass_4JE3BE = 0;
    if(jets_all == 4 && tags_all >= 4) Pass_4JE4BI = 1;
    else Pass_4JE4BI = 0;
    if(jets_all == 5 && tags_all == 2) Pass_5JE2BE = 1;
    else Pass_5JE2BE = 0;
    if(jets_all == 5 && tags_all == 3) Pass_5JE3BE = 1;
    else Pass_5JE3BE = 0;
    if(jets_all == 5 && tags_all >= 4) Pass_5JE4BI = 1;
    else Pass_5JE4BI = 0;
    if(jets_all >= 6 && tags_all == 2) Pass_6JI2BE = 1;
    else Pass_6JI2BE = 0;
    if(jets_all >= 6 && tags_all == 3) Pass_6JI3BE = 1;
    else Pass_6JI3BE = 0;
    if(jets_all >= 6 && tags_all >= 4) Pass_6JI4BI = 1;
    else Pass_6JI4BI = 0;

    double fakesWeight = 1.0;

    //std::cout << fSampleType << "\t" << fChannel << "\t" << fFlatTree -> boosted_ejets_2016 << "\t" << fFlatTree -> boosted_mujets_2016 << std::endl;

    if(fSampleType == "QCD_Nedaa"){
      
      //      if(fChannel == "boosted_ejets_2015"   && fFlatTree  -> boosted_ejets_2015  == 0)
      
      if(fChannel == "boosted_emujets_2015" && fFlatTree -> boosted_ejets_2015_Loose) 
	fakesWeight = fFlatTree -> fakesMM_weight_ejets_2015_Loose_test2015config5;
      if(fChannel == "boosted_emujets_2015" && fFlatTree -> boosted_mujets_2015_Loose)
        fakesWeight = fFlatTree -> fakesMM_weight_mujets_2015_Loose_test2015config5;
      if(fChannel == "boosted_ejets_2016" && fFlatTree -> boosted_ejets_2016_Loose)
        fakesWeight = fFlatTree -> weight_mm_ejets;
      if(fChannel == "boosted_mujets_2016" && fFlatTree -> boosted_mujets_2016_Loose)
        fakesWeight = fFlatTree -> weight_mm_mujets;
      if(fChannel == "boosted_emujets_2016" && fFlatTree -> boosted_mujets_2016_Loose)
        fakesWeight = fFlatTree -> fakesMM_weight_mujets_2016_Loose_test2016config5;
      if(fChannel == "emujets_2015" && fFlatTree -> ejets_2015_Loose)
        fakesWeight = fFlatTree -> fakesMM_weight_ejets_2015_Loose_test2015config5;
      if(fChannel == "emujets_2015" && fFlatTree -> mujets_2015_Loose)
        fakesWeight = fFlatTree -> fakesMM_weight_mujets_2015_Loose_test2015config5;
      if(fChannel == "emujets_2016" && fFlatTree -> ejets_2016_Loose)
        fakesWeight = fFlatTree -> fakesMM_weight_ejets_2016_Loose_test2016config5;
      if(fChannel == "emujets_2016" && fFlatTree -> mujets_2016_Loose)
	fakesWeight = fFlatTree -> fakesMM_weight_mujets_2016_Loose_test2016config5;
      if(fChannel == "ejets_2016" && fFlatTree -> ejets_2016_Loose)
        fakesWeight = fFlatTree -> weight_mm_ejets;
      if(fChannel == "mujets_2016" && fFlatTree -> mujets_2016_Loose)
	fakesWeight = fFlatTree -> weight_mm_mujets;

      double prescale = 1.0;

      if(fChannel == "boosted_ejets_2016" )
	prescale = fFlatTree -> PS_HLT_e24_lhmedium_nod0_L1EM18VH;
      if(fChannel == "boosted_ejets_2016" )
	prescale = fFlatTree -> PS_HLT_mu24;
      
      
      if(prescale <1){
	std::cout << "prescale " << prescale << std::endl;
	prescale = 1.0;
      }
      
      std::cout << "prescale " << prescale; //commentout this line 
      FinalWeight = fakesWeight*prescale;

      std::cout << " FinalWeight " << FinalWeight << std::endl; //commentout this line 
      
    }

        std::cout << "Hier 10       " << FinalWeight << std::endl;
    
	iEntryExtras -> SetEntryWeight(FinalWeight);

    FillCoreHistos(iEntryExtras);

    for(unsigned int iSet = 0; iSet<fSetList.size(); iSet++){
      if(fSetList[iSet]=="CoreBoostedHistos") FillCoreBoostedHistos(iEntryExtras, fSampleType, fTreeName);
      if(fAnalysisType=="ttH"){
	if(fSetList[iSet]=="BoostedTruthHistos")  FillBoostedTruthHistos(iEntryExtras, fSampleType, fTreeName);
      }
      if(fSetList[iSet]=="SmallJetsAll"){
	FillSmallJetsAllHistos(iEntryExtras);
      }
      if(fSetList[iSet]=="SmallJetsAdditional") FillSmallJetsAdditionalHistos(iEntryExtras);
      if(fSetList[iSet]=="MVAResolved"){
	FillMVAResolvedHistos(iEntryExtras);
      }
      if(fSetList[iSet]=="MVABoosted")          FillMVABoostedHistos(iEntryExtras);
      if(fSetList[iSet]=="TTHExtra"){
      FillTTHExtraHistos(iEntryExtras);
      }
      if(fSetList[iSet]=="Weights" && fTreeName == "nominal_Loose"){
	FillWeights(iEntryExtras);
      }
    }

    if(fChannel.find("boosted")!=std::string::npos || (fMVATreesForResolved && isResolvedSignalRegion)) t2.Fill();
    else t3.Fill();
  }

  if(fChannel.find("boosted")!=std::string::npos || (fMVATreesForResolved && isResolvedSignalRegion)) t2.Write();
  else t3.Write();
  
  fOutTreeFile -> Close();
  
  std::cout << "Write to file: " << fOutputFile.c_str() << std::endl;  

  TFile *fOutFile = TFile::Open(fOutputFile.c_str(), "RECREATE");

  for(unsigned int iHist = 0; iHist < fHistoVector.size(); ++iHist){   
    fHistoVector[iHist] = AddOverflow(fHistoVector[iHist]);
    fHistoVector[iHist].Write(fHistoName[iHist].c_str());
    if(fVariables[iHist] == "met_met")
      std::cout << std::setprecision(8) << fHistoVector[iHist].Integral() << "\t" << fHistoName[iHist].c_str() << std::endl;    
  }
  for(unsigned int iHist = 0; iHist < fCutflowHistoVector.size(); ++iHist){
    fCutflowHistoVector[iHist].Write();
  }
  for (unsigned int iHist2D =0; iHist2D < fHistoVector_2D.size(); ++iHist2D){
    fHistoVector_2D[iHist2D].Write(fHistoName_2D[iHist2D].c_str());
  }

  if(fNtupleCutflowHisto_e.Integral()==0)  fNtupleCutflowHisto_e  = TH2D("NtupleCutflow_ejets", "NtupleCutflow_ejets",  1, 0, 1, 1, 0, 1);
  if(fNtupleCutflowHisto_mu.Integral()==0) fNtupleCutflowHisto_mu = TH2D("NtupleCutflow_mujets","NtupleCutflow_mujets", 1, 0, 1, 1, 0, 1);

  fNtupleCutflowHisto_e.Write();
  fNtupleCutflowHisto_mu.Write();
  
  fOutFile -> Close();

}


float HistoCreator::CalculateLumiWeight(){
  
  SampleXsection tdp;
  const char* const rc = getenv("ROOTCOREBIN");
    std::string filename = std::string(rc) + "/data/TopDataPreparation/XSection-MC15-13TeV.data";
  if (!tdp.readFromFile(filename.c_str())) {
    std::cout << "ERROR:TopDataPreparation - could not read file\n";
    std::cout << filename << "\n";
    exit(1);
  }
  
  float xsection  = tdp.getXsection(fDSID); 
  fSampleXSection = xsection;
  
  float lumi = fTotalEventsMC/(xsection*fLumi);
  float SF   = 1/lumi; 
   
    std::cout << "===================================================================================> " << fSampleXSection << "\t" << fTotalEventsMC << "\t" << fLumi << std::endl;

  return SF;

}


