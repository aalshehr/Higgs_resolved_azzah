#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

void HistoCreator::FillCoreBoostedHistos(EventExtras* iEntryExtrasInfo, string fSampleType, string fTreeName)
{
  
  float histoweight = iEntryExtrasInfo->GetEntryWeight();

  int nGoodLargeR = iEntryExtrasInfo->GetNLargeJets();
  int nJetCount = iEntryExtrasInfo->GetNJetsAdditional();
  int nTopTagCount = iEntryExtrasInfo->GetNTopTags();

  TLorentzVector goodLepton = iEntryExtrasInfo->GetGoodLepton();
  bool isMu = iEntryExtrasInfo->IsMu();
  bool isEl = iEntryExtrasInfo->IsEl();

  std::vector<int> good_largeR;
  good_largeR.clear();
  good_largeR = iEntryExtrasInfo->GetGoodLJetIndex();

  std::vector<int> largeR_T_loose_index;
  largeR_T_loose_index.clear();
  largeR_T_loose_index = iEntryExtrasInfo->GetTopTagJetIndex();
  
  std::vector<TLorentzVector> GoodSmallJets;
  GoodSmallJets.clear();
  GoodSmallJets = iEntryExtrasInfo->GetAdditionalSmallJets();

  std::vector<TLorentzVector> largeR;
  largeR.clear();
  largeR = iEntryExtrasInfo->GetGoodLJets();

  std::vector<TLorentzVector> largeR_T_loose;
  largeR_T_loose.clear();
  largeR_T_loose = iEntryExtrasInfo->GetTopTaggedJets();

  int n_bTagClose;
  n_bTagClose = iEntryExtrasInfo->GetNBTagsInTop();

  int n_nbTagClose;
  n_nbTagClose =iEntryExtrasInfo->GetNJetsInTop();

  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){

    //std::cout << "variable " << fVariables[iVar] << std::endl;

    if(fVariables[iVar] == "bTagClose_n_leading"){
      fHistoVector[iVar].Fill(n_bTagClose, histoweight);
      fTreeVariables[iVar] = n_bTagClose;
    }

    if(fVariables[iVar] == "nbTagClose_n_leading"){
      fHistoVector[iVar].Fill(n_nbTagClose, histoweight);
      fTreeVariables[iVar] = n_nbTagClose;
    }

    if(fVariables[iVar] == "ljet0_pt"){
      fHistoVector[iVar].Fill(fFlatTree -> ljet_pt  -> at(good_largeR[0])/1000.0, histoweight);  
      fTreeVariables[iVar] = fFlatTree -> ljet_pt  -> at(good_largeR[0])/1000.0;
    }
    else if(fVariables[iVar] == "ljet0_eta"){
      fHistoVector[iVar].Fill(fFlatTree -> ljet_eta  -> at(good_largeR[0]), histoweight);
      fTreeVariables[iVar] = fFlatTree -> ljet_eta  -> at(good_largeR[0])/1000.0;
    }
    else if(fVariables[iVar] == "ljet0_phi"){
      fHistoVector[iVar].Fill(fFlatTree -> ljet_phi  -> at(good_largeR[0]), histoweight);
      fTreeVariables[iVar] = fFlatTree -> ljet_phi  -> at(good_largeR[0])/1000.0;
    }
    else if(fVariables[iVar] == "ljet0_m"){
      fHistoVector[iVar].Fill(fFlatTree -> ljet_m  -> at(good_largeR[0])/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> ljet_m  -> at(good_largeR[0])/1000.0;
    }
    else if(fVariables[iVar] == "ljet_pt"){                                                                                                                        
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){                                                                                                             
	fHistoVector[iVar].Fill(fFlatTree -> ljet_pt  -> at(good_largeR[iJet])/1000.0, histoweight);                                                             
      }                                                                                                                                                          
    }
    if( nTopTagCount > 0){
      if(fVariables[iVar] == "ljet_top_pt"){
	for(int iJet = 0; iJet < nTopTagCount; ++iJet){
	  fHistoVector[iVar].Fill(fFlatTree -> ljet_pt -> at(largeR_T_loose_index[iJet])/1000.0, histoweight);
	}
      }
       if(fVariables[iVar] == "ljet_top_eta"){
	for(int iJet = 0; iJet < nTopTagCount; ++iJet){
	  fHistoVector[iVar].Fill(fFlatTree -> ljet_eta -> at(largeR_T_loose_index[iJet]), histoweight);
	}
      } 
       if(fVariables[iVar] == "ljet_top_phi"){
	for(int iJet = 0; iJet < nTopTagCount; ++iJet){
	  fHistoVector[iVar].Fill(fFlatTree -> ljet_phi -> at(largeR_T_loose_index[iJet]), histoweight);
	}
      } 
       if(fVariables[iVar] == "ljet_top_m"){
	for(int iJet = 0; iJet < nTopTagCount; ++iJet){
	  fHistoVector[iVar].Fill(fFlatTree -> ljet_m -> at(largeR_T_loose_index[iJet])/1000.0, histoweight);
	  if(iJet == 0){
	    fTreeVariables[iVar] = fFlatTree -> ljet_m -> at(largeR_T_loose_index[iJet])/1000.0;
	  }
	}
      } 
       if(fVariables[iVar] == "ljet_top0_pt"){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_pt -> at(largeR_T_loose_index[0])/1000.0, histoweight);
	fTreeVariables[iVar] = fFlatTree -> ljet_pt -> at(largeR_T_loose_index[0])/1000.0;
      }
       if(fVariables[iVar] == "ljet_top0_eta"){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_eta -> at(largeR_T_loose_index[0]), histoweight);
	fTreeVariables[iVar] = fFlatTree -> ljet_eta -> at(largeR_T_loose_index[0])/1000.0;
      }
       if(fVariables[iVar] == "ljet_top0_phi"){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_phi -> at(largeR_T_loose_index[0]), histoweight);
	fTreeVariables[iVar] = fFlatTree -> ljet_phi -> at(largeR_T_loose_index[0])/1000.0;
      }
       if(fVariables[iVar] == "ljet_top0_m"){
	float topm = fFlatTree -> ljet_m -> at(largeR_T_loose_index[0])/1000.0; 
	fHistoVector[iVar].Fill(fFlatTree -> ljet_m -> at(largeR_T_loose_index[0])/1000.0, histoweight);
	fTreeVariables[iVar] = topm;
      }
    }
    if(fVariables[iVar] == "ljet_pt_varbin"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_pt  -> at(good_largeR[iJet])/1000.0, histoweight);
      }
    }
    else if(fVariables[iVar] == "ljet_eta"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_eta  -> at(good_largeR[iJet]), histoweight);
      }
    }
    else if(fVariables[iVar] == "ljet_phi"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_phi  -> at(good_largeR[iJet]), histoweight);
      }
    }
    else if(fVariables[iVar] == "ljet_m"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_m  -> at(good_largeR[iJet])/1000.0, histoweight);
	if(iJet == 0){
	  fTreeVariables[iVar] = fFlatTree -> ljet_m  -> at(good_largeR[iJet])/1000.0;
	}
      }
    }
    else if(fVariables[iVar] == "ljet_topTagN"){
      fHistoVector[iVar].Fill(nTopTagCount, histoweight);
      fTreeVariables[iVar] = nTopTagCount;
    }
    else if(fVariables[iVar] == "ljet_sd23"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_sd23  -> at(good_largeR[iJet])/1000.0, histoweight);
	if(iJet == 0){
	  float sd23 = fFlatTree -> ljet_sd23  -> at(good_largeR[0])/1000.0;
	  fTreeVariables[iVar] = sd23;
	}
      }
    }
    else if(fVariables[iVar] == "ljet_sd23_top0"){
      for(int iJet = 0; iJet < nTopTagCount; ++iJet){
        fHistoVector[iVar].Fill(fFlatTree -> ljet_sd23  -> at(largeR_T_loose_index[iJet])/1000.0, histoweight);
        if(iJet == 0){
          float sd23 = fFlatTree -> ljet_sd23  -> at(largeR_T_loose_index[0])/1000.0;
          fTreeVariables[iVar] = sd23;
        }
      }
    }
    else if(fVariables[iVar] == "ljet_tau21"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_tau21_wta  -> at(good_largeR[iJet]), histoweight);
	//fTreeVariables[iVar] = fFlatTree -> ljet_tau21  -> at(good_largeR[iJet]);
      }
    }
    else if(fVariables[iVar] == "ljet_tau32"){
      for(int iJet = 0; iJet < nGoodLargeR; ++iJet){
	fHistoVector[iVar].Fill(fFlatTree -> ljet_tau32_wta  -> at(good_largeR[iJet]), histoweight);
	if(iJet == 0){
	  if((fFlatTree -> ljet_tau32_wta  -> at(good_largeR[0])) < 0){
	    fTreeVariables[iVar] = 0;
	  }
	  else{
	    fTreeVariables[iVar] = fFlatTree -> ljet_tau32_wta  -> at(good_largeR[0]);
	  }
	}
      }
    }
    else if(fVariables[iVar] == "ljet_tau32_top0"){
      for(int iJet = 0; iJet < nTopTagCount; ++iJet){
        fHistoVector[iVar].Fill(fFlatTree -> ljet_tau32_wta  -> at(largeR_T_loose_index[iJet]), histoweight);
        if(iJet == 0){
          if((fFlatTree -> ljet_tau32_wta  -> at(largeR_T_loose_index[0])) < 0){
            fTreeVariables[iVar] = 0;
          }
          else{
            fTreeVariables[iVar] = fFlatTree -> ljet_tau32_wta  -> at(largeR_T_loose_index[0]);
          }
        }
      }
    }
    else if(fVariables[iVar] == "ljet_n"){
      fHistoVector[iVar].Fill(nGoodLargeR, histoweight);
      fTreeVariables[iVar] = nGoodLargeR;
    }
    else if(fVariables[iVar] == "ljet_jet_dR"){
      for(int i = 0; i<nJetCount; i++){
	for(int j = 0; j<nGoodLargeR; j++){
	  double dR = GoodSmallJets[i].DeltaR(largeR[j]);
	  fHistoVector[iVar].Fill(dR, histoweight);
	  fTreeVariables[iVar] = dR;
	}
      }
    }

    if (isMu){
      if(fVariables[iVar] == "dR_ljet_mu"){
	for (int iii = 0; iii<nGoodLargeR; ++iii){
	  double dR = goodLepton.DeltaR(largeR[iii]);
	  fHistoVector[iVar].Fill(dR, histoweight);
	  fTreeVariables[iVar] = dR;
	}
      }
      else if(fVariables[iVar] == "dPhi_ljet_mu"){
	for (int iii = 0; iii<nGoodLargeR; ++iii){
	  double dPhi = goodLepton.DeltaPhi(largeR[iii]);
	  fHistoVector[iVar].Fill(dPhi, histoweight);
	  fTreeVariables[iVar] = dPhi;
	}
      }
    }
    if(isEl){
      if(fVariables[iVar] == "dR_ljet_el"){
	for (int iii = 0; iii<nGoodLargeR; ++iii){
	  double dR = goodLepton.DeltaR(largeR[iii]);
	  fHistoVector[iVar].Fill(dR, histoweight);
	  fTreeVariables[iVar] = dR;
	}
      }
      
      else if(fVariables[iVar] == "dPhi_ljet_el"){
	for (int iii = 0; iii<nGoodLargeR; ++iii){
	  double dPhi = goodLepton.DeltaPhi(largeR[iii]);
	  fHistoVector[iVar].Fill(dPhi, histoweight);
	  fTreeVariables[iVar] = dPhi;
	}
      }
    }




  }
  
}


