#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

void HistoCreator::FillCoreHistos(EventExtras* iEntryExtrasInfo)
{
  
  float histoweight = iEntryExtrasInfo->GetEntryWeight();
  TLorentzVector goodLepton = iEntryExtrasInfo->GetGoodLepton();

  double met = fFlatTree -> met_met/1000.0;
  double dPhi = goodLepton.Phi() - fFlatTree -> met_phi;
  if (dPhi > 3.142) dPhi = 6.28 - dPhi;
  if (dPhi < - 3.142) dPhi = -6.28 - dPhi;
  double dPhiAbs = fabs(dPhi);
  double mtw = sqrt(2*goodLepton.Pt()*met*(1-cos(dPhi)));

  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){

    int mu_n = fFlatTree -> mu_pt -> size();
    int el_n = fFlatTree -> el_pt -> size();
 
    if (fVariables[iVar] == "nEvents")
      fHistoVector[iVar].Fill(1, histoweight);
    
    if(fVariables[iVar] == "nBTags"){    
      fHistoVector[iVar].Fill(iEntryExtrasInfo->GetNBTagsAll(), histoweight);
      fTreeVariables[iVar] = iEntryExtrasInfo->GetNBTagsAll();
    }
    else if(fVariables[iVar] == "jet_n"){
      //std::cout << "n jet in core " << iEntryExtrasInfo->GetNJetsAll() << std::endl;
      fHistoVector[iVar].Fill(iEntryExtrasInfo->GetNJetsAll(), histoweight);
      fTreeVariables[iVar] = iEntryExtrasInfo->GetNJetsAll();
    }
    else if(fVariables[iVar] == "met_met"){
      fHistoVector[iVar].Fill(fFlatTree -> met_met/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> met_met/1000.0;
    }
    else if(fVariables[iVar] == "met_phi"){
      fHistoVector[iVar].Fill(fFlatTree -> met_phi, histoweight);
      fTreeVariables[iVar] = fFlatTree -> met_phi;
    }
    else if(fVariables[iVar] == "mtw") {
      fHistoVector[iVar].Fill(mtw, histoweight);
      fTreeVariables[iVar] = mtw;
    }
    else if(fVariables[iVar] == "mtw_extended") {

      fHistoVector[iVar].Fill(mtw, histoweight);
      fTreeVariables[iVar] = mtw;
    } 
    else if(fVariables[iVar] == "met_plus_mtw") {

      fHistoVector[iVar].Fill(met+mtw, histoweight);
      fTreeVariables[iVar] = met+mtw;
    }
    else if(fVariables[iVar] == "dphi_lep_met"){
      fHistoVector[iVar].Fill(dPhi, histoweight);
      fTreeVariables[iVar] = dPhi;
    }
    else if(fVariables[iVar] == "dphi_lep_met_abs"){
      fHistoVector[iVar].Fill(dPhiAbs, histoweight);
      fTreeVariables[iVar] = dPhiAbs;
    }
    else if(fVariables[iVar] == "mu_n"){

      float lepPtCut = iEntryExtrasInfo -> GetLeptonPtCut();

      int counter = 0;

      for(int iMu = 0; iMu < mu_n; ++iMu){
	
	if(fFlatTree -> mu_pt -> at(iMu)/1000.0 > lepPtCut)
	  counter++;

      }

      fHistoVector[iVar].Fill(counter, histoweight);
      fTreeVariables[iVar] = counter;
    }
    else if(fVariables[iVar] == "el_n"){

      float lepPtCut = iEntryExtrasInfo -> GetLeptonPtCut();

      int counter = 0;

      for(int iEl = 0; iEl < el_n; ++iEl){
	if(fFlatTree -> el_pt -> at(iEl)/1000.0 > lepPtCut)
	  counter++;
      }

      fHistoVector[iVar].Fill(counter, histoweight);
      fTreeVariables[iVar] = counter;

    }

    float lepPtCut = iEntryExtrasInfo -> GetLeptonPtCut();

    for(int iMu = 0; iMu < mu_n; ++iMu){
      if(fVariables[iVar] == "mu_pt"){
	if(fFlatTree -> mu_pt -> at(iMu)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> mu_pt -> at(iMu)/1000.0, histoweight);
	  if(iMu == 0) fTreeVariables[iVar] = fFlatTree -> mu_pt -> at(iMu)/1000.0;
	}
      }
      else if(fVariables[iVar] == "mu_eta"){
	if(fFlatTree -> mu_pt -> at(iMu)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> mu_eta -> at(iMu), histoweight);
	  if(iMu == 0) fTreeVariables[iVar] = fFlatTree -> mu_eta -> at(iMu);
	}
      }
      else if(fVariables[iVar] == "mu_phi"){
	if(fFlatTree -> mu_pt -> at(iMu)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> mu_phi -> at(iMu), histoweight);
	  if(iMu == 0) fTreeVariables[iVar] = fFlatTree -> mu_phi -> at(iMu);
	}
      }
      else if(fVariables[iVar] == "mu_charge"){
	if(fFlatTree -> mu_pt -> at(iMu)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> mu_charge -> at(iMu), histoweight);
	  if(iMu == 0) fTreeVariables[iVar] = fFlatTree -> mu_charge -> at(iMu);
	}
      }
    }
    for(int iEl = 0; iEl < el_n; ++iEl){
      if(fVariables[iVar] == "el_pt"){
	if(fFlatTree -> el_pt -> at(iEl)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> el_pt -> at(iEl)/1000.0, histoweight);
	  if(iEl == 0) fTreeVariables[iVar] = fFlatTree -> el_pt -> at(iEl)/1000.0;
	}
      }
      else if(fVariables[iVar] == "el_eta"){
	if(fFlatTree -> el_pt -> at(iEl)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> el_eta -> at(iEl), histoweight);
	  if(iEl == 0) fTreeVariables[iVar] = fFlatTree -> el_eta -> at(iEl);
	}
      }
      else if(fVariables[iVar] == "el_phi"){
	if(fFlatTree -> el_pt -> at(iEl)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> el_phi -> at(iEl), histoweight);
	  if(iEl == 0) fTreeVariables[iVar] = fFlatTree -> el_phi -> at(iEl);
	}
      }
      else if(fVariables[iVar] == "el_charge"){
	if(fFlatTree -> el_pt -> at(iEl)/1000.0 > lepPtCut){
	  fHistoVector[iVar].Fill(fFlatTree -> el_charge -> at(iEl), histoweight);
	  if(iEl == 0) fTreeVariables[iVar] = fFlatTree -> el_charge -> at(iEl);
	}
      }
    }  
    
  }
}


