#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

void HistoCreator::FillMVAResolvedHistos(EventExtras* iEntryExtrasInfo)
{
  
  float histoweight = iEntryExtrasInfo->GetEntryWeight();

  std::vector<TLorentzVector> largeR;
  largeR.clear();
  largeR = iEntryExtrasInfo->GetGoodLJets();

  std::vector<TLorentzVector> smallR;
  smallR.clear();
  smallR = iEntryExtrasInfo->GetGoodSmallJets();

  std::vector<TLorentzVector> smallR_B;
  smallR_B.clear();
  smallR_B = iEntryExtrasInfo->GetGoodSmallTags();
 


  // Loop over bb pairs
  int nhiggs30 = 0;
  float dRbbl_max = 0.0; 
  float dRbbl_min = 999999999.;
  float dRbbl_temp = 0.;
  float dMbb = 999999.;
  float Mbb = 0.;
  float dRbb = 0.;
  float Mbb_temp = 0.;
  float Max_Mbb = 0.;
  unsigned int first_b = 0;
  unsigned int second_b = 0;
  unsigned int bmaxeta1 = 0;
  unsigned int bmaxeta2 = 0;
  float dEta_bb = 0.;
  float maxdEta_temp = 0.;
  TLorentzVector bbSum;
  float b_maxM = 0.;
  float SMdrop_temp = 0.;
  float SMdrop_bb = 99999.;
  unsigned int bjet_min1 = 0;
  unsigned int bjet_min2 = 0;
  for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
    for(unsigned int bsmalls_2 = bsmalls+1; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
      bbSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
      if(fabs(bbSum.M() - 125.) < 30.) nhiggs30++;
      if(fabs((bbSum.M()) - 125.) < dMbb) {
        dMbb = fabs((bbSum.M()) - 125.);
        Mbb = bbSum.M();
        if(smallR_B[bsmalls].Pt() > smallR_B[bsmalls_2].Pt()){
          first_b  = bsmalls;
          second_b = bsmalls_2;
        }
        else{
          second_b = bsmalls;
          first_b  = bsmalls_2;
        }
      }
      dRbbl_temp = (iEntryExtrasInfo->GetGoodLepton()).DeltaR(bbSum);
      if(dRbbl_temp > dRbbl_max)
        dRbbl_max = dRbbl_temp;
      if(dRbbl_temp < dRbbl_min)
        dRbbl_min = dRbbl_temp;
      Mbb_temp = bbSum.M();
      if(Mbb_temp > Max_Mbb) {
        Max_Mbb = Mbb_temp;
        dRbb = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
      }
      maxdEta_temp = fabs((smallR_B[bsmalls].Eta()) - (smallR_B[bsmalls_2].Eta()));
      if(maxdEta_temp > dEta_bb){
        dEta_bb = maxdEta_temp;
        bmaxeta1 = bsmalls;
        bmaxeta2 = bsmalls_2;
      }
      //calculate mass drop value of the two jets
      b_maxM = (smallR_B[bsmalls].M() > smallR_B[bsmalls_2].M()) ? smallR_B[bsmalls].M() : smallR_B[bsmalls_2].M();
      dRbb = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
      SMdrop_temp = dRbb*b_maxM/bbSum.M();
      if(SMdrop_temp < SMdrop_bb){
        SMdrop_bb = SMdrop_temp;
        bjet_min1 = bsmalls;  //indexes of the two bjets with the smallest mass drop
        bjet_min2 = bsmalls_2;
      }
    }
  }

  //Loop over lj pairs
  float dRjl_max = 0.;
  float dRjl_temp = 0.;
  for(unsigned int jsmalls = 0; jsmalls < smallR.size(); ++jsmalls){
    dRjl_temp = (iEntryExtrasInfo->GetGoodLepton()).DeltaR(smallR[jsmalls]);
    if(dRjl_temp > dRjl_max){
      dRjl_max = dRjl_temp;
    }
  }

  //Loop over jj pairs  
  float dEtajj = 0.;
  float maxdEtajj_temp = 0.;
  float dMjj = 999999.;
  float Mjj = 0.;
  float Mjj_temp = 0.;
  float Mjj_Min = 999999.;
  unsigned int first_j = 0;
  unsigned int second_j = 0;
  float j_maxM;
  float SMdrop_jj = 99999.;
  float dRjj;
  unsigned int jet_min1 = 0;
  unsigned int jet_min2 = 0;
  float Mjj_Max = -1;
  TLorentzVector jjSum;
  //std::cout << "NEW EVENT" << std::endl;
  for(unsigned int jsmalls = 0; jsmalls < smallR.size(); ++jsmalls){
    for(unsigned int jsmalls_2 = jsmalls+1; jsmalls_2 < smallR.size(); ++jsmalls_2){
	maxdEtajj_temp = fabs((smallR[jsmalls].Eta()) - (smallR[jsmalls_2].Eta()));
	if(maxdEtajj_temp > dEtajj) {
	  dEtajj = maxdEtajj_temp;
	}
	jjSum = smallR[jsmalls] + smallR[jsmalls_2];
	if(fabs((jjSum.M()) - 125.) < dMjj) {
          first_j = jsmalls;
          second_j = jsmalls_2;
	  dMjj = fabs((jjSum.M()) - 125.);
	  Mjj =jjSum.M();
	}
	Mjj_temp = jjSum.M();
	if (Mjj_temp < Mjj_Min) {
          Mjj_Min = Mjj_temp;
	}
        if (Mjj_temp > Mjj_Max) {
          Mjj_Max = Mjj_temp;
	}

        j_maxM = (smallR[jsmalls].M() > smallR[jsmalls_2].M()) ? smallR[jsmalls].M() : smallR[jsmalls_2].M();
        dRjj = smallR[jsmalls].DeltaR(smallR[jsmalls_2]);
        SMdrop_temp = dRjj*j_maxM/jjSum.M();
        if(SMdrop_temp < SMdrop_jj){
          SMdrop_jj = SMdrop_temp;
          jet_min1 = jsmalls; // indexes of the two jets with the smallest mass drop.
          jet_min2 = jsmalls_2;
        }
    }
  }

  //Loop over jb pairs
  float dMjb = 999999.;
  float dEta_jb = 0.;
  float dR_jb   = 0.;
  float maxdR_temp = 0.;
  float Mjb = 0.;
  int first_jb  = 0;
  int second_jb = 0;
  TLorentzVector jbSum;

  for(unsigned int jsmalls = 0; jsmalls < smallR.size(); ++jsmalls){
    for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
      if(smallR[jsmalls] == smallR_B[bsmalls]){
        continue;
      }
      jbSum = smallR[jsmalls] + smallR_B[bsmalls];
      if(fabs((jbSum.M()) - 125.) < dMjb) {
	first_jb = jsmalls;
	second_jb = bsmalls;
	dMjb = fabs((jbSum.M()) - 125.);
	Mjb = jbSum.M();
      }
      maxdEta_temp = fabs((smallR[jsmalls].Eta()) - (smallR_B[bsmalls].Eta()));
      maxdR_temp   = smallR[jsmalls].DeltaR(smallR_B[bsmalls]);
      if(maxdEta_temp > dEta_jb)
        dEta_jb = maxdEta_temp;
      if(maxdR_temp > dR_jb)
        dR_jb = maxdR_temp;
    }
  }


  //Loop over untagged pairs
  float dEta_uu = 0.;
  float dEta_ub = 0.;
  for(unsigned int jsmalls = 0; jsmalls < smallR.size(); ++jsmalls){
    for(unsigned int jsmalls_2 = jsmalls+1; jsmalls_2 < smallR.size(); ++jsmalls_2){
      for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
        if(smallR[jsmalls] != smallR_B[bsmalls] && (bsmalls == first_b || bsmalls == second_b)) {
          maxdEta_temp = fabs((smallR[jsmalls].Eta()) - (smallR_B[bsmalls].Eta()));
          if(maxdEta_temp > dEta_ub)
            dEta_ub = maxdEta_temp;
        }
        if(smallR[jsmalls] == smallR_B[bsmalls] || smallR[jsmalls_2] == smallR_B[bsmalls])
          continue;
        maxdEta_temp = fabs((smallR[jsmalls].Eta()) - (smallR[jsmalls_2].Eta()));
        if(maxdEta_temp > dEta_uu)
          dEta_uu = maxdEta_temp;
      }
    }
  }
  
  // calculate mass drop for jets with mass closest to 125GeV
  j_maxM = (smallR[first_j].M() > smallR[second_j].M()) ? smallR[first_j].M() : smallR[second_j].M();
  jjSum = smallR[first_j] + smallR[second_j];
  float SMdrop_jj_Higgs = smallR[first_j].DeltaR(smallR[second_j])*j_maxM/jjSum.M();

  j_maxM = (smallR[jet_min1].M() > smallR[jet_min2].M()) ? smallR[jet_min1].M() : smallR[jet_min2].M();
  jjSum = smallR[jet_min1] + smallR[jet_min2];

  // calculate mass drop for bjets with mass closest to 125GeV
  b_maxM = (smallR_B[first_b].M() > smallR_B[second_b].M()) ? smallR_B[first_b].M() : smallR_B[second_b].M();
  bbSum = smallR_B[first_b] + smallR_B[second_b];
  float SMdrop_bb_Higgs = smallR_B[first_b].DeltaR(smallR_B[second_b])*b_maxM/bbSum.M();
  
  TLorentzVector bbSum_maxpT;
  float b_maxM_maxpT = 0.;
  float SMdrop_bb_maxpT = 0.;

  if(smallR_B.size() >1){
    // mass drop for two b's with largest pT
    bbSum_maxpT = smallR_B[0] + smallR_B[1];
    b_maxM_maxpT = (smallR_B[0].M() > smallR_B[1].M()) ? smallR_B[0].M() : smallR_B[1].M();
    SMdrop_bb_maxpT = smallR_B[0].DeltaR(smallR_B[1])*b_maxM_maxpT/bbSum_maxpT.M(); 
  }
  // mass drop for two b's with biggest separation in pseudorapidity
  TLorentzVector bbSum_maxdEta = smallR_B[bmaxeta1] + smallR_B[bmaxeta2];
  float b_maxM_maxdEta = (smallR_B[bmaxeta1].M() > smallR_B[bmaxeta2].M()) ? smallR_B[bmaxeta1].M() : smallR_B[bmaxeta2].M();
  float SMdrop_bb_maxdEta = smallR_B[bmaxeta1].DeltaR(smallR_B[bmaxeta2])*b_maxM_maxdEta/bbSum_maxdEta.M(); 
  
  
  // 2D plots
  for(unsigned iVar_2D = 0; iVar_2D < fVar_2D.size(); ++iVar_2D){
    // only positive weights
    if(fVariables_2D[iVar_2D] == "dRbb_m12_pos"){
      if(histoweight > 0){
        fHistoVector_2D[iVar_2D].Fill(b_maxM/bbSum.M(), smallR_B[first_b].DeltaR(smallR_B[second_b]), histoweight);
      }
    }
    // all weights included
    if(fVariables_2D[iVar_2D] == "dRbb_m12_all"){
      fHistoVector_2D[iVar_2D].Fill(b_maxM/bbSum.M(), smallR_B[first_b].DeltaR(smallR_B[second_b]), 1);
    }
    //only positive weights, minimum mass drop
    if(fVariables_2D[iVar_2D] == "dRjj_m12_min"){
      if(histoweight > 0)
        fHistoVector_2D[iVar_2D].Fill(j_maxM/jjSum.M(), smallR[jet_min1].DeltaR(smallR[jet_min2]), histoweight);
    }
  }

  // 1D plots
  // redifining this as sum of two b's with smallest massdrop
  bbSum = smallR_B[bjet_min1] + smallR_B[bjet_min2];
  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){

    /* if(fVariables[iVar] == "klfitter_model_Higgs_b1_pt"){
      fHistoVector[iVar].Fill(fFlatTree -> klfitter_model_Higgs_b1_pt, histoweight);
      fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b1_pt;
      }*/
    
    if(fVariables[iVar] == "Centrality"){
      fHistoVector[iVar].Fill(fFlatTree -> Centrality_all, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Centrality_all;
    }
    if(fVariables[iVar] == "Mbb_MindR_77"){
      fHistoVector[iVar].Fill(fFlatTree -> Mbb_MindR_77/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mbb_MindR_77/1000.0;
    }
    if(fVariables[iVar] == "dRbb_MaxPt_77"){
      fHistoVector[iVar].Fill(fFlatTree -> dRbb_MaxPt_77, histoweight);
      fTreeVariables[iVar] = fFlatTree -> dRbb_MaxPt_77;
    }
    if(fVariables[iVar] == "Mjj_MaxPt"){
      fHistoVector[iVar].Fill(fFlatTree -> Mjj_MaxPt/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mjj_MaxPt/1000.0;
    }
    if(fVariables[iVar] == "pT_jet5"){
      fHistoVector[iVar].Fill(fFlatTree -> pT_jet5/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> pT_jet5/1000.0;
    }
    if(fVariables[iVar] == "H1_all"){
      fHistoVector[iVar].Fill(fFlatTree -> H1_all, histoweight);
      fTreeVariables[iVar] = fFlatTree -> H1_all;
    }
    if(fVariables[iVar] == "dRbb_avg_77"){
      fHistoVector[iVar].Fill(fFlatTree -> dRbb_avg_77, histoweight);
      fTreeVariables[iVar] = fFlatTree -> dRbb_avg_77;
    }
    if(fVariables[iVar] == "Mbj_MaxPt_77"){
      fHistoVector[iVar].Fill(fFlatTree -> Mbj_MaxPt_77/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mbj_MaxPt_77/1000.0;
    }
    if(fVariables[iVar] == "dRlepbb_MindR_77"){
      fHistoVector[iVar].Fill(fFlatTree -> dRlepbb_MindR_77, histoweight);
      fTreeVariables[iVar] = fFlatTree -> dRlepbb_MindR_77;
    }
    if(fVariables[iVar] == "Muu_MindR"){
      fHistoVector[iVar].Fill(fFlatTree -> Muu_MindR/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Muu_MindR/1000.0;
    }
    if(fVariables[iVar] == "Aplan_bjets"){
      fHistoVector[iVar].Fill(fFlatTree -> Aplan_bjets, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Aplan_bjets;
    }
    if(fVariables[iVar] == "nJets_Pt40"){
      fHistoVector[iVar].Fill(fFlatTree -> nJets_Pt40, histoweight);
      fTreeVariables[iVar] = fFlatTree -> nJets_Pt40;
    }
    if(fVariables[iVar] == "Mbj_MindR"){
      fHistoVector[iVar].Fill(fFlatTree -> Mbj_MindR/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mbj_MindR/1000.0;
    }
    if(fVariables[iVar] == "HhadT_jets"){
      fHistoVector[iVar].Fill(fFlatTree -> HT_jets/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> HT_jets/1000.0;
    }
    if(fVariables[iVar] == "Mjj_MindR"){
      fHistoVector[iVar].Fill(fFlatTree -> Mjj_MindR/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mjj_MindR/1000.0;
    }
    if(fVariables[iVar] == "Mbb_MaxPt"){
      fHistoVector[iVar].Fill(fFlatTree -> Mbb_MaxPt/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mbb_MaxPt/1000.0;
    }
    if(fVariables[iVar] == "pTuu_MindR"){
      fHistoVector[iVar].Fill(fFlatTree -> pTuu_MindR/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> pTuu_MindR/1000.0;
    }
    if(fVariables[iVar] == "Mbb_MaxM"){
      fHistoVector[iVar].Fill(fFlatTree -> Mbb_MaxM/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mbb_MaxM/1000.0;
    }
    if(fVariables[iVar] == "dRuu_MindR"){
      fHistoVector[iVar].Fill(fFlatTree -> dRuu_MindR, histoweight);
      fTreeVariables[iVar] = fFlatTree -> dRuu_MindR;
    }
    if(fVariables[iVar] == "Mjjj_MaxPt"){
      fHistoVector[iVar].Fill(fFlatTree -> Mjjj_MaxPt/1000.0, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Mjjj_MaxPt/1000.0;
    }
    if(fVariables[iVar] == "dEtajj_MaxdEta"){
      fHistoVector[iVar].Fill(dEtajj, histoweight);
      fTreeVariables[iVar] = dEtajj;
    }
    /* if(fVariables[iVar] == "nHiggsbb30_77"){
      fHistoVector[iVar].Fill(nHiggsbb30_77, histoweight);
      fTreeVariables[iVar] = nHiggsbb30_77;
      }*/
    if(fVariables[iVar] == "Aplanarity_jets"){
      fHistoVector[iVar].Fill(fFlatTree -> Aplanarity_jets, histoweight);
      fTreeVariables[iVar] = fFlatTree -> Aplanarity_jets;
    }
    if(fVariables[iVar] == "Mjj_MinM"){
      fHistoVector[iVar].Fill(Mjj_Min, histoweight);
      fTreeVariables[iVar] = Mjj_Min;
    }
    if(fVariables[iVar] == "dRHl_MaxdR"){
      fHistoVector[iVar].Fill(dRbbl_max, histoweight);
      fTreeVariables[iVar] = dRbbl_max;
    }
    if(fVariables[iVar] == "dRHl_MindR"){
      fHistoVector[iVar].Fill(dRbbl_min, histoweight);
      fTreeVariables[iVar] = dRbbl_min;
    }
    if(fVariables[iVar] == "Mjj_HiggsMass"){
      fHistoVector[iVar].Fill(Mjj, histoweight);
      fTreeVariables[iVar] = Mjj;
    }
    if(fVariables[iVar] == "Mbb_HiggsMass"){
      fHistoVector[iVar].Fill(Mbb, histoweight);
      fTreeVariables[iVar] = Mbb;
    }
    if(fVariables[iVar] == "Mjb_HiggsMass"){
      fHistoVector[iVar].Fill(Mjb, histoweight);
      fTreeVariables[iVar] = Mjb;
    }
    if(fVariables[iVar] == "HT_all"){
      float HT_all = 0.;
      for(unsigned int jsmall = 0; jsmall < smallR.size(); ++jsmall){
        HT_all = HT_all + (smallR[jsmall].Perp());
      }
      HT_all = HT_all + (iEntryExtrasInfo->GetGoodLepton()).Pt();
      fHistoVector[iVar].Fill(HT_all, histoweight);
      fTreeVariables[iVar] = HT_all;
    }
    if(fVariables[iVar] == "dRbb_MaxM"){
      fHistoVector[iVar].Fill(dRbb, histoweight);
      fTreeVariables[iVar] = dRbb;
    }
    if(fVariables[iVar] == "dRlj_MaxdR"){
      fHistoVector[iVar].Fill(dRjl_max, histoweight);
      fTreeVariables[iVar] = dRjl_max;
    }
    if(fVariables[iVar] == "H4_all"){
      fHistoVector[iVar].Fill(fFlatTree -> H4_all, histoweight);
      fTreeVariables[iVar] = fFlatTree -> H4_all;
    }
    if(fVariables[iVar] == "dEtabb_MaxdEta"){
      fHistoVector[iVar].Fill(dEta_bb, histoweight);
      fTreeVariables[iVar] = dEta_bb;
    }
    if(fVariables[iVar] == "dRjb_MaxdR"){
      fHistoVector[iVar].Fill(dR_jb, histoweight);
      fTreeVariables[iVar] = dR_jb;
    }
    if(fVariables[iVar] == "dRbb_MHiggs"){
      float dRbb_MHiggs = 0.;
      dRbb_MHiggs = smallR_B[first_b].DeltaR(smallR_B[second_b]);
      fHistoVector[iVar].Fill(dRbb_MHiggs, histoweight);
      fTreeVariables[iVar] = dRbb_MHiggs;
    }
    float dEtabb_MHiggs = 0.;
    dEtabb_MHiggs = abs(smallR_B[first_b].Eta() - smallR_B[second_b].Eta());
    if(fVariables[iVar] == "dEtabb_MHiggs"){
      fHistoVector[iVar].Fill(dEtabb_MHiggs, histoweight);
      fTreeVariables[iVar] = dEtabb_MHiggs;
    }
    float dEtajb_MHiggs = 0.;
    dEtajb_MHiggs = smallR[first_jb].Eta() - smallR_B[second_jb].Eta();
    if(fVariables[iVar] == "dEtajb_MHiggs"){
      fHistoVector[iVar].Fill(dEtajb_MHiggs, histoweight);
      fTreeVariables[iVar] = dEtajb_MHiggs;
    }
    if(fVariables[iVar] == "SMdrop_jj_Higgs"){
      fHistoVector[iVar].Fill(SMdrop_jj_Higgs, histoweight);
      fTreeVariables[iVar] = SMdrop_jj_Higgs;
    }
    if(fVariables[iVar] == "SMdrop_bb_Higgs"){
      fHistoVector[iVar].Fill(SMdrop_bb_Higgs, histoweight);
      fTreeVariables[iVar] = SMdrop_bb_Higgs;
    }
    if(fVariables[iVar] == "SMdrop_bb_maxpT"){
      fHistoVector[iVar].Fill(SMdrop_bb_maxpT, histoweight);
      fTreeVariables[iVar] = SMdrop_bb_maxpT;
    }
    if(fVariables[iVar] == "SMdrop_bb_maxdEta"){
      fHistoVector[iVar].Fill(SMdrop_bb_maxdEta, histoweight);
      fTreeVariables[iVar] = SMdrop_bb_maxdEta;
    }


    if(fVariables[iVar] == "Acoplanarity"){
      float Acoplanarity =
            fabs(3.14 - fabs(smallR_B[first_b].Phi() - smallR_B[second_b].Phi()))+fabs(3.14 - smallR_B[first_b].Theta() - smallR_B[second_b].Theta());
      fHistoVector[iVar].Fill(Acoplanarity, histoweight);
      fTreeVariables[iVar] = Acoplanarity;
    }
    if(fVariables[iVar] == "pT_bb"){
      float pT_bb = smallR_B[first_b].Pt()+smallR_B[second_b].Pt();
      fHistoVector[iVar].Fill(pT_bb, histoweight);
      fTreeVariables[iVar] = pT_bb;
    }
    if(fVariables[iVar] == "pT_imbalance"){
      float pT_imbalance =
          fabs(smallR_B[first_b].Pt()+smallR_B[second_b].Pt()+iEntryExtrasInfo->GetGoodLepton().Pt()-iEntryExtrasInfo->GetNeutrino().Pt());
      fHistoVector[iVar].Fill(pT_imbalance, histoweight);
      fTreeVariables[iVar] = pT_imbalance;
    }
    if(fVariables[iVar] == "twistjj_maxM"){
      float dPhijj_temp = fabs(smallR[0].Phi()-smallR[1].Phi());
      float dEtajj_temp = fabs(smallR[0].Eta()-smallR[1].Phi());
      float twistjj_maxM = atan(fabs(dPhijj_temp/dEtajj_temp));
      fHistoVector[iVar].Fill(twistjj_maxM, histoweight);
      fTreeVariables[iVar] = twistjj_maxM;
    }
    if(fVariables[iVar] == "twistbb_maxM"){
      float dPhibb_temp = fabs(smallR_B[0].Phi()-smallR_B[1].Phi());
      float dEtabb_temp = fabs(smallR_B[0].Eta()-smallR_B[1].Phi());
      float twistbb_maxM = atan(fabs(dPhibb_temp/dEtabb_temp));
      fHistoVector[iVar].Fill(twistbb_maxM, histoweight);
      fTreeVariables[iVar] = twistbb_maxM;
    }
    if(fVariables[iVar] == "twist_MaxdEta"){
      float twist_MaxdEta = atan(fabs(smallR_B[bmaxeta1].Phi()-smallR_B[bmaxeta2].Phi())/fabs(smallR_B[bmaxeta1].Eta()-smallR_B[bmaxeta2].Eta()));
      fHistoVector[iVar].Fill(twist_MaxdEta, histoweight);
      fTreeVariables[iVar] = twist_MaxdEta;
    }
    if(fVariables[iVar] == "dYHb_minPt_mindrop"){
      float dYHb_minPt = bbSum.Rapidity()-smallR_B[bjet_min2].Rapidity();
      fHistoVector[iVar].Fill(dYHb_minPt, histoweight);
      fTreeVariables[iVar] = dYHb_minPt;
    }
    if(fVariables[iVar] == "dYHb_maxPt_mindrop"){
      float dYHb_minPt = bbSum.Rapidity()-smallR_B[bjet_min1].Rapidity();
      fHistoVector[iVar].Fill(dYHb_minPt, histoweight);
      fTreeVariables[iVar] = dYHb_minPt;
    }
    if(fVariables[iVar] == "dEtaHl_mindrop"){
      float dEtaHl = bbSum.Eta()-iEntryExtrasInfo->GetGoodLepton().Eta();
      fHistoVector[iVar].Fill(dEtaHl, histoweight);
      fTreeVariables[iVar] = dEtaHl;
    }
    if(fVariables[iVar] == "Mjj_mindrop"){
      fHistoVector[iVar].Fill(jjSum.M() , histoweight);
      fTreeVariables[iVar] = jjSum.M();
    }
    if(fVariables[iVar] == "Mbb_mindrop"){
      fHistoVector[iVar].Fill(bbSum.M() , histoweight);
      fTreeVariables[iVar] = bbSum.M();
    }
    if(fVariables[iVar] == "dRjj_mindrop"){
      fHistoVector[iVar].Fill(smallR[jet_min1].DeltaR(smallR[jet_min2]) , histoweight);
      fTreeVariables[iVar] = smallR[jet_min1].DeltaR(smallR[jet_min2]);
    }
    if(fVariables[iVar] == "dRbb_mindrop"){
      fHistoVector[iVar].Fill(smallR_B[bjet_min1].DeltaR(smallR_B[bjet_min2]) , histoweight);
      fTreeVariables[iVar] = smallR_B[bjet_min1].DeltaR(smallR_B[bjet_min2]);
    }
    if(fVariables[iVar] == "dEtabb_mindrop"){
      fHistoVector[iVar].Fill(fabs(smallR_B[bjet_min1].Eta()-smallR_B[bjet_min2].Eta()) , histoweight);
      fTreeVariables[iVar] = fabs(smallR_B[bjet_min1].Eta()-smallR_B[bjet_min2].Eta());
    }
    if(fVariables[iVar] == "dEtajj_mindrop"){
      fHistoVector[iVar].Fill(fabs(smallR[jet_min1].Eta()-smallR[jet_min2].Eta()) , histoweight);
      fTreeVariables[iVar] = fabs(smallR[jet_min1].Eta()-smallR[jet_min2].Eta());
    }
    if(fVariables[iVar] == "twistbb_mindrop"){
      float dPhibb_temp = fabs(smallR_B[bjet_min1].Phi()-smallR_B[bjet_min2].Phi());
      float dEtabb_temp = fabs(smallR_B[bjet_min1].Eta()-smallR_B[bjet_min2].Phi());
      float twist_mindrop = atan(fabs(dPhibb_temp/dEtabb_temp));
      fHistoVector[iVar].Fill(twist_mindrop, histoweight);
      fTreeVariables[iVar] = twist_mindrop;
    }
    if(fVariables[iVar] == "m1_m12"){
      fHistoVector[iVar].Fill(b_maxM/bbSum.M() , histoweight);
      fTreeVariables[iVar] = b_maxM/bbSum.M();
    }
    if(fVariables[iVar] == "m1m2_m12"){
      fHistoVector[iVar].Fill((smallR_B[bjet_min1].M()+smallR_B[bjet_min2].M())/bbSum.M() , histoweight);
    }
    if(fVariables[iVar] == "ClassifBDTOutput_withReco_basic"){                                                                  
      
      fHistoVector[iVar].Fill(fFlatTree -> ClassifBDTOutput_withReco_basic, histoweight);                                                                        
      fTreeVariables[iVar] = fFlatTree -> ClassifBDTOutput_withReco_basic;                                                                                        
      
    }
    if(fVariables[iVar] == "ClassifBDTOutput_basic"){

      fHistoVector[iVar].Fill(fFlatTree -> ClassifBDTOutput_basic, histoweight);
      fTreeVariables[iVar] = fFlatTree -> ClassifBDTOutput_basic;

    }
    if(fVariables[iVar] == "ClassifBDTOutput_withReco_6jsplit"){

      fHistoVector[iVar].Fill(fFlatTree -> ClassifBDTOutput_withReco_6jsplit, histoweight);
      fTreeVariables[iVar] = fFlatTree -> ClassifBDTOutput_withReco_6jsplit;

    }
    if(fVariables[iVar] == "ClassifBDTOutput_6jsplit"){

      fHistoVector[iVar].Fill(fFlatTree -> ClassifBDTOutput_6jsplit, histoweight);
      fTreeVariables[iVar] = fFlatTree -> ClassifBDTOutput_6jsplit;

    }
    if(fVariables[iVar] == "NBFricoNN_ljets"){
      fHistoVector[iVar].Fill(fFlatTree -> NBFricoNN_ljets, histoweight);
      fTreeVariables[iVar] = fFlatTree -> NBFricoNN_ljets;
    }
    if(fVariables[iVar] == "NBFricoNN_dil"){
      fHistoVector[iVar].Fill(fFlatTree -> NBFricoNN_dil, histoweight);
      fTreeVariables[iVar] = fFlatTree -> NBFricoNN_dil;
    }
    if(fVariables[iVar] == "dRbb_min"){
      fHistoVector[iVar].Fill(fFlatTree -> dRbb_min, histoweight);
      fTreeVariables[iVar] = fFlatTree -> dRbb_min;
    }
    if(fVariables[iVar] == "dRjj_min"){
      fHistoVector[iVar].Fill(fFlatTree -> dRjj_min, histoweight);
      fTreeVariables[iVar] = fFlatTree -> dRjj_min;
    }
    if(fVariables[iVar] == "HiggsbbM"){
      fHistoVector[iVar].Fill(fFlatTree -> HiggsbbM, histoweight);
      fTreeVariables[iVar] = fFlatTree -> HiggsbbM;
    }
    if(fVariables[iVar] == "HiggsjjM"){
      fHistoVector[iVar].Fill(fFlatTree -> HiggsjjM, histoweight);
      fTreeVariables[iVar] = fFlatTree -> HiggsjjM;
    }


  }
}


