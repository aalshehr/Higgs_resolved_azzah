#include "PlotFactoryBoosted/ControlPlots.h"
#include "PlotFactoryBoosted/AtlasStyle.h"
#include "PlotFactoryBoosted/HistoUtilities.h"
#include "PlotFactoryBoosted/FancyPlots.h"

#include <sys/stat.h>
#include "TSystem.h"

#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TH1D.h"
#include "TROOT.h"
#include "TPad.h"
#include "TMath.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "THStack.h"
#include "TGraphErrors.h"
#include "TLine.h"

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <stdio.h>

using namespace std;

ControlPlots::ControlPlots(std::shared_ptr<ConfigClass> Config)
{

  fTTbarHList.clear();
  fTTbarHFList.clear();
  fTTbarZList.clear();
  fTTbarWList.clear();
  fOtherTTbarVList.clear();
  fTTbarList.clear();
  fTTLightList.clear();
  fTTBBList.clear();
  fTTCCList.clear();
  fWjetsList.clear();
  fZjetsList.clear();
  fDibosonList.clear();
  fSingleTopList.clear();
  fDataList.clear();
  fQCDList.clear();

  gErrorIgnoreLevel = kError;

  TH1::AddDirectory(kFALSE);

  gConfig = Config;

  std::stringstream oss;
  oss   << setprecision(2) << gConfig -> GetLumiLabel();

  // get here all values from your config file                                                                                                                           
  fLumi           = gConfig -> GetLumi();
  fJetBin         = gConfig -> GetJetBin();
  fBTagBin        = gConfig -> GetBTagBin();
  fUseHF          = gConfig -> GetUseHFSplitting();
  fLeptonType     = gConfig -> GetChannel();
  fAnalysisType   = gConfig -> GetAnalysisType();
  fVetoJetBin     = gConfig -> GetVetoJetBin();
  fVetoBTagBin    = gConfig -> GetVetoBTagBin();
  fLumiLabel      = oss.str()+" fb^{-1}";
  fDataLabel      = gConfig -> GetDataLabel();
  fQCDScale       = gConfig -> GetLumiLabel()/3.21296;

  std::cout << "========================================================================> QCD scale = " << fQCDScale << std::endl;

}

ControlPlots::~ControlPlots()
{

  std::ofstream fOut;
  fOut.open((fOutputFolder+"/SeparationList.txt").c_str(), std::ios::out | std::ios::app );

  std::sort(fSeparationList.begin(), fSeparationList.end());

  for(int i = fSeparationList.size()-1; i >= 0; --i){

    fOut      << i << "\t" << fSeparationList[i].second << "\t" << fSeparationList[i].first << std::endl;

  }

  fOut.close();

}

void ControlPlots::SetChannelLabel(std::string label)
{
  
  fChannelLabel = "--resolved--";

  if(label != "0incl")
    fChannelLabel = "--boosted--";

}

void ControlPlots::SetLargeJetLabel(std::string label)
{
  
  fLargeJetLabel = " ";

  if(label == "0incl") fLargeJetLabel = "#geq 0 large jets";
  if(label == "1incl") fLargeJetLabel = "#geq 1 large jets";
  if(label == "2incl") fLargeJetLabel = "#geq 2 large jets";
  if(label == "0excl") fLargeJetLabel = "0 large jets";
  if(label == "1excl") fLargeJetLabel = "1 large jets";
  if(label == "2excl") fLargeJetLabel = "2 large jets";

  std::cout << "Set Large Jet Label = " << label << std::endl;


}

void ControlPlots::SetTopTagLabel(std::string label)
{
  
  fTopTagLabel = " ";

  if(label == "0incl") fTopTagLabel = "#geq 0 top-tags";
  if(label == "1incl") fTopTagLabel = "#geq 1 top-tags";
  if(label == "2incl") fTopTagLabel = "#geq 2 top-tags";
  if(label == "3incl") fTopTagLabel = "#geq 3 top-tags";
  if(label == "0excl") fTopTagLabel = "0 top-tags";
  if(label == "1excl") fTopTagLabel = "1 top-tags";
  if(label == "2excl") fTopTagLabel = "2 top-tags";
  if(label == "3excl") fTopTagLabel = "3 top-tags";

  std::cout << "Set Top Tag Label = " << label << std::endl;


}

void ControlPlots::SetHiggsTagLabel(std::string label)
{
  
  fHiggsTagLabel = " ";

  if(label == "0incl") fHiggsTagLabel = "#geq 0 Higgs-tags";
  if(label == "1incl") fHiggsTagLabel = "#geq 1 Higgs-tags";
  if(label == "2incl") fHiggsTagLabel = "#geq 2 Higgs-tags";
  if(label == "3incl") fHiggsTagLabel = "#geq 3 Higgs-tags";
  if(label == "0excl") fHiggsTagLabel = "0 Higgs-tags";
  if(label == "1excl") fHiggsTagLabel = "1 Higgs-tags";
  if(label == "2excl") fHiggsTagLabel = "2 Higgs-tags";
  if(label == "3excl") fHiggsTagLabel = "3 Higgs-tags";

  std::cout << "Set Higgs Tag Label = " << label << std::endl;


}

void ControlPlots::SetVetoJetBinLabel(std::string label)
{

  fVetoJetBin = label;

}
void ControlPlots::SetJetBinLabel(std::string label)
{

  fJetBin = label;

  std::string multi;

  std::cout << "Set Jet Bin Label = " << label << std::endl;

  if(label == "0incl") multi = "#geq 0 jets";
  if(label == "1incl") multi = "#geq 1 jets";
  if(label == "2incl") multi = "#geq 2 jets";
  if(label == "3incl") multi = "#geq 3 jets";
  if(label == "4incl") multi = "#geq 4 jets";
  if(label == "6incl") multi = "#geq 6 jets";
  if(label == "0excl") multi = "0 jets";
  if(label == "1excl") multi = "1 jets";
  if(label == "2excl") multi = "2 jets";
  if(label == "3excl") multi = "3 jets";
  if(label == "4excl") multi = "4 jets";
  if(label == "5excl") multi = "5 jets";
  if(label == "6excl") multi = "6 jets";

  std::string lep;
  if(fLeptonType == "ejets_2015")           lep = "e";
  if(fLeptonType == "mujets_2015")          lep = "#mu";
  if(fLeptonType == "boosted_ejets_2015")   lep = "e";
  if(fLeptonType == "boosted_mujets_2015")  lep = "#mu";
  if(fLeptonType == "emujets_2015")         lep = "e/#mu";
  if(fLeptonType == "boosted_emujets_2015") lep = "e/#mu";
  if(fLeptonType == "ejets_2016")           lep = "e";
  if(fLeptonType == "mujets_2016")          lep = "#mu";
  if(fLeptonType == "boosted_ejets_2016")   lep = "e";
  if(fLeptonType == "boosted_mujets_2016")  lep = "#mu";
  if(fLeptonType == "emujets_2016")         lep = "e/#mu";
  if(fLeptonType == "boosted_emujets_2016") lep = "e/#mu";

  if(fCombined) lep = "e/#mu";
  
  fJetBinLabel = lep+" + "+multi;
  
  fHTMLLabel = "index.html";
  
}

void ControlPlots::SetVetoBtagLabel(std::string btag_label)
{

  fVetoBTagBin = btag_label;

}

void ControlPlots::SetBtagLabel(std::string btag_label)
{

  fBTagBin = btag_label;
  
  if(btag_label == "0incl")
    fBtagLabel = "#geq 0 b-tags";
  else if(btag_label == "1incl")
    fBtagLabel = "#geq 1 b-tags";
  else if(btag_label == "2incl")
    fBtagLabel = "#geq 2 b-tags";
  else if(btag_label == "3incl")
    fBtagLabel = "#geq 3 b-tags";
  else if(btag_label == "4incl")
    fBtagLabel = "#geq 4 b-tags";
  else if(btag_label == "0excl")
    fBtagLabel = "0 b-tags";
  else if(btag_label == "1excl")
    fBtagLabel = "1 b-tags";
  else if(btag_label == "2excl")
    fBtagLabel = "2 b-tags";
  else if(btag_label == "3excl")
    fBtagLabel = "3 b-tags";
  else if(btag_label == "4excl")
    fBtagLabel = "4 b-tags";
  else
    std::cout << "ERROR::ControlPlots -> There is no such btag label!" << std::endl;
  
}

void ControlPlots::MakePlots(std::string histo_name, std::string x_axis_label, std::string outputFile, bool ResidualFlag, bool PrintLegend, bool PrintYield, int twoDflag)
{
  createHTML("PlotIndex");

  std::cout << histo_name << std::endl;

  TH1D TotalUnc;

  if (twoDflag == 2)
    {
      
      if(!fUseHF){
	
	fTTbarSum2D     = this -> AddHistos2D(histo_name, fTTbarList);
	fAllProc2D.push_back(*fTTbarSum2D);
	fAllProcNames.push_back("$t\\bar{t}$");

      }
      else{

	fTTLightSum2D     = this -> AddHistos2D(histo_name, fTTLightList);
        fAllProc2D.push_back(*fTTLightSum2D);
        fAllProcNames.push_back("$t\\bar{t}+light$");

	fTTBBSum2D     = this -> AddHistos2D(histo_name, fTTBBList);
	fAllProc2D.push_back(*fTTBBSum2D);
	fAllProcNames.push_back("$t\\bar{t}+b\\bar{b}$");

	fTTCCSum2D     = this -> AddHistos2D(histo_name, fTTCCList);
	fAllProc2D.push_back(*fTTCCSum2D);
	fAllProcNames.push_back("$t\\bar{t}+c\\bar{c}$");

      }


      fWjetsSum2D     = this -> AddHistos2D(histo_name, fWjetsList);
      fZjetsSum2D     = this -> AddHistos2D(histo_name, fZjetsList);
      fDataSum2D      = this -> AddHistos2D(histo_name, fDataList);
      //fQCDSum2D       = this -> AddHistos2D(histo_name, fQCDList);
      fDibosonSum2D   = this -> AddHistos2D(histo_name, fDibosonList);
      fSingleTopSum2D = this -> AddHistos2D(histo_name, fSingleTopList);
      fTTbarZSum2D    = this -> AddHistos2D(histo_name, fTTbarZList);
      fTTbarWSum2D    = this -> AddHistos2D(histo_name, fTTbarWList);
      fOtherTTbarVSum2D    = this -> AddHistos2D(histo_name, fOtherTTbarVList);
      
      if(fAnalysisType == "ttH")
	fTTbarHSum2D    = this -> AddHistos2D(histo_name, fTTbarHList);


      fAllProc2D.push_back(*fWjetsSum2D);
      fAllProc2D.push_back(*fZjetsSum2D);
      fAllProc2D.push_back(*fDibosonSum2D);
      fAllProc2D.push_back(*fSingleTopSum2D);
      //fAllProc.push_back(*fQCDSum2D);
      fAllProc2D.push_back(*fTTbarZSum2D);
      fAllProc2D.push_back(*fTTbarWSum2D);
      fAllProc2D.push_back(*fOtherTTbarVSum2D);
      
      fAllProcNames.push_back("$W$+jets");
      fAllProcNames.push_back("$Z$+jets");
      fAllProcNames.push_back("Diboson");
      fAllProcNames.push_back("Single top");
      //fAllProcNames.push_back("Multijets");
      fAllProcNames.push_back("$t\\bar{t}+V$");

    }
  else {

    if(!fUseHF){
      fTTbarSum     = this -> AddHistos(histo_name, fTTbarList);
      fAllProc.push_back(*fTTbarSum);
      fAllProcNames.push_back("$t\\bar{t}$");
    }
    else{

      fTTLightSum = this -> AddHistos(histo_name, fTTLightList);
      fAllProc.push_back(*fTTLightSum);
      fAllProcNames.push_back("$t\\bar{t}$+light");
      
      fTTBBSum = this -> AddHistos(histo_name, fTTBBList);
      fAllProc.push_back(*fTTBBSum);
      fAllProcNames.push_back("$t\\bar{t}+b\\bar{b}$");

      fTTCCSum = this -> AddHistos(histo_name, fTTCCList);
      fAllProc.push_back(*fTTCCSum);
      fAllProcNames.push_back("$t\\bar{t}+c\\bar{c}$");

    }


    fWjetsSum     = this -> AddHistos(histo_name, fWjetsList);
    fZjetsSum     = this -> AddHistos(histo_name, fZjetsList);
    fDataSum      = this -> AddHistos(histo_name, fDataList);
    //fQCDSum       = this -> AddHistos(histo_name, fQCDList);
    fDibosonSum   = this -> AddHistos(histo_name, fDibosonList);
    fSingleTopSum = this -> AddHistos(histo_name, fSingleTopList);
    fTTbarZSum    = this -> AddHistos(histo_name, fTTbarZList);
    fTTbarWSum    = this -> AddHistos(histo_name, fTTbarWList);
    fOtherTTbarVSum    = this -> AddHistos(histo_name, fOtherTTbarVList);

    if(fAnalysisType == "ttH")
      fTTbarHSum    = this -> AddHistos(histo_name, fTTbarHList);
   
    //fQCDSum -> Scale(fQCDScale);

    fAllProc.push_back(*fWjetsSum);
    fAllProc.push_back(*fZjetsSum);
    fAllProc.push_back(*fDibosonSum);
    fAllProc.push_back(*fSingleTopSum);
    //fAllProc.push_back(*fQCDSum);
    fAllProc.push_back(*fTTbarWSum);
    fAllProc.push_back(*fTTbarZSum);
    fAllProc.push_back(*fOtherTTbarVSum);
    fAllProcNames.push_back("$W$+jets");
    fAllProcNames.push_back("$Z$+jets");
    fAllProcNames.push_back("Diboson");
    fAllProcNames.push_back("Single top");
    //fAllProcNames.push_back("Multijets");
    fAllProcNames.push_back("$t\\bar{t}+W$");
    fAllProcNames.push_back("$t\\bar{t}+Z$");
    fAllProcNames.push_back("Other $t\\bar{t}+X/t+X$");
    std::vector<TH1D> SystUp;
    std::vector<TH1D> SystDown;
    std::vector<TH1D> SystDiff;


    if(fPlotErrorBand){
  
       

      // now start adding up histograms for systematics!
      // use just one list for all processes
      
      std::vector<std::string> SystNamesUp;
      std::vector<std::string> SystNamesDown;
      
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Top_CrossCalib_Tau32__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Top_Run1_Tau32__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Rtrk_Baseline_mass__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Rtrk_Modelling_mass__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Rtrk_Tracking_mass__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Rtrk_Baseline_pT__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Rtrk_Modelling_pT__1up");
      // SystNamesUp.push_back("LARGERJET_Weak_JET_Rtrk_Tracking_pT__1up");

      // SystNamesUp.push_back("LARGERJET_Strong_JET_Top_CrossCalib_Tau32__1up");
      // SystNamesUp.push_back("LARGERJET_Strong_JET_Top_Run1_Tau32__1up");
      // SystNamesUp.push_back("LARGERJET_Strong_JET_Rtrk_Baseline__1up");
      // SystNamesUp.push_back("LARGERJET_Strong_JET_Rtrk_ModellingAndTracking__1up");

      // SystNamesUp.push_back("LARGERJET_Medium_JET_Top_CrossCalib_Tau32__1up");
      // SystNamesUp.push_back("LARGERJET_Medium_JET_Top_Run1_Tau32__1up");
      // SystNamesUp.push_back("LARGERJET_Medium_JET_Rtrk_Baseline__1up");
      // SystNamesUp.push_back("LARGERJET_Medium_JET_Rtrk_Tracking__1up");
      // SystNamesUp.push_back("LARGERJET_Medium_JET_Rtrk_Modelling__1up");

      
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Top_CrossCalib_Tau32__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Top_Run1_Tau32__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Rtrk_Baseline_mass__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Rtrk_Modelling_mass__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Rtrk_Tracking_mass__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Rtrk_Baseline_pT__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Rtrk_Modelling_pT__1down");
      // SystNamesDown.push_back("LARGERJET_Weak_JET_Rtrk_Tracking_pT__1down");

      // SystNamesDown.push_back("LARGERJET_Strong_JET_Top_CrossCalib_Tau32__1down");
      // SystNamesDown.push_back("LARGERJET_Strong_JET_Top_Run1_Tau32__1down");
      // SystNamesDown.push_back("LARGERJET_Strong_JET_Rtrk_Baseline__1down");
      // SystNamesDown.push_back("LARGERJET_Strong_JET_Rtrk_ModellingAndTracking__1down");

      // SystNamesDown.push_back("LARGERJET_Medium_JET_Top_CrossCalib_Tau32__1down");
      // SystNamesDown.push_back("LARGERJET_Medium_JET_Top_Run1_Tau32__1down");
      // SystNamesDown.push_back("LARGERJET_Medium_JET_Rtrk_Baseline__1down");
      // SystNamesDown.push_back("LARGERJET_Medium_JET_Rtrk_Tracking__1down");
      // SystNamesDown.push_back("LARGERJET_Medium_JET_Rtrk_Modelling__1down");

      // This is kinda ugly, with the data file hardcoded (to avoid an extra arg) and the
      // formatting of the up and down variations having to be anticipated in advance, so
      // it is slightly liable to break if something new is added. Use with care!

      std::string rc = getenv("ROOTCOREBIN");
      std::string filename = "";

      if (fAnalysisType == "ttbar")
	filename = rc + "/../PlotFactoryBoosted/data/SystListTTbar.txt";
      else
	filename = rc + "/../PlotFactoryBoosted/data/SystList.txt";

      //filename = rc+"/../PlotFactoryBoosted/data/QCDSystList.txt";

      //std::cout << "Using " << filename << "as syst list" << std::endl;

      std::ifstream SystListFile(filename);

      if (!SystListFile)
	std::cout << "Syst list file " << filename << " was not found! Plotting without error bar..." << std::endl;
      
      std::string SystName;
      while (std::getline (SystListFile, SystName)){

      	if (SystName == "" ) continue; // ignore empty lines

      	// this one only has an up, which is annoying, so treat it separately and add to both lists
      	if (SystName == "JET_JER_SINGLE_NP__1up"){
      	    SystNamesUp.push_back(SystName);
      	    SystNamesDown.push_back(SystName);
      	}	  
      	// up variations; careful! Beware eg "Pileup" sneaking in...
      	else if(SystName.find( "__1up"   ) != std::string::npos || 
		SystName.find( "_up"     ) != std::string::npos ||
		SystName.find( "ScaleUp" ) != std::string::npos ||
		SystName.find( "UP"      ) != std::string::npos ||
		SystName.find( "_U"      ) != std::string::npos	)
      	  {
      	    SystNamesUp.push_back(SystName);
      	  }
      	// down variations;
      	else if(SystName.find( "__1down"   ) != std::string::npos || 
		SystName.find( "_down"     ) != std::string::npos ||
		SystName.find( "ScaleDown" ) != std::string::npos ||
		SystName.find( "DOWN"      ) != std::string::npos ||
		SystName.find( "_D" ) != std::string::npos)
      	  {
      	    SystNamesDown.push_back(SystName);
      	  }
      	// otherwise (ie single variation) add it to both and this will symmetrise it
      	else{
      	    SystNamesUp.push_back(SystName);
      	    SystNamesDown.push_back(SystName);
      	}
      }
      SystListFile.close();

      
      // first add up all up variation processes

      for(unsigned int iUp = 0; iUp < SystNamesUp.size(); ++iUp){
	//std::cout << SystNamesUp[iUp] << std::endl;
	SystUp.push_back(*this -> AddHistos(histo_name, fSystList, SystNamesUp[iUp], true));
	
      }
      
      // then add up all down variation processes
      for(unsigned int iDown = 0; iDown < SystNamesDown.size(); ++iDown){
	//std::cout << SystNamesDown[iDown] << std::endl;

	SystDown.push_back(*this -> AddHistos(histo_name, fSystList, SystNamesDown[iDown], true));
	
      }

    // now get the nominal histogram
    TH1D NominalHist = *this -> AddHistos(histo_name, fSystList);
      
    TotalUnc = *(TH1D*) NominalHist.Clone(0);
      
    // loop over histograms, calculate difference between variation and nominal hist and store the largest absolute difference in a new vector
    for(unsigned int iUp = 0; iUp < SystUp.size(); ++iUp){
      
      TH1D HelpUp       = SystUp[iUp];
      TH1D HelpDown     = SystDown[iUp];
      TH1D SystDiffHelp = *(TH1D*) SystUp[iUp].Clone(0);

      for(int iBin = 1; iBin <= HelpUp.GetNbinsX(); ++iBin){
	
	float VarUp   = fabs(NominalHist.GetBinContent(iBin) - HelpUp.GetBinContent(iBin));
	float VarDown = fabs(NominalHist.GetBinContent(iBin) - HelpDown.GetBinContent(iBin));

	// for now take half of the full difference between up and down!
	SystDiffHelp.SetBinContent(iBin, 0.5*(VarUp+VarDown));
	if(histo_name == "hist_nEvents") std::cout << "SystName = " << SystNamesUp[iUp] <<  ", VarUp = " << VarUp << std::endl;

	
      }

      SystDiff.push_back(SystDiffHelp);

    }

    // now add all the variations in quadrature
    for(int iBin = 1; iBin <= NominalHist.GetNbinsX(); ++iBin){
      
      float total2 = 0;
      
      for(unsigned int iSyst = 0; iSyst < SystDiff.size(); ++iSyst){
	total2 += SystDiff[iSyst].GetBinContent(iBin)*SystDiff[iSyst].GetBinContent(iBin);
	
	
      }
      
      // stat error
      total2 += NominalHist.GetBinError(iBin)*NominalHist.GetBinError(iBin);

      // Hard coded Lumi uncertainty
      double LumiUncSize = 0.05;
      total2 += (NominalHist.GetBinContent(iBin)*LumiUncSize)*(NominalHist.GetBinContent(iBin)*LumiUncSize);
    
      TotalUnc.SetBinContent(iBin, 1.0);
      TotalUnc.SetBinError(iBin, sqrt(total2));
      if(histo_name == "hist_nEvents") std::cout << "finalerr= " << sqrt(total2) << std::endl;
      
    }
   }
    else{ // not plotting systematics
      // otherwise just use the nominal hist
      TotalUnc = *this -> AddHistos(histo_name, fSystList);
    }

  }

  if(PrintYield){
    
    this -> WriteEventYield(true);
  
  }

  if  (twoDflag == 2)
    {
      // add here alternative for correlation instead of matrix
      if(fAnalysisType == "ttbar")
	this -> DrawMatrix(x_axis_label, outputFile, histo_name, PrintLegend);
    }
  else{

    if(histo_name.find( "HiggsDecays" ) != std::string::npos){
      std::cout << "HiggsDecay Pie Chart 1 " << std::endl;
      std::string PieLabel = (fChannelLabel).c_str();
      std::cout << "HiggsDecay Pie Chart 2 " << std::endl;
      MakeHiggsDecayPieChart(*fTTbarHSum, fOutputFolder, PieLabel, histo_name);
      std::cout << "HiggsDecay Pie Chart 3 " << std::endl;
    }
    else if (histo_name.find( "truth" ) != std::string::npos && fAnalysisType == "ttbar")
      this -> DrawFinalTruthPlot(x_axis_label, outputFile, histo_name, PrintLegend);
    else 
      this -> DrawFinalPlot(x_axis_label, outputFile, histo_name, ResidualFlag, PrintLegend, TotalUnc);
  }

  this -> DeleteFiles();

}


void ControlPlots::PlotSeparation(TH1D sig, TH1D bkg, std::string outputFile, std::string axis_label)
{
  
  sig.Scale(1.0/sig.Integral());
  bkg.Scale(1.0/bkg.Integral());

  double nmax_sig = sig.GetBinContent(sig.GetMaximumBin());
  double nmax_bkg = bkg.GetBinContent(bkg.GetMaximumBin());

  double nmax = nmax_sig;

  if(nmax_sig < nmax_bkg)
    nmax = nmax_bkg;

  if(axis_label.find( "phi" ) != std::string::npos)
    nmax = nmax*1.6;
  else
    nmax = nmax*1.4;

  SetAtlasStyle();

  // define canvas

  for(int i = 1; i <= sig.GetNbinsX(); ++i)
    bkg.SetBinError(i, 0);

  TCanvas *c1 = new TCanvas("", "", 720, 800);

  // define plotting styles

  int nBins = sig.GetNbinsX();

  for(int iBin = 1; iBin <= nBins; ++iBin)
    sig.SetBinError(iBin, 0);

  sig.SetFillColor(kRed);
  sig.SetMarkerStyle(21);
  sig.SetFillStyle(3354);
  sig.SetLineColor(kRed);

  bkg.SetFillColor(kBlue);
  bkg.SetMarkerStyle(21);
  bkg.SetFillStyle(3354);
  bkg.SetLineColor(kBlue);

  std::string x_axis_label = GetXAxisLabel(axis_label);

  sig.GetXaxis() -> SetTitle(x_axis_label.c_str());
  sig.GetYaxis() -> SetTitle("a. u.");

  sig.SetMaximum(nmax);

  sig.SetMinimum(0.0);

  sig.Draw();
  bkg.Draw("SAME");

  // define TLegend

  TLegend *fLegend = new TLegend(0.645, 0.64, 0.645+0.255, 0.845);
  fLegend -> AddEntry(&sig,  "Signal",   "f");
  fLegend -> AddEntry(&bkg,  "all Bkg.", "f");
  fLegend -> SetFillColor(0);
  fLegend -> SetLineColor(0);
  fLegend -> SetBorderSize(0);
  fLegend -> SetTextFont(72);
  fLegend -> SetTextSize(0.035);
  fLegend -> Draw("SAME");

  // get lables and write them

  //set labels....
  TLatex l2;
  l2.SetTextAlign(9);
  l2.SetTextFont(72);
  l2.SetTextSize(0.04);
  l2.SetNDC();
  l2.DrawLatex(0.21, 0.880, "ATLAS");
  TLatex l3;
  l3.SetTextAlign(9);
  l3.SetTextSize(0.04);
  l3.SetNDC();
  l3.DrawLatex(0.21, 0.845, "Internal");
  TLatex l4;
  l4.SetTextAlign(9);
  l4.SetTextSize(0.040);
  l4.SetTextFont(72);
  l4.SetNDC();
  TLatex l4b;
  l4b.SetTextAlign(9);
  l4b.SetTextSize(0.04);
  l4b.SetNDC();

  l4.DrawLatex(0.525, 0.88, (fJetBinLabel+", "+fBtagLabel).c_str());
  l4b.DrawLatex(0.21, 0.795, fChannelLabel.c_str());

  // calculate Seperation power of variables

  float Separation = 0;

  for(int i = 1; i <= sig.GetNbinsX(); ++i){

    float y_sig = sig.GetBinContent(i);
    float y_bkg = bkg.GetBinContent(i);

    //    std::cout << y_sig << "\t" << y_bkg << "\t" << Separation << std::endl;

    if((y_sig + y_bkg) != 0)
      Separation += (y_sig - y_bkg)*(y_sig - y_bkg)/(y_sig + y_bkg);

  }

  Separation = 0.5*Separation;

  std::stringstream oss_Sep;
  oss_Sep   << setprecision(3) << Separation;

  TLatex l5;
  l5.SetTextAlign(9);
  l5.SetTextSize(0.038);
  l5.SetNDC();
  l5.DrawLatex(0.635, 0.95, ("Separation: " + oss_Sep.str()).c_str());

  std::pair<float, std::string> Val;

  Val.second = outputFile;
  Val.first  = Separation; 

  //  std::cout << outputFile.c_str() << "\t" << Separation << std::endl;

  fSeparationList.push_back(Val);

  c1 -> Print((outputFile+"_Sep.eps").c_str());
  c1 -> Print((outputFile+"_Sep.pdf").c_str());
  c1 -> Print((outputFile+"_Sep.png").c_str());
  //  c1 -> Print((outputFile+"_Sep.pdf").c_str());

}

bool ControlPlots::pairCompare(const std::pair<std::string, float>& firstElem, const std::pair<std::string, float>& secondElem) {
 
  return firstElem.second < secondElem.second;

}


void ControlPlots::DeleteFiles()
{

}

void ControlPlots::DrawMatrix(std::string title, std::string outputFile, std::string histo_name, bool PrintHTML)
{

   TCanvas *c0;
   TPad *pad1;
   c0 = new TCanvas("", title.c_str(), 800, 800);
  pad1 = new TPad("pad1","pad1", 0.0, 0.0, 1.0, 1.00);

  pad1->SetBottomMargin(0.001);
  pad1->SetBorderMode(0);
  
  // pad1->SetTicks(1,1);


  const Int_t Number = 2;
  Double_t Red[Number]    = { 1.0, 1.0};
  Double_t Green[Number]  = { 1.00, 0.00};
  Double_t Blue[Number]   = { 1.0, 0.0};
  Double_t Length[Number] = { 0.00, 1.00};
  Int_t nb=20;
  TColor::CreateGradientColorTable(Number,Length,Red,Green,Blue,nb,1);
  fTTbarSum2D->SetContour(nb);
        
  Int_t nbins = fTTbarSum2D->GetSize();
  Int_t nbinsx = sqrt(nbins)-1, nbinsy = sqrt(nbins)-1;
  // need to sum all the bins in each col, then normalise...
  for (int ybin = 1; ybin < nbinsy; ++ybin)
    {
      Double_t sum=0;
      for (int xbin = 1; xbin < nbinsx; ++xbin)
	sum += fTTbarSum2D->GetBinContent(xbin,ybin);
      
      for (int xbin = 1; xbin < nbinsx; ++xbin)
	{
	  Double_t norm = fTTbarSum2D->GetBinContent(xbin,ybin);
	  if (sum== 0) sum = 1;
	  norm = (norm/sum)*100;
	  int normed = (int)(norm+0.5f);
	  fTTbarSum2D->SetBinContent(xbin,ybin, normed);
	}
    }

  std::string x_axis_label = GetXAxisLabel(title);
  std::string y_axis_label = GetYAxisLabel(title);

  fTTbarSum2D->GetXaxis() -> SetTitle(x_axis_label.c_str());
  fTTbarSum2D->GetYaxis() -> SetTitle(y_axis_label.c_str());
  fTTbarSum2D->GetYaxis() -> SetTitleOffset(1.5);   

  
  
  
  fTTbarSum2D->SetTitle(title.c_str());
  TLatex label;
  label.SetNDC(); 
  fTTbarSum2D->Draw("COL1, TEXT");
  label.SetTextAlign(9);
  label.SetTextFont(72);
  label.SetTextSize(0.03);
  label.DrawLatex(0.2,0.87, "ATLAS Internal ");

  TLatex l3;
  l3.SetTextAlign(9);
  l3.SetTextSize(0.04);
  l3.SetNDC();
  l3.DrawLatex(0.2, 0.825, "#sqrt{s}=13TeV");

  c0 -> Print((outputFile+".eps").c_str());
  c0 -> Print((outputFile+".pdf").c_str());
  c0 -> Print((outputFile+".png").c_str());
  //  c0 -> Print((outputFile+".pdf").c_str());


  std::ofstream* htmlfile = new std::ofstream();

  if(PrintHTML){

    std::string teps = (outputFile+".eps").c_str();
    std::string tpng = (outputFile+".png").c_str();
    std::string tpdf = (outputFile+".pdf").c_str();
    
    std::string tsep = (outputFile+"_Sep.png").c_str();
    std::string tove = (outputFile+"_overlap.png").c_str();
    
    tpng.replace(tpng.find(fOutputFolder.c_str()), strlen(fOutputFolder.c_str()), "");
    tpng.replace(tpng.find("/"),                   strlen("/"),                   "");


    htmlfile->open((_htmls[fHTMLLabel]).c_str(),fstream::app);
    // if normal, print tab begin
    if(strstr(title.c_str(), "norm") == NULL)
      *htmlfile << "<tr>"
		<< "<td>"<<title<<"</td>";
    
    if(fAnalysisType == "ttH"){
      *htmlfile << "<td>";
      *htmlfile << "<p><a href=\"" << tpng.c_str() << "\">";
      *htmlfile << "<img src=\""   << tpng.c_str() << "\" width=\"70%\"/>";
      *htmlfile << "</td>";
      if(histo_name=="hist_resolved_overlap"){
	*htmlfile << "<td>";
	*htmlfile << "<p><a href=\"" << tove.c_str() << "\">";
	*htmlfile << "<img src=\""   << tove.c_str() << "\" width=\"80%\"/>";
	*htmlfile << "</td>" << std::endl;
      }
      else{
	*htmlfile << "<td>";
	*htmlfile << "<p><a href=\"" << tsep.c_str() << "\">";
	*htmlfile << "<img src=\""   << tsep.c_str() << "\" width=\"80%\"/>";
	*htmlfile << "</td>" << std::endl;
      }
      if(strstr(title.c_str(), "norm") != NULL)
	*htmlfile << "</tr>"<< std::endl;

    }
    else{

      *htmlfile << "<td>";
      *htmlfile << "<p><a href=\"" << tpng.c_str() << "\">";
      *htmlfile << "<img src=\""   << tpng.c_str() << "\" width=\"70%\"/>";
      *htmlfile << "</td>";
   
      if(strstr(title.c_str(), "norm") != NULL)
        *htmlfile << "</tr>"<< std::endl;
    
    }

    htmlfile->close();
    
  }

  //delete c0;

  fcloseall();
}

void ControlPlots::DrawFinalTruthPlot(std::string x_axis_label, std::string outputFile, std::string histo_name, bool PrintHTML)
{
  SetAtlasStyle();

  TCanvas *c0;
  TPad *pad1;

  c0 = new TCanvas("", "", 800, 600);
  pad1 = new TPad("pad1","pad1", 0.0, 0.15, 1.0, 1.00);

  pad1->SetBottomMargin(0.001);
  pad1->SetBorderMode(0);
  
  pad1->SetTicks(1,1);

  if (histo_name.find("logscale") != std::string::npos) 
    pad1->SetLogy(1);

  pad1->Draw();

  double nmax = fTTbarSum -> GetBinContent(fTTbarSum -> GetMaximumBin())*1.50;

  THStack   *stack0 = new THStack("stack0", "");
  fTTbarSum     -> SetFillColor(0);
  fTTbarSum     -> SetFillStyle(0);
  fTTbarSum     -> SetMarkerStyle(21);
  fTTbarSum     -> SetLineColor(1);
  fTTbarSum     -> SetLineWidth(2);
  stack0        -> Add(fTTbarSum);

  pad1 -> cd();
  stack0 -> Draw("HIST");


  stack0 -> GetXaxis() -> SetTitle(x_axis_label.c_str());
  stack0 -> GetYaxis() -> SetTitle("Events");
  stack0 -> GetYaxis() -> SetLabelFont(43);
  stack0 -> GetYaxis() -> SetLabelSize(31);
  stack0 -> GetYaxis() -> SetTitleFont(43);
  stack0 -> GetYaxis() -> SetTitleSize(34);

  stack0 -> SetMaximum(nmax);
  stack0 -> SetMinimum(0.1);

  TLatex l1;
  l1.SetTextAlign(9);
  l1.SetTextSize(0.04);
  l1.SetNDC();
  l1.DrawLatex(0.21, 0.775, "Truth Distribution");
  TLatex l2;
  l2.SetTextAlign(9);
  l2.SetTextFont(72);
  l2.SetTextSize(0.04);
  l2.SetNDC();
  l2.DrawLatex(0.21, 0.880, "ATLAS");
  TLatex l3;
  l3.SetTextAlign(9);
  l3.SetTextSize(0.04);
  l3.SetNDC();
  l3.DrawLatex(0.21, 0.825, "work in progress");
  TLatex l4;
  l4.SetTextAlign(9);
  l4.SetTextSize(0.040);
  l4.SetTextFont(72);
  l4.SetNDC();
  TLatex l4b;
  l4b.SetTextAlign(9);
  l4b.SetTextSize(0.04);
  l4b.SetNDC();

  l4b.DrawLatex(0.535, 0.825, fChannelLabel.c_str());
  l4.DrawLatex(0.535, 0.89, (fJetBinLabel+", "+fBtagLabel).c_str());


  c0 -> Draw();

  c0 -> Print((outputFile+".eps").c_str());
  c0 -> Print((outputFile+".pdf").c_str());
  c0 -> Print((outputFile+".png").c_str());

  std::string title = x_axis_label;

  std::ofstream* htmlfile = new std::ofstream();

  if(PrintHTML){

    std::string teps = (outputFile+".eps").c_str();
    std::string tpng = (outputFile+".png").c_str();
    std::string tpdf = (outputFile+".pdf").c_str();
    
    std::string tsep = (outputFile+"_Sep.png").c_str();
    std::string tove = (outputFile+"_overlap.png").c_str();
    
    tpng.replace(tpng.find(fOutputFolder.c_str()), strlen(fOutputFolder.c_str()), "");
    tpng.replace(tpng.find("/"),                   strlen("/"),                   "");


    htmlfile->open((_htmls[fHTMLLabel]).c_str(),fstream::app);
    // if normal, print tab begin
    if(strstr(title.c_str(), "norm") == NULL)
      *htmlfile << "<tr>"
		<< "<td>"<<title<<"</td>";
    
    *htmlfile << "<td>";
    *htmlfile << "<p><a href=\"" << tpng.c_str() << "\">";
    *htmlfile << "<img src=\""   << tpng.c_str() << "\" width=\"70%\"/>";
    *htmlfile << "</td>";

    if(fAnalysisType == "ttH"){

      if(histo_name=="hist_resolved_overlap"){
	*htmlfile << "<td>";
	*htmlfile << "<p><a href=\"" << tove.c_str() << "\">";
	*htmlfile << "<img src=\""   << tove.c_str() << "\" width=\"80%\"/>";
	*htmlfile << "</td>" << std::endl;
      }
      else{
	*htmlfile << "<td>";
	*htmlfile << "<p><a href=\"" << tsep.c_str() << "\">";
	*htmlfile << "<img src=\""   << tsep.c_str() << "\" width=\"80%\"/>";
	*htmlfile << "</td>" << std::endl;
      }

    }

    if(strstr(title.c_str(), "norm") != NULL)
      *htmlfile << "</tr>"<< std::endl;
    htmlfile->close();
    
  }

  delete stack0;
  delete c0;

  fcloseall();

}

void ControlPlots::DrawFinalPlot(std::string x_axis_label, std::string outputFile, std::string histo_name, bool ResidualFlag, bool PrintLegend, TH1D TotalUnc)
{

  SetAtlasStyle();

  TCanvas *c0;
  
  TPad *pad1;
  TPad *pad2;
  
  if(!ResidualFlag){
    c0   = new TCanvas("", "", 720, 720);
    pad1 = new TPad("pad1","pad1", 0.0, 0.0, 1.0, 1.0);
    pad2 = new TPad("pad2","pad2", 0.0, 0.0, 1.0, 0.0);
  }
  else{
    
    if(histo_name != "CosThetaNom_4excl_5incl")
      c0   = new TCanvas("", "", 720, 900);
    else
      c0   = new TCanvas("", "", 2500, 900);
    
    pad1 = new TPad("pad1","pad1", 0.0, 0.233, 1.0, 1.00);
    pad2 = new TPad("pad2","pad2", 0.0, 0.010, 1.0, 0.232);
    
    pad1->SetBottomMargin(0.001);
    pad1->SetBorderMode(0);
    pad2->SetBottomMargin(0.4);
    
    pad1->SetTicks(1,1);
    pad2->SetTicks(1,1);
    
    pad1->Draw();
    pad2->Draw();
    
  }

  double nmax = 0.0;

  if(histo_name.find( "pt" ) != std::string::npos)
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*1.65;
  else if(histo_name.find( "met_met" ) != std::string::npos)
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*1.65;
  else if(histo_name.find( "jvt" ) != std::string::npos)
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*1.65;
  else if(histo_name.find( "mv2" ) != std::string::npos)
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*1.65;
  else if(histo_name.find( "phi" ) != std::string::npos)
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*2.1;
  else if(histo_name.find( "eta" ) != std::string::npos)
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*2.1;
  else
    nmax = fDataSum -> GetBinContent(fDataSum -> GetMaximumBin())*1.90;

  if(histo_name.find( "logscale" ) != std::string::npos)
    nmax = nmax * 100;
  
  int nbins = fDataSum -> GetNbinsX();

  float lower_edge  = fDataSum -> GetBinLowEdge(1);
  float bin_width   = fDataSum -> GetBinWidth(1);
  float number_bins = fDataSum -> GetNbinsX();
  //float upper_edge  = lower_edge + number_bins*bin_width;
  float upper_edge = fDataSum -> GetBinLowEdge(number_bins) + fDataSum->GetBinWidth(number_bins);
  TH1D *fRatioUnc;
  if (upper_edge != lower_edge + number_bins*bin_width){

    double* edges_array = new double[ nbins+1];
    for ( int iii = 0; iii <= nbins; ++iii)
      {
	    edges_array[iii] = fDataSum -> GetBinLowEdge(iii+1);
      }
    
    fTotalSum = new TH1D("","",nbins, edges_array);
    fTotalSum2 = new TH1D("","",nbins, edges_array);
    fRatioUnc = new TH1D("","",nbins, edges_array);
    
    delete edges_array;
    
    
  }
  else{
    
    fTotalSum    = new TH1D("", "", nbins, lower_edge, upper_edge);
    fTotalSum2   = new TH1D("", "", nbins, lower_edge, upper_edge);
    
    fRatioUnc = new TH1D("", "", nbins, lower_edge, upper_edge); 
    
  }

  // for(int ibin = 1; ibin <= nbins; ++ibin){

  //   float binContent = fQCDSum -> GetBinContent(ibin);
  //   fQCDSum -> SetBinError(ibin, binContent*0.5);   // assign a 50 % normalisation unc. to QCD

  // }

  if(!fUseHF)
    fTotalSum    -> Add(fTTbarSum);
  else{
    
    fTotalSum    -> Add(fTTLightSum);
    fTotalSum    -> Add(fTTBBSum);
    fTotalSum    -> Add(fTTCCSum);

  }


  fTotalSum    -> Add(fTTbarZSum);
  fTotalSum    -> Add(fTTbarWSum);
  fTotalSum    -> Add(fOtherTTbarVSum);
  fTotalSum    -> Add(fWjetsSum);
  fTotalSum    -> Add(fZjetsSum);
  fTotalSum    -> Add(fDibosonSum);
  fTotalSum    -> Add(fSingleTopSum);
  //fTotalSum    -> Add(fQCDSum);

  double KS   = fDataSum -> KolmogorovTest(fTotalSum);
   //double chi2 = fDataSum -> Chi2Test(fTotalSum, "UW");

  //fQCDOut   = fQCDSum;
  fWjetsOut = fWjetsSum;


  fZjetsSumClone     = (TH1D*)fZjetsSum     -> Clone();
  fDibosonSumClone   = (TH1D*)fDibosonSum   -> Clone();
  fSingleTopSumClone = (TH1D*)fSingleTopSum -> Clone();
  //fQCDSumClone       = (TH1D*)fQCDSum       -> Clone();
  fTTbarZSumClone    = (TH1D*)fTTbarZSum    -> Clone();
  fTTbarWSumClone    = (TH1D*)fTTbarWSum    -> Clone();
  fOtherTTbarVSumClone    = (TH1D*)fOtherTTbarVSum    -> Clone();

  if(!fUseHF)
    fTTbarSumClone     = (TH1D*)fTTbarSum     -> Clone();
  else{

    fTTLightSumClone  = (TH1D*)fTTLightSum  -> Clone();
    fTTBBSumClone     = (TH1D*)fTTBBSum     -> Clone();
    fTTCCSumClone     = (TH1D*)fTTCCSum     -> Clone();

  }

  fWjetsSumClone     = (TH1D*)fWjetsSum     -> Clone();
  fDataSumClone      = (TH1D*)fDataSum      -> Clone();

  for(int ibin = 1; ibin <= nbins; ++ibin){

    //fQCDSum       -> SetBinError(ibin, 0);

    if(!fUseHF)
      fTTbarSum     -> SetBinError(ibin, 0);
    else{

      fTTLightSum  -> SetBinError(ibin, 0);
      fTTBBSum     -> SetBinError(ibin, 0);
      fTTCCSum     -> SetBinError(ibin, 0);

    }
    //fTTbarHFSum   -> SetBinError(ibin, 0);
    fTTbarWSum    -> SetBinError(ibin, 0);
    fTTbarZSum    -> SetBinError(ibin, 0);
    fOtherTTbarVSum    -> SetBinError(ibin, 0);
    fWjetsSum     -> SetBinError(ibin, 0);
    fZjetsSum     -> SetBinError(ibin, 0);
    fDibosonSum   -> SetBinError(ibin, 0);
    fSingleTopSum -> SetBinError(ibin, 0);

  }
  
  THStack   *stack0 = new THStack("stack0", "");

  /*fQCDSum       -> SetFillColor(619);
  fQCDSum       -> SetMarkerStyle(21);
  fQCDSum       -> SetLineColor(619);
  stack0        -> Add(fQCDSum);
  */
  fSingleTopSum -> SetFillColor(62);
  fSingleTopSum -> SetMarkerStyle(21);
  fSingleTopSum -> SetLineColor(62);
  stack0        -> Add(fSingleTopSum);

  fDibosonSum   -> SetFillColor(5);
  fDibosonSum   -> SetMarkerStyle(21);
  fDibosonSum   -> SetLineColor(5);
  stack0        -> Add(fDibosonSum);

  fZjetsSum     -> SetFillColor(95);
  fZjetsSum     -> SetMarkerStyle(21);
  fZjetsSum     -> SetLineColor(95);
  stack0        -> Add(fZjetsSum);

  fWjetsSum     -> SetFillColor(92);
  fWjetsSum     -> SetMarkerStyle(21);
  fWjetsSum     -> SetLineColor(92);
  stack0        -> Add(fWjetsSum);

  /*fTTbarHFSum   -> SetFillColor(kTeal+10);
  fTTbarHFSum   -> SetMarkerStyle(21);
  fTTbarHFSum   -> SetLineColor(kTeal+10);
  stack0        -> Add(fTTbarHFSum); 
  */
  
  fTTbarWSum    -> SetFillColor(kPink+7);
  fTTbarWSum    -> SetMarkerStyle(21);
  fTTbarWSum    -> SetLineColor(kPink+7);
  stack0        -> Add(fTTbarWSum); 

  fTTbarZSum    -> SetFillColor(kPink+8);
  fTTbarZSum    -> SetMarkerStyle(21);
  fTTbarZSum    -> SetLineColor(kPink+8);
  stack0        -> Add(fTTbarZSum); 

  fOtherTTbarVSum    -> SetFillColor(kPink+9);
  fOtherTTbarVSum    -> SetMarkerStyle(21);
  fOtherTTbarVSum    -> SetLineColor(kPink+9);
  stack0        -> Add(fOtherTTbarVSum); 
  
  if(!fUseHF){

    fTTbarSum     -> SetFillColor(0);
    fTTbarSum     -> SetMarkerStyle(21);
    fTTbarSum     -> SetLineColor(1);
    fTTbarSum     -> SetLineWidth(1);
    stack0        -> Add(fTTbarSum);

  }
  else{
    
    fTTLightSum     -> SetFillColor(0);
    fTTLightSum     -> SetMarkerStyle(21);
    fTTLightSum     -> SetLineColor(1);
    fTTLightSum     -> SetLineWidth(1);
    stack0          -> Add(fTTLightSum);

    fTTBBSum        -> SetFillColor(kBlue-5);
    fTTBBSum        -> SetMarkerStyle(21);
    fTTBBSum        -> SetLineColor(1);
    fTTBBSum        -> SetLineWidth(1);
    stack0          -> Add(fTTBBSum);

    fTTCCSum        -> SetFillColor(kBlue-8);
    fTTCCSum        -> SetMarkerStyle(21);
    fTTCCSum        -> SetLineColor(1);
    fTTCCSum        -> SetLineWidth(1);
    stack0          -> Add(fTTCCSum);

  }

  pad1 -> cd();

  fTotalSum    -> SetFillStyle(3354);
  fTotalSum    -> SetFillColor(kBlack);
  fTotalSum    -> SetLineWidth(0);
  fTotalSum    -> SetMarkerStyle(0);

  /*  fTotalSumTTH -> SetFillStyle(0);
  fTotalSumTTH -> SetFillColor(0);
  fTotalSumTTH -> SetLineWidth(1);
  fTotalSumTTH -> SetLineColor(kRed); */

  int counter_zero = 0;

  for(int ibin = 1; ibin <= nbins; ++ibin)
    if(fDataSum  -> GetBinContent(ibin) == 0)
      counter_zero++;

  /// Blind bins with S/B > 2%


  TH1F* h_blind = (TH1F*)fDataSum->Clone();

  if(fAnalysisType == "ttH"){

    for(int i_bin=1;i_bin<fDataSum->GetNbinsX()+1;i_bin++){
      
      if( fTTbarHSum->GetBinContent(i_bin) / fTotalSum->GetBinContent(i_bin) > 0.05 ){
	
	//	std::cout << "Common::INFO: Blinding bin n." << i_bin << " S/B = " << fTTbarHSum->GetBinContent(i_bin) << " / " << fTotalSum->GetBinContent(i_bin) << " = " << fTTbarHSum->GetBinContent(i_bin) / fTotalSum->GetBinContent(i_bin) << std::endl;
	fDataSum->SetBinContent(i_bin,0.);
	h_blind->SetBinContent(i_bin,fTotalSum->GetBinContent(i_bin)*1000.);
	
      }
      else{
	h_blind->SetBinContent(i_bin,0.);
      }
      
    }
  }
  

  int ngoodbins = nbins - counter_zero;

  /*  float x[10],         xerr[10];                                                                                                                        
  float y_new[10],     y_new_err[10]; 

  float quotient[10],  quotient_err[10];                                                                                                                
  float quotient2[10], quotient_err2[10]; */

  /*  ngoodbins = 10; */

  float x[ngoodbins],         xerr[ngoodbins];
  float y_new[ngoodbins],     y_new_err[ngoodbins];
  float y_nom[ngoodbins],     y_nom_err[ngoodbins];
  //  float y_nom2[ngoodbins],    y_nom_err2[ngoodbins];
  float quotient[ngoodbins],  quotient_err[ngoodbins];
  // float quotient2[ngoodbins], quotient_err2[ngoodbins];
  
  int count_bins = 0;
  
  for(int ibin = 1; ibin <= nbins; ++ibin){
    
    if(fDataSum  -> GetBinContent(ibin) > 0){
      
      x[count_bins]             = fTotalSum -> GetBinCenter(ibin);
      xerr[count_bins]          = 0.0;
      y_nom[count_bins]         = fTotalSum  -> GetBinContent(ibin);

      // take now the total unc that contains the syst error as well if specified!
      if(fPlotErrorBand)
	y_nom_err[count_bins]     = TotalUnc.GetBinError(ibin);
      else
	y_nom_err[count_bins]     = fTotalSum  -> GetBinError(ibin);
      
      y_new[count_bins]         = fDataSum  -> GetBinContent(ibin);
      y_new_err[count_bins]     = fDataSum  -> GetBinError(ibin);
      
      quotient[count_bins]      = 1/y_nom[count_bins]*y_new[count_bins];

      quotient_err[count_bins]  = sqrt(pow(y_nom_err[count_bins]/y_nom[count_bins],2) + pow(y_new_err[count_bins]/y_new[count_bins],2));
	
      
      fRatioUnc -> SetBinContent(ibin, 1.0);
      fRatioUnc -> SetBinError(ibin, quotient_err[count_bins]);

      if(fPlotErrorBand)
	fTotalSum -> SetBinError(ibin, TotalUnc.GetBinError(ibin));

      quotient_err[count_bins]  = 0.0;

      count_bins++;

    }
    
  } 

  TGraphErrors *new_gr  = new TGraphErrors(ngoodbins, x,   y_new,  xerr,    y_new_err);

  new_gr     -> SetLineWidth(2);
  new_gr     -> SetMarkerStyle(20);
  new_gr     -> SetMarkerSize(1.45);
  new_gr     -> SetLineColor(kBlack);
  new_gr     -> SetLineStyle(1);

  TGraphErrors *gr_comp  = new TGraphErrors(ngoodbins, x, quotient,  xerr, quotient_err);

  gr_comp  -> GetXaxis() -> SetLimits(lower_edge, upper_edge);

  // float max   = TMath::MaxElement(ngoodbins, quotient)*1.10;
  // float min   = TMath::MinElement(ngoodbins, quotient)*0.90;

  std::stringstream width;
  if(bin_width > 9.5){
    width << std::setprecision(2) << bin_width;
  }
  else
    width << std::setprecision(1) << bin_width;

  stack0 -> Draw();

  fTotalSum    -> Draw("E2same");
  // fTotalSum2   -> Draw("SAME");

  //  fTotalSumTTH -> Draw("SAME");

 
  //  int is_in_label = x_axis_label.compare(x_axis_label.size(), 5, "[GeV]");

  stack0 -> GetXaxis() -> SetTitle(x_axis_label.c_str());
  //if(is_in_label == 0)
  //  stack0 -> GetYaxis() -> SetTitle(("Events / "+width.str()+" GeV").c_str());
  //else
  //  stack0 -> GetYaxis() -> SetTitle(("Events / "+width.str()).c_str());

  stack0 -> GetYaxis() -> SetTitle("Events");

  stack0 -> GetYaxis() -> SetTitleOffset(1.85);

  stack0 -> GetYaxis() -> SetLabelFont(43);
  stack0 -> GetYaxis() -> SetLabelSize(31);

  stack0 -> GetYaxis() -> SetTitleFont(43);
  stack0 -> GetYaxis() -> SetTitleSize(34);

  stack0 -> GetXaxis() -> SetTitleOffset(2.5);

  float legpos = 0.48;
  float legtop = legpos + 0.255;
  //define the legend...
  //if(histo_name.find( "hadtop_pt_varbin_logscale" ) != std::string::npos){
  //   legpos = 0.63; // This prevents the legend drawing on top of the data for this plot
  //   legtop = legpos + 0.255;
  // }


  TLegend *fLegend = new TLegend(0.585, legpos, legtop, 0.865);


  fLegend -> AddEntry(new_gr,        "Data(#sqrt{s} = 13 TeV)",     "p");
  if(!fUseHF)
    fLegend -> AddEntry(fTTbarSum,                  "t#bar{t}",  "f");
  else{

    fLegend -> AddEntry(fTTLightSum, "t#bar{t}+light",     "f");
    fLegend -> AddEntry(fTTBBSum,    "t#bar{t}+b#bar{b}",  "f");
    fLegend -> AddEntry(fTTCCSum,    "t#bar{t}+c#bar{c}",  "f");

  }
  //fLegend -> AddEntry(fTTbarHFSum,                "t#bar{t}+HF",    "f");
  fLegend -> AddEntry(fTTbarWSum,                 "t#bar{t}+W",     "f");
  fLegend -> AddEntry(fTTbarZSum,                 "t#bar{t}+Z",     "f");
  fLegend -> AddEntry(fOtherTTbarVSum,           "Other t#bar{t}+X/t+X",     "f");
  fLegend -> AddEntry(fWjetsSum,                  "W+jets",         "f");
  fLegend -> AddEntry(fZjetsSum,                  "Z+jets",         "f");
  fLegend -> AddEntry(fDibosonSum,                "Diboson",        "f");
  fLegend -> AddEntry(fSingleTopSum,              "Single top",     "f");
  fLegend -> AddEntry(fQCDSum,                    "Multijet",       "f");
  //  fLegend -> AddEntry(fTotalSum,                  "Uncertainty",    "f");
  if(fPlotErrorBand)
    fLegend -> AddEntry(fRatioUnc,            "Stat.+Syst",  "f");
  else
    fLegend -> AddEntry(fRatioUnc,            "Stat.",  "f");

  fLegend -> SetFillColor(0);
  fLegend -> SetLineColor(0);
  fLegend -> SetBorderSize(0);
  fLegend -> SetTextFont(72);
  fLegend -> SetTextSize(0.035);

  fDataSum -> SetMarkerStyle(20);
  fDataSum -> SetMarkerSize(1);
  fDataSum -> SetLineColor(kBlack);
  //  fDataSum -> Sumw2();
  // fDataSum -> Draw("SAME");
  new_gr   -> Draw("PZSame");

  //h_blind->Multiply(fTotalSum);


  if(fAnalysisType == "ttH"){
   
    h_blind->SetFillColorAlpha(kWhite,0.5);
    h_blind->SetLineColorAlpha(kWhite,0.5);
    h_blind->Draw("hist same");
  
  }

  if (histo_name.find("noLegend") != std::string::npos)
    PrintLegend=false;

  if(PrintLegend == true)
    fLegend -> Draw(); 

  if(histo_name == "Converged")
    stack0 -> SetMaximum(50000.0);
  else
    stack0 -> SetMaximum(nmax);

   if (histo_name.find("logscale") != std::string::npos)
     stack0 -> SetMinimum(1.0);
   else
    stack0 -> SetMinimum(0.1);

  if(histo_name == "Converged")
    pad1 -> SetLogy(1); 
  if (histo_name.find("logscale") != std::string::npos) 
    pad1 -> SetLogy(1);
  // if (histo_name.find("hadtop_pt_varbin_largerbin") != std::string::npos) 
  //   pad1 -> SetLogy(1);
  
  //set labels....
  TLatex l1;
  l1.SetTextAlign(9);
  l1.SetTextSize(0.04);
  l1.SetNDC();
  l1.DrawLatex(0.21, 0.775, (fLumiLabel+" ("+fDataLabel+")").c_str());


  TLatex l2;
  l2.SetTextAlign(9);
  l2.SetTextFont(72);
  l2.SetTextSize(0.04);
  l2.SetNDC();
  l2.DrawLatex(0.21, 0.880, "ATLAS");
  TLatex l3;
  l3.SetTextAlign(9);
  l3.SetTextSize(0.04);
  l3.SetNDC();
  l3.DrawLatex(0.21, 0.835, "Internal");
  TLatex l4;
  l4.SetTextAlign(9);
  l4.SetTextSize(0.040);
  l4.SetTextFont(72);
  l4.SetNDC();
  TLatex l4b;
  l4b.SetTextAlign(9);
  l4b.SetTextSize(0.04);
  l4b.SetNDC();
  l4b.DrawLatex(0.21, 0.72, fChannelLabel.c_str());
  
  l4.DrawLatex(0.515, 0.89, (fJetBinLabel+", "+fBtagLabel).c_str());

  TLatex l51;
  l51.SetTextAlign(9);
  l51.SetTextSize(0.04);
  l51.SetNDC();
  //l51.DrawLatex(0.21, 0.68, fDataLabel.c_str());

  std::stringstream oss_KS;
  oss_KS   << setprecision(3) << KS;

  //std::stringstream oss_chi2;
  //oss_chi2 << setprecision(3) << chi2;

  TLatex l5;
  l5.SetTextAlign(9);
  l5.SetTextSize(0.038);
  l5.SetNDC();
  l5.DrawLatex(0.705, 0.95, ("KS-Prob: "    + oss_KS.str()).c_str());
  //l5.DrawLatex(0.205, 0.95, ("#chi2-Prob: " + oss_chi2.str()).c_str());

  TLine line2;

  if(histo_name == "hist_met_et"){
  
    line2 = TLine(30.0, 0.0, 30.0, nmax*0.7);
    line2.SetLineColor(kRed);
    line2.SetLineStyle(2);
    line2.SetLineWidth(3);

    TLatex l3b;
    l3b.SetTextAlign(9);
    l3b.SetTextSize(0.046);
    l2.SetTextFont(72);
    l3b.SetNDC();
  
  }

  
  pad2->cd();

  if(ResidualFlag){

    // gr_comp, fRatioUnc

    //    fRatioUnc -> SetFillStyle(1001);
    //  fRatioUnc -> SetLineColor(kYellow-7);
    // fRatioUnc -> SetFillColor(kYellow-7);
    fRatioUnc -> SetFillStyle(3354);
    fRatioUnc -> SetFillColor(kBlack);
    fRatioUnc -> SetLineWidth(0);
    fRatioUnc -> SetMarkerStyle(0);

    gr_comp -> SetTitle("");
    gr_comp -> SetMarkerColor(1);
    gr_comp -> SetMarkerStyle(20);
    gr_comp -> SetMarkerSize(1.3);

    fRatioUnc -> GetXaxis() -> SetLabelFont(43);
    fRatioUnc -> GetYaxis() -> SetLabelFont(43);
    fRatioUnc -> GetXaxis() -> SetLabelSize(31);
    fRatioUnc -> GetYaxis() -> SetLabelSize(31);
    fRatioUnc -> GetXaxis() -> SetLabelOffset(0.01);
    fRatioUnc -> GetYaxis() -> SetTitleFont(43);
    fRatioUnc -> GetXaxis() -> SetTitleFont(43);
    fRatioUnc -> GetXaxis() -> SetTitleSize(34);
    fRatioUnc -> GetYaxis() -> SetTitleSize(33);
    fRatioUnc -> GetXaxis() -> SetTitleOffset(4.85);
    fRatioUnc -> GetYaxis() -> SetTitleOffset(1.85);
    fRatioUnc -> GetYaxis() -> SetTitle("Data/MC");
    fRatioUnc -> GetXaxis() -> SetNdivisions(505);
    fRatioUnc -> GetYaxis() -> SetNdivisions(505);
    fRatioUnc -> GetYaxis() -> CenterTitle(true);

    x_axis_label = GetXAxisLabel(x_axis_label);

    fRatioUnc -> GetXaxis() -> SetTitle(x_axis_label.c_str());
    fRatioUnc -> SetMaximum(1.5);
    fRatioUnc -> SetMinimum(0.5);

  }

  fRatioUnc -> Draw("E2");

  gr_comp  -> Draw("PSame");

  // gr_comp  -> Draw("AP");

  TF1 *norm1 = new TF1("fa1","1", lower_edge, upper_edge);
  norm1 -> SetLineColor(kRed);
  norm1 -> SetLineStyle(1);
  norm1 -> SetLineWidth(2);
  norm1->Draw("Same");

  pad1 -> cd();
  c0   -> Draw();

  
  if(PrintLegend == true){
    
    c0 -> Print((outputFile+".eps").c_str());
    c0 -> Print((outputFile+".pdf").c_str());
    c0 -> Print((outputFile+".png").c_str());
    //c0 -> Print((outputFile+".pdf").c_str());

  }
  else{ 
    c0 -> Print((outputFile+"_woLegend.eps").c_str());
    c0 -> Print((outputFile+"_woLegend.pdf").c_str());
    c0 -> Print((outputFile+"_woLegend.png").c_str());
    // c0 -> Print((outputFile+"_woLegend.pdf").c_str());

  }

  // geklaut von Anna
  std::string teps = (outputFile+".eps").c_str();
  std::string tpng = (outputFile+".png").c_str();
  std::string tpdf = (outputFile+".pdf").c_str();

  std::string tsep = (outputFile+"_Sep.png").c_str();
  std::string tove = (outputFile+"_overlap.png").c_str();

  //  r.replace( r.find( "Fuchs"), strlen("Fuchs"), "Hund");

  tpng.replace(tpng.find(fOutputFolder.c_str()), strlen(fOutputFolder.c_str()), "");
  tpng.replace(tpng.find("/"),                   strlen("/"),                   "");

  tsep.replace(tsep.find(fOutputFolder.c_str()), strlen(fOutputFolder.c_str()), "");
  tsep.replace(tsep.find("/"),                   strlen("/"),                   "");

  tove.replace(tove.find(fOutputFolder.c_str()), strlen(fOutputFolder.c_str()), "");
  tove.replace(tove.find("/"),                   strlen("/"),                   "");


  std::string title = x_axis_label;

  std::ofstream* htmlfile = new std::ofstream();

  //  std::cout << "Open again: " << (_htmls[fHTMLLabel]).c_str() << std::endl;

  if(PrintLegend){

    htmlfile->open((_htmls[fHTMLLabel]).c_str(),fstream::app);
    // if normal, print tab begin
    if(strstr(title.c_str(), "norm") == NULL)
      *htmlfile << "<tr>"
		<< "<td>"<<title<<"</td>";
    
    *htmlfile << "<td>";
    *htmlfile << "<p><a href=\"" << tpng.c_str() << "\">";
    *htmlfile << "<img src=\""   << tpng.c_str() << "\" width=\"70%\"/>";
    *htmlfile << "</td>";

    if(fAnalysisType == "ttH"){

      if(histo_name=="hist_resolved_overlap"){
	*htmlfile << "<td>";
	*htmlfile << "<p><a href=\"" << tove.c_str() << "\">";
	*htmlfile << "<img src=\""   << tove.c_str() << "\" width=\"80%\"/>";
	*htmlfile << "</td>" << std::endl;
      }
      else{
	*htmlfile << "<td>";
	*htmlfile << "<p><a href=\"" << tsep.c_str() << "\">";
	*htmlfile << "<img src=\""   << tsep.c_str() << "\" width=\"80%\"/>";
	*htmlfile << "</td>" << std::endl;
      }

    }

    if(strstr(title.c_str(), "norm") != NULL)
      *htmlfile << "</tr>"<< std::endl;
    htmlfile->close();
    
  }

  delete stack0;
  //  delete fLegend;
  delete c0;

  delete htmlfile;

  if(fAnalysisType == "ttH"){

    PlotSeparation(*fTTbarHSum, *fTotalSum, outputFile, x_axis_label);
    if(histo_name=="hist_resolved_overlap"){
      PlotOverlap(*fTTbarHSum, *fTotalSum, x_axis_label, outputFile);

      if(fJetBin=="4incl" && fBTagBin ==  "2incl" && fVetoJetBin=="none"){
	// std::cout << "resolved..................." << std::endl;
	string pathToConf         = gSystem->Getenv("PWD");
	std::string ResolvedFileName = pathToConf+"/../data/Resolved_Overlay.root";
	TFile *ResolvedFile = new TFile(ResolvedFileName.c_str(), "RECREATE");
	fTTbarHSum->SetName("resolved_sig");
	fTTbarHSum->Write("resolved_sig",TObject::kWriteDelete);
	fTotalSum->SetName("resolved_bkg");
	fTotalSum->Write("resolved_bkg",TObject::kWriteDelete);
	ResolvedFile->Close();
      }
    }
  }

}


TH1D* ControlPlots::AddHistos(std::string histo_name, std::vector<std::string> List, std::string SystName, bool DoReplace)
{

  TH1D fHelpHist1,fHelpHist2;
  
  for(unsigned int iFile = 0; iFile < List.size(); ++iFile){
  
    std::string FilePath = List[iFile].c_str();

    //    std::cout << "before = " << FilePath.c_str() << std::endl;

    if(DoReplace)
      FilePath.replace(FilePath.find("nominal"), strlen("nominal"), SystName.c_str());
		       
    TFile *histo_file    = TFile::Open(FilePath.c_str());

    //    std::cout << histo_name.c_str() << std::endl;

    fHelpHist2 = *((TH1D*) histo_file -> Get(histo_name.c_str()));

    // zero any negative bins we find (ie QCD)
    for(int i = 1; i <= fHelpHist2.GetNbinsX(); ++i){
      if (fHelpHist2.GetBinContent(i) < 0){
	fHelpHist2.SetBinContent(i, 0);
      }
    }
      
    if(iFile == 0)
      fHelpHist1 = fHelpHist2;
    else
      fHelpHist1 = fHelpHist1 + fHelpHist2;

    delete histo_file;
    
  }
  
  int nbin = fHelpHist1.GetNbinsX();

  TH1D *fOutput = new TH1D(fHelpHist1);

  for(int i = 1; i <= nbin; ++i){

    float content = fHelpHist1.GetBinContent(i);
    fOutput -> SetBinContent(i, content);
  }

  return fOutput;
  
}

TH2D* ControlPlots::AddHistos2D(std::string histo_name, std::vector<std::string> List)
{

  TH2D fHelpHist1,fHelpHist2;   

//  fHelpHist1.Sumw2();
//  fHelpHist2.Sumw2();

  for(unsigned int iFile = 0; iFile < List.size(); ++iFile)
    {
    TFile *histo_file = TFile::Open(List[iFile].c_str());
    
    fHelpHist2 = *((TH2D*)histo_file -> Get(histo_name.c_str()));
    
      
    if(iFile == 0)
      fHelpHist1 = fHelpHist2;
    else
      fHelpHist1 = fHelpHist1 + fHelpHist2;
    
    //    std::cout << iFile << "\t" << fHelpHist2.Integral() << std::endl;

    delete histo_file;
    
  }
  
  // not sure what I was doing here, surely this is wrong...
  int nbin = fHelpHist1.GetNbinsX();

  TH2D *fOutput = new TH2D(fHelpHist1);

  for(int i = 1; i <= nbin; ++i){

    float content = fHelpHist1.GetBinContent(i);
//    float err     = fHelpHist1.GetBinError(i);

    fOutput -> SetBinContent(i, content);
//    fOutput -> SetBinError(i,   err);

  }
     
  return fOutput;
    
 }


void ControlPlots::PlotOverlap(TH1D sig, TH1D bkg, std::string x_axis_label, std::string outputFile)
{

  string pathToConf         = gSystem->Getenv("PWD");
  std::string ResolvedFileName = pathToConf+"/../data/Resolved_Overlay.root";

  TFile *f = new TFile(ResolvedFileName.c_str()); 
  TH1F * res_sig = (TH1F*)f->Get("resolved_sig");
  TH1F * res_bkg = (TH1F*)f->Get("resolved_bkg");

  res_bkg->GetXaxis()->SetBinLabel(1,"4j, 2b");
  res_bkg->GetXaxis()->SetBinLabel(2,"5j, 2b");
  res_bkg->GetXaxis()->SetBinLabel(3,"#geq 6j, 2b");
  res_bkg->GetXaxis()->SetBinLabel(4,"4j, 3b");
  res_bkg->GetXaxis()->SetBinLabel(5,"5j, 3b");
  res_bkg->GetXaxis()->SetBinLabel(6,"#geq 6j, 3b");
  res_bkg->GetXaxis()->SetBinLabel(7,"4j, #geq 4b");
  res_bkg->GetXaxis()->SetBinLabel(8,"5j, #geq 4b");
  res_bkg->GetXaxis()->SetBinLabel(9,"#geq 6j, #geq 4b");
  res_bkg->GetXaxis()->SetBinLabel(10,"OTHER");
  res_bkg->LabelsOption("u");

  res_bkg->GetXaxis()->SetLabelSize(0.06);
  res_bkg->GetXaxis()->SetLabelOffset(0.008);
  res_bkg->GetXaxis()->SetTitleOffset(1.5);
  res_bkg->GetXaxis()->SetTitle("Resolved Analysis Regions");
  res_bkg->GetYaxis()->SetTitleSize(0.055);
  res_bkg->GetYaxis()->SetTitleOffset(0.85);
  res_sig->GetYaxis()->SetTitleSize(0.06);
  res_sig->GetYaxis()->SetTitleOffset(0.8);
  res_sig->GetYaxis()->SetLabelSize(0.06);

  SetAtlasStyle();

  // define canvas

  for(int i = 1; i <= sig.GetNbinsX(); ++i)
    bkg.SetBinError(i, 0);

  TCanvas *c1 = new TCanvas("", "", 1200, 1200);
  TPad *pad1 = new TPad("pad1","pad1", 0.0, 0.53, 1.0, 0.99);
  TPad *pad2 = new TPad("pad2","pad2", 0.0, 0.010, 1.0, 0.53);

  // define plotting styles
  pad1->SetBottomMargin(0.005);
  pad2->SetTopMargin(0.005);
  //pad2->SetBottomMargin(0.01);
  pad1->SetBorderMode(0);
  pad2->SetBorderMode(0);
  //pad2->SetBottomMargin(0.4);

  pad1->SetTicks(1,1);
  pad2->SetTicks(1,1);
  pad1->Draw();
  pad2->Draw();


  int nBins = sig.GetNbinsX();

  for(int iBin = 1; iBin <= nBins; ++iBin)
    sig.SetBinError(iBin, 0);

  sig.SetFillColor(kRed);
  sig.SetMarkerStyle(21);
  sig.SetFillStyle(1001);
  sig.SetLineColor(kRed);

  bkg.SetFillColor(kBlue);
  bkg.SetMarkerStyle(21);
  bkg.SetFillStyle(1001);
  bkg.SetLineColor(kBlue);

  res_sig->SetFillColor(kRed);
  res_sig->SetMarkerStyle(21);
  res_sig->SetFillStyle(3354);
  res_sig->SetLineColor(kRed);

  res_bkg->SetFillColor(kBlue);
  res_bkg->SetMarkerStyle(21);
  res_bkg->SetFillStyle(3354);
  res_bkg->SetLineColor(kBlue);


  std::string x_axis = GetXAxisLabel(x_axis_label);

  sig.GetXaxis() -> SetTitle(x_axis.c_str());
  res_sig->GetYaxis() -> SetTitle("Signal Yield");
  res_bkg->GetYaxis() -> SetTitle("Background Yield");


  sig.SetMinimum(0.0);

  pad1->cd();
  res_sig->Draw("hist");
  sig.Draw("same");
  pad2->cd();
  res_bkg->Draw("hist");
  bkg.Draw("same");

  // define TLegend

  string signalLabel = "Boosted Analysis Signal";
  string backgroundLabel = "Boosted Analysis Bkg.";

  float legendx = 0.62;

  if(fVetoJetBin != "none"){
    signalLabel = "Resolved-minus-boosted Analysis Signal";
    backgroundLabel = "Resolved-minus-boosted Analysis Bkg.";
    legendx = 0.45;
  }

  bool showoverlay = true;
  if(fJetBin=="4incl" && fBTagBin ==  "2incl" && fVetoJetBin=="none"){
    showoverlay = false;
    signalLabel = "Resolved Analysis Signal";
    backgroundLabel = "Resolved Analysis Bkg.";
  }
  

  TLegend *fLegend = new TLegend(legendx, 0.68, legendx+0.275, 0.85);

  if(showoverlay) fLegend -> AddEntry(res_sig,  "Resolved Analysis Signal",   "f");
  fLegend -> AddEntry(&sig,  signalLabel.c_str(),   "f");
  if(showoverlay) fLegend -> AddEntry(res_bkg,  "Resolved Analysis Bkg.", "f");
  fLegend -> AddEntry(&bkg,  backgroundLabel.c_str(), "f");
  fLegend -> SetFillColor(0);
  fLegend -> SetLineColor(0);
  fLegend -> SetBorderSize(0);
  fLegend -> SetTextFont(42);
  fLegend -> SetTextSize(0.035);
  fLegend -> Draw("SAME");

  // get lables and write them
  pad1->cd();
  //set labels....
  TLatex l2;
  l2.SetTextAlign(9);
  l2.SetTextFont(72);
  l2.SetTextSize(0.06);
  l2.SetNDC();
  l2.DrawLatex(0.83, 0.860, "ATLAS");
  TLatex l3;
  l3.SetTextAlign(9);
  l3.SetTextSize(0.06);
  l3.SetNDC();
  l3.DrawLatex(0.73, 0.81, "work in progress");
  TLatex l4;
  l4.SetTextAlign(9);
  l4.SetTextSize(0.040);
  l4.SetTextFont(72);
  l4.SetNDC();
  TLatex l5;
  l5.SetTextAlign(9);
  l5.SetTextSize(0.040);
  l5.SetTextFont(72);
  l5.SetNDC();
  TLatex l4b;
  l4b.SetTextAlign(9);
  l4b.SetTextSize(0.04);
  l4b.SetNDC();

  l4.DrawLatex(0.625, 0.75, (fJetBinLabel+"(additional), "+fBtagLabel).c_str());
  l5.DrawLatex(0.705, 0.71, (fLargeJetLabel+", "+fTopTagLabel).c_str());
  //l4b.DrawLatex(0.21, 0.795, fChannelLabel.c_str());


  c1 -> Print((outputFile+"_overlap.eps").c_str());
  c1 -> Print((outputFile+"_overlap.pdf").c_str());
  c1 -> Print((outputFile+"_overlap.png").c_str());




}

void ControlPlots::WriteEventYield(bool flag)
{

  std::ofstream* htmlfile = new std::ofstream();

  htmlfile->open((_htmls[fHTMLLabel]).c_str(),fstream::app);

  *htmlfile << "<tr> <td>"<< " EventYield" << " <td> "<< " " << std::endl;  //"</td>";
  *htmlfile << "<table border style=\"float&#58;left;margin-right:7cm;margin-left:5cm;\"> " << std::endl;

  float SumBkg     = 0.0;
  float SumBkgUnc2 = 0.0;

  int nAllProc = fAllProc.size();

  std::vector<float> yields;

  int i = 0;

  if(fAnalysisType == "ttbar")
    i = 1;

  for(int startBkg = 0; startBkg < nAllProc; ++startBkg){
   
    float unc   = GetHistoUncertainty(fAllProc[startBkg], 1.0);
    float value = fAllProc[startBkg].Integral();
    
    if(startBkg >= i){
      SumBkg     += value;
      SumBkgUnc2 += unc*unc;
    }

    if(fAnalysisType == "ttbar")
      *htmlfile << std::fixed << std::setprecision(4) << "<tr>  <td> " << fAllProcNames[startBkg]  << " <td> " << value << " <td> " << unc << std::endl;
    else
      *htmlfile << "<tr>  <td> " << fAllProcNames[startBkg]  << " <td> " << value << " <td> " << unc << std::endl;
    

    yields.push_back(value);

  }

  float SignalExp    = 0.0;
  float SignalExpUnc = 0.0;

  if(fAnalysisType == "ttbar"){
    SignalExp    = fTTbarSum -> Integral();
    SignalExpUnc = GetHistoUncertainty(*fTTbarSum, 1.0);
  }

  if(fAnalysisType == "ttH"){

    SignalExp    = fTTbarHSum -> Integral();
    SignalExpUnc = GetHistoUncertainty(*fTTbarHSum, 1.0); 

    *htmlfile << "<tr>  <td> Sum bkg <td> "            << SumBkg << " <td> " << sqrt(SumBkgUnc2) << std::endl;
    *htmlfile << "<tr bgcolor=pink>  <td> Data  <td> " << fDataSum -> Integral() << " <td> " << std::endl;
    *htmlfile << "<tr> <td> ttH exp. <td> " << SignalExp << " <td> " << SignalExpUnc << std::endl;
    *htmlfile << "</tr>"    << std::endl;
    *htmlfile << "</table>" << std::endl;
    *htmlfile << "</td>"    << std::endl;
 
  }
  else{
    *htmlfile <<std::fixed << std::setprecision(2) << "<tr>  <td> Sum sig+bkg <td> "        << SignalExp+SumBkg << " <td> " << sqrt(SumBkgUnc2 + SignalExpUnc*SignalExpUnc) << std::endl;
    *htmlfile <<std::fixed << std::setprecision(0) <<  "<tr bgcolor=pink>  <td> Data  <td> " << fDataSum -> Integral() << " <td> " << std::endl;
    *htmlfile <<std::fixed << std::setprecision(2) << "<tr>  <td> Data/Prediction <td> "        << fDataSum->Integral()/(SignalExp+SumBkg) << " <td> " << std::endl;

    *htmlfile << "</tr>"    << std::endl;
    *htmlfile << "</table>" << std::endl;
    *htmlfile << "</td>"    << std::endl;


  }

  std::string SigBkgName     = "SigBkg.png";
  std::string SigSqrtBkgName = "SigSqrtBkg.png";

  if(fAnalysisType == "ttH")
    PlotSignalOverBkg(fOutputFolder, fTTbarHSum -> Integral(), SumBkg);

  std::string PieLabel = (fChannelLabel).c_str();
  MakePieChart(fOutputFolder, PieLabel, yields, fUseHF);

  std::string PieName   = "PieChart.png";
  std::string PieLegend = "PieChartLegend.png";

  *htmlfile << "<tr> <td> Background pie chart ";
  *htmlfile << "<td>";
  *htmlfile << "<p><a href=\"" << PieName.c_str() << "\">";
  *htmlfile << "<img src=\""   << PieName.c_str() << "\" width=\"70%\"/>";
  *htmlfile << "</td>";

  if(fAnalysisType == "ttH"){

    *htmlfile << "<td>";
    *htmlfile << "<p><a href=\"" << PieLegend.c_str() << "\">";
    *htmlfile << "<img src=\""   << PieLegend.c_str() << "\" width=\"80%\"/>";
    *htmlfile << "</td>" << std::endl; 

    *htmlfile << "<tr> <td> Signal over Bkg  </td>";
    *htmlfile << "<td>";
    *htmlfile << "<p><a href=\"" << SigBkgName.c_str() << "\">";
    *htmlfile << "<img src=\""   << SigBkgName.c_str() << "\" width=\"70%\"/>";
    *htmlfile << "</td>";
    *htmlfile << "<td>";
    *htmlfile << "<p><a href=\"" << SigSqrtBkgName.c_str() << "\">";
    *htmlfile << "<img src=\""   << SigSqrtBkgName.c_str() << "\" width=\"80%\"/>";
    *htmlfile << "</td>" << std::endl;
    
  }

  htmlfile -> close();

  if(flag){

    std::ofstream fOut;
    
    fOut.open((fOutputFolder+"/EventYield.tex").c_str(), std::ios::out );//| std::ios::app );
    
    fOut << "\\begin{tabular}{l|r|r}"    << std::endl;
    fOut << "\\hline"                    << std::endl;
    fOut << "\\hline"                    << std::endl;
    fOut << "Process & Events & Stat. unc. \\\\ " << std::endl;
    fOut << "\\hline"                             << std::endl;

    // ASDF

    for(int startBkg = 0; startBkg < nAllProc; ++startBkg){

      float unc   = GetHistoUncertainty(fAllProc[startBkg], 1.0);
      float value = fAllProc[startBkg].Integral();

       if(fAnalysisType == "ttbar")
	 fOut << std::fixed << std::setprecision(4) << fAllProcNames[startBkg] << " & " << value << " & " << unc << "\\\\" << std::endl;
      else
	fOut << fAllProcNames[startBkg] << " & " << std::setprecision(6) << value << " & " << std::setprecision(5) << unc << "\\\\" << std::endl; 

    }

    float SB        = SignalExp/SumBkg;
    float SsqrtB    = SignalExp/sqrt(SumBkg);

    if(fAnalysisType == "ttH"){

      fOut << "\\hline"                              << std::endl;
      fOut << "Total bkg & " << std::setprecision(6) << SumBkg << " & " << std::setprecision(6) << sqrt(SumBkgUnc2) << "\\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;
      fOut << "Data & "      << std::setprecision(6) << fDataSum -> Integral()       << " & \\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;
      fOut << "ttH exp. & " << std::setprecision(6) << SignalExp <<  " & " << std::setprecision(6) << SignalExpUnc << "\\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;
      fOut << "S/Bkg & "            << std::setprecision(4) << SB       << " & \\\\" << std::endl;
      fOut << "S/sqrt(Bkg) & "      << std::setprecision(4) << SsqrtB   << " & \\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;
      fOut << "\\end{tabular}"                       << std::endl;
    
    }
    else{

      fOut << "\\hline"                              << std::endl;
      fOut <<std::fixed << std::setprecision(4) <<  "Total sig+bkg & " << SignalExp+SumBkg << " & "  << sqrt(SumBkgUnc2 + SignalExpUnc*SignalExpUnc) << "\\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;
      fOut << std::fixed << std::setprecision(0) <<  "Data & "  << fDataSum -> Integral()       << " & \\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;

      fOut <<std::fixed << std::setprecision(4) <<  "Data/Pred. &" << fDataSum->Integral() / (SignalExp + SumBkg) << "& \\\\" << std::endl; 
      // fOut << "S/Bkg & "            << std::setprecision(6) << SB       << " & \\\\" << std::endl;
      // fOut << "S/sqrt(Bkg) & "      << std::setprecision(6) << SsqrtB   << " & \\\\" << std::endl;
      fOut << "\\hline"                              << std::endl;
      fOut << "\\end{tabular}"                       << std::endl;


    }

    fOut.close();

    /*    std::ofstream fOut2;

    fOut2.open((fOutputFolder+"/EventYield5fb.tex").c_str(), std::ios::out | std::ios::app );

    fOut2 << "\\begin{tabular}{l|r|r}"    << std::endl;
    fOut2 << "\\hline"                    << std::endl;
    fOut2 << "\\hline"                    << std::endl;
    fOut2 << "Process & Events & Stat. unc. \\\\ " << std::endl;
    fOut2 << "\\hline"                             << std::endl;

    float SumBkgScaled    = 0.0;
    float SumBkgUncScaled = 0.0;

    for(int startBkg = i; startBkg < nAllProc; ++startBkg){

      float unc   = GetHistoUncertainty(fAllProc[startBkg], 5000.0/fLumi);
      float value = fAllProc[startBkg].Integral()*5000.0/fLumi;

      SumBkgScaled    += value;
      SumBkgUncScaled += unc;

      fOut2 << fAllProcNames[startBkg] << " & " << std::setprecision(6) << value << " & " << std::setprecision(6) << unc << "\\\\" << std::endl;

    }

    fOut2 << "\\hline"                              << std::endl;
    fOut2 << "Total bkg & " << std::setprecision(6) << SumBkgScaled << " & " << std::setprecision(6) << sqrt(SumBkgUncScaled) << "\\\\" << std::endl;
    fOut2 << "\\hline"                              << std::endl;
    fOut2 << "ttH exp. & " << std::setprecision(6) << SignalExp*5000.0/fLumi <<  " & " << std::setprecision(6) << SignalExpUnc << "\\\\" << std::endl;
    fOut2 << "S/Bkg & "            << std::setprecision(6) << SB       << " & \\\\" << std::endl;
    fOut2 << "S/sqrt(Bkg) & "      << std::setprecision(6) << SsqrtB       << " & \\\\" << std::endl;
    fOut2 << "\\hline"                              << std::endl;
    fOut2 << "\\end{tabular}"                       << std::endl;
    fOut2.close();
    */
    
  
  }
  
}

void ControlPlots::createHTML(std::string name){

  // create the page
  std::ofstream page;
  std::string pname = fOutputFolder+"/"+fHTMLLabel;

  struct stat buf;
  if(stat(pname.c_str(), &buf) == -1){

    page.open(pname.c_str());
    
    // main
    if(name == "index"){
      
      page << "<html><head><title> Comparison of different MC truth settings </title></head>" << std::endl;
      page << "<body>" << std::endl;
      page << "<h1> Comparison of different ttbar modelling </h1>" << std::endl;
      page << "<a href=" << fHTMLLabel.c_str() << "> >=4 jets </a>"<< std::endl;
      
      page <<"<br> <br>" << std::endl;
    }

    if(fAnalysisType == "ttH"){

      page << "<table border = 1> <tr>"
	   << "<th> name </th>"
	   << "<th> variable </th>"
	   << "<th> variable separation </th>"
	   << "</tr>" << std::endl;

    }
    else{

      page << "<table border = 1> <tr>"
           << "<th> name </th>"
           << "<th> variable </th>"
	   << "</tr>" << std::endl;

    }

    _htmls[fHTMLLabel] = pname;
    page.close();

  }

}

std::string ControlPlots::GetYAxisLabel(std::string label)
{

 if(label == "matrix_hadtopjet_truthhadtopjet_pt_varbin") return "Particle Level p_{T}^{t,had}";
 if(label == "matrix_hadtopjet_truthhadtopjet_pt_varbin_largerbin") return "Particle Level p_{T}^{t,had}  ";
 if(label == "matrix_hadtopjet_truthhadtopjet_pt_varbin_lessbin") return "Particle Level p_{T}^{t,had}";
 
 if(label == "matrix_hadtopjet_truthhadtopjet_y_varbin") return "Particle Level |y^{t,had}|";
 if(label == "matrix_hadtopjet_truthhadtopjet_absy_varbin") return "Particle Level |y^{t,had}|";
 if(label == "matrix_hadtopjet_truthhadtopjet_absy_varbin_largerbin") return "Particle Level |y^{t,had}|";

 if(label == "matrix_hadtopjet_truthhadtopjet_y") return "Particle Level y^{t,had}";
 if(label == "matrix_hadtopjet_truthhadtopjet_absy") return "Particle Level |y^{t,had}|";

 if(label == "matrix_hadtopjet_truthtopparton_y") return "Parton Level y^{t,had}";
 if(label == "matrix_hadtopjet_truthtopparton_absy") return "Parton Level |y^{t,had}|";
 if(label == "matrix_truthhadtopjet_truthtopparton_y") return "Parton Level y^{t,had}";
 if(label == "matrix_truthhadtopjet_truthtopparton_absy") return "Parton Level |y^{t,had}|";


  return label;

}


std::string ControlPlots::GetXAxisLabel(std::string label)
{

  if(label == "klfitter_bestPerm_topLep_pt")  return "leptonic top p_{T} [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_topLep_eta") return "leptonic top #eta (KLFitter)";
  if(label == "klfitter_bestPerm_topLep_phi") return "leptonic top #phi (KLFitter)";
  if(label == "klfitter_bestPerm_topLep_E")   return "leptonic top E [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_topLep_m")   return "leptonic top mass [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_topHad_pt")  return "hadronic top p_{T} [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_topHad_eta") return "hadronic top #eta (KLFitter)";
  if(label == "klfitter_bestPerm_topHad_phi") return "hadronic top #phi (KLFitter)";
  if(label == "klfitter_bestPerm_topHad_E")   return "hadronic top E [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_topHad_m")   return "hadronic top mass [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_ttbar_pt")   return "t#bar{t} p_{T} [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_ttbar_eta")  return "t#bar{t} #eta (KLFitter)";
  if(label == "klfitter_bestPerm_ttbar_phi")  return "t#bar{t} #phi (KLFitter)";
  if(label == "klfitter_bestPerm_ttbar_E")    return "t#bar{t} E [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_ttbar_m")    return "t#bar{t} mass [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_top_pt")  return "reco. top p_{T} [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_top_eta") return "reco. top #eta (KLFitter)";
  if(label == "klfitter_bestPerm_top_phi") return "reco. top #phi (KLFitter)";
  if(label == "klfitter_bestPerm_top_E")   return "reco. top E [GeV] (KLFitter)";
  if(label == "klfitter_bestPerm_top_m")   return "reco. top mass [GeV] (KLFitter)";

  
  if(label == "met_sumet")   return "Sum E_{T} [GeV]";
  if(label == "met_met")     return "Missing E_{T} [GeV]";
  if(label == "met_phi")     return "#phi(Missing E_{T})";
  if(label == "mwt")         return "m_{T,W} [GeV]";
  if(label == "pvxp_n")      return "Number of primary vertices";
  if(label == "mu")          return "Number of interactions per bunch crossing";
  if(label == "mu_pt")       return "Muon p_{T} [GeV]";
  if(label == "mu_eta")      return "Muon #eta";
  if(label == "mu_phi")      return "Muon #phi";
  if(label == "mu_charge")   return "Muon charge";
  if(label == "mu_n")        return "Number of Muons";
  if(label == "el_pt")       return "Electron p_{T} [GeV]";
  if(label == "el_eta")      return "Electron #eta";
  if(label == "el_phi")      return "Electron #phi";
  if(label == "el_charge")   return "Electron charge";
  if(label == "el_n")        return "Number of Electrons";
  if(label == "jet_n")       return "Number of Jets";
  if(label == "nBTags")      return "Number of tagged jets (MV2c10 @ 70%)";
  if(label == "num_jet_40")  return "Number of Jets (40 GeV)";
  if(label == "num_jet_60")  return "Number of Jets (60 GeV)";
  if(label == "num_jet_80")  return "Number of Jets (80 GeV)"; 
  if(label == "num_MV1_60")  return "Number of tagged jets (60% WP)";
  if(label == "num_MV1_70")  return "Number of tagged jets (70% WP)";
  if(label == "num_MV1_80")  return "Number of tagged jets (80% WP)";
  if(label == "jet0_pt")     return "Leading jet p_{T} [GeV]";
  if(label == "jet0_eta")    return "Leading jet #eta";
  if(label == "jet0_phi")    return "Leading jet #phi";
  if(label == "ljet0_pt")     return "Leading Large Jet p_{T} [GeV]";
  if(label == "ljet0_eta")    return "Leading Large Jet #eta";
  if(label == "ljet0_phi")    return "Leading Large Jet #phi";
  if(label == "ljet0_m")     return "Leading Large Jet Mass [GeV]";
  if(label == "ljet_top0_pt")     return "Leading Large Top Jet p_{T} [GeV]";
  if(label == "ljet_top0_eta")    return "Leading Large Top Jet #eta";
  if(label == "ljet_top0_phi")    return "Leading Large Top Jet #phi";
  if(label == "ljet_top0_m")     return "Leading Large Top Jet Mass [GeV]";
  if(label == "jet1_pt")     return "Second jet p_{T} [GeV]";
  if(label == "jet1_eta")    return "Second jet #eta";
  if(label == "jet1_phi")    return "Second jet #phi";
  if(label == "jet2_pt")     return "3rd jet p_{T} [GeV]";
  if(label == "jet2_eta")    return "3rd jet #eta";
  if(label == "jet2_phi")    return "3rd jet #phi";
  if(label == "jet3_pt")     return "4th jet p_{T} [GeV]";
  if(label == "jet3_eta")    return "4th jet #eta";
  if(label == "jet3_phi")    return "4th jet #phi";
  if(label == "jet0_pt_noJ")     return "Leading jet p_{T} [GeV]";
  if(label == "jet0_eta_noJ")    return "Leading jet #eta";
  if(label == "jet0_phi_noJ")    return "Leading jet #phi";
  if(label == "jet1_pt_noJ")     return "Second jet p_{T} [GeV]";
  if(label == "jet1_eta_noJ")    return "Second jet #eta";
  if(label == "jet1_phi_noJ")    return "Second jet #phi";
  if(label == "jet2_pt_noJ")     return "3rd jet p_{T} [GeV]";
  if(label == "jet2_eta_noJ")    return "3rd jet #eta";
  if(label == "jet2_phi_noJ")    return "3rd jet #phi";
  if(label == "jet3_pt_noJ")     return "4th jet p_{T} [GeV]";
  if(label == "jet3_eta_noJ")    return "4th jet #eta";
  if(label == "jet3_phi_noJ")    return "4th jet #phi";
  if(label == "pT_jet5")     return "5th jet p_{T} [GeV]";
  if(label == "pT_jet5_noJ")     return "5th jet p_{T} [GeV]";
  if(label == "jet_eta")     return "Jet #eta";
  if(label == "jet_phi")     return "Jet #phi";
  if(label == "jet_pt")      return "Jet p_{T} [GeV]";
  if(label == "jet_eta_noJ")     return "Jet #eta";
  if(label == "jet_phi_noJ")     return "Jet #phi";
  if(label == "jet_pT_noJ")      return "Jet p_{T} [GeV]";
  if(label == "jet_mv2c10")  return "MV2c10 @ 70%";
  if(label == "jet_jvt")     return "Jet JVT value";
  if(label == "jetEtaMax")   return "Max. jet #eta";
  if(label == "jetEtaMin")   return "Min. jet #eta";
  if(label == "MaxDelta_lj") return "Max. #Delta(lep,jet)";
  if(label == "dR_lj0")      return "#Delta R(lep,j1)";
  if(label == "dR_lj_min")   return "#Delta R(lep,jet_{min})";
  if(label == "HhadT_jets")  return "H_{T}^{had} [GeV]";
  if(label == "HhadT_jets_J")  return "H_{T}^{all jets} [GeV]";
  if(label == "HhadT_jets_noJ")  return "H_{T}^{small jets} [GeV]";
  if(label == "HT")          return "H_{T} [GeV]";
  if(label == "HT_all")      return "H_{T}^{all} [GeV]";
  if(label == "HT_all_noJ")      return "H_{T}^{all} [GeV]";
  if(label == "HT_nomet")    return "H_{T,lj} [GeV]";
  if(label == "HTj")         return "H_{T,j} [GeV]";
  if(label == "HTj3")        return "H_{T,jjj} [GeV]";
  if(label == "Mjjj")        return "M_{jjj} [GeV]";
  if(label == "Meff")        return "M_{eff} [GeV]";
  if(label == "Meff_met")    return "M_{eff,met} [GeV]";
  if(label == "Centrality")  return "Centrality";
  if(label == "Centrality_noJ")  return "Centrality";
  if(label == "apla")        return "Aplanarity";
  if(label == "spher")       return "Sphericity";
  if(label == "H0")          return "H0";
  if(label == "H1")          return "H1";
  if(label == "H2")          return "H2";
  if(label == "H3")          return "H3";
  if(label == "H4")          return "H4";
  if(label == "pla")         return "Planarity";
  if(label == "varC")        return "varC";
  if(label == "varD")        return "varD";
  if(label == "circ")        return "circ";
  if(label == "pflow")       return "pflow";
  if(label == "thrust")      return "Thrust";
  if(label == "cent_jet")    return "Jet centrality";
  if(label == "apla_jet")    return "Jet aplanarity";
  if(label == "Aplanarity_jets")   return "Jet aplanarity";
  if(label == "Aplan_bjets") return "b-Jet aplanarity";
  if(label == "spher_jet")   return "Jet sphericity";
  if(label == "H0_jet")      return "H0_{jet}";
  if(label == "H1_jet ")     return "H1_{jet}";
  if(label == "H2_jet")      return "H2_{jet}";
  if(label == "H3_jet")      return "H3_{jet}";
  if(label == "H4_jet")      return "H4_{jet}";
  if(label == "H4_all")      return "H4_{all}";
  if(label == "H4_all_noJ")      return "H4_{all}";
  if(label == "H1_all")     return "H1_{all}";
  if(label == "H1_all_noJ")     return "H1_{all}";
  if(label == "pla_jet")     return "Jet planarity";
  if(label == "varC_jet")    return "varC jet";
  if(label == "varD_jet")    return "varD jet";
  if(label == "circ_jet")    return "circ jet";
  if(label == "pflow_jet")   return "pflow jet";
  if(label == "thrust_jet")  return "Jet thrust";
  if(label == "MHiggs")      return "M_{Higgs} [GeV]";
  if(label == "Mbb_MaxM")    return "M_{bb}^{Max M} [GeV]";
  if(label == "Mbb_MindR_77")   return "M_{bb}^{min #Delta R} [GeV]";
  if(label == "Mbb_mindR_noJ")   return "M_{bb}^{min #Delta R} [GeV]";
  if(label == "Mbb_maxdR_noJ")   return "M_{bb}^{max #Delta R} [GeV]";
  if(label == "Mbu_MindR_noJ")   return "M_{bu}^{min #Delta R} [GeV]";
  if(label == "Mbu_MaxpT_noJ") return "M_{bu}^{max. pT} [GeV]";
  if(label == "Mbb_avg_noJ") return "M_{bb}^{avg} [GeV]";
  if(label == "Mbb_minpT_noJ") return "M_{bb}^{min. pT} [GeV]";
  if(label == "Mbb_maxpT_noJ") return "M_{bb}^{max. pT} [GeV]";
  if(label == "Mbb_MaxM_noJ") return "M_{bb}^{max. M} [GeV]";
  if(label == "Mbj_MindR")   return "M_{bj}^{min #Delta R} [GeV]";
  if(label == "Muu_MindR")   return "M_{uu}^{min #Delta R} [GeV]";
  if(label == "Muu_mindR_noJ")   return "M_{uu}^{min #Delta R} [GeV]";
  if(label == "Mjj_MindR")   return "M_{jj}^{min #Delta R} [GeV]";
  if(label == "Mjj_mindR_noJ")   return "M_{jj}^{min #Delta R} [GeV]";
  if(label == "Mjj_MinM")    return "M_{jj}^{min M} [GeV]";
  if(label == "Mjj_minM_noJ")    return "M_{jj}^{min M} [GeV]";
  if(label == "Mjj_MaxPt")   return "M_{jj}^{max. p_{T}} [GeV]";
  if(label == "Mjj_maxpT_noJ")   return "M_{jj}^{max. p_{T}} [GeV]";
  if(label == "Mjj_HiggsMass")return "M_{jj}^{Higgs Mass} [GeV]";
  if(label == "Mjj_HiggsMass_noJ")return "M_{jj}^{Higgs Mass} [GeV]";
  if(label == "Mjjj_MaxPt")  return "M_{jjj}^{max. p_{T}} [GeV]";
  if(label == "Mbj_MaxPt_77")   return "M_{bj}^{max. p_{T}} [GeV]";
  if(label == "Mbb_MaxPt")   return "M_{bb}^{max. p_{T}} [GeV]";
  if(label == "dRbb_MaxPt_77")  return "#DeltaR_{bb}^{max. p_{T}}";
  if(label == "dRbb_mindR_noJ") return "#DeltaR_{bb}^{min. #Delta R}";
  if(label == "dRbb_MaxM")   return "#DeltaR_{bb}^{max. M}";
  if(label == "dRbb_maxM_noJ")   return "#DeltaR_{bb}^{max. M}";
  if(label == "dRbb_avg_77")    return "#DeltaR_{bb}^{avg}";
  if(label == "dRbb_avg_noJ")    return "#DeltaR_{bb}^{avg}";
  if(label == "dRbb_maxdR_noJ") return "#DeltaR_{bb}^{max. #Delta R}";
  if(label == "dRbb_maxpT_noJ") return "#DeltaR_{bb}^{max. pT}";
  if(label == "dRbb_minpT_noJ") return "#DeltaR_{bb}^{min. pT}";
  if(label == "dRJj_avg") return "#DeltaR_{Jj}^{avg}";
  if(label == "dRJj_mindR") return "#DeltaR_{Jj}^{min. #Delta R}";
  if(label == "dRJj_maxdR") return "#DeltaR_{Jj}^{max. #Delta R}";
  if(label == "dRJj") return "#DeltaR_{Jj}";
  if(label == "dRlj_MindR")  return "#DeltaR_{lep-j}^{Min #Delta R}";
  if(label == "dRlj_mindR_noJ")  return "#DeltaR_{lep-j}^{Min #Delta R}";
  if(label == "dRlepbb_MindR_77")return "#DeltaR_{lep-bb}^{Min #Delta R}";
  if(label == "dRlepbb_mindR_noJ")return "#DeltaR_{lep-bb}^{Min #Delta R}";
  if(label == "dRuu_MindR")  return "#DeltaR_{uu}^{Min #Delta R}";
  if(label == "dRuu_mindR_noJ")  return "#DeltaR_{uu}^{Min #Delta R}";
  if(label == "dRHl_MindR")  return "#DeltaR_{H-lep}^{Min #Delta R}";
  if(label == "dRHl_MaxdR")  return "#DeltaR_{H-lep}^{Max #Delta R}";
  if(label == "dEtajj_MaxdEta")return "#Delta#eta^{Max #Delta #eta}";
  if(label == "dEtajj_maxdEta_noJ")return "#Delta#eta_{jj}^{Max #Delta #eta}";
  if(label == "pTuu_MindR")  return "p_{T,uu}^{min #Delta R} [GeV]";
  if(label == "pTuu_mindR_noJ")  return "p_{T,uu}^{min #Delta R} [GeV]";
  if(label == "nJets_Pt40")   return "Number of Jets with p_{T}>40GeV"; 
  if(label == "Njet_pT40_noJ")   return "Number of Additional Jets with p_{T}>40GeV";
  if(label == "nHiggsbb30_77")   return "N^{Higgs}_30"; 
  if(label == "jet_n_noMatch") return "Number of Additional Jets";
  if(label == "tag_n_noMatch") return "Number of b-tagged Additional Jets";
  if(label == "ljet_pt")     return "Large-R Jet p_{T} [GeV]";
  if(label == "ljet_eta")    return "Large-R Jet #eta";
  if(label == "ljet_phi")    return "Large-R Jet #phi";
  if(label == "ljet_m")      return "Large-R Jet Mass [GeV]";
  if(label == "ljet_top_pt")     return "Large-R Top Jet p_{T} [GeV]";
  if(label == "ljet_top_eta")    return "Large-R Top Jet #eta";
  if(label == "ljet_top_phi")    return "Large-R Top Jet #phi";
  if(label == "ljet_top_m")      return "Large-R Top Jet Mass [GeV]";
  if(label == "ljet_n")      return "Number of Large-R Jets";
  if(label == "ljet_tau32")  return "#tau_{32}(wta)";
  if(label == "ljet_tau21")  return "#tau_{21}(wta)";
  if(label == "ljet_jet_dR") return "#Delta R(large-R jet,small-R jet)";
  if(label == "ljet_topTagN")return "Number of (tight) top-tagged Large-R jets";
  if(label == "ljet_topTagN_loose")return "Number of (loose) top-tagged Large-R jets";
  if(label == "ljet_sd23")   return "#sqrt{d_{23}}";
  if(label == "dPhiJj_avg") return "#Delta#phi_{Jj}^{avg}";
  if(label == "dPhiJj_mindR") return "#Delta#phi_{Jj}^{min. #Delta R}";
  if(label == "dPhiJj_maxdR") return "#Delta#phi_{Jj}^{max. #Delta R}";

  if(label == "loosetagged_LargeR_pT") return "Large-R Jet p_{T} (Loose Tagged) [GeV]";
  if(label == "tighttagged_LargeR_pT") return "Large-R Jet p_{T} (Tight Tagged) [GeV]";
  if(label == "loosetagged_LargeR_mass") return "Large-R Jet Mass (Loose Tagged) [GeV]";
  if(label == "tighttagged_LargeR_mass") return "Large-R Jet Mass (Tight Tagged) [GeV]";

  if (label== "ljet_pt_varbin") return "Large-R Jet p_{T} [GeV]";
  if (label== "ljet_pt_varbin_logscale") return "Large-R Jet p_{T} [GeV]";
  if (label== "loosetagged_LargeR_pT_varbin") return "(Loose Tagged) Large-R Jet p_{T} [GeV]";
  if (label== "loosetagged_LargeR_pT_varbin_logscale") return "(Loose Tagged) Large-R Jet p_{T} [GeV]";
  if (label== "tighttagged_LargeR_pT_varbin") return "(Tight Tagged) Large-R Jet p_{T} [GeV]";
  if (label== "tighttagged_LargeR_pT_varbin_logscale") return "(Tight Tagged) Large-R Jet p_{T} [GeV]";

 if(label == "ljet_mass_lowpt") return "Large-R Jet Mass ( 200 < p_{T} < 500 ) [GeV]";
  if(label == "ljet_mass_midpt") return "Large-R Jet Mass ( 500 < p_{T} < 1000 ) [GeV]";
  if(label == "ljet_mass_highpt") return "Large-R Jet Mass ( 1000 < p_{T} < 1500 ) [GeV]";

  if(label == "truth_leptop_mass") return "Parton Level top_{l} Mass [GeV]";
  if(label == "truth_leptop_pt") return "Parton Level top_{l} p_{T} [GeV]";
  if(label == "truth_leptop_phi") return "Parton Level top_{l} #phi";
  if(label == "truth_leptop_eta") return "Parton Level top_{l} #eta";
  if(label == "truth_hadtop_mass") return "Parton Level top_{h} Mass [GeV]";
  if(label == "truth_hadtop_pt") return "Parton Level top_{h} p_{T} [GeV]";
  if(label == "truth_hadtop_phi") return "Parton Level top_{h} #phi";
  if(label == "truth_hadtop_eta") return "Parton Level top_{h} #eta";
  if(label == "truth_ttbar_mass") return "Parton Level t#bar{t} Mass [GeV]";
  if(label == "truth_ttbar_pt") return "Parton Level t#bar{t} p_{T} [GeV]";
  if(label == "truth_ttbar_phi") return "Parton Level t#bar{t} #phi";
  if(label == "truth_ttbar_eta") return "Parton Level t#bar{t} #eta";


  if(label == "truth_dR_top_ljet") return "#Delta R (Parton level hadtop, Particle Level Large Jet)";
  if(label == "truth_ljet_pt") return "Particle Level Large Jet p_{T} [GeV]";
  if(label == "truth_ljet_eta") return "Particle Level Large Jet #eta";
  if(label == "truth_ljet_phi") return "Particle Level Large Jet #phi";
  if(label == "truth_ljet_mass") return "Particle Level Large Jet Mass [GeV]";
  if(label == "truth_mu_pt") return "Particle Level Muon p_{T} [GeV]";
  if(label == "truth_mu_eta") return "Particle Level Muon #eta";
  if(label == "truth_mu_phi") return "Particle Level Muon #phi";
  if(label == "truth_el_pt") return "Particle Level Electron p_{T} [GeV]";
  if(label == "truth_el_eta") return "Particle Level Electron #eta";
  if(label == "truth_el_phi") return "Particle Level Electron #phi";

  if(label == "truth_dR_truthel_truthljet") return "#Delta R (Particle Level Electron, Particle Level Large Jet)";
  if(label == "truth_dPhi_truthel_truthljet") return "#Delta #phi (Particle Level Electron, Particle Level Large Jet)";
  if(label == "truth_dR_recoljet_truthljet") return "#Delta R (Detector Level Large Jet, Particle Level Large Jet)";

  if(label == "matrix_LJet_pt_truth_tight") return "Tight Tagged LargeR Jet p_{T}";
  if(label == "matrix_LJet_pt_truth_tight_varbin") return "Tight Tagged LargeR Jet p_{T}";
  if(label == "matrix_LJet_pt_truth_matched_varbin") return "Loose Tagged LargeR Jet p_{T} (Matched)";
  if(label == "matrix_truthLJet_recoLJet_pt_truth_varbin") return "Detector Level LargeR Jet p_{T}";
  if(label == "matrix_truthhadtop_truthLJet_pt_truth_varbin") return "Particle Level Large R Jet p_{T}";  
  if(label == "matrix_LJet_pt_truth_varbin") return "Loose Tagged LargeR Jet p_{T}";
  if(label == "matrix_LJet_pt_truth") return "Loose Tagged LargeR Jet p_{T}";

 if(label == "matrix_hadtopjet_truthhadtopjet_pt_varbin") return "Detector Level p_{T}^{t,had}";
 if(label == "matrix_hadtopjet_truthhadtopjet_pt_varbin_largerbin") return "Detector Level p_{T}^{t,had}";
 if(label == "matrix_hadtopjet_truthhadtopjet_pt_varbin_lessbin") return "Detector Level p_{T}^{t,had}";
  
 if(label == "matrix_hadtopjet_truthhadtopjet_y_varbin") return "Detector Level |y^{t,had}|";
 if(label == "matrix_hadtopjet_truthhadtopjet_absy_varbin") return "Detector Level |y^{t,had}|";
  if(label == "matrix_hadtopjet_truthhadtopjet_absy_varbin_largerbin") return "Detector Level |y^{t,had}|";

 if(label == "matrix_hadtopjet_truthhadtopjet_y") return "Detector Level y^{t,had}";
 if(label == "matrix_hadtopjet_truthhadtopjet_absy") return "Detector Level |y^{t,had}|";
 if(label == "matrix_hadtopjet_truthtopparton_y") return "Detector Level y^{t,had}";
 if(label == "matrix_hadtopjet_truthtopparton_absy") return "Detector Level |y^{t,had}|";
 if(label == "matrix_truthhadtopjet_truthtopparton_y") return "Particle Level y^{t,had}";
 if(label == "matrix_truthhadtopjet_truthtopparton_absy") return "Particle Level |y^{t,had}|";
 if(label == "truth_particlelevel_hadtopjet_pt_varbin_lumiweight") return "Particle Level p_T^{t,had}";
 if(label == "truth_particlelevel_hadtopjet_pt_varbin_lumiweight_largerbin") return "Particle Level p_T^{t,had}";
 if(label == "truth_particlelevel_hadtopjet_pt_varbin_lumiweight_lessbin") return "Particle Level p_T^{t,had}";
 if(label == "truth_particlelevel_hadtopjet_absy_varbin_lumiweight") return "Particle Level |y^{t,had}|";
 if(label == "truth_particlelevel_hadtopjet_absy_varbin_largerbin_lumiweight") return "Particle Level |y^{t,had}|";



  if(label == "dR_largeR_mu") return "#Delta R(large-R Jet, Muon)";
  if(label == "dR_largeR_el") return "#Delta R(large-R Jet, Electron)";
  if(label == "dPhi_J_mu") return "#Delta#phi(large-R Jet, Muon)";
  if(label == "dPhi_J_el") return "#Delta#phi(large-R Jet, Electron)";
  if(label == "el_pT_dR_outside_ljet") return "Electron p_{T} (#Delta R_{J} > 1) [GeV]";
  if(label == "el_pT_dR_inside_ljet") return "Electron p_{T} (#Delta R_{J} < 1) [GeV]";
  if(label == "el_ljet_dR_dPhi_cut") return "#Delta R(large-R Jet, Electron) (Post #Delta#phi Cut)";
  if(label == "el_pt_dPhi_cut") return "Electron p_{T} (Post #Delta#phi Cut) [GeV]";
  if(label == "dR_mu_bjet") return "#Delta R (Muon, b-jet)";
  if(label == "dR_mu_bjet_min") return "Min #Delta R (Muon, b-jet)";
  if(label == "dR_el_bjet") return "#Delta R (Electron, b-jet)";
  if(label == "dR_el_bjet_min") return "Min #Delta R (Electron, b-jet)";
  if(label == "dR_mu_ljet") return "#Delta R (Muon, jet)";
  if(label == "dR_mu_ljet_min") return "Min #Delta R (Muon, jet)";
  if(label == "dR_el_ljet") return "#Delta R (Electron, jet)";
  if(label == "dR_el_ljet_min") return "Min #Delta R (Electron, jet)";
  if(label == "dPhi_el_ljet") return "#Delta#phi (Electron, jet)";
  if(label == "dPhi_mu_ljet") return "#Delta#phi (Muon, jet)";
  if(label == "dR_lep_smalljets") return "#Delta R (Lepton, small jets)";
  if(label == "dR_lep_smalljets_min") return "Min #Delta R (Lepton, small jets)";


  if(label == "W_bjet_dR_min") return "Min #Delta R (W, b-jet)";
  if(label == "W_bjet_dR_all") return "#Delta R (W, b-jet)";
  if(label == "W_mass") return "W Mass [GeV]";
  if(label == "W_pt") return "W p_{T} [GeV]";
  if(label == "W_eta") return"W #eta";
  if(label == "W_phi") return "W #phi";
  if(label == "leptop_mass") return "top^{lep} Mass [GeV]";
  if(label == "leptop_pt") return "top^{lep} p_{T} [GeV]";
  if(label == "leptop_eta") return "top^{lep} #eta";
  if(label == "leptop_phi") return "top^{lep} #phi";
  if(label == "ttbar_mass") return "t#bar{t} Mass [GeV]";
  if(label == "ttbar_pt") return "t#bar{t} p_{T} [GeV]";
  if(label == "ttbar_eta") return "t#bar{t} #eta";
  if(label == "ttbar_phi") return "t#bar{t} #phi";

  if(label == "ttbar_min_dR_mass") return "t#bar{t} Mass [GeV] ";
  if(label == "ttbar_min_dR_pt") return "t#bar{t} p_{T} [GeV]";
  if(label == "ttbar_min_dR_eta") return "t#bar{t} #eta";
  if(label == "ttbar_min_dR_phi") return "t#bar{t} #phi";
  if(label == "truth_leptop_phi") return "Parton Level t^{l} #phi";
  if(label == "truth_leptop_eta") return "Parton Level t^{l} #eta";
  if(label == "truth_hadtop_mass") return "Parton Level t^{h} Mass [GeV]";
  if(label == "truth_hadtop_pt") return "Parton Level t^{h} p_{T} [GeV]";
  if(label == "truth_hadtop_phi") return "Parton Level t^{h} #phi";
  if(label == "truth_hadtop_eta") return "Parton Level t^{h} #eta";

  if(label == "hadtop_mass") return "Top-jet candidate mass [GeV]";
  if(label == "hadtop_pt") return "Top-jet candidate p_{T} [GeV]";
  if(label == "hadtop_pt_varbin") return "Top-jet candidate p_{T} [GeV]";
  if(label == "hadtop_pt_varbin_largerbin") return "Top-jet candidate p_{T} [GeV]";
  if(label == "hadtop_pt_varbin_lessbin") return "Top-jet candidate p_{T} [GeV]";
 

  if(label == "hadtop_pt_varbin_logscale") return "Top-jet candidate p_{T} [GeV]";
  if(label == "hadtop_pt_logscale") return "Top-jet candidate p_{T} [GeV]";
  if(label == "hadtop_eta") return "Top-jet candidate #eta";
  if(label == "hadtop_phi") return "Top-jet candidate #phi";
  if(label == "hadtop_y") return "Top-jet candidate y";
  if(label == "hadtop_absy") return "Top-jet candidate |y|";
  if(label == "hadtop_y_varbin") return "Top-jet candidate y";
  if(label == "hadtop_absy_varbin") return "Top-jet candidate |y|";
  if(label == "hadtop_absy_varbin_largerbin") return "Top-jet candidate |y|";



  if(label == "mtw") return "M_{T}^{W} [GeV]";
  if(label == "met_plus_mtw") return "E_{T}^{miss} + M_{T}^{W} [GeV]";
  
  if(label == "dR_leptop_hadtop") return "#Delta R(Leptop, Hadtop)";
  if(label == "dR_leadingbjet_el") return "#Delta R(Leading b-jet, Electron)";
  if(label == "leadingbjet_pt") return "#Leading b-jet p_{T} [GeV]";

  if(label == "dR_hadtopjet_el") return "#Delta R(Hadtop Jet, Electron)";
  if(label == "dR_hadtopjet_mu") return "#Delta R(Hadtop Jet, Muon)";
  if(label == "dPhi_hadtopjet_el") return "#Delta#phi (Hadtop Jet, Electron)";
  if(label == "dPhi_hadtopjet_mu") return "#Delta#phi (Hadtop Jet, Muon)";

  if(label == "dR_lep_smalljets_OutT") return "#Delta R(Lepton, Jets outside HadTop)";
  if(label == "dR_lep_smalljets_min_OutT") return "Min #Delta R(Lepton, Jets outside HadTop)";

  if(label == "jet_n_OutT")       return "No. of Small-R jets outside Top-jet candidate";
  if(label == "tag_n_OutT")       return "No. of B-Tags outside Top-jet candidate";
  if(label == "jet0_pt_OutT")     return "Leading Small-R jet p_{T} outside Top-jet candidate [GeV]";
  if(label == "jet0_eta_OutT")    return "Leading Small-R jet #eta outside Top-jet candidate";
  if(label == "jet0_phi_OutT")    return "Leading Small-R jet #phi outside Top-jet candidate ";
  if(label == "jet1_pt_OutT")     return "Second Small-R jet p_{T} outside Top-jet candidate [GeV]";
  if(label == "jet1_eta_OutT")    return "Second Small-R jet #eta outside Top-jet candidate";
  if(label == "jet1_phi_OutT")    return "Second Small-R jet #phi outside Top-jet candidate";
  if(label == "jet2_pt_OutT")     return "3rd Small-R jet p_{T} outside Top-jet candidate [GeV]";
  if(label == "jet2_eta_OutT")    return "3rd Small-R jet #eta outside Top-jet candidate";
  if(label == "jet2_phi_OutT")    return "3rd Small-R jet #phi outside Top-jet candidate";
  if(label == "jet3_pt_OutT")     return "4th Small-R jet p_{T} outside Top-jet candidate [GeV]";
  if(label == "jet3_eta_OutT")    return "4th Small-R jet #eta outside Top-jet candidate";
  if(label == "jet3_phi_OutT")    return "4th Small-R jet #phi outside Top-jet candidate";
  if(label == "jet_pt_OutT")     return "Small-R jets p_{T} outside Top-jet candidate [GeV]";
  if(label == "jet_eta_OutT")    return "Small-R jets #eta outside Top-jet candidate ";
  if(label == "jet_phi_OutT")    return "Small-R jets #phi outside Top-jet candidate ";
  if(label == "dR_truthHiggs_ljet0") return "Minimum #Delta R(Truth Higgs, Leading Large Jet)";
  if(label == "dR_truthHiggs_ljetTop0") return "#Delta R(Truth Higgs, Leading Top-tagged Large Jet)";
  if(label == "dR_truthhadTop_ljet0") return "#Delta R(Truth hadronic Top, Leading Large Jet)";
  if(label == "dR_truthlepTop_ljet0") return "#Delta R(Truth leptonic Top, Leading Large Jet)";
  if(label == "dR_truthhadTop_ljetTop0") return "#Delta R(Truth hadronic Top, Leading Top-tagged Large Jet)";
  if(label == "dR_truthlepTop_ljetTop0") return "#Delta R(Truth leptonic Top, Leading Top-tagged Large Jet)";
  if(label == "dR_truthHiggs_truthhadTop") return "#Delta R(Truth Higgs, Truth Hadronic Top)";
  if(label == "dPhi_truthHiggs_truthhadTop") return "#Delta#phi(Truth Higgs, Truth Hadronic Top)";
  if(label == "dEta_truthHiggs_truthhadTop") return "#Delta#eta(Truth Higgs, Truth Hadronic Top)";
  if(label == "dR_truthHiggs_truthlepTop") return "#Delta R(Truth Higgs, Truth Leptonic Top)";
  if(label == "dPhi_truthHiggs_truthlepTop") return "#Delta#phi(Truth Higgs, Truth Leptonic Top)";
  if(label == "dEta_truthHiggs_truthlepTop") return "#Delta#eta(Truth Higgs, Truth Leptonic Top)";
  if(label == "dR_truthlepTop_truthhadTop") return "#Delta R(Truth Leptonic Top, Truth Hadronic Top)";
  if(label == "dPhi_truthlepTop_truthhadTop") return "#Delta#phi(Truth Leptonic Top, Truth Hadronic Top)";
  if(label == "dEta_truthlepTop_truthhadTop") return "#Delta#eta(Truth Leptonic Top, Truth Hadronic Top)";
  /* if(label == "dRjj_maxPt")  return "";
  if(label == "pTjj_maxPt")  return "";
  if(label == "mjj_maxPt")   return "";
  if(label == "dRjj_mindR")  return "";
  if(label == "pTjj_mindR")  return "";
  if(label == "mjj_mindR")   return "";
  if(label == "dRjj_maxM")   return "";
  if(label == "pTjj_maxM")   return "";
  if(label == "mjj_maxM")    return "";
  if(label == "dRjj_av")     return "";
  if(label == "pTjj_av")     return "";
  if(label == "mjj_av")      return "";
  if(label == "W_had_dR")    return "";
  if(label == "W_had_pT")    return "";
  if(label == "W_had_mass")  return "";
  if(label == "pT_bjet1")    return "";
  if(label == "pT_bjet2")    return "";
  if(label == "pT_bjet3")    return "";
  if(label == "pT_bjet4")    return "";
  if(label == "dRlep_bjet1") return "";
  if(label == "dRlep_bjet2") return "";
  if(label == "dRlep_bjet3") return "";
  if(label == "dRlep_bjet4") return "";
  if(label == "HTb")         return "";
  if(label == "cent_b")      return "";
  if(label == "apla_b")      return "";
  if(label == "spher_b")     return "";
  if(label == "H0_b")        return "";
  if(label == "H1_b")        return "";
  if(label == "H2_b")        return "";
  if(label == "H3_b")        return "";
  if(label == "H4_b")        return "";
  if(label == "pla_b")       return "";
  if(label == "varC_b")      return "";
  if(label == "varD_b")          return "";
  if(label == "circ_b")          return "";
  if(label == "pflow_b")         return "";
  if(label == "thrust_b")        return "";
  if(label == "dRbj_maxPt")      return "";
  if(label == "pTbj_maxPt")      return "";
  if(label == "dRbj_mindR")      return "";
  if(label == "pTbj_mindR")      return "";
  if(label == "mbj_mindR")       return "";
  if(label == "dRbj_maxM")       return "";
  if(label == "pTbj_maxM")       return "";
  if(label == "mbj_maxM")        return "";
  if(label == "dRbj_av")         return "";
  if(label == "pTbj_av")         return "";
  if(label == "mbj_av")          return "";
  if(label == "dRbb_mindR")      return "";
  if(label == "pTbb_mindR")      return "";
  if(label == "Mbb_MindR")       return "";
  if(label == "dRbb_maxM")       return "";
  if(label == "pTbb_maxM")       return "";
  if(label == "mbb_maxM")        return "";
  if(label == "dRbb_av")         return "";
  if(label == "pTbb_av")         return "";
  if(label == "mbb_av")          return "";
  if(label == "dRlepbb_maxPt")   return "";
  if(label == "dRlepbb_mindR")   return "";
  if(label == "dRlepbb_maxM")    return "";
  if(label == "dRlepbj_maxPt")   return "";
  if(label == "dRlepbj_mindR")   return "";
  if(label == "dRlepbj_maxM")    return "";
  if(label == "dPhiMetbb_maxPt") return "";
  if(label == "dPhiMetbb_mindR") return "";
  if(label == "dPhiMetbb_maxM")  return "";
  if(label == "dPhiMetbj_maxPt") return "";
  if(label == "dPhiMetbj_mindR") return "";
  if(label == "dPhiMetbj_maxM")  return ""; */
 
  return label;

}
