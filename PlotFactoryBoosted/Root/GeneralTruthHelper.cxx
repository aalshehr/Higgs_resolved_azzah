#include "PlotFactoryBoosted/GeneralTruthHelper.h"
#include <cmath>

bool TTHbbTruth::isHiggs(int pdg, int info){
  return pdg==25 && (info & (1<<TTHbbTruth::higgs) );
}
bool TTHbbTruth::isFromHiggs(int info){
  return info & (1<<TTHbbTruth::childFromHiggs);
}
bool TTHbbTruth::isLeadingFromHiggs(int info){
  return info & (1<<TTHbbTruth::leadingChildFromHiggs);
}
bool TTHbbTruth::isSubleadingFromHiggs(int info){
  return info & (1<<TTHbbTruth::subleadingChildFromHiggs);
}
bool TTHbbTruth::isFromWFromHiggs(int info){
  return info & (1<<TTHbbTruth::childFromWHiggs);
}
bool TTHbbTruth::isFromZFromHiggs(int info){
  return info & (1<<TTHbbTruth::childFromZHiggs);
}
bool TTHbbTruth::isHadTop(int pdg, int info){
  return fabs(pdg)==6 && (info & (1<<TTHbbTruth::hadTop));
}
bool TTHbbTruth::isLepTop(int pdg, int info){
  return fabs(pdg)==6 && (info & (1<<TTHbbTruth::lepTop));
}
bool TTHbbTruth::isLeadingTop(int pdg, int info){
  return fabs(pdg)==6 && (info & (1<<TTHbbTruth::leadingTop));
}
bool TTHbbTruth::isSubLeadingTop(int pdg, int info){
  return fabs(pdg)==6 && (info & (1<<TTHbbTruth::subleadingTop));
}
bool TTHbbTruth::isFromHadTop(int info){
  return info & (1<<TTHbbTruth::childFromHadTop);
} 
bool TTHbbTruth::isFromLepTop(int info){
  return info & (1<<TTHbbTruth::childFromLepTop);
}
bool TTHbbTruth::isFromLeadingTop(int info){
  return info & (1<<TTHbbTruth::childFromLeadingTop);
}
bool TTHbbTruth::isFromSubLeadingTop(int info){
  return info & (1<<TTHbbTruth::childFromSubleadingTop);
}
bool TTHbbTruth::isDirectlyFromTop(int info){
  return info & (1<<TTHbbTruth::directChildFromTop);
}
bool TTHbbTruth::isLeadingFromWTop(int info){
  return info & (1<<TTHbbTruth::leadingChildFromWTop);
}
bool TTHbbTruth::isSubleadingFromWTop(int info){
  return info & (1<<TTHbbTruth::subleadingChildFromWTop);
}

bool TTHbbTruth::isFromTop(int info){
  return info & (1<<TTHbbTruth::childFromTop);
}
  
bool TTHbbTruth::isFromAntiTop(int info){
  return info & (1<<TTHbbTruth::childFromAntiTop);
}
  
bool TTHbbTruth::isVectorBoson(int pdg, int info){
  return (fabs(pdg)==24 ||fabs(pdg)==23) && (info & (1<<TTHbbTruth::vectorBoson));
} 
bool TTHbbTruth::isFromVectorBoson(int info){
  return info & (1<<TTHbbTruth::childFromVectorBoson);
} 

bool TTHbbTruth::isBSMHiggs(int info){
  return info & (1<<TTHbbTruth::BSMHiggs);
}
bool TTHbbTruth::isChargedHiggs(int info){
  return info & (1<<TTHbbTruth::ChargedHiggs);
}
bool TTHbbTruth::isFromChargedHiggs(int info){
  return info & (1<<TTHbbTruth::childFromChargedHiggs);
}
bool TTHbbTruth::isAssociatedBwithChargedHiggs(int info){
  return info & (1<<TTHbbTruth::bFromCollide);
}

bool TTHbbTruth::isLeadingAFromHiggs(int info){
  return info & (1<<TTHbbTruth::leadingAFromHiggs);
}
bool TTHbbTruth::isSubleadingAFromHiggs(int info){
  return info & (1<<TTHbbTruth::subleadingAFromHiggs);
}
bool TTHbbTruth::isLeadingChildFromLeadingA(int info){
  return info & (1<<TTHbbTruth::leadingChildFromLeadingA);
}
bool TTHbbTruth::isSubleadingChildFromLeadingA(int info){
  return info & (1<<TTHbbTruth::subleadingChildFromLeadingA);
}
bool TTHbbTruth::isLeadingChildFromSubleadingA(int info){
  return info & (1<<TTHbbTruth::leadingChildFromSubleadingA);
}
bool TTHbbTruth::isSubleadingChildFromSubleadingA(int info){
  return info & (1<<TTHbbTruth::subleadingChildFromSubleadingA);
}
