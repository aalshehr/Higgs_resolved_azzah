#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

void HistoCreator::FillMVABoostedHistos(EventExtras* iEntryExtrasInfo)
{

  float histoweight = iEntryExtrasInfo->GetEntryWeight();
  int nGoodLargeR = iEntryExtrasInfo->GetNTopTags();

  std::vector<TLorentzVector> largeR;
  largeR.clear();
  largeR = iEntryExtrasInfo->GetTopTaggedJets();

  std::vector<TLorentzVector> smallR_noMatched;
  smallR_noMatched.clear();
  smallR_noMatched = iEntryExtrasInfo->GetAdditionalSmallJetsOutAllT();

  std::vector<TLorentzVector> smallR_B;
  smallR_B.clear();
  smallR_B = iEntryExtrasInfo->GetAdditionalSmallTagsOutAllT();

  std::vector<TLorentzVector> smallR_noB;
  smallR_noB.clear();
  smallR_noB = iEntryExtrasInfo->GetAdditionalSmallAntiTagsOutAllT();

  TLorentzVector leptop = iEntryExtrasInfo->GetLepTop();
  TLorentzVector hadtop = iEntryExtrasInfo->GetHadTop();

  int N_Higgs_40_noTops = 0;
  int N_Higgs_30_noTops = 0;
  int N_Higgs_20_noTops = 0;

  int N_Higgs_40_noJ = 0;
  int N_Higgs_30_noJ = 0;
  int N_Higgs_20_noJ = 0;
  float mjj_temp = 99999.;
  unsigned int first_j = 0;
  unsigned int second_j = 0;
  float j_maxM = 0.;
  float SMdrop_temp = 0.;
  float SMdrop_jj = 99999.;
  float dRjj = 0.;
  unsigned int jet_min1_1 = 0;
  unsigned int jet_min2_1 = 0;
  TLorentzVector jjSum;

  // Loop over all jets not in tops 
  for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){

    for(unsigned int ismall = 0; ismall < smallR_noMatched.size(); ++ismall){

      if(ismall >= jsmall)
        continue;

      float inv_mass = (smallR_noMatched[ismall]+smallR_noMatched[jsmall]).M();

      if(fabs(inv_mass-125.0) < 40) N_Higgs_40_noTops++;
      if(fabs(inv_mass-125.0) < 30) N_Higgs_30_noTops++;
      if(fabs(inv_mass-125.0) < 20) N_Higgs_20_noTops++;

      if(fabs(inv_mass - 125.0) < mjj_temp){
        mjj_temp = fabs(inv_mass - 125.0);
        first_j  = ismall;
        second_j = jsmall;
      }
      jjSum = smallR_noMatched[jsmall] + smallR_noMatched[ismall];
      j_maxM = (smallR_noMatched[jsmall].M() > smallR_noMatched[ismall].M()) ? smallR_noMatched[jsmall].M() : smallR_noMatched[ismall].M();
      dRjj = smallR_noMatched[jsmall].DeltaR(smallR_noMatched[ismall]);
      SMdrop_temp = dRjj*j_maxM/jjSum.M();
      //std::cout << "Delta R = " << dRjj << "; m12 = " << j_maxM/jjSum.M() << std::endl;
      if(SMdrop_temp < SMdrop_jj){
        SMdrop_jj = SMdrop_temp;
        jet_min1_1 = jsmall;
        jet_min2_1 = ismall;
      }


    }

  }

  // loop over smallR_noMatched
  unsigned int jet_min1_2 = 0;
  unsigned int jet_min2_2 = 0;
  unsigned int jet_max1 = 0;
  unsigned int jet_max2 = 0;
  float SMdropmin_jj = 99999.;
  float SMdropmax_jj = -1.;
  for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){

    for(unsigned int ismall = 0; ismall < smallR_noMatched.size(); ++ismall){

      if(ismall >= jsmall)
        continue;

      float inv_mass = (smallR_noMatched[ismall]+smallR_noMatched[jsmall]).M();

      if(fabs(inv_mass-125.0) < 40) N_Higgs_40_noJ++;
      if(fabs(inv_mass-125.0) < 30) N_Higgs_30_noJ++;
      if(fabs(inv_mass-125.0) < 20) N_Higgs_20_noJ++;

      jjSum = smallR_noMatched[jsmall] + smallR_noMatched[ismall];
      j_maxM = (smallR_noMatched[jsmall].M() > smallR_noMatched[ismall].M()) ? smallR_noMatched[jsmall].M() : smallR_noMatched[ismall].M();
      dRjj = smallR_noMatched[jsmall].DeltaR(smallR_noMatched[ismall]);
      SMdrop_temp = dRjj*j_maxM/jjSum.M();

      if(SMdrop_temp < SMdropmin_jj){
        SMdropmin_jj = SMdrop_temp;
        jet_min1_2 = jsmall; // jet indexes for those with smallest mass drop
        jet_min2_2 = ismall;
      }
      if(SMdrop_temp > SMdropmax_jj){
        SMdropmax_jj = SMdrop_temp;
        jet_max1 = jsmall; // jet indexes for those with largest mass drop
        jet_max2 = ismall;
      }

    }

  }


  // Loop over smallR_b jets
  float mbb_temp = 99999.;
  unsigned int first_b  = 0;
  unsigned int second_b = 0;
  float b_maxM = 0.;
  float SMdropmin_bb = 99999.;
  float SMdropmax_bb = -1.;
  float dRbb = 0.;
  unsigned int bjet_min1 = 0;
  unsigned int bjet_min2 = 0;
  unsigned int bjet_max1 = 0;
  unsigned int bjet_max2 = 0;
  TLorentzVector bbSum;
  for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
    for(unsigned int bsmalls_2 = bsmalls+1; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
      float inv_mass = (smallR_B[bsmalls]+smallR_B[bsmalls_2]).M();
      if(fabs(inv_mass - 125.0) < mbb_temp){
        mbb_temp = fabs(inv_mass - 125.0);
        first_b  = bsmalls;
        second_b = bsmalls_2;
      }
      bbSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
      b_maxM = (smallR_B[bsmalls].M() > smallR_B[bsmalls_2].M()) ? smallR_B[bsmalls].M() : smallR_B[bsmalls_2].M();
      dRbb = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
      SMdrop_temp = dRbb*b_maxM/bbSum.M();
      //std::cout << "Delta R = " << dRjj << "; m12 = " << j_maxM/jjSum.M() << std::endl;
      if(SMdrop_temp < SMdropmin_bb){
        SMdropmin_bb = SMdrop_temp;
        bjet_min1 = bsmalls; //indexes of two b's with smallest massdrop
        bjet_min2 = bsmalls_2;
      }
      if(SMdrop_temp > SMdropmax_bb){
        SMdropmax_bb = SMdrop_temp;
        bjet_max1 = bsmalls; //indexes of two b's with largest massdrop
        bjet_max2 = bsmalls_2;
      }
    }
  }
  



  // mass drop of two jets with mass closest to 125GeV
  //jjSum = smallR_noMatched[first_j] + smallR_noMatched[second_j];
  //j_maxM = (smallR_noMatched[first_j].M() > smallR_noMatched[second_j].M()) ? smallR_noMatched[first_j].M() : smallR_noMatched[second_j].M();
  //SMdrop_jj = smallR_noMatched[first_j].DeltaR(smallR_noMatched[second_j])*j_maxM/jjSum.M(); 
  // mass drop of two b's with mass closest to 125GeV
  //bbSum = smallR_B[first_b] + smallR_B[second_b];
  //b_maxM = (smallR_B[first_b].M() > smallR_B[second_b].M()) ? smallR_B[first_b].M() : smallR_B[second_b].M();
  //float SMdrop_bb = smallR_B[first_b].DeltaR(smallR_B[second_b])*b_maxM/bbSum.M(); 

  float SMdrop_bb = 0;

  // 2D histograms
  /*  for(unsigned iVar_2D = 0; iVar_2D < fVar_2D.size(); ++iVar_2D){
    // only positive weights, b's with mass closest to 125GeV
    if(fVariables_2D[iVar_2D] == "dRbb_m12_pos"){
      if(histoweight > 0){
        fHistoVector_2D[iVar_2D].Fill(b_maxM/bbSum.M(), smallR_B[first_b].DeltaR(smallR_B[second_b]), histoweight);
      }
    }
    //only positive weights, jets with mass closest to 125GeV
    if(fVariables_2D[iVar_2D] == "dRjj_m12_pos"){
      if(histoweight > 0)
        fHistoVector_2D[iVar_2D].Fill(j_maxM/jjSum.M(), smallR_noMatched[first_j].DeltaR(smallR_noMatched[second_j]), histoweight);
    }
    // only positive weights, b's with smallest mass drop
    if(fVariables_2D[iVar_2D] == "dRbb_m12_min"){
      if(histoweight > 0){
        b_maxM = (smallR_B[bjet_min1].M() > smallR_B[bjet_min2].M()) ? smallR_B[bjet_min1].M() : smallR_B[bjet_min2].M();
        bbSum = smallR_B[bjet_min1] + smallR_B[bjet_min2];
        fHistoVector_2D[iVar_2D].Fill(b_maxM/bbSum.M(), smallR_B[bjet_min1].DeltaR(smallR_B[bjet_min2]), histoweight);
      }
    }
    // only positive weights, jets with smallest mass drop
    if(fVariables_2D[iVar_2D] == "dRjj_m12_min"){
      if(histoweight > 0){
        jjSum = smallR_noMatched[jet_min1_1] + smallR_noMatched[jet_min2_1];
        j_maxM = (smallR_noMatched[jet_min1_1].M() > smallR_noMatched[jet_min2_1].M()) ? smallR_noMatched[jet_min1_1].M() : smallR_noMatched[jet_min2_1].M();
        fHistoVector_2D[iVar_2D].Fill(j_maxM/jjSum.M(), smallR_noMatched[jet_min1_1].DeltaR(smallR_noMatched[jet_min2_1]), histoweight);
      }
    }
    // b's with largest mass drop
    if(fVariables_2D[iVar_2D] == "dRbb_m12_max"){
      if(histoweight > 0){
        b_maxM = (smallR_B[bjet_max1].M() > smallR_B[bjet_max2].M()) ? smallR_B[bjet_max1].M() : smallR_B[bjet_max2].M();
        bbSum = smallR_B[bjet_max1] + smallR_B[bjet_max2];
        fHistoVector_2D[iVar_2D].Fill(b_maxM/bbSum.M(), smallR_B[bjet_max1].DeltaR(smallR_B[bjet_max2]), histoweight);
      }
    }
    // b's with largest mass drop
    if(fVariables_2D[iVar_2D] == "dRjj_m12_max"){
      if(histoweight > 0){
        jjSum = smallR_noMatched[jet_max1] + smallR_noMatched[jet_max2];
        j_maxM = (smallR_noMatched[jet_max1].M() > smallR_noMatched[jet_max2].M()) ? smallR_noMatched[jet_max1].M() : smallR_noMatched[jet_max2].M();
        fHistoVector_2D[iVar_2D].Fill(j_maxM/jjSum.M(), smallR_noMatched[jet_max1].DeltaR(smallR_noMatched[jet_max2]), histoweight);
      }
    }
    } */
  
  // lorentz vectors to be used in variable construction


  bbSum = smallR_B[bjet_min1] + smallR_B[bjet_min2];
  TLorentzVector bbSum_max = smallR_B[bjet_max1] + smallR_B[bjet_max2];
  TLorentzVector jjSum_max = smallR_noMatched[jet_max1] + smallR_noMatched[jet_max2];
  // two ways of defining jets outside tops, noticeable difference
  //TLorentzVector jjSum_1 = smallR_noMatched[jet_min1_1] + smallR_noMatched[jet_min2_1];
  //TLorentzVector jjSum_2 = smallR_noMatched[jet_min1_2] + smallR_noMatched[jet_min2_2];



  // 1D histograms
  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){

    //    std::cout << fVariables[iVar] << std::endl;
    
    int mu_n = fFlatTree -> mu_pt -> size();
    int el_n = fFlatTree -> el_pt -> size();

    if(fVariables[iVar] == "NHiggs_40_noTops"){

      fHistoVector[iVar].Fill(N_Higgs_40_noTops, histoweight);
      fTreeVariables[iVar] = N_Higgs_40_noTops;   

    } 
    if(fVariables[iVar] == "NHiggs_30_noTops"){

      fHistoVector[iVar].Fill(N_Higgs_30_noTops, histoweight);
      fTreeVariables[iVar] = N_Higgs_30_noTops;

    }
    if(fVariables[iVar] == "NHiggs_20_noTops"){

      fHistoVector[iVar].Fill(N_Higgs_20_noTops, histoweight);
      fTreeVariables[iVar] = N_Higgs_20_noTops;

    }
    if(fVariables[iVar] == "NHiggs_40_noJ"){

      fHistoVector[iVar].Fill(N_Higgs_40_noJ, histoweight);
      fTreeVariables[iVar] = N_Higgs_40_noJ;

    }
    if(fVariables[iVar] == "NHiggs_30_noJ"){

      fHistoVector[iVar].Fill(N_Higgs_30_noJ, histoweight);
      fTreeVariables[iVar] = N_Higgs_30_noJ;

    }
    if(fVariables[iVar] == "NHiggs_20_noJ"){

      fHistoVector[iVar].Fill(N_Higgs_20_noJ, histoweight);
      fTreeVariables[iVar] = N_Higgs_20_noJ;

    }
    if(fVariables[iVar] == "HT_all_noJ"){
      float HT_all = 0;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        HT_all = HT_all + (smallR_noMatched[jsmall].Perp());
      }
      for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
        HT_all = HT_all + (largeR[jlarge].Perp());
      }
      for(int eln = 0; eln < el_n; ++eln){
        HT_all = HT_all + (fFlatTree -> el_pt -> at(eln)/1000.0);
      }
      for(int mun = 0; mun < mu_n; ++mun){
        HT_all = HT_all + (fFlatTree -> mu_pt -> at(mun)/1000.0);
      }
      fHistoVector[iVar].Fill(HT_all, histoweight);
      fTreeVariables[iVar] = HT_all;
    }
    if(fVariables[iVar] == "H1_all_noJ"){
      float H[5] = {};
      float H1_all = 0;
      float P[5] = {};
      float sumE = 0, sumPt = 0, norm = 0;
      auto getx = [&](const TLorentzVector lv1, const TLorentzVector lv2){
        return cos( lv1.Angle(lv2.Vect()) );
      };

      std::vector<TLorentzVector> parts;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        parts.push_back(smallR_noMatched[jsmall]);
      }
      for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
        parts.push_back(largeR[jlarge]);
      }
      for(int eln = 0; eln < el_n; ++eln){
        float pt = ((fFlatTree -> el_pt -> at(eln))/1000.0);
        float eta = fFlatTree -> el_eta -> at(eln);
        float phi = fFlatTree -> el_phi -> at(eln);
        float e = (fFlatTree -> el_e -> at(eln))/1000.0;
        TLorentzVector electron;
        electron.SetPtEtaPhiE(pt, eta, phi, e);
        parts.push_back(electron);
      }
      for(int mun = 0; mun < mu_n; ++mun){
        float pt = ((fFlatTree -> mu_pt-> at(mun))/1000.0);
        float eta = fFlatTree -> mu_eta -> at(mun);
        float phi = fFlatTree-> mu_phi -> at(mun);
        float e = (fFlatTree -> mu_e -> at(mun))/1000.0;
        TLorentzVector muon;
        muon.SetPtEtaPhiE(pt, eta, phi, e);
        parts.push_back(muon);
      }
      for(unsigned int i = 0; i < parts.size(); i++){
        sumE += parts[i].E();
        sumPt += parts[i].Pt();
        for(unsigned int j = i; j < parts.size(); j++){
          float x = getx(parts[i],parts[j]);
          P[0] = 1;
          P[1] = x;
          P[2] = 0.5*(3*x*x-1);
          P[3] = 0.5*(5*x*x*x-3*x);
          P[4] = 0.125*(35*x*x*x*x-30*x*x+3);
          for(int k = 0; k<5; k++){
            if(j==i)
              H[k] += pow(fabs(parts[i].P()),2)*P[k];
            else
              H[k] += 2*fabs(parts[i].P()*parts[j].P())*P[k];
          }
        }
      }
      norm = sumE*sumE;
      if(norm != 0){
        H1_all = H[1] / norm;
      }
      fHistoVector[iVar].Fill(H1_all, histoweight);
      fTreeVariables[iVar] = H1_all;
    }


    if(fVariables[iVar] == "HT_all_noJ"){
      float HT_all = 0;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        HT_all = HT_all + (smallR_noMatched[jsmall].Perp());
      }
      for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
        HT_all = HT_all + (largeR[jlarge].Perp());
      }
      for(int eln = 0; eln < el_n; ++eln){
        HT_all = HT_all + (fFlatTree -> el_pt -> at(eln)/1000.0);
      }
      for(int mun = 0; mun < mu_n; ++mun){
        HT_all = HT_all + (fFlatTree -> mu_pt -> at(mun)/1000.0);
      }
      fHistoVector[iVar].Fill(HT_all, histoweight);
      fTreeVariables[iVar] = HT_all;
    }
    if(fVariables[iVar] == "H4_all_noJ"){
      float H[5] = {};
      float H4_all = 0;
      float P[5] = {};
      float sumE = 0, sumPt = 0, norm = 0;
      auto getx = [&](const TLorentzVector lv1, const TLorentzVector lv2){
        return cos( lv1.Angle(lv2.Vect()) );
      };

      std::vector<TLorentzVector> parts;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        parts.push_back(smallR_noMatched[jsmall]);
      }
      for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
        parts.push_back(largeR[jlarge]);
      }        for(int eln = 0; eln < el_n; ++eln){
        float pt = ((fFlatTree -> el_pt -> at(eln))/1000.0);
        float eta = fFlatTree -> el_eta -> at(eln);
        float phi = fFlatTree -> el_phi -> at(eln);
        float e = (fFlatTree -> el_e -> at(eln))/1000.0;
        TLorentzVector electron;
        electron.SetPtEtaPhiE(pt, eta, phi, e);
        parts.push_back(electron);
      }
      for(int mun = 0; mun < mu_n; ++mun){
        float pt = ((fFlatTree -> mu_pt-> at(mun))/1000.0);
        float eta = fFlatTree -> mu_eta -> at(mun);
        float phi = fFlatTree-> mu_phi -> at(mun);
        float e = (fFlatTree -> mu_e -> at(mun))/1000.0;
        TLorentzVector muon;
        muon.SetPtEtaPhiE(pt, eta, phi, e);
        parts.push_back(muon);
      }
      for(unsigned int i = 0; i < parts.size(); i++){
        sumE += parts[i].E();
        sumPt += parts[i].Pt();
        for(unsigned int j = i; j < parts.size(); j++){
          float x = getx(parts[i],parts[j]);
          P[0] = 1;
          P[1] = x;
          P[2] = 0.5*(3*x*x-1);
          P[3] = 0.5*(5*x*x*x-3*x);
          P[4] = 0.125*(35*x*x*x*x-30*x*x+3);
          for(int k = 0; k<5; k++){
            if(j==i)
              H[k] += pow(fabs(parts[i].P()),2)*P[k];
            else
              H[k] += 2*fabs(parts[i].P()*parts[j].P())*P[k];
          }
        }
      }
      norm = sumE*sumE;
      if(norm != 0){
        H4_all = H[4] / norm;
      }
      fHistoVector[iVar].Fill(H4_all, histoweight);
      fTreeVariables[iVar] = H4_all;
    }



    if(fVariables[iVar] == "Centrality_noJ"){
      float HT_all = 0;
      float E_all = 0;
      float Centrality = 0;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        HT_all = HT_all + (smallR_noMatched[jsmall].Perp());
        E_all = E_all + (smallR_noMatched[jsmall].E());
      }
      for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
        HT_all = HT_all + (largeR[jlarge].Perp());
        E_all = E_all + (largeR[jlarge].E());
      }
      for(int eln = 0; eln < el_n; ++eln){
        HT_all = HT_all + (fFlatTree -> el_pt -> at(eln)/1000.0);
        E_all = E_all + (fFlatTree -> el_e -> at(eln)/1000.0);
      }
      for(int mun = 0; mun < mu_n; ++mun){
        HT_all = HT_all + (fFlatTree -> mu_pt -> at(mun)/1000.0);
        E_all = E_all + (fFlatTree -> mu_e -> at(mun)/1000.0);
      }
      if(E_all == 0) {
        Centrality = 0;
      } else
        Centrality = HT_all / E_all;
      fHistoVector[iVar].Fill(Centrality, histoweight);
      fTreeVariables[iVar] = Centrality;
    }
    if(fVariables[iVar] == "HhadT_jets_J"){
      float HT_all = 0;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        HT_all = HT_all + (smallR_noMatched[jsmall].Perp());
      }
      for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
        HT_all = HT_all + (largeR[jlarge].Perp());
      }
      fHistoVector[iVar].Fill(HT_all, histoweight);
      fTreeVariables[iVar] = HT_all;
    }
    if(fVariables[iVar] == "HhadT_jets_noJ"){
      float HT_all = 0;
      for(unsigned int jsmall = 0; jsmall < smallR_noMatched.size(); ++jsmall){
        HT_all = HT_all + (smallR_noMatched[jsmall].Perp());
      }
      fHistoVector[iVar].Fill(HT_all, histoweight);
      fTreeVariables[iVar] = HT_all;
    }
    if(fVariables[iVar] == "Njet_pT40_noJ"){
      int counter = 0;
      for(unsigned int jsmalls = 0; jsmalls < smallR_noMatched.size(); ++jsmalls){
        if(smallR_noMatched[jsmalls].Perp() >= 40) {
          counter = counter + 1;
        }
      }
      fHistoVector[iVar].Fill(counter, histoweight);
      fTreeVariables[iVar] = counter;
    }
    if (smallR_noMatched.size() >= 5) {
      if(fVariables[iVar] == "pT_jet5_noJ"){
        float pT_jet5 = 0;
        pT_jet5 = smallR_noMatched[4].Perp();
        fHistoVector[iVar].Fill(pT_jet5, histoweight);
        fTreeVariables[iVar] = pT_jet5;
      }
    }
    if (smallR_noMatched.size() >= 2) {
      if(fVariables[iVar] == "Mjj_maxpT_noJ"){
        float maxpT = 0;
        float Mjj = 0;
        TLorentzVector TSum;
        for(unsigned int jsmalls = 0; jsmalls < smallR_noMatched.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_noMatched.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              TSum = smallR_noMatched[jsmalls] + smallR_noMatched[jsmalls_2];
              if(TSum.Perp() > maxpT) {
                maxpT = TSum.Perp();
                Mjj = TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Mjj, histoweight);
        fTreeVariables[iVar] = Mjj;
      }
      if(fVariables[iVar] == "Mjj_mindR_noJ"){
        float mindR =99999;
        float Mjj = 0;
        TLorentzVector TSum;
        for(unsigned int jsmalls = 0; jsmalls < smallR_noMatched.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_noMatched.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              if((smallR_noMatched[jsmalls].DeltaR(smallR_noMatched[jsmalls_2])) < mindR) {
                mindR = smallR_noMatched[jsmalls].DeltaR(smallR_noMatched[jsmalls_2]);
                TSum = smallR_noMatched[jsmalls] + smallR_noMatched[jsmalls_2];
                Mjj =TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Mjj, histoweight);
        fTreeVariables[iVar] = Mjj;
      }
      if(fVariables[iVar] == "Mjj_minM_noJ"){
        float Mjj = 99999;
        TLorentzVector TSum;
        for(unsigned int jsmalls = 0; jsmalls < smallR_noMatched.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_noMatched.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              TSum = smallR_noMatched[jsmalls] + smallR_noMatched[jsmalls_2];
              if(TSum.M() < Mjj) {
                Mjj =TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Mjj, histoweight);
        fTreeVariables[iVar] = Mjj;
      }


      if(fVariables[iVar] == "Mjj_HiggsMass_noJ"){
        float dM = 999999;
        float Mjj = 0;
        TLorentzVector TSum;
        for(unsigned int jsmalls = 0; jsmalls < smallR_noMatched.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_noMatched.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              TSum = smallR_noMatched[jsmalls] + smallR_noMatched[jsmalls_2];
              if(fabs((TSum.M()) - 125) < dM) {
                dM = fabs((TSum.M()) - 125);
                Mjj =TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Mjj, histoweight);
        fTreeVariables[iVar] = Mjj;
      }
      if(fVariables[iVar] == "Mbb_HiggsMass_noJ"){
        float dM = 999999;
        float Mjj = 0;
        TLorentzVector TSum;
        for(unsigned int jsmalls = 0; jsmalls < smallR_B.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_B.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              TSum = smallR_B[jsmalls] + smallR_B[jsmalls_2];
              if(fabs((TSum.M()) - 125) < dM) {
                dM = fabs((TSum.M()) - 125);
                Mjj =TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Mjj, histoweight);
        fTreeVariables[iVar] = Mjj;
      }
      if(fVariables[iVar] == "Mjb_HiggsMass_noJ"){
        float dM = 999999;
        float Mjj = 0;
        TLorentzVector TSum;
        for(unsigned int jsmalls = 0; jsmalls < smallR_B.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_B.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              TSum = smallR_B[jsmalls] + smallR_B[jsmalls_2];
              if(fabs((TSum.M()) - 125) < dM) {
                dM = fabs((TSum.M()) - 125);
                Mjj =TSum.M();
              }
            }
          }
          for(unsigned int usmalls = 0; usmalls < smallR_noB.size(); ++usmalls){
            TSum = smallR_B[jsmalls] + smallR_noB[usmalls];
            if(fabs((TSum.M()) - 125) < dM) {
              dM = fabs((TSum.M()) - 125);
              Mjj = TSum.M();
            }
          }
        }
        fHistoVector[iVar].Fill(Mjj, histoweight);
        fTreeVariables[iVar] = Mjj;
      }
      if(fVariables[iVar] == "dEtajj_mindEta_noJ"){
        float dEta = 99999.;
        float mindEta_temp = 0;
        for(unsigned int jsmalls = 0; jsmalls < smallR_noMatched.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_noMatched.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              mindEta_temp = fabs((smallR_noMatched[jsmalls].Eta()) - (smallR_noMatched[jsmalls_2].Eta()));
              if(mindEta_temp < dEta) {
                dEta = mindEta_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dEta, histoweight);
        fTreeVariables[iVar] = dEta;
      }
      if(fVariables[iVar] == "dEtabb_mindEta_noJ"){
        float dEta = 99999.;
        float mindEta_temp = 0.;
        for(unsigned int jsmalls = 0; jsmalls < smallR_B.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_B.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              mindEta_temp = fabs((smallR_B[jsmalls].Eta()) - (smallR_B[jsmalls_2].Eta()));
              if(mindEta_temp < dEta) {
                dEta = mindEta_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dEta, histoweight);
        fTreeVariables[iVar] = dEta;
      }
      if(fVariables[iVar] == "dEtabb_MHiggs_noJ"){
        float dEta = 0;
        float dEta_temp = 0;
        TLorentzVector TSum;
        float dM = 9999999;
        for(unsigned int jsmalls = 0; jsmalls < smallR_B.size(); ++jsmalls){
          for(unsigned int jsmalls_2= 0; jsmalls_2 < smallR_B.size(); ++jsmalls_2){
            if (jsmalls < jsmalls_2){
              dEta_temp = fabs((smallR_B[jsmalls].Eta()) - (smallR_B[jsmalls_2].Eta()));
              TSum = smallR_B[jsmalls] + smallR_B[jsmalls_2];
              if(fabs(TSum.M() - 125) < dM) {
                dM = fabs(TSum.M() - 125);
                dEta = dEta_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dEta, histoweight);
        fTreeVariables[iVar] = dEta;
      }
    }
    if(smallR_noB.size() >= 1 && smallR_B.size() >= 1){
      if(fVariables[iVar] == "Mbu_MaxpT_noJ"){
        float Mbu = 0;
        float pT_max = 0;
        float pT_temp = 0;
        TLorentzVector TSum;
        for(unsigned int usmall = 0; usmall < smallR_noB.size(); ++usmall){
          for(unsigned int bsmall = 0; bsmall < smallR_B.size(); ++bsmall){
            TSum = smallR_noB[usmall] + smallR_B[bsmall];
            pT_temp = TSum.Perp();
            if(pT_temp > pT_max){
              pT_max = pT_temp;
              Mbu = TSum.M();
            }
          }
        }
        fHistoVector[iVar].Fill(Mbu, histoweight);
        fTreeVariables[iVar] = Mbu;
      }
      if(fVariables[iVar] == "Mbu_MindR_noJ"){
        float Mbu = 0;
        float dR_min = 999999999;
        float dR_temp = 0;
        TLorentzVector TSum;
        for(unsigned int usmall = 0; usmall <smallR_noB.size(); ++usmall){
          for(unsigned int bsmall = 0; bsmall< smallR_B.size(); ++bsmall){
            TSum = smallR_noB[usmall] + smallR_B[bsmall];
            dR_temp = smallR_noB[usmall].DeltaR(smallR_B[bsmall]);
            if(dR_temp < dR_min){
              dR_min = dR_temp;
              Mbu = TSum.M();
            }
          }
        }
        fHistoVector[iVar].Fill(Mbu, histoweight);
        fTreeVariables[iVar] = Mbu;
      }
    }
    if(smallR_noB.size() >= 2){
      if(fVariables[iVar] == "Muu_mindR_noJ"){
        float mindR = 9999;
        float Muu = 0;
        TLorentzVector TSum;
        for(unsigned int usmalls = 0; usmalls < smallR_noB.size(); ++usmalls){
          for(unsigned int usmalls_2= 0; usmalls_2 < smallR_noB.size(); ++usmalls_2){
            if (usmalls < usmalls_2){
              if ((smallR_noB[usmalls].DeltaR(smallR_noB[usmalls_2])) < mindR) {
                mindR = (smallR_noB[usmalls].DeltaR(smallR_noB[usmalls_2]));
                TSum = smallR_noB[usmalls] + smallR_noB[usmalls_2];
                Muu = TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Muu, histoweight);
        fTreeVariables[iVar] = Muu;
      }
      if(fVariables[iVar] == "pTuu_mindR_noJ"){
        float mindR = 9999;
        float pTuu = 0;
        TLorentzVector TSum;
        for(unsigned int usmalls = 0; usmalls < smallR_noB.size(); ++usmalls){
          for(unsigned int usmalls_2= 0; usmalls_2 < smallR_noB.size(); ++usmalls_2){
            if (usmalls < usmalls_2){
              if ((smallR_noB[usmalls].DeltaR(smallR_noB[usmalls_2])) < mindR) {
                mindR= (smallR_noB[usmalls].DeltaR(smallR_noB[usmalls_2]));
                TSum = smallR_noB[usmalls] + smallR_noB[usmalls_2];
                pTuu =TSum.Perp();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(pTuu, histoweight);
        fTreeVariables[iVar] = pTuu;
      }
      if(fVariables[iVar] == "dRuu_mindR_noJ"){
        float mindR = 9999;
        for(unsigned int usmalls = 0; usmalls < smallR_noB.size(); ++usmalls){
          for(unsigned int usmalls_2= 0; usmalls_2 < smallR_noB.size(); ++usmalls_2){
            if (usmalls < usmalls_2){
              if ((smallR_noB[usmalls].DeltaR(smallR_noB[usmalls_2])) < mindR) {
                mindR = (smallR_noB[usmalls].DeltaR(smallR_noB[usmalls_2]));
              }
            }
          }
        }
        fHistoVector[iVar].Fill(mindR, histoweight);
        fTreeVariables[iVar] = mindR;
      }
    }

    if (smallR_B.size() >= 2) {
      if(fVariables[iVar] == "Mbb_avg_noJ"){
        float dM_avg = 0;
        int counter = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              dM_avg = dM_avg + TSum.M();
              counter = counter + 1;
            }
          }
        }
        dM_avg = dM_avg / counter;
        fHistoVector[iVar].Fill(dM_avg, histoweight);
        fTreeVariables[iVar] = dM_avg;
      }
      if(fVariables[iVar] == "Mbb_mindR_noJ"){
        float dM = 0;
        float dR_min = 99999;
        float dR_temp = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              dR_temp = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              if (dR_temp < dR_min) {
                dR_min = dR_temp;
                dM = TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dM, histoweight);
        fTreeVariables[iVar] = dM;
      }
      if(fVariables[iVar] == "Mbb_maxdR_noJ"){
        float dM = 0;
        float dR_max = 0;
        float dR_temp= 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              dR_temp= smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              if (dR_temp > dR_max) {
                dR_max = dR_temp;
                dM = TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dM, histoweight);
        fTreeVariables[iVar] = dM;
      }
      if(fVariables[iVar] == "Mbb_MaxM_noJ"){
        float M_temp = 0;
        float Max_M = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              M_temp = TSum.M();
              if (M_temp > Max_M) {
                Max_M = M_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(Max_M, histoweight);
        fTreeVariables[iVar] = Max_M;
      }
      if(fVariables[iVar] == "Mbb_minpT_noJ"){
        float dM = 0;
        float pT_min = 99999;
        float pT_temp = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              pT_temp = TSum.Perp();
              if (pT_temp < pT_min) {
                pT_min = pT_temp;
                dM = TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dM, histoweight);
        fTreeVariables[iVar] = dM;
      }
      if(fVariables[iVar] == "Mbb_maxpT_noJ"){
        float dM = 0;
        float pT_max = 0;
        float pT_temp= 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              pT_temp = TSum.Perp();
              if (pT_temp > pT_max) {
                pT_max = pT_temp;
                dM = TSum.M();
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dM, histoweight);
        fTreeVariables[iVar] = dM;
      }
      if(fVariables[iVar] == "dRbb_avg_noJ"){
        float dR_avg = 0;
        int counter = 0;
        float dR = 0;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              dR = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
              counter = counter + 1;
              dR_avg = dR_avg + dR;
            }
          }
        }
        dR_avg = dR_avg / counter;
        fHistoVector[iVar].Fill(dR_avg, histoweight);
        fTreeVariables[iVar] = dR_avg;
      }
      //  dRTb_avg_outTs - Average dR between the top tag and a b-jet (b-jet being outside the top tag)                                 
      if(fVariables[iVar] == "dRTb_avg_outTs"){
	float dR_avg = 0;
        int counter = 0;
        float dR = 0;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int tops= 0; tops < largeR.size(); ++tops){
	    dR = smallR_B[bsmalls].DeltaR(largeR[tops]);
	    counter = counter + 1;
	    dR_avg = dR_avg + dR;
          }
        }
        dR_avg = dR_avg / counter;
        fHistoVector[iVar].Fill(dR_avg, histoweight);
        fTreeVariables[iVar] = dR_avg;
      }
      if(fVariables[iVar] == "dRbb_mindR_noJ"){
        float dR_min = 999999;
        float dR_temp = 0;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              dR_temp = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
              if (dR_temp < dR_min) {
                dR_min = dR_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dR_min, histoweight);
        fTreeVariables[iVar] = dR_min;
      }
      if(fVariables[iVar] == "dRbb_maxdR_noJ"){
        float dR_max = 0;
        float dR_temp = 0;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              dR_temp = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
              if (dR_temp > dR_max) {
                dR_max = dR_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dR_max, histoweight);
        fTreeVariables[iVar] = dR_max;
      }
      if(fVariables[iVar] == "dRbb_maxM_noJ"){
        float dRbb = 0;
        float M_temp = 0;
        float Max_M = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              M_temp = TSum.M();
              if (M_temp > Max_M) {
                Max_M = M_temp;
                dRbb = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dRbb, histoweight);
        fTreeVariables[iVar] = dRbb;
      }
      if(fVariables[iVar] == "dRbb_maxpT_noJ"){
        float dR = 0;
        float pT_max = 0;
        float pT_temp = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              pT_temp = TSum.Perp();
              if (pT_temp > pT_max){
                dR = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
                pT_max = pT_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dR, histoweight);
        fTreeVariables[iVar] = dR;
      }
      if(fVariables[iVar] == "dRbb_minpT_noJ"){
        float dR = 0;
        float pT_min = 9999999;
        float pT_temp = 0;
        TLorentzVector TSum;
        for(unsigned int bsmalls = 0; bsmalls < smallR_B.size(); ++bsmalls){
          for(unsigned int bsmalls_2= 0; bsmalls_2 < smallR_B.size(); ++bsmalls_2){
            if (bsmalls < bsmalls_2){
              TSum = smallR_B[bsmalls] + smallR_B[bsmalls_2];
              pT_temp = TSum.Perp();
              if (pT_temp < pT_min){
                dR = smallR_B[bsmalls].DeltaR(smallR_B[bsmalls_2]);
                pT_min = pT_temp;
              }
            }
          }
        }
        fHistoVector[iVar].Fill(dR, histoweight);
        fTreeVariables[iVar] = dR;
      }
    }
    if (nGoodLargeR >= 1) {
      for(int i = 0; i < nGoodLargeR; ++i){
        if(fVariables[iVar] == "LargeR_M"){
          float J_M = largeR[i].M();
          fHistoVector[iVar].Fill(J_M, histoweight);
          fTreeVariables[iVar] = J_M;
        }
      }

      if(fVariables[iVar] == "dRJj"){
        float dR = 0;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dR = largeR[jlarge].DeltaR(smallR_noMatched[jsmall]);
            fHistoVector[iVar].Fill(dR, histoweight);
            fTreeVariables[iVar] = dR;
          }
        }
      }
      if(fVariables[iVar] == "dRJj_avg"){
        float dR = 0;
        float dR_avg = 0;
        float counter = 0;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dR = largeR[jlarge].DeltaR(smallR_noMatched[jsmall]);
            dR_avg = dR_avg + dR;
            counter = counter + 1;
          }
        }
        dR_avg = dR_avg / counter;
        fHistoVector[iVar].Fill(dR_avg, histoweight);
        fTreeVariables[iVar] = dR_avg;
      }
      if(fVariables[iVar] == "dRJb_avg"){
        float dR = 0;
        float dR_avg = 0;
        float counter = 0;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_B.size(); ++jsmall){
            dR = largeR[jlarge].DeltaR(smallR_B[jsmall]);
            dR_avg = dR_avg + dR;
            counter = counter + 1;
          }
        }
        dR_avg = dR_avg / counter;
        fHistoVector[iVar].Fill(dR_avg, histoweight);
        fTreeVariables[iVar] = dR_avg;
      }      
      if(fVariables[iVar] == "dRJj_mindR"){
        float dR_temp = 0;
        float mindR = 999999;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dR_temp = largeR[jlarge].DeltaR(smallR_noMatched[jsmall]);
            if (dR_temp < mindR) {
              mindR = dR_temp;
            }
          }
        }
        fHistoVector[iVar].Fill(mindR, histoweight);
        fTreeVariables[iVar] = mindR;
      }
      if(fVariables[iVar] == "dRJj_maxdR"){
        float dR_temp = 0;
        float maxdR = 0;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dR_temp = largeR[jlarge].DeltaR(smallR_noMatched[jsmall]);
            if (dR_temp > maxdR) {
              maxdR = dR_temp;
            }
          }
        }
        fHistoVector[iVar].Fill(maxdR, histoweight);
        fTreeVariables[iVar] = maxdR;
      }
      if(fVariables[iVar] == "dPhiJj_avg"){
        float dphi = 0;
        float dphi_avg = 0;
        float counter = 0;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dphi = largeR[jlarge].DeltaPhi(smallR_noMatched[jsmall]);
            dphi_avg = dphi_avg + dphi;
            counter = counter + 1;
          }
        }
        dphi_avg = dphi_avg / counter;
        fHistoVector[iVar].Fill(dphi_avg, histoweight);
        fTreeVariables[iVar] = dphi_avg;
      }
      if(fVariables[iVar] == "dPhiJj_mindR"){
        float dR_temp = 0;
        float dphi = 0;
        float mindR = 999999;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dR_temp = largeR[jlarge].DeltaR(smallR_noMatched[jsmall]);
            if (dR_temp < mindR) {
              mindR = dR_temp;
              dphi = largeR[jlarge].DeltaPhi(smallR_noMatched[jsmall]);
            }
          }
        }
        fHistoVector[iVar].Fill(dphi, histoweight);
        fTreeVariables[iVar] = dphi;
      }
      if(fVariables[iVar] == "dPhiJj_maxdR"){
        float dR_temp = 0;
        float dphi = 0;
        float maxdR = 0;
        for(int jlarge = 0; jlarge < nGoodLargeR; ++jlarge){
          for(unsigned int jsmall = 0; jsmall< smallR_noMatched.size(); ++jsmall){
            dR_temp = largeR[jlarge].DeltaR(smallR_noMatched[jsmall]);
            if (dR_temp > maxdR) {
              maxdR = dR_temp;
              dphi = largeR[jlarge].DeltaPhi(smallR_noMatched[jsmall]);
            }
          }
        }
        fHistoVector[iVar].Fill(dphi, histoweight);
        fTreeVariables[iVar] = dphi;
      }
      

      // variables added by Donatas
      /*if(fVariables[iVar] == "SMdrop_jj"){
        fHistoVector[iVar].Fill(SMdrop_jj, histoweight);
        fTreeVariables[iVar] = SMdrop_jj;
      }
      if(fVariables[iVar] == "SMdrop_bb"){
        fHistoVector[iVar].Fill(SMdrop_bb, histoweight);
        fTreeVariables[iVar] = SMdrop_bb;
      }
      if(fVariables[iVar] == "Mjj_mindrop_1"){
        fHistoVector[iVar].Fill(jjSum_1.M() , histoweight);
        fTreeVariables[iVar] = jjSum_1.M();
      }
      if(fVariables[iVar] == "Mjj_mindrop_2"){
        fHistoVector[iVar].Fill(jjSum_2.M() , histoweight);
        fTreeVariables[iVar] = jjSum_2.M();
      }
      if(fVariables[iVar] == "Mbb_mindrop"){
        fHistoVector[iVar].Fill(bbSum.M() , histoweight);
        fTreeVariables[iVar] = bbSum.M();
      }
      if(fVariables[iVar] == "dRjj_mindrop_1"){
        fHistoVector[iVar].Fill(smallR_noMatched[jet_min1_1].DeltaR(smallR_noMatched[jet_min2_1]) , histoweight);
        fTreeVariables[iVar] = smallR_noMatched[jet_min1_1].DeltaR(smallR_noMatched[jet_min2_1]);
      }
      if(fVariables[iVar] == "dRjj_mindrop_2"){
        fHistoVector[iVar].Fill(smallR_noMatched[jet_min1_2].DeltaR(smallR_noMatched[jet_min2_2]) , histoweight);
        fTreeVariables[iVar] = smallR_noMatched[jet_min1_2].DeltaR(smallR_noMatched[jet_min2_2]);
      }
      if(fVariables[iVar] == "dRbb_mindrop"){
        fHistoVector[iVar].Fill(smallR_B[bjet_min1].DeltaR(smallR_B[bjet_min2]) , histoweight);
        fTreeVariables[iVar] = smallR_B[bjet_min1].DeltaR(smallR_B[bjet_min2]);
      }
      if(fVariables[iVar] == "dEtabb_mindrop"){
        fHistoVector[iVar].Fill(fabs(smallR_B[bjet_min1].Eta()-smallR_B[bjet_min2].Eta()) , histoweight);
        fTreeVariables[iVar] = fabs(smallR_B[bjet_min1].Eta()-smallR_B[bjet_min2].Eta());
      }
      if(fVariables[iVar] == "dEtajj_mindrop_1"){
        fHistoVector[iVar].Fill(fabs(smallR_noMatched[jet_min1_1].Eta()-smallR_noMatched[jet_min2_1].Eta()) , histoweight);
        fTreeVariables[iVar] = fabs(smallR_noMatched[jet_min1_1].Eta()-smallR_noMatched[jet_min2_1].Eta());
      }
      if(fVariables[iVar] == "dEtajj_mindrop_2"){
        fHistoVector[iVar].Fill(fabs(smallR_noMatched[jet_min1_2].Eta()-smallR_noMatched[jet_min2_2].Eta()) , histoweight);
        fTreeVariables[iVar] = fabs(smallR_noMatched[jet_min1_2].Eta()-smallR_noMatched[jet_min2_2].Eta());
      }


      if(fVariables[iVar] == "m1_m12_jj_mindrop"){
        j_maxM = (smallR_noMatched[jet_min1_2].M() > smallR_noMatched[jet_min2_2].M()) ? smallR_noMatched[jet_min1_2].M() : smallR_noMatched[jet_min2_2].M();
        fHistoVector[iVar].Fill(j_maxM/jjSum_2.M() , histoweight);
      }
      if(fVariables[iVar] == "m1m2_m12_jj_mindrop"){
        fHistoVector[iVar].Fill((smallR_noMatched[jet_min1_2].M()+smallR_noMatched[jet_min2_2].M())/jjSum_2.M() , histoweight);
      }
      if(fVariables[iVar] == "m1_m12_bb_mindrop"){
        b_maxM = (smallR_B[bjet_min1].M() > smallR_B[bjet_min2].M()) ? smallR_B[bjet_min1].M() : smallR_B[bjet_min2].M();
        fHistoVector[iVar].Fill(b_maxM/bbSum.M() , histoweight);
      }
      if(fVariables[iVar] == "m1m2_m12_bb_mindrop"){
        fHistoVector[iVar].Fill((smallR_B[bjet_min1].M()+smallR_B[bjet_min2].M())/bbSum.M() , histoweight);
      }
      if(fVariables[iVar] == "m1_m12_jj_maxdrop"){
      j_maxM = (smallR_noMatched[jet_max1].M() > smallR_noMatched[jet_max2].M()) ? smallR_noMatched[jet_max1].M() : smallR_noMatched[jet_max2].M();
        fHistoVector[iVar].Fill(j_maxM/jjSum_max.M() , histoweight);
        }
      if(fVariables[iVar] == "m1m2_m12_jj_maxdrop"){
        fHistoVector[iVar].Fill((smallR_noMatched[jet_max1].M()+smallR_noMatched[jet_max2].M())/jjSum_max.M() , histoweight);
      }
      if(fVariables[iVar] == "m1_m12_bb_maxdrop"){
        b_maxM = (smallR_B[bjet_max1].M() > smallR_B[bjet_max2].M()) ? smallR_B[bjet_max1].M() : smallR_B[bjet_max2].M();
        fHistoVector[iVar].Fill(b_maxM/bbSum_max.M() , histoweight);
      }
      if(fVariables[iVar] == "m1m2_m12_bb_maxdrop"){
        fHistoVector[iVar].Fill((smallR_B[bjet_max1].M()+smallR_B[bjet_max2].M())/bbSum_max.M() , histoweight);
      }

      std::cout << "Hier 9" << std::endl;

      */

    }

  }

}


