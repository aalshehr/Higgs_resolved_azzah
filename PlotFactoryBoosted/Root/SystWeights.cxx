#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

float HistoCreator::GetSystWeights(std::string Syst)
{
  
  //if (Syst == "weight_pileup")
  //   fTreeVariables[iVar] = fFlatTree -> weight_pileup;
  // if (Syst == "weight_leptonSF")
  //   fTreeVariables[iVar] = fFlatTree -> weight_leptonSF;

  if (Syst == "weight_pileup_UP")
    {
      // since PU weight is sometimes 0, for the syst we need to treat it differently
      // std::cout << "nominal = " << fFlatTree -> weight_pileup << std::endl;
      //std::cout << "up = " << fFlatTree -> weight_pileup_UP << std::endl;
      //std::cout << "ratio = " << fFlatTree->weight_pileup_UP / fFlatTree -> weight_pileup << std::endl;;
    return fFlatTree -> weight_pileup_UP;
    }
  if (Syst == "weight_pileup_DOWN")
    return fFlatTree -> weight_pileup_DOWN;

    if (Syst == "leptonSF_EL_SF_Trigger_UP")
      return fFlatTree -> weight_leptonSF_EL_SF_Trigger_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_Trigger_DOWN")
      return fFlatTree -> weight_leptonSF_EL_SF_Trigger_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_Reco_UP")
      return fFlatTree -> weight_leptonSF_EL_SF_Reco_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_Reco_DOWN")
      return fFlatTree -> weight_leptonSF_EL_SF_Reco_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_ID_UP")
      return fFlatTree -> weight_leptonSF_EL_SF_ID_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_ID_DOWN")
      return fFlatTree -> weight_leptonSF_EL_SF_ID_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_Isol_UP")
      return fFlatTree -> weight_leptonSF_EL_SF_Isol_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_EL_SF_Isol_DOWN")
      return fFlatTree -> weight_leptonSF_EL_SF_Isol_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Trigger_STAT_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_Trigger_STAT_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Trigger_STAT_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_Trigger_STAT_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Trigger_SYST_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_Trigger_SYST_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Trigger_SYST_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_Trigger_SYST_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_ID_STAT_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_ID_STAT_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_ID_STAT_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_ID_STAT_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_ID_SYST_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_ID_SYST_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_ID_SYST_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_ID_SYST_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Isol_STAT_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_Isol_STAT_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Isol_STAT_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_Isol_STAT_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Isol_SYST_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_Isol_SYST_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_Isol_SYST_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_Isol_SYST_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_TTVA_STAT_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_TTVA_STAT_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_TTVA_STAT_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_TTVA_STAT_DOWN/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_TTVA_SYST_UP")
      return fFlatTree -> weight_leptonSF_MU_SF_TTVA_SYST_UP/fFlatTree -> weight_leptonSF;
    if (Syst == "leptonSF_MU_SF_TTVA_SYST_DOWN")
      return fFlatTree -> weight_leptonSF_MU_SF_TTVA_SYST_DOWN/fFlatTree -> weight_leptonSF;

    if (Syst == "bTagSF_77_extrapolation_up")
      return fFlatTree -> weight_bTagSF_77_extrapolation_up/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_extrapolation_down")
      return fFlatTree -> weight_bTagSF_77_extrapolation_down/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_extrapolation_from_charm_up")
      return fFlatTree -> weight_bTagSF_77_extrapolation_from_charm_up/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_extrapolation_from_charm_down")
      return fFlatTree -> weight_bTagSF_77_extrapolation_from_charm_down/fFlatTree -> weight_bTagSF_77;

    if (Syst == "jvt_UP")
      return fFlatTree -> weight_jvt_UP/fFlatTree -> weight_jvt;
    if (Syst == "jvt_DOWN")
      return fFlatTree -> weight_jvt_DOWN/fFlatTree -> weight_jvt;

    if (Syst == "bTagSF_77_eigenvars_B_up_0")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_up ->at(0)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_up_1")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_up ->at(1)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_up_2")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_up ->at(2)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_up_3")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_up ->at(3)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_up_4")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_up ->at(4)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_down_0")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_down ->at(0)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_down_1")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_down ->at(1)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_down_2")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_down ->at(2)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_down_3")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_down ->at(3)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_B_down_4")
      return fFlatTree -> weight_bTagSF_77_eigenvars_B_down ->at(4)/fFlatTree -> weight_bTagSF_77;

    if (Syst == "bTagSF_77_eigenvars_C_up_0")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_up ->at(0)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_up_1")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_up ->at(1)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_up_2")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_up ->at(2)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_up_3")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_up ->at(3)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_down_0")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_down ->at(0)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_down_1")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_down ->at(1)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_down_2")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_down ->at(2)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_C_down_3")
      return fFlatTree -> weight_bTagSF_77_eigenvars_C_down ->at(3)/fFlatTree -> weight_bTagSF_77;

    /*    if (Syst == "bTagSF_77_eigenvars_Light_up_0")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(0)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_1")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(1)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_2")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(2)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_3")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(3)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_4")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(4)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_5")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(5)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_6")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(6)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_7")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(7)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_8")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(8)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_9")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(9)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_10")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(10)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_up_11")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_up ->at(11)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_0")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(0)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_1")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(1)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_2")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(2)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_3")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(3)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_4")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(4)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_5")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(5)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_6")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(6)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_7")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(7)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_8")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(8)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_9")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(9)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_10")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(10)/fFlatTree -> weight_bTagSF_77;
    if (Syst == "bTagSF_77_eigenvars_Light_down_11")
      return fFlatTree -> weight_bTagSF_77_eigenvars_Light_down ->at(11)/fFlatTree -> weight_bTagSF_77;
    */

}


