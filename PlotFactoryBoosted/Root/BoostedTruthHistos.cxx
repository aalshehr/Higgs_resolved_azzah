#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"
#include "PlotFactoryBoosted/GeneralTruthHelper.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

void HistoCreator::FillBoostedTruthHistos(EventExtras* iEntryExtrasInfo, string fSampleType, string fTreeName)
{
  
  float histoweight = iEntryExtrasInfo->GetEntryWeight();

  
  std::vector<TLorentzVector> largeR;
  largeR.clear();
  largeR = iEntryExtrasInfo->GetGoodLJets();

  std::vector<TLorentzVector> largeR_T_loose;
  largeR_T_loose.clear();
  largeR_T_loose = iEntryExtrasInfo->GetTopTaggedJets();
  
  bool lepTop = false;
  bool hadTop = false;
  bool Higgs = false;

  TLorentzVector truthlepTop;
  TLorentzVector truthhadTop;
  TLorentzVector truthTopTop;
  TLorentzVector truthTopTopHiggs;
  TLorentzVector truthHiggs;

  int decay = fFlatTree -> truth_HDecay;

  if(fSampleType == "MC" && fTreeName == "nominal_Loose"){
    int truthJets = fFlatTree -> truth_pt -> size();
    for(int iTop = 0; iTop < truthJets; ++iTop){
      float eta = (fFlatTree -> truth_eta -> at(iTop));
      float phi = (fFlatTree -> truth_phi -> at(iTop));
      float pt = (fFlatTree -> truth_pt -> at(iTop))/1000;
      float m = (fFlatTree -> truth_m -> at(iTop))/1000;

      if(TTHbbTruth::isHadTop( fFlatTree -> truth_pdgid->at(iTop), fFlatTree -> truth_tthbb_info->at(iTop))){
	truthhadTop.SetPtEtaPhiM(pt, eta, phi, m);
	hadTop = true;
      }
      else if (TTHbbTruth::isLepTop( fFlatTree -> truth_pdgid->at(iTop), fFlatTree -> truth_tthbb_info->at(iTop))){
	truthlepTop.SetPtEtaPhiM(pt, eta, phi, m);
	lepTop = true;
      }
      else if(TTHbbTruth::isHiggs( fFlatTree -> truth_pdgid->at(iTop), fFlatTree -> truth_tthbb_info->at(iTop))){
	Higgs = true;
	truthHiggs.SetPtEtaPhiM(pt, eta, phi, m);
      }
    }
  }
  
  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){
    
    if(Higgs == true && lepTop == true && hadTop == true && decay==0){
      
      if(fVariables[iVar] == "dPhi_truthHiggs_truthhadTop"){
	fHistoVector[iVar].Fill((truthHiggs.DeltaPhi(truthhadTop)), histoweight);
      }
      if(fVariables[iVar] == "dPhi_truthHiggs_truthlepTop"){
	fHistoVector[iVar].Fill((truthHiggs.DeltaPhi(truthlepTop)), histoweight);
      }
      if(fVariables[iVar] == "dR_truthHiggs_truthhadTop"){
	fHistoVector[iVar].Fill(fabs(truthHiggs.DeltaR(truthhadTop)), histoweight);
      }
      if(fVariables[iVar] == "dR_truthHiggs_truthlepTop"){
	fHistoVector[iVar].Fill(fabs(truthHiggs.DeltaR(truthlepTop)), histoweight);
      }
      if(fVariables[iVar] == "dEta_truthHiggs_truthhadTop"){
	float deta = (truthHiggs.Eta() - truthhadTop.Eta());
	fHistoVector[iVar].Fill(deta, histoweight);
      }
      if(fVariables[iVar] == "dEta_truthHiggs_truthlepTop"){
	float deta = (truthHiggs.Eta() - truthlepTop.Eta());
	fHistoVector[iVar].Fill(deta,histoweight);
      }
      if(fVariables[iVar] == "dR_truthHiggs_ljet0"){
	fHistoVector[iVar].Fill(fabs(truthHiggs.DeltaR(largeR[0])));
      }
      if(fVariables[iVar] == "dR_truthHiggs_ljetTop0"){
	fHistoVector[iVar].Fill(fabs(truthHiggs.DeltaR(largeR_T_loose[0])));
      }
      if(fVariables[iVar] == "leading_ljetTop_Higgs"){
	if(truthHiggs.DeltaR(largeR_T_loose[0]) <= 1.0){
	  fHistoVector[iVar].Fill(1, histoweight);
	  fTreeVariables[iVar] = 1;
	}
	if(truthHiggs.DeltaR(largeR_T_loose[0]) > 1.0){
	  fHistoVector[iVar].Fill(2, histoweight);
	  fTreeVariables[iVar] = 2;
	}
      }
    
      if(fVariables[iVar] == "truth_hadTop_pt"){
	fHistoVector[iVar].Fill(truthhadTop.Pt());
      }
      if(fVariables[iVar] == "truth_hadTop_eta"){
	fHistoVector[iVar].Fill(truthhadTop.Eta());
      }
      if(fVariables[iVar] == "truth_lepTop_pt"){
	fHistoVector[iVar].Fill(truthlepTop.Pt());
      }
      if(fVariables[iVar] == "truth_lepTop_eta"){
	fHistoVector[iVar].Fill(truthlepTop.Eta());
      }
      if(fVariables[iVar] == "truth_Higgs_pt"){
	fHistoVector[iVar].Fill(truthHiggs.Pt());
      }
      if(fVariables[iVar] == "truth_Higgs_eta"){
	fHistoVector[iVar].Fill(truthHiggs.Eta());
      }
      truthTopTop = truthlepTop + truthhadTop;
      truthTopTopHiggs = truthlepTop + truthhadTop + truthHiggs;
      if(fVariables[iVar] == "truth_ttbar_pt"){
        fHistoVector[iVar].Fill(truthTopTop.Pt());
      }
      if(fVariables[iVar] == "truth_ttbar_eta"){
        fHistoVector[iVar].Fill(truthTopTop.Eta());
      }
      if(fVariables[iVar] == "truth_ttH_pt"){
        fHistoVector[iVar].Fill(truthTopTopHiggs.Pt());
      }
      if(fVariables[iVar] == "truth_ttH_eta"){
        fHistoVector[iVar].Fill(truthTopTopHiggs.Eta());
      }

    }
  
    if(lepTop == true && hadTop == true){
      if(fVariables[iVar] == "dPhi_truthlepTop_truthhadTop"){
	fHistoVector[iVar].Fill((truthlepTop.DeltaPhi(truthhadTop)), histoweight);
      }
      if(fVariables[iVar] == "dR_truthlepTop_truthhadTop"){
	fHistoVector[iVar].Fill(truthlepTop.DeltaR(truthhadTop), histoweight);
      }
      if(fVariables[iVar] == "dEta_truthlepTop_truthhadTop"){
	float deta = (truthlepTop.Eta() - truthhadTop.Eta());
	fHistoVector[iVar].Fill(deta,histoweight);
      }
      if(fVariables[iVar] == "dR_truthhadTop_ljet0"){
	fHistoVector[iVar].Fill(truthhadTop.DeltaR(largeR[0]));
      }
      if(fVariables[iVar] == "dR_truthhadTop_ljetTop0"){
	fHistoVector[iVar].Fill(truthhadTop.DeltaR(largeR_T_loose[0]));
      }
      if(fVariables[iVar] == "dR_truthlepTop_ljet0"){
	fHistoVector[iVar].Fill(truthlepTop.DeltaR(largeR[0]));
      }
      if(fVariables[iVar] == "dR_truthlepTop_ljetTop0"){
	fHistoVector[iVar].Fill(truthlepTop.DeltaR(largeR_T_loose[0]));
      }
      if(fVariables[iVar] == "leading_ljetTop_leptonic"){
	if(truthlepTop.DeltaR(largeR_T_loose[0]) <= 1.0){
	  fHistoVector[iVar].Fill(1, histoweight);
	  fTreeVariables[iVar] = 1;
	}
	if(truthlepTop.DeltaR(largeR_T_loose[0]) > 1.0){
	  fHistoVector[iVar].Fill(2, histoweight);
	  fTreeVariables[iVar] = 2;
	}
      }
      if(fVariables[iVar] == "leading_ljetTop_hadronic"){
	if(truthhadTop.DeltaR(largeR_T_loose[0]) <= 1.0){
	  fHistoVector[iVar].Fill(1, histoweight);
	  fTreeVariables[iVar] = 1;
	}
	if(truthhadTop.DeltaR(largeR_T_loose[0]) > 1.0){
	  fHistoVector[iVar].Fill(2, histoweight);
	  fTreeVariables[iVar] = 2;
	}
      }

    }
  
  } 
 
  for(unsigned iVar_2D = 0; iVar_2D < fVar_2D.size(); ++iVar_2D){
    if(fVariables_2D[iVar_2D] == "hadtopPt_vs_ttbarpt"){
	
      int xVal = 0;
      int yVal = 0;

      if(lepTop == true && hadTop == true){

	truthTopTop = truthlepTop + truthhadTop;
	if(truthhadTop.Pt() > 0.   && truthhadTop.Pt() <= 200.)  xVal = 1;
	if(truthhadTop.Pt() > 200. && truthhadTop.Pt() <= 500.)  xVal = 2;
	if(truthhadTop.Pt() > 500. && truthhadTop.Pt() <= 6500.) xVal = 3;
	
	if(truthTopTop.Pt() > 0.   && truthTopTop.Pt() <= 150.)   yVal = 1;
	if(truthTopTop.Pt() > 150. && truthTopTop.Pt() <= 350.)   yVal = 2;
	if(truthTopTop.Pt() > 350. && truthTopTop.Pt() <= 13000.) yVal = 3;

      }	
      fHistoVector_2D[iVar_2D].Fill(xVal, yVal, histoweight);	
    }
    	       
  }

}


