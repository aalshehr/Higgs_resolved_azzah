#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"
#include "PlotFactoryBoosted/GeneralTruthHelper.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<int> >* +;
#endif

int eventcounter=0;

void HistoCreator::FillTTHExtraHistos(EventExtras* iEntryExtrasInfo)
{
  //first up date to send it to git ..  
  float histoweight = iEntryExtrasInfo->GetEntryWeight();

  int nJetCount = iEntryExtrasInfo->GetNJetsAll();
  int nTagCount = iEntryExtrasInfo->GetNBTagsAll();

  float eventNumber;
  float dRlj_MindR;
  int nHiggsbb30_77;
  float BTagCut = iEntryExtrasInfo->GetBTagCut();
  //std::cout<<"AZZAH BTAG CUT: "<<BTagCut <<std::endl;

  int nb1_count_pt = fFlatTree->klfitter_model_Higgs_b1_pt->size(); 
  int nb1_count_eta = fFlatTree->klfitter_model_Higgs_b1_eta->size(); 
  int nb1_count_phi = fFlatTree->klfitter_model_Higgs_b1_phi->size(); 
  int nb1_count_E = fFlatTree->klfitter_model_Higgs_b1_E->size(); 
  int nb1_count_jetIndex = fFlatTree->klfitter_model_Higgs_b1_jetIndex->size(); 
  //---------b2-------------------
  int nb2_count_pt = fFlatTree->klfitter_model_Higgs_b2_pt->size(); 
  int nb2_count_eta = fFlatTree->klfitter_model_Higgs_b2_eta->size(); 
  int nb2_count_phi = fFlatTree->klfitter_model_Higgs_b2_phi->size(); 
  int nb2_count_E = fFlatTree->klfitter_model_Higgs_b2_E->size(); 
  int nb2_count_jetIndex = fFlatTree->klfitter_model_Higgs_b2_jetIndex->size(); 
  //---------top had--------------
  float         klfitter_bestPerm_topHad_pt;
  float         klfitter_bestPerm_topHad_eta;
  float         klfitter_bestPerm_topHad_phi;
  float         klfitter_bestPerm_topHad_E;
 //---------top lep--------------
  float         klfitter_bestPerm_topLep_pt;
  float         klfitter_bestPerm_topLep_eta;
  float         klfitter_bestPerm_topLep_phi;
  float         klfitter_bestPerm_topLep_E;
   //---------ttbar--------------
  float         klfitter_bestPerm_ttbar_pt;
  float         klfitter_bestPerm_ttbar_eta;
  float         klfitter_bestPerm_ttbar_phi;
  float         klfitter_bestPerm_ttbar_E;
  
  TLorentzVector Higgs_b1_R;
  TLorentzVector Higgs_b2_R;
  TLorentzVector Higgs_b1_plus_b2_R;
  
  TLorentzVector Top_h_R;
  TLorentzVector Top_Mh_R;
  TLorentzVector Top_L_R;
  TLorentzVector Top_ML_R;
  TLorentzVector ttbar;
  
  float Higgs_deltaR;
  float Top_H_deltaR;
  float Top_MH_deltaR;
  float Top_L_deltaR;
  float Higgs_b1b2_deltaR;
  
  
  float b1_temp_pt;
  float b1_temp_eta;
  float b1_temp_phi;
  float b1_temp_E;

  float b2_temp_pt;
  float b2_temp_eta;
  float b2_temp_phi;
  float b2_temp_E;
  
  float thad_temp_pt;
  float thad_temp_eta;
  float thad_temp_phi;
  float thad_temp_E;

  float tlep_temp_pt;
  float tlep_temp_eta;
  float tlep_temp_phi;
  float tlep_temp_E;

  float ttbar_temp_pt;
  float ttbar_temp_eta;
  float ttbar_temp_phi;
  float ttbar_temp_E;
  
  float matched_Higgs_pt;
  float matched_Higgs_Phi;
  float matched_Higgs_E;
  float matched_Higgs_Eta;
  float matched_Higgs_mass;
  int   matchingHiggs;

  float failedmatch_Higgs_pt;
  float failedmatch_Higgs_Phi;
  float failedmatch_Higgs_E;
  float failedmatch_Higgs_Eta;
  float failedmatch_Higgs_mass;


  float allevents_Higgs_pt;
  float allevents_Higgs_Phi;
  float allevents_Higgs_E;
  float allevents_Higgs_Eta;
  float allevents_Higgs_mass;

  float Higgs_matched_b1b2_deltaR;
  float Higgs_failedmatch_b1b2_deltaR;
  float Higgs_allevents_b1b2_deltaR;

  float matched_thad_M;
  float matched_thad_pt;
  float matched_thad_Eta;
  float matched_thad_Phi;
  float matched_thad_E;
  int   matchingthad;
  int   matchingtlep;

  float failedmatch_thad_M;
  float failedmatch_thad_pt;
  float failedmatch_thad_Eta;
  float failedmatch_thad_Phi;
  float failedmatch_thad_E;

  float allevents_thad_M;
  float allevents_thad_pt;
  float allevents_thad_Eta;
  float allevents_thad_Phi;
  float allevents_thad_E;
  
  float matched_tlep_M;
  float matched_tlep_pt;
  float matched_tlep_Eta;
  float matched_tlep_Phi;
  float matched_tlep_E;

  float failedmatch_tlep_M;
  float failedmatch_tlep_pt;
  float failedmatch_tlep_Eta;
  float failedmatch_tlep_Phi;
  float failedmatch_tlep_E;

  float allevents_tlep_M;
  float allevents_tlep_pt;
  float allevents_tlep_Eta;
  float allevents_tlep_Phi;
  float allevents_tlep_E;
  
  float matched_tt_M;
  float matched_tt_pt;
  float matched_tt_Eta;
  float matched_tt_Phi;
  float matched_tt_E;

  float failedmatch_tt_M;
  float failedmatch_tt_pt;
  float failedmatch_tt_Eta;
  float failedmatch_tt_Phi;
  float failedmatch_tt_E;

  float allevents_tt_M;
  float allevents_tt_pt;
  float allevents_tt_Eta;
  float allevents_tt_Phi;
  float allevents_tt_E;

  float EnergyResolutionhadTop;
  float EnergyResolutionlepTop;

  float btag;
  std::vector<float>btagvector;
  btagvector.clear();
  
  float Higgs_Ji_deltaR;
  std::vector<float> Higgs_Ji_deltaRs;
  Higgs_Ji_deltaRs.clear();

  std::vector<float> Higgs_Ji_deltaRs_sorted;
  Higgs_Ji_deltaRs_sorted.clear();
  
  std::vector<int>special_k;
  std::vector<float>special_k_pt;
  special_k.clear();
  special_k_pt.clear();

  std::vector<int>special_k_matched;
  std::vector<float>special_k_pt_matched;
  special_k_matched.clear();
  special_k_pt_matched.clear();
  //-------------special_thad_k---------------
  float thad_Ji_deltaR;
  std::vector<float> thad_Ji_deltaRs;
  thad_Ji_deltaRs.clear();
  
  std::vector<float> thad_Ji_deltaRs_sorted;
  thad_Ji_deltaRs_sorted.clear();
  
  std::vector<int>special_thad_k;
  std::vector<float>special_thad_k_pt;
  special_thad_k.clear();
  special_thad_k_pt.clear();

  std::vector<int>special_thad_k_matched;
  std::vector<float>special_thad_k_pt_matched;
  special_thad_k_matched.clear();
  special_thad_k_pt_matched.clear();
//-------------special_tlep_k---------------
  float tlep_Ji_deltaR;
  std::vector<float> tlep_Ji_deltaRs;
  tlep_Ji_deltaRs.clear();
  
  std::vector<float> tlep_Ji_deltaRs_sorted;
  tlep_Ji_deltaRs_sorted.clear();
  
  std::vector<int>special_tlep_k;
  std::vector<float>special_tlep_k_pt;
  special_tlep_k.clear();
  special_tlep_k_pt.clear();

  std::vector<int>special_tlep_k_matched;
  std::vector<float>special_tlep_k_pt_matched;
  special_tlep_k_matched.clear();
  special_tlep_k_pt_matched.clear();


  int resolvedchannel = 0;
  if(nJetCount==4 && nTagCount==2) resolvedchannel = 1;
  else if(nJetCount==5 && nTagCount==2) resolvedchannel = 2;
  else if(nJetCount>=6 && nTagCount==2) resolvedchannel = 3;
  else if(nJetCount==4 && nTagCount==3) resolvedchannel = 4;
  else if(nJetCount==5 && nTagCount==3) resolvedchannel = 5;
  else if(nJetCount>=6 && nTagCount==3) resolvedchannel = 6;
  else if(nJetCount==4 && nTagCount>=4) resolvedchannel = 7;
  else if(nJetCount==5 && nTagCount>=4) resolvedchannel = 8;
  else if(nJetCount>=6 && nTagCount>=4) resolvedchannel = 9;
  else resolvedchannel = 10;

  bool lepTop = false;
  bool hadTop = false;
  bool Higgs = false;
  bool GoodEvent = false;
  bool PoorEvent = false;

  TLorentzVector truthlepTop;
  TLorentzVector truthhadTop;
  TLorentzVector truthTopTop;
  TLorentzVector truthTopTopHiggs;
  TLorentzVector truthHiggs;
  TLorentzVector recojet;
  TLorentzVector recojet_ordered;

  
  int recoJetCounter = 0;
  int recobJetCounter = 0;

  //===========================



  truthlepTop.SetPtEtaPhiE(0.0,0.0,0.0,0.0);
  truthhadTop.SetPtEtaPhiE(0.0,0.0,0.0,0.0);
  truthHiggs.SetPtEtaPhiE(0.0,0.0,0.0,0.0);
  recojet_ordered.SetPtEtaPhiE(0.0,0.0,0.0,0.0);


  if(fSampleType == "MC" && fTreeName == "nominal_Loose"){
    int truthParticles = fFlatTree ->truth_pdgid -> size();                                                                 
     
    for(int iTop = 0; iTop < truthParticles; ++iTop){                                                                       
      float eta = (fFlatTree -> truth_eta -> at(iTop));
      float phi = (fFlatTree -> truth_phi -> at(iTop));
      float pt = (fFlatTree -> truth_pt -> at(iTop))/1000;
      float m = (fFlatTree -> truth_m -> at(iTop))/1000;                                                                    
          
      if(TTHbbTruth::isHadTop( fFlatTree -> truth_pdgid->at(iTop), fFlatTree -> truth_tthbb_info->at(iTop))){
        truthhadTop.SetPtEtaPhiE(pt, eta, phi, m);
        hadTop = true;
      }                                                                                                                   
      else if (TTHbbTruth::isLepTop( fFlatTree -> truth_pdgid->at(iTop), fFlatTree -> truth_tthbb_info->at(iTop))){
        truthlepTop.SetPtEtaPhiE(pt, eta, phi, m);
        lepTop = true;
      }                                                                                                                   
      else if(TTHbbTruth::isHiggs( fFlatTree -> truth_pdgid->at(iTop), fFlatTree -> truth_tthbb_info->at(iTop))){
        Higgs = true;
        truthHiggs.SetPtEtaPhiE(pt, eta, phi, m);
      }                                                                                                                   
    }
  } 
    
    for(int i = 0; i < 4; ++i){
      float Jeta = (fFlatTree -> jet_eta -> at(i));
      float Jphi = (fFlatTree -> jet_phi -> at(i));
      float Jpt = (fFlatTree -> jet_pt -> at(i))/1000.;
      float Je = (fFlatTree -> jet_e -> at(i))/1000.;
      float btag = (fFlatTree -> jet_mv2c20 -> at(i));
      if (btag > -0.4434) {
        recobJetCounter++;
      }
      recojet.SetPtEtaPhiE(Jpt, Jeta, Jphi, Je);
      if(recojet.DeltaR(truthhadTop) < 1.2 || recojet.DeltaR(truthlepTop)< 1.2){
	recoJetCounter++;

      }
    }
    
      
    // Higgs_b1_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero.                                     
    // Higgs_b2_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero.                                     
    Higgs_b1_plus_b2_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero  
    Top_h_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero.                                                         
    Top_Mh_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero.
    Top_L_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero.                                                          
    Top_ML_R.SetPtEtaPhiE(0.0,0.0,0.0,0.0);//set the vTlonrentzvector elements to zero. 
    ttbar.SetPtEtaPhiE(0.0,0.0,0.0,0.0);
    
    eventNumber=-999.9;
    dRlj_MindR=-999.9;
    //Higgs
    matched_Higgs_pt=-999.9;
    matched_Higgs_Phi=-999.9;
    matched_Higgs_E=-999.9;
    matched_Higgs_Eta=-999.9;
    matched_Higgs_mass=-999.9;
    matchingHiggs=0;
    
    failedmatch_Higgs_pt=-999.9;
    failedmatch_Higgs_Phi=-999.9;
    failedmatch_Higgs_E=-999.9;
    failedmatch_Higgs_Eta=-999.9;
    failedmatch_Higgs_mass=-999.9;
    
    /*allevents_Higgs_pt=-999.9;
    allevents_Higgs_Phi=-999.9;
    allevents_Higgs_E=-999.9;
    allevents_Higgs_Eta=-999.9;
    allevents_Higgs_mass=-999.9;
    */
    //change the setting just for now to add the plots to TMVA, because I have some empty events from KLFitter and they are not calculated here and they ARE SETTED TO -999.9. Now to ignore them, we will set them to zero and I have to remove this setting when the problem is solved by Andrea..
    allevents_Higgs_pt=0;
    allevents_Higgs_Phi=0;
    allevents_Higgs_E=0;
    allevents_Higgs_Eta=0;
    allevents_Higgs_mass=0;
    Higgs_matched_b1b2_deltaR=0;
    Higgs_failedmatch_b1b2_deltaR=0;
    Higgs_allevents_b1b2_deltaR=0;
    

    /*Higgs_matched_b1b2_deltaR=-999.9;
    Higgs_failedmatch_b1b2_deltaR=-999.9;
    Higgs_allevents_b1b2_deltaR=-999.9;*/
    
    Higgs_deltaR=-999.9;
    Higgs_b1b2_deltaR=-999.9;
    Higgs_Ji_deltaR=999.9;//because we want to find the lowest dR and for that we set it to high value=999.9
    
    b1_temp_pt =0;
    b1_temp_eta =0;
    b1_temp_phi =0;
    b1_temp_E =0;
    
    b2_temp_pt =0;
    b2_temp_eta =0;
    b2_temp_phi =0;
    b2_temp_E =0;
    
    thad_temp_pt =0;
    thad_temp_eta =0;
    thad_temp_phi =0;
    thad_temp_E =0;
    
    tlep_temp_pt =0;
    tlep_temp_eta =0;
    tlep_temp_phi =0;
    tlep_temp_E =0;
    
    ttbar_temp_pt =0;
    ttbar_temp_eta =0;
    ttbar_temp_phi =0;
    ttbar_temp_E =0;
    //--------------------------------------------Top had-------------------------------------------------
    matched_thad_pt=-999.9;
    matched_thad_Eta=-999.9;
    matched_thad_Phi=-999.9;
    matched_thad_E=-999.9;
    matchingthad=0;
    matchingtlep=0;
    
    failedmatch_thad_pt=-999.9;
    failedmatch_thad_Eta=-999.9;
    failedmatch_thad_Phi=-999.9;
    failedmatch_thad_E=-999.9;
    
    allevents_thad_pt=-999.9;
    allevents_thad_Eta=-999.9;
    allevents_thad_Phi=-999.9;
    allevents_thad_E=-999.9;
    //-------------------------------------------to lep---------------------------------------------------
    matched_tlep_pt=-999.9;
    matched_tlep_Eta=-999.9;
    matched_tlep_Phi=-999.9;
    matched_tlep_E=-999.9;
    
    failedmatch_tlep_pt=-999.9;
    failedmatch_tlep_Eta=-999.9;
    failedmatch_tlep_Phi=-999.9;
    failedmatch_tlep_E=-999.9;
    
    allevents_tlep_pt=-999.9;
    allevents_tlep_Eta=-999.9;
    allevents_tlep_Phi=-999.9;
    allevents_tlep_E=-999.9;
    
    EnergyResolutionhadTop=-999.9;
    EnergyResolutionlepTop=-999.9;
    //--------------------------------------------ttbar---------------------------------------------------
    matched_tt_pt=-999.9;
    matched_tt_Eta=-999.9;
    matched_tt_Phi=-999.9;
    matched_tt_E=-999.9;
    
    failedmatch_tt_pt=-999.9;
    failedmatch_tt_Eta=-999.9;
    failedmatch_tt_Phi=-999.9;
    failedmatch_tt_E=-999.9;
    
    allevents_tt_pt=-999.9;
    allevents_tt_Eta=-999.9;
    allevents_tt_Phi=-999.9;
    allevents_tt_E=-999.9;
    
    
    eventcounter=eventcounter+1;
    std::cout<<" NEW EVENT XXX: "<< fFlatTree->eventNumber<< "=====================================================================  "<<eventcounter<<std::endl;   
  //DELETEME
  //std::cout<<" I FOUND EVENT "<<fFlatTree->eventNumber<<std::endl;
  /*if(fFlatTree->eventNumber==3336167){
    std::cout<<" I FOUND2 EVENT "<<fFlatTree->eventNumber<<std::endl;
    std::cout<<"nb1_count_pt: "<< fFlatTree->klfitter_model_Higgs_b1_pt->size()<<endl;
  }
  */
    std::cout<<"nb1_count_pt: "<< fFlatTree->klfitter_model_Higgs_b1_pt->size()<<endl;
 
    for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){

      if(fVariables[iVar] == "LeadingFourJetsFromTops"){
	fHistoVector[iVar].Fill(recoJetCounter, histoweight);
	fTreeVariables[iVar] = recoJetCounter;
      }
      
      if(fVariables[iVar] == "eventNumber"){
	fHistoVector[iVar].Fill(fFlatTree->eventNumber, histoweight);
	fTreeVariables[iVar] = fFlatTree->eventNumber;
      }

      if(fVariables[iVar] == "nHiggsbb30_77"){
        fHistoVector[iVar].Fill(fFlatTree->nHiggsbb30_77, histoweight);
        fTreeVariables[iVar] = fFlatTree->nHiggsbb30_77;
      }

      if(fVariables[iVar] == "dRlj_MindR"){
	fHistoVector[iVar].Fill(fFlatTree->dRlj_MindR, histoweight);
	fTreeVariables[iVar] = fFlatTree->dRlj_MindR;
      }
      
      
      if(fVariables[iVar] == "resolved_overlap"){
	fHistoVector[iVar].Fill(resolvedchannel,histoweight);
      }
      //else if(fVariables[iVar] == "resolved_overlap_bb"){
      //if(fabs(fFlatTree->truthHiggsDaughtersID->at(0)) == 5 && fabs(fFlatTree->truthHiggsDaughtersID->at(1)) == 5){
      //	fHistoVector[iVar].Fill(resolvedchannel,histoweight);
      //}
      //}
      
      if(fVariables[iVar] == "HiggsDecays"){  
	fHistoVector[iVar].Fill((int)fFlatTree->truth_HDecay,histoweight);      
      }
      
      //make the ib1_pt index for looping over b1's
      for(int ib1_pt=0; ib1_pt<nb1_count_pt;++ib1_pt){
	if(fVariables[iVar] == "klfitter_model_Higgs_b1_pt"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b1_pt -> at(ib1_pt),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b1_pt -> at(ib1_pt);
	  b1_temp_pt=fFlatTree->klfitter_model_Higgs_b1_pt -> at(ib1_pt);
	  std::cout<<"ib1_pt: "<<ib1_pt<<" b1_temp_pt: "<<fFlatTree->klfitter_model_Higgs_b1_pt -> at(ib1_pt)<<endl;
	}
      }
      for(int ib1_eta=0; ib1_eta<nb1_count_eta;++ib1_eta){
	if(fVariables[iVar] == "klfitter_model_Higgs_b1_eta"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b1_eta -> at(ib1_eta),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b1_eta -> at(ib1_eta);
	  b1_temp_eta=fFlatTree->klfitter_model_Higgs_b1_eta -> at(ib1_eta);
	  std::cout<<"ib1_eta: "<<ib1_eta<<" b1_temp_eta: "<<fFlatTree->klfitter_model_Higgs_b1_eta -> at(ib1_eta)<<endl;
	}
      }
      
      for(int ib1_phi=0; ib1_phi<nb1_count_phi;++ib1_phi){
	if(fVariables[iVar] == "klfitter_model_Higgs_b1_phi"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b1_phi -> at(ib1_phi),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b1_phi -> at(ib1_phi);
	  b1_temp_phi=fFlatTree->klfitter_model_Higgs_b1_phi -> at(ib1_phi);
	  std::cout<<"ib1_phi: "<<ib1_phi<<" b1_temp_phi: "<<fFlatTree->klfitter_model_Higgs_b1_phi -> at(ib1_phi)<<endl;
	}
      }
      
      for(int ib1_E=0; ib1_E<nb1_count_E;++ib1_E){
      if(fVariables[iVar] == "klfitter_model_Higgs_b1_E"){
	fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b1_E -> at(ib1_E),histoweight);
	fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b1_E -> at(ib1_E);
	b1_temp_E=fFlatTree->klfitter_model_Higgs_b1_E -> at(ib1_E);
	std::cout<<"ib1_E: "<<ib1_E<<" b1_temp_E: "<<fFlatTree->klfitter_model_Higgs_b1_E -> at(ib1_E)<<endl;
      }
      }
      
      for(int ib1_jetIndex=0; ib1_jetIndex<nb1_count_jetIndex;++ib1_jetIndex){
	if(fVariables[iVar] == "klfitter_model_Higgs_b1_jetIndex"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b1_jetIndex -> at(ib1_jetIndex),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b1_jetIndex -> at(ib1_jetIndex);
	}
      }
      //-----------b2---------------------------------
      
      for(int ib2_pt=0; ib2_pt<nb2_count_pt;++ib2_pt){
	if(fVariables[iVar] == "klfitter_model_Higgs_b2_pt"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b2_pt -> at(ib2_pt),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b2_pt -> at(ib2_pt);
	  b2_temp_pt=fFlatTree->klfitter_model_Higgs_b2_pt -> at(ib2_pt);
	  std::cout<<"ib2_pt: "<<ib2_pt<<" b2_temp_pt: "<<fFlatTree->klfitter_model_Higgs_b2_pt -> at(ib2_pt)<<endl;
	}
      }
      
      for(int ib2_eta=0; ib2_eta<nb2_count_eta;++ib2_eta){
	if(fVariables[iVar] == "klfitter_model_Higgs_b2_eta"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b2_eta -> at(ib2_eta),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b2_eta -> at(ib2_eta);
	  b2_temp_eta=fFlatTree->klfitter_model_Higgs_b2_eta -> at(ib2_eta);
	}
      }
      
      
      for(int ib2_phi=0; ib2_phi<nb2_count_phi;++ib2_phi){
	if(fVariables[iVar] == "klfitter_model_Higgs_b2_phi"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b2_phi -> at(ib2_phi),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b2_phi -> at(ib2_phi);
	  b2_temp_phi=fFlatTree->klfitter_model_Higgs_b2_phi -> at(ib2_phi);
	}
      }
      
      for(int ib2_E=0; ib2_E<nb2_count_E;++ib2_E){
	if(fVariables[iVar] == "klfitter_model_Higgs_b2_E"){
	  fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b2_E -> at(ib2_E),histoweight);
	  fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b2_E -> at(ib2_E);
	  b2_temp_E=fFlatTree->klfitter_model_Higgs_b2_E -> at(ib2_E);
      }
    }
    
    for(int ib2_jetIndex=0; ib2_jetIndex<nb2_count_jetIndex;++ib2_jetIndex){
      if(fVariables[iVar] == "klfitter_model_Higgs_b2_jetIndex"){
	fHistoVector[iVar].Fill(fFlatTree->klfitter_model_Higgs_b2_jetIndex -> at(ib2_jetIndex),histoweight);
	fTreeVariables[iVar] = fFlatTree -> klfitter_model_Higgs_b2_jetIndex -> at(ib2_jetIndex);
      }
    }
    //------------------top had---------------------

    if(fVariables[iVar] == "klfitter_bestPerm_topHad_pt"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topHad_pt, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topHad_pt;
      thad_temp_pt=fFlatTree->klfitter_bestPerm_topHad_pt;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_topHad_eta"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topHad_eta, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topHad_eta;
      thad_temp_eta=fFlatTree->klfitter_bestPerm_topHad_eta;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_topHad_phi"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topHad_phi, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topHad_phi;
      thad_temp_phi=fFlatTree->klfitter_bestPerm_topHad_phi;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_topHad_E"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topHad_E, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topHad_E;
      thad_temp_E=fFlatTree->klfitter_bestPerm_topHad_E;
    }
    //-------------------top lep--------------------
   if(fVariables[iVar] == "klfitter_bestPerm_topLep_pt"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topLep_pt, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topLep_pt;
      tlep_temp_pt=fFlatTree->klfitter_bestPerm_topLep_pt;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_topLep_eta"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topLep_eta, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topLep_eta;
      tlep_temp_eta=fFlatTree->klfitter_bestPerm_topLep_eta;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_topLep_phi"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topLep_phi, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topLep_phi;
      tlep_temp_phi=fFlatTree->klfitter_bestPerm_topLep_phi;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_topLep_E"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_topLep_E, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_topLep_E;
      tlep_temp_E=fFlatTree->klfitter_bestPerm_topLep_E;
    }
    //------------------ttbar system---------------------
       if(fVariables[iVar] == "klfitter_bestPerm_ttbar_pt"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_ttbar_pt, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_ttbar_pt;
      ttbar_temp_pt=fFlatTree->klfitter_bestPerm_ttbar_pt;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_ttbar_eta"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_ttbar_eta, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_ttbar_eta;
      ttbar_temp_eta=fFlatTree->klfitter_bestPerm_ttbar_eta;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_ttbar_phi"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_ttbar_phi, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_ttbar_phi;
      ttbar_temp_phi=fFlatTree->klfitter_bestPerm_ttbar_phi;
    }
    if(fVariables[iVar] == "klfitter_bestPerm_ttbar_E"){
      fHistoVector[iVar].Fill(fFlatTree->klfitter_bestPerm_ttbar_E, histoweight);
      fTreeVariables[iVar] = fFlatTree->klfitter_bestPerm_ttbar_E;
      ttbar_temp_E=fFlatTree->klfitter_bestPerm_ttbar_E;
    }
    
    }//END THE FIRST LOOP OVER VARIABLES IN nominal_Loose






  //=========================================================================================================================
    
  //This really only needs to run once per event - not for each variable in nominal_loose

  //-----------------------------top had-----------------------------------------------------------------------
  Top_h_R.SetPtEtaPhiE(thad_temp_pt,thad_temp_eta,thad_temp_phi,thad_temp_E);
  cout<<"Top_h_R.Pt: "<<Top_h_R.Pt()
      <<" Top_h_R.Eta:"<<Top_h_R.Eta()
      <<" Top_h_R.Phi:"<<Top_h_R.Phi()
      <<"Top_h_R.E:"<<Top_h_R.E()
      <<"Top_h_R.M:"<<Top_h_R.M()
      <<  endl;


  
  Top_H_deltaR = truthhadTop.DeltaR(Top_h_R);
  cout<<"thad_deltaR: "<<Top_H_deltaR<<endl;
  if (Top_H_deltaR<=0.5)
    {
      Top_Mh_R.SetPtEtaPhiE(
			    (thad_temp_pt),
			    (thad_temp_eta),
			    (thad_temp_phi),
			    (thad_temp_E)
			    );

      matched_thad_pt=Top_Mh_R.Pt();
      matched_thad_Eta=Top_Mh_R.Eta();
      matched_thad_Phi=Top_Mh_R.Phi();
      matched_thad_E=Top_Mh_R.E();
      matched_thad_M=Top_Mh_R.M();
      matchingthad=1;
      EnergyResolutionhadTop = ((truthhadTop.E()) - matched_thad_E)/(truthhadTop.E());

    }else{

    failedmatch_thad_pt=Top_h_R.Pt();
    failedmatch_thad_Eta=Top_h_R.Eta();
    failedmatch_thad_Phi=Top_h_R.Phi();
    failedmatch_thad_E=Top_h_R.E();
    failedmatch_thad_M=Top_h_R.M();

  }
  
  allevents_thad_pt=Top_h_R.Pt();
  allevents_thad_Eta=Top_h_R.Eta();
  allevents_thad_Phi=Top_h_R.Phi();
  allevents_thad_E=Top_h_R.E();
  allevents_thad_M=Top_h_R.M();
  //----------------------------top lep--------------------------------------------------------
  Top_L_R.SetPtEtaPhiE(tlep_temp_pt,tlep_temp_eta,tlep_temp_phi,tlep_temp_E);
  cout<<"Top_L_R.Pt: "<<Top_L_R.Pt()
      <<" Top_L_R.Eta:"<<Top_L_R.Eta()
      <<" Top_L_R.Phi:"<<Top_L_R.Phi()
      <<"Top_L_R.E:"<<Top_L_R.E()
      <<"Top_L_R.M:"<<Top_L_R.M()
      <<  endl;


  
  Top_L_deltaR = truthlepTop.DeltaR(Top_L_R);
  cout<<"tlep_deltaR: "<<Top_L_deltaR<<endl;
  if (Top_L_deltaR<=0.5)
    {
      Top_ML_R.SetPtEtaPhiE(
			    (tlep_temp_pt),
			    (tlep_temp_eta),
			    (tlep_temp_phi),
			    (tlep_temp_E)
			    );

      matched_tlep_pt=Top_ML_R.Pt();
      matched_tlep_Eta=Top_ML_R.Eta();
      matched_tlep_Phi=Top_ML_R.Phi();
      matched_tlep_E=Top_ML_R.E();
      matched_tlep_M=Top_ML_R.M();
      matchingtlep=1;
      EnergyResolutionlepTop = ((truthlepTop.E()) - klfitter_bestPerm_topLep_E)/(truthlepTop.E());

    }else{

    failedmatch_tlep_pt=Top_L_R.Pt();
    failedmatch_tlep_Eta=Top_L_R.Eta();
    failedmatch_tlep_Phi=Top_L_R.Phi();
    failedmatch_tlep_E=Top_L_R.E();
    failedmatch_tlep_M=Top_L_R.M();

  }
  
  allevents_tlep_pt=Top_L_R.Pt();
  allevents_tlep_Eta=Top_L_R.Eta();
  allevents_tlep_Phi=Top_L_R.Phi();
  allevents_tlep_E=Top_L_R.E();
  allevents_tlep_M=Top_L_R.M();
  //-------------------------------------------ttbar-------------------------------------------
  ttbar.SetPtEtaPhiE(thad_temp_pt,thad_temp_eta,thad_temp_phi,thad_temp_E);
  
  if (Top_H_deltaR<=0.5 && Top_L_deltaR<=0.5)
    {
      matched_tt_pt=ttbar.Pt();
      matched_tt_Eta=ttbar.Eta();
      matched_tt_Phi=ttbar.Phi();
      matched_tt_E=ttbar.E();
      matched_tt_M=ttbar.M();
      
    }else{
    failedmatch_tt_pt=ttbar.Pt();
    failedmatch_tt_Eta=ttbar.Eta();
    failedmatch_tt_Phi=ttbar.Phi();
    failedmatch_tt_E=ttbar.E();
    failedmatch_tt_M=ttbar.M();
    
  }

  allevents_tt_pt=ttbar.Pt();
  allevents_tt_Eta=ttbar.Eta();
  allevents_tt_Phi=ttbar.Phi();
  allevents_tt_E=ttbar.E();
  allevents_tt_M=ttbar.M();
  //-----------------Higgs--------------------------------------------------------------------- 
  Higgs_b1_R.SetPtEtaPhiE(b1_temp_pt,b1_temp_eta,b1_temp_phi,b1_temp_E);
  std::cout<<"Higgs_b1_R  b1 pt:" << Higgs_b1_R.Pt()<<std::endl; //these statemts only to debug the code
  //std::cout<<"b1_temp_pt       :" << b1_temp_pt<<std::endl;
  
  Higgs_b2_R.SetPtEtaPhiE(b2_temp_pt,b2_temp_eta,b2_temp_phi,b2_temp_E);
  std::cout<<"Higgs_b2_R  b2 pt:" << Higgs_b2_R.Pt()<<std::endl;
  //std::cout<<"b2_temp_pt       :" << b2_temp_pt<<std::endl;
  
  if(Higgs_b1_R.Pt()!=0 && Higgs_b2_R.Pt()!=0 ){
    std::cout<<"MADE IT!" << Higgs_b1_R.Pt()<<std::endl;
    Higgs_b1_plus_b2_R=Higgs_b1_R+Higgs_b2_R;
      
  }
  //std::cout<<"Higgs_b1_plus_b2_R: " <<Higgs_b1_plus_b2_R<<std::endl;  
  if (Higgs==true){
    Higgs_deltaR = truthHiggs.DeltaR(Higgs_b1_plus_b2_R);
    cout<<"Higgs_deltaR: "<<Higgs_deltaR<<endl;
   
    
    if (Higgs_deltaR<=0.5)
      {
	//cout<<"INSIDE DR<0.5 "<<Higgs_b1_plus_b2_R.Pt()<<endl;//to debug the code
	matched_Higgs_pt=Higgs_b1_plus_b2_R.Pt();
	matched_Higgs_Eta=Higgs_b1_plus_b2_R.Eta();
	matched_Higgs_Phi=Higgs_b1_plus_b2_R.Phi();
	matched_Higgs_E=Higgs_b1_plus_b2_R.E();
	matched_Higgs_mass=Higgs_b1_plus_b2_R.M();
	
	matchingHiggs=1;
      }
    else
      {
	failedmatch_Higgs_pt=Higgs_b1_plus_b2_R.Pt();
	failedmatch_Higgs_Eta=Higgs_b1_plus_b2_R.Eta();
	failedmatch_Higgs_Phi=Higgs_b1_plus_b2_R.Phi();
	failedmatch_Higgs_E=Higgs_b1_plus_b2_R.E();
	failedmatch_Higgs_mass=Higgs_b1_plus_b2_R.M();
      }
  }
  if(Higgs_b1_R.Pt()!=0 && Higgs_b2_R.Pt()!=0 ){
    allevents_Higgs_pt=Higgs_b1_plus_b2_R.Pt();
    allevents_Higgs_Eta=fabs(Higgs_b1_plus_b2_R.Eta());
    allevents_Higgs_Phi=Higgs_b1_plus_b2_R.Phi();
    allevents_Higgs_E=Higgs_b1_plus_b2_R.E();
    allevents_Higgs_mass=Higgs_b1_plus_b2_R.M();
  }
  //--------------------------------Higgs_b1b2_deltaR----------------------------------------------------------

  if(Higgs_b1_plus_b2_R.Pt()!=0 ){
    Higgs_b1b2_deltaR = Higgs_b1_R.DeltaR(Higgs_b2_R);
    if (matchingHiggs==1){
      Higgs_matched_b1b2_deltaR=Higgs_b1b2_deltaR;
      
    }
    else
      {
	Higgs_failedmatch_b1b2_deltaR=Higgs_b1b2_deltaR;

      }
    Higgs_allevents_b1b2_deltaR=Higgs_b1b2_deltaR;
  }



  //--------------------------------Higgs_Ji_deltaR and find lowest2--------------------------------------------
  Higgs_Ji_deltaRs.clear();
  Higgs_Ji_deltaRs_sorted.clear();
  special_k.clear();
  special_k_pt.clear();
  special_k_matched.clear();
  special_k_pt_matched.clear();
  btagvector.clear();
  if (Higgs==true){//we don't want to loop over the jets if there is no truth Higgs!!

    for(int j = 0; j < nJetCount; ++j){
      float Jet_eta = (fFlatTree -> jet_eta -> at(j));
      float Jet_phi = (fFlatTree -> jet_phi -> at(j));
      float Jet_pt = (fFlatTree -> jet_pt -> at(j))/1000.;
      float Jet_e = (fFlatTree -> jet_e -> at(j))/1000.;
      float btag = (fFlatTree -> jet_mv2c20 -> at(j));
      recojet_ordered.SetPtEtaPhiE(Jet_pt, Jet_eta, Jet_phi, Jet_e);
      //Store the btag values for later...(in the original order of Pt)
      btagvector.push_back(btag);
      Higgs_Ji_deltaR = truthHiggs.DeltaR(recojet_ordered);
      if (btag >= BTagCut){
	cout<<"Higgs_Ji_deltaR: "<<j<<"     "<<Higgs_Ji_deltaR<<" --- pT="<<Jet_pt << "BJET:"<<btag  << endl;
      }else{
	cout<<"Higgs_Ji_deltaR: "<<j<<"     "<<Higgs_Ji_deltaR<<" --- pT="<<Jet_pt << "LIGHT:"<<btag  << endl;
      }
      
      Higgs_Ji_deltaRs.push_back(Higgs_Ji_deltaR); 
      Higgs_Ji_deltaRs_sorted.push_back(Higgs_Ji_deltaR); 
    }
    std::sort(Higgs_Ji_deltaRs_sorted.begin(),Higgs_Ji_deltaRs_sorted.end());
  
    for (unsigned int k=0; k<Higgs_Ji_deltaRs_sorted.size(); ++k){
      cout<<"Higgs_Ji_deltaRs_sorted "<<k<<": " <<Higgs_Ji_deltaRs_sorted[k]<<endl;
    }
    float Higgs_J3_deltaRs_sorted=Higgs_Ji_deltaRs_sorted[2];
    
    for (unsigned int k=0; k<Higgs_Ji_deltaRs.size(); ++k){
      
      if(Higgs_Ji_deltaRs[k]<Higgs_J3_deltaRs_sorted){
	cout<<"Higgs_Ji_deltaRs "<<k<<": " <<Higgs_Ji_deltaRs[k]<<endl;
	cout<<"Higgs_Ji_deltaRs Pt "<<k<<": " <<((fFlatTree -> jet_pt -> at(k))/1000.)   <<endl;
	cout<<"Higgs_Ji_deltaRs Btag"<<k<<": "<<btagvector[k]<<endl;
	//we will have two values to save. Special k1 Special k2..
	special_k.push_back(k);
	special_k_pt.push_back((fFlatTree -> jet_pt -> at(k))/1000.);
	//I don't thjink we need this.. because we know 'k' and we know the btagvector[k]
	//special_k_btag.push_back(btagvector[k]);

	//I have a vector of tow integers, vector length=2 how to plot them below..??? 	

	//add if(matchingHiggs==1)) here... and fill special_k_higgsmatched and special_k_pt_higgsmatched
	if (matchingHiggs==1){
	  special_k_matched.push_back(k);
	  special_k_pt_matched.push_back((fFlatTree -> jet_pt -> at(k))/1000.);
	  cout<<"Higgs_Ji_deltaRs_matched "<<k<<": " <<Higgs_Ji_deltaRs[k]<<endl;
	  cout<<"Higgs_Ji_deltaRs Pt matched"<<k<<": " <<((fFlatTree -> jet_pt -> at(k))/1000.)   <<endl;
	
	  //cout<<"special_k_matched[0]:   "<<special_k_matched[0]<<endl;
	  //cout<<"special_k_matched[1]:   "<<special_k_matched[1]<<endl;
	}

      }
    }
    

  }
 
 //--------------------------------thad_Ji_deltaR and find lowest2--------------------------------------------
  thad_Ji_deltaRs.clear();
  thad_Ji_deltaRs_sorted.clear();
  special_thad_k.clear();
  special_thad_k_pt.clear();
  special_thad_k_matched.clear();
  special_thad_k_pt_matched.clear();
  if (hadTop = true){//we don't want to loop over the jets if there is no truth Higgs!!

    for(int j = 0; j < nJetCount; ++j){
      float Jet_eta = (fFlatTree -> jet_eta -> at(j));
      float Jet_phi = (fFlatTree -> jet_phi -> at(j));
      float Jet_pt = (fFlatTree -> jet_pt -> at(j))/1000.;
      float Jet_e = (fFlatTree -> jet_e -> at(j))/1000.;
      recojet_ordered.SetPtEtaPhiE(Jet_pt, Jet_eta, Jet_phi, Jet_e);
      thad_Ji_deltaR = truthhadTop.DeltaR(recojet_ordered);
      cout<<"thad_Ji_deltaR: "<<j<<"     "<<thad_Ji_deltaR<<" --- pT="<<Jet_pt<<  endl;
      thad_Ji_deltaRs.push_back(thad_Ji_deltaR); 
      thad_Ji_deltaRs_sorted.push_back(thad_Ji_deltaR); 
    }
    std::sort(thad_Ji_deltaRs_sorted.begin(),thad_Ji_deltaRs_sorted.end());
  
    for (unsigned int k=0; k<thad_Ji_deltaRs_sorted.size(); ++k){
      cout<<"thad_Ji_deltaRs_sorted "<<k<<": " <<thad_Ji_deltaRs_sorted[k]<<endl;
    }
    float thad_J4_deltaRs_sorted=thad_Ji_deltaRs_sorted[3];
    
    for (unsigned int k=0; k<thad_Ji_deltaRs.size(); ++k){
      
      if(thad_Ji_deltaRs[k]<thad_J4_deltaRs_sorted){
	cout<<"thad_J"<<k<<"_deltaRs "<<k<<": " <<thad_Ji_deltaRs[k]<<endl;
	cout<<"thad_J"<<k<<"_deltaRs Pt "<<k<<": " <<((fFlatTree -> jet_pt -> at(k))/1000.)   <<endl;
	
	//we will have three values to save. Special k1 Special k2 k3..
	special_thad_k.push_back(k);
	special_thad_k_pt.push_back((fFlatTree -> jet_pt -> at(k))/1000.);
	//I have a vector of tow integers, vector length=2 how to plot them below..??? 	

	//add if(matchingthad==1)) here... and fill special_thad_k_higgsmatched and special_thad_k_pt_higgsmatched
	if (matchingthad==1){
	  special_thad_k_matched.push_back(k);
	  special_thad_k_pt_matched.push_back((fFlatTree -> jet_pt -> at(k))/1000.);
	  cout<<"thad_J"<<k<<"_deltaRs_matched "<<k<<": " <<thad_Ji_deltaRs[k]<<endl;
	  cout<<"thad_J"<<k<<"_deltaRs Pt matched"<<k<<": " <<((fFlatTree -> jet_pt -> at(k))/1000.)   <<endl;
	
	  //cout<<"special_thad_k_matched[0]:   "<<special_thad_k_matched[0]<<endl;
	  //cout<<"special_thad_k_matched[1]:   "<<special_thad_k_matched[1]<<endl;
	}

      }
    }
    

  }
 //--------------------------------tlep_Ji_deltaR and find lowest2--------------------------------------------
  tlep_Ji_deltaRs.clear();
  tlep_Ji_deltaRs_sorted.clear();
  special_tlep_k.clear();
  special_tlep_k_pt.clear();
  special_tlep_k_matched.clear();
  special_tlep_k_pt_matched.clear();
  if (lepTop = true){//we don't want to loop over the jets if there is no truth Higgs!!

    for(int j = 0; j < nJetCount; ++j){
      float Jet_eta = (fFlatTree -> jet_eta -> at(j));
      float Jet_phi = (fFlatTree -> jet_phi -> at(j));
      float Jet_pt = (fFlatTree -> jet_pt -> at(j))/1000.;
      float Jet_e = (fFlatTree -> jet_e -> at(j))/1000.;
      recojet_ordered.SetPtEtaPhiE(Jet_pt, Jet_eta, Jet_phi, Jet_e);
      tlep_Ji_deltaR = truthlepTop.DeltaR(recojet_ordered);
      cout<<"tlep_Ji_deltaR: "<<j<<"     "<<tlep_Ji_deltaR<<" --- pT="<<Jet_pt<<  endl;
      tlep_Ji_deltaRs.push_back(tlep_Ji_deltaR); 
      tlep_Ji_deltaRs_sorted.push_back(tlep_Ji_deltaR); 
    }
    std::sort(tlep_Ji_deltaRs_sorted.begin(),tlep_Ji_deltaRs_sorted.end());
  
    for (unsigned int k=0; k<tlep_Ji_deltaRs_sorted.size(); ++k){
      cout<<"tlep_Ji_deltaRs_sorted "<<k<<": " <<tlep_Ji_deltaRs_sorted[k]<<endl;
    }
    float tlep_J1_deltaRs_sorted=tlep_Ji_deltaRs_sorted[1];
    
    for (unsigned int k=0; k<tlep_Ji_deltaRs.size(); ++k){
      
      if(tlep_Ji_deltaRs[k]<tlep_J1_deltaRs_sorted){
	cout<<"tlep_J"<<k<<"_deltaRs "<<k<<": " <<tlep_Ji_deltaRs[k]<<endl;
	cout<<"tlep_J"<<k<<"_deltaRs Pt "<<k<<": " <<((fFlatTree -> jet_pt -> at(k))/1000.)   <<endl;
	
	//we will have three values to save. Special k1 Special k2 k3..
	special_tlep_k.push_back(k);
	special_tlep_k_pt.push_back((fFlatTree -> jet_pt -> at(k))/1000.);
	//I have a vector of tow integers, vector length=2 how to plot them below..??? 	

	//add if(matchingtlep==1)) here... and fill special_tlep_k_higgsmatched and special_tlep_k_pt_higgsmatched
	if (matchingtlep==1){
	  special_tlep_k_matched.push_back(k);
	  special_tlep_k_pt_matched.push_back((fFlatTree -> jet_pt -> at(k))/1000.);
	  cout<<"tlep_J"<<k<<"_deltaRs_matched "<<k<<": " <<tlep_Ji_deltaRs[k]<<endl;
	  cout<<"tlep_J"<<k<<"_deltaRs Pt matched"<<k<<": " <<((fFlatTree -> jet_pt -> at(k))/1000.)   <<endl;
	
	  //cout<<"special_tlep_k_matched[0]:   "<<special_tlep_k_matched[0]<<endl;
	  //cout<<"special_tlep_k_matched[1]:   "<<special_tlep_k_matched[1]<<endl;
	}

      }
    }
    

  }
  




  //=================================NEW LOOP OVR VARIABLES in data/TTHExtras.txt========================================
  //=================================NEW LOOP OVR VARIABLES========================================
  //=================================NEW LOOP OVR VARIABLES========================================
  //=================================NEW LOOP OVR VARIABLES========================================
  //=================================NEW LOOP OVR VARIABLES========================================
  //=================================NEW LOOP OVR VARIABLES========================================
  //=================================NEW LOOP OVR VARIABLES========================================
  //Now we have b1_temp_pt etc., we can start a NEW For Loop over the variables...
  for(unsigned iVar = 0; iVar < fVar.size(); ++iVar){
     //-------------------------------------special_k and special_k_pt--------------------------------------------------------------
    if (Higgs==true){
      if(fVariables[iVar] == "special_k"){
      
	fHistoVector[iVar].Fill(special_k[0]);
	fHistoVector[iVar].Fill(special_k[1]);
	
	std::cout<<"Filled special_k: 0:"<<special_k[0]<<"     1:"<<special_k[1]<<std::endl;
      
	//	fTreeVariables[iVar] = special_k;
      }
      
      if(fVariables[iVar] == "special_k_pt"){
	  
	fHistoVector[iVar].Fill(special_k_pt[0]);
	fHistoVector[iVar].Fill(special_k_pt[1]);
	
	std::cout<<"Filled special_k_pt: 0:"<<special_k_pt[0]<<"     1:"<<special_k_pt[1]<<std::endl;
	
      }
      
        if (matchingHiggs==1){
	if(fVariables[iVar] == "special_k_matched"){
	  
	  fHistoVector[iVar].Fill(special_k_matched[0]);
	  fHistoVector[iVar].Fill(special_k_matched[1]);
	  
	  std::cout<<"Filled special_k_matched: 0:"<<special_k_matched[0]<<"     1:"<<special_k_matched[1]<<std::endl;
	  
	  //	fTreeVariables[iVar] = special_k;
	}
       
      
	if(fVariables[iVar] == "special_k_pt_matched"){
	  
	  fHistoVector[iVar].Fill(special_k_pt_matched[0]);
	  fHistoVector[iVar].Fill(special_k_pt_matched[1]);
	  
	  std::cout<<"Filled special_k_pt_matched: 0:"<<special_k_pt_matched[0]<<"     1:"<<special_k_pt_matched[1]<<std::endl;
	 
	}
	}
    }
     //-------------------------------------special_thad_k and special_thad_k_pt--------------------------------------------------------------
    if (hadTop==true){
      if(fVariables[iVar] == "special_thad_k"){
      
	fHistoVector[iVar].Fill(special_thad_k[0]);
	fHistoVector[iVar].Fill(special_thad_k[1]);
	fHistoVector[iVar].Fill(special_thad_k[2]);
	
	std::cout<<"Filled special_thad_k: 0:"<<special_thad_k[0]<<"     1:"<<special_thad_k[1]<<"     2:"<<special_thad_k[2]<<std::endl;
      
	//	fTreeVariables[iVar] = special_thad_k;
      }
      
      if(fVariables[iVar] == "special_thad_k_pt"){
	  
	fHistoVector[iVar].Fill(special_thad_k_pt[0]);
	fHistoVector[iVar].Fill(special_thad_k_pt[1]);
	fHistoVector[iVar].Fill(special_thad_k_pt[2]);
	
	//std::cout<<"Filled special_thad_k_pt: 0:"<<special_thad_k_pt[0]<<"     1:"<<special_thad_k_pt[1]<<std::endl;
	
      }
      
      if (matchingthad==1){
	if(fVariables[iVar] == "special_thad_k_matched"){
	  
	  fHistoVector[iVar].Fill(special_thad_k_matched[0]);
	  fHistoVector[iVar].Fill(special_thad_k_matched[1]);
	  fHistoVector[iVar].Fill(special_thad_k_matched[2]);
	  
	  //std::cout<<"Filled special_thad_k_matched: 0:"<<special_thad_k_matched[0]<<"     1:"<<special_thad_k_matched[1]<<std::endl;
	  
	  //	fTreeVariables[iVar] = special_thad_k;
	}
       
      
	if(fVariables[iVar] == "special_thad_k_pt_matched"){
	  
	  fHistoVector[iVar].Fill(special_thad_k_pt_matched[0]);
	  fHistoVector[iVar].Fill(special_thad_k_pt_matched[1]);
	  fHistoVector[iVar].Fill(special_thad_k_pt_matched[2]);
	  
	  std::cout<<"Filled special_thad_k_pt_matched: 0:"<<special_thad_k_pt_matched[0]<<"     1:"<<special_thad_k_pt_matched[1]<<std::endl;
	  
	}
      }
    }
     //-------------------------------------special_tlep_k and special_tlep_k_pt--------------------------------------------------------------
    if (lepTop==true){
      if(fVariables[iVar] == "special_tlep_k"){
      
	fHistoVector[iVar].Fill(special_tlep_k[0]);
	
	std::cout<<"Filled special_tlep_k: 0:"<<special_tlep_k[0]<<"     1:"<<special_tlep_k[1]<<"     2:"<<special_tlep_k[2]<<std::endl;
      
	//	fTreeVariables[iVar] = special_tlep_k;
      }
      
      if(fVariables[iVar] == "special_tlep_k_pt"){
	  
	fHistoVector[iVar].Fill(special_tlep_k_pt[0]);
	
	
	//std::cout<<"Filled special_tlep_k_pt: 0:"<<special_tlep_k_pt[0]<<"     1:"<<special_tlep_k_pt[1]<<std::endl;
	
      }
      
      if (matchingtlep==1){
	if(fVariables[iVar] == "special_tlep_k_matched"){
	  
	  fHistoVector[iVar].Fill(special_tlep_k_matched[0]);
	  //std::cout<<"Filled special_tlep_k_matched: 0:"<<special_tlep_k_matched[0]<<"     1:"<<special_tlep_k_matched[1]<<std::endl;
	  
	  //	fTreeVariables[iVar] = special_tlep_k;
	}
       
      
	if(fVariables[iVar] == "special_tlep_k_pt_matched"){
	  
	  fHistoVector[iVar].Fill(special_tlep_k_pt_matched[0]);
	  
	  std::cout<<"Filled special_tlep_k_pt_matched: 0:"<<special_tlep_k_pt_matched[0]<<"     1:"<<special_tlep_k_pt_matched[1]<<std::endl;
	 
	}
      } 
    }
      
    //-------------------------------------top had--------------------------------------------------------------
    if(fVariables[iVar] == "matched_thad_pt"){
      if (matched_thad_pt>0.0){
	fHistoVector[iVar].Fill(matched_thad_pt,histoweight);
	//fTreeVariables[iVar] = matched_thad_pt;
	std::cout<<"matched_thad_pt:   "<<matched_thad_pt<<std::endl;
      }
      fTreeVariables[iVar] = matched_thad_pt;
    }
    
    if(fVariables[iVar] == "matched_thad_Eta"){
      if (matched_thad_Eta>-4.0){
	fHistoVector[iVar].Fill(matched_thad_Eta,histoweight);
	//fTreeVariables[iVar] = matched_thad_pt;
	std::cout<<"matched_thad_Eta:   "<<matched_thad_Eta<<std::endl;
      }
      fTreeVariables[iVar] = matched_thad_Eta;
    }
    
    if(fVariables[iVar] == "matched_thad_Phi"){
      if (matched_thad_Phi>-4.0){
	fHistoVector[iVar].Fill(matched_thad_Phi,histoweight);
	//fTreeVariables[iVar] = matched_thad_Phi;
	std::cout<<"matched_thad_Phi:   "<<matched_thad_Phi<<std::endl;
      }
      fTreeVariables[iVar] = matched_thad_Phi;
    }
    
    if(fVariables[iVar] == "matched_thad_E"){
      if (matched_thad_E>0.0){
	fHistoVector[iVar].Fill(matched_thad_E,histoweight);
	//fTreeVariables[iVar] = matched_thad_E;
	std::cout<<"matched_thad_E:   "<<matched_thad_E<<std::endl;
      }
      fTreeVariables[iVar] = matched_thad_E;
    }

         if(fVariables[iVar] == "failedmatch_thad_pt"){
      if (failedmatch_thad_pt>0.0){
	fHistoVector[iVar].Fill(failedmatch_thad_pt,histoweight);
	//fTreeVariables[iVar] = failedmatch_thad_pt;
	std::cout<<"failedmatch_thad_pt:   "<<failedmatch_thad_pt<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_thad_pt;
    }
    
    if(fVariables[iVar] == "failedmatch_thad_Eta"){
      if (failedmatch_thad_Eta>-4.0){
	fHistoVector[iVar].Fill(failedmatch_thad_Eta,histoweight);
	//fTreeVariables[iVar] = failedmatch_thad_pt;
	std::cout<<"failedmatch_thad_Eta:   "<<failedmatch_thad_Eta<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_thad_Eta;
    }
    
    if(fVariables[iVar] == "failedmatch_thad_Phi"){
      if (failedmatch_thad_Phi>-4.0){
	fHistoVector[iVar].Fill(failedmatch_thad_Phi,histoweight);
	//fTreeVariables[iVar] = failedmatch_thad_Phi;
	std::cout<<"failedmatch_thad_Phi:   "<<failedmatch_thad_Phi<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_thad_Phi;
    }
    
    if(fVariables[iVar] == "failedmatch_thad_E"){
      if (failedmatch_thad_E>0.0){
	fHistoVector[iVar].Fill(failedmatch_thad_E,histoweight);
	//fTreeVariables[iVar] = failedmatch_thad_E;
	std::cout<<"failedmatch_thad_E:   "<<failedmatch_thad_E<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_thad_E;
    }
    
     if(fVariables[iVar] == "allevents_thad_pt"){
      if (allevents_thad_pt>0.0){
	fHistoVector[iVar].Fill(allevents_thad_pt,histoweight);
	//fTreeVariables[iVar] = allevents_thad_pt;
	std::cout<<"allevents_thad_pt:   "<<allevents_thad_pt<<std::endl;
      }
      fTreeVariables[iVar] = allevents_thad_pt;
    }
    
    if(fVariables[iVar] == "allevents_thad_Eta"){
      if (allevents_thad_Eta>-4.0){
	fHistoVector[iVar].Fill(allevents_thad_Eta,histoweight);
	//fTreeVariables[iVar] = allevents_thad_pt;
	std::cout<<"allevents_thad_Eta:   "<<allevents_thad_Eta<<std::endl;
      }
      fTreeVariables[iVar] = allevents_thad_Eta;
    }
    
    if(fVariables[iVar] == "allevents_thad_Phi"){
      if (allevents_thad_Phi>-4.0){
	fHistoVector[iVar].Fill(allevents_thad_Phi,histoweight);
	//fTreeVariables[iVar] = allevents_thad_Phi;
	std::cout<<"allevents_thad_Phi:   "<<allevents_thad_Phi<<std::endl;
      }
      fTreeVariables[iVar] = allevents_thad_Phi;
    }
    
    if(fVariables[iVar] == "allevents_thad_E"){
      if (allevents_thad_E>0.0){
	fHistoVector[iVar].Fill(allevents_thad_E,histoweight);
	//fTreeVariables[iVar] = allevents_thad_E;
	std::cout<<"allevents_thad_E:   "<<allevents_thad_E<<std::endl;
      }
      fTreeVariables[iVar] = allevents_thad_E;
    }

    //---------------------------------------------------------top lep-----------------------------------------
    if(fVariables[iVar] == "matched_tlep_pt"){
      if (matched_tlep_pt>0.0){
	fHistoVector[iVar].Fill(matched_tlep_pt,histoweight);
	//fTreeVariables[iVar] = matched_tlep_pt;
	std::cout<<"matched_tlep_pt:   "<<matched_tlep_pt<<std::endl;
      }
      fTreeVariables[iVar] = matched_tlep_pt;
    }
    
    if(fVariables[iVar] == "matched_tlep_Eta"){
      if (matched_tlep_Eta>-4.0){
	fHistoVector[iVar].Fill(matched_tlep_Eta,histoweight);
	//fTreeVariables[iVar] = matched_tlep_pt;
	std::cout<<"matched_tlep_Eta:   "<<matched_tlep_Eta<<std::endl;
      }
      fTreeVariables[iVar] = matched_tlep_Eta;
    }
    
    if(fVariables[iVar] == "matched_tlep_Phi"){
      if (matched_tlep_Phi>-4.0){
	fHistoVector[iVar].Fill(matched_tlep_Phi,histoweight);
	//fTreeVariables[iVar] = matched_tlep_Phi;
	std::cout<<"matched_tlep_Phi:   "<<matched_tlep_Phi<<std::endl;
      }
      fTreeVariables[iVar] = matched_tlep_Phi;
    }
    
    if(fVariables[iVar] == "matched_tlep_E"){
      if (matched_tlep_E>0.0){
	fHistoVector[iVar].Fill(matched_tlep_E,histoweight);
	//fTreeVariables[iVar] = matched_tlep_E;
	std::cout<<"matched_tlep_E:   "<<matched_tlep_E<<std::endl;
      }
      fTreeVariables[iVar] = matched_tlep_E;
    }

         if(fVariables[iVar] == "failedmatch_tlep_pt"){
      if (failedmatch_tlep_pt>0.0){
	fHistoVector[iVar].Fill(failedmatch_tlep_pt,histoweight);
	//fTreeVariables[iVar] = failedmatch_tlep_pt;
	std::cout<<"failedmatch_tlep_pt:   "<<failedmatch_tlep_pt<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tlep_pt;
    }
    
    if(fVariables[iVar] == "failedmatch_tlep_Eta"){
      if (failedmatch_tlep_Eta>-4.0){
	fHistoVector[iVar].Fill(failedmatch_tlep_Eta,histoweight);
	//fTreeVariables[iVar] = failedmatch_tlep_pt;
	std::cout<<"failedmatch_tlep_Eta:   "<<failedmatch_tlep_Eta<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tlep_Eta;
    }
    
    if(fVariables[iVar] == "failedmatch_tlep_Phi"){
      if (failedmatch_tlep_Phi>-4.0){
	fHistoVector[iVar].Fill(failedmatch_tlep_Phi,histoweight);
	//fTreeVariables[iVar] = failedmatch_tlep_Phi;
	std::cout<<"failedmatch_tlep_Phi:   "<<failedmatch_tlep_Phi<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tlep_Phi;
    }
    
    if(fVariables[iVar] == "failedmatch_tlep_E"){
      if (failedmatch_tlep_E>0.0){
	fHistoVector[iVar].Fill(failedmatch_tlep_E,histoweight);
	//fTreeVariables[iVar] = failedmatch_tlep_E;
	std::cout<<"failedmatch_tlep_E:   "<<failedmatch_tlep_E<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tlep_E;
    }
    
    if(fVariables[iVar] == "allevents_tlep_pt"){
      if (allevents_tlep_pt>0.0){
	fHistoVector[iVar].Fill(allevents_tlep_pt,histoweight);
	//fTreeVariables[iVar] = allevents_tlep_pt;
	std::cout<<"allevents_tlep_pt:   "<<allevents_tlep_pt<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tlep_pt;
    }
    
    if(fVariables[iVar] == "allevents_tlep_Eta"){
      if (allevents_tlep_Eta>-4.0){
	fHistoVector[iVar].Fill(allevents_tlep_Eta,histoweight);
	//fTreeVariables[iVar] = allevents_tlep_pt;
	std::cout<<"allevents_tlep_Eta:   "<<allevents_tlep_Eta<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tlep_Eta;
    }
    
    if(fVariables[iVar] == "allevents_tlep_Phi"){
      if (allevents_tlep_Phi>-4.0){
	fHistoVector[iVar].Fill(allevents_tlep_Phi,histoweight);
	//fTreeVariables[iVar] = allevents_tlep_Phi;
	std::cout<<"allevents_tlep_Phi:   "<<allevents_tlep_Phi<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tlep_Phi;
    }
    
    if(fVariables[iVar] == "allevents_tlep_E"){
      if (allevents_tlep_E>0.0){
	fHistoVector[iVar].Fill(allevents_tlep_E,histoweight);
	//fTreeVariables[iVar] = allevents_tlep_E;
	std::cout<<"allevents_tlep_E:   "<<allevents_tlep_E<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tlep_E;
    }

     //---------------------------------------------------------ttbar-----------------------------------------
    if(fVariables[iVar] == "matched_tt_pt"){
      if (matched_tt_pt>0.0){
	fHistoVector[iVar].Fill(matched_tt_pt,histoweight);
	//fTreeVariables[iVar] = matched_tt_pt;
	std::cout<<"matched_tt_pt:   "<<matched_tt_pt<<std::endl;
      }
      fTreeVariables[iVar] = matched_tt_pt;
    }
    
    if(fVariables[iVar] == "matched_tt_Eta"){
      if (matched_tt_Eta>-4.0){
	fHistoVector[iVar].Fill(matched_tt_Eta,histoweight);
	//fTreeVariables[iVar] = matched_tt_pt;
	std::cout<<"matched_tt_Eta:   "<<matched_tt_Eta<<std::endl;
      }
      fTreeVariables[iVar] = matched_tt_Eta;
    }
    
    if(fVariables[iVar] == "matched_tt_Phi"){
      if (matched_tt_Phi>-4.0){
	fHistoVector[iVar].Fill(matched_tt_Phi,histoweight);
	//fTreeVariables[iVar] = matched_tt_Phi;
	std::cout<<"matched_tt_Phi:   "<<matched_tt_Phi<<std::endl;
      }
      fTreeVariables[iVar] = matched_tt_Phi;
    }
    
    if(fVariables[iVar] == "matched_tt_E"){
      if (matched_tt_E>0.0){
	fHistoVector[iVar].Fill(matched_tt_E,histoweight);
	//fTreeVariables[iVar] = matched_tt_E;
	std::cout<<"matched_tt_E:   "<<matched_tt_E<<std::endl;
      }
      fTreeVariables[iVar] = matched_tt_E;
    }

         if(fVariables[iVar] == "failedmatch_tt_pt"){
      if (failedmatch_tt_pt>0.0){
	fHistoVector[iVar].Fill(failedmatch_tt_pt,histoweight);
	//fTreeVariables[iVar] = failedmatch_tt_pt;
	std::cout<<"failedmatch_tt_pt:   "<<failedmatch_tt_pt<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tt_pt;
    }
    
    if(fVariables[iVar] == "failedmatch_tt_Eta"){
      if (failedmatch_tt_Eta>-4.0){
	fHistoVector[iVar].Fill(failedmatch_tt_Eta,histoweight);
	//fTreeVariables[iVar] = failedmatch_tt_pt;
	std::cout<<"failedmatch_tt_Eta:   "<<failedmatch_tt_Eta<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tt_Eta;
    }
    
    if(fVariables[iVar] == "failedmatch_tt_Phi"){
      if (failedmatch_tt_Phi>-4.0){
	fHistoVector[iVar].Fill(failedmatch_tt_Phi,histoweight);
	//fTreeVariables[iVar] = failedmatch_tt_Phi;
	std::cout<<"failedmatch_tt_Phi:   "<<failedmatch_tt_Phi<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tt_Phi;
    }
    
    if(fVariables[iVar] == "failedmatch_tt_E"){
      if (failedmatch_tt_E>0.0){
	fHistoVector[iVar].Fill(failedmatch_tt_E,histoweight);
	//fTreeVariables[iVar] = failedmatch_tt_E;
	std::cout<<"failedmatch_tt_E:   "<<failedmatch_tt_E<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_tt_E;
    }
    
    if(fVariables[iVar] == "allevents_tt_pt"){
      if (allevents_tt_pt>0.0){
	fHistoVector[iVar].Fill(allevents_tt_pt,histoweight);
	//fTreeVariables[iVar] = allevents_tt_pt;
	std::cout<<"allevents_tt_pt:   "<<allevents_tt_pt<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tt_pt;
    }
    
    if(fVariables[iVar] == "allevents_tt_Eta"){
      if (allevents_tt_Eta>-4.0){
	fHistoVector[iVar].Fill(allevents_tt_Eta,histoweight);
	//fTreeVariables[iVar] = allevents_tt_pt;
	std::cout<<"allevents_tt_Eta:   "<<allevents_tt_Eta<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tt_Eta;
    }
    
    if(fVariables[iVar] == "allevents_tt_Phi"){
      if (allevents_tt_Phi>-4.0){
	fHistoVector[iVar].Fill(allevents_tt_Phi,histoweight);
	//fTreeVariables[iVar] = allevents_tt_Phi;
	std::cout<<"allevents_tt_Phi:   "<<allevents_tt_Phi<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tt_Phi;
    }

    
    if(fVariables[iVar] == "allevents_tt_E"){
      if (allevents_tt_E>0.0){
	fHistoVector[iVar].Fill(allevents_tt_E,histoweight);
	//fTreeVariables[iVar] = allevents_tt_E;
	std::cout<<"allevents_tt_E:   "<<allevents_tt_E<<std::endl;
      }
      fTreeVariables[iVar] = allevents_tt_E;
    }
    //------------Higgs----------------------------------------------------------------------------------------

    
    if(fVariables[iVar] == "matched_Higgs_pt"){
      if (matched_Higgs_pt>0.0){
	fHistoVector[iVar].Fill(matched_Higgs_pt,histoweight);
	//fTreeVariables[iVar] = matched_Higgs_pt;
	std::cout<<"matched_Higgs_pt:   "<<matched_Higgs_pt<<std::endl;
      }
      fTreeVariables[iVar] = matched_Higgs_pt;
    }
    
    if(fVariables[iVar] == "matched_Higgs_Eta"){
      if (matched_Higgs_Eta>-4.0){
	fHistoVector[iVar].Fill(matched_Higgs_Eta,histoweight);
	//fTreeVariables[iVar] = matched_Higgs_pt;
	std::cout<<"matched_Higgs_Eta:   "<<matched_Higgs_Eta<<std::endl;
      }
      fTreeVariables[iVar] = matched_Higgs_Eta;
    }
    
    if(fVariables[iVar] == "matched_Higgs_Phi"){
      if (matched_Higgs_Phi>-4.0){
	fHistoVector[iVar].Fill(matched_Higgs_Phi,histoweight);
	//fTreeVariables[iVar] = matched_Higgs_Phi;
	std::cout<<"matched_Higgs_Phi:   "<<matched_Higgs_Phi<<std::endl;
      }
      fTreeVariables[iVar] = matched_Higgs_Phi;
    }
    
    if(fVariables[iVar] == "matched_Higgs_E"){
      if (matched_Higgs_E>0.0){
	fHistoVector[iVar].Fill(matched_Higgs_E,histoweight);
	//fTreeVariables[iVar] = matched_Higgs_E;
	std::cout<<"matched_Higgs_E:   "<<matched_Higgs_E<<std::endl;
      }
      fTreeVariables[iVar] = matched_Higgs_E;
    }
    
    if(fVariables[iVar] == "matched_Higgs_mass"){
      if (matched_Higgs_mass>0.0){
	fHistoVector[iVar].Fill(matched_Higgs_mass,histoweight);
	//fTreeVariables[iVar] = matched_Higgs_mass;
	std::cout<<"matched_Higgs_mass:   "<<matched_Higgs_mass<<std::endl;
      }
      fTreeVariables[iVar] = matched_Higgs_mass;
    }

    if(fVariables[iVar] == "failedmatch_Higgs_pt"){
      if (failedmatch_Higgs_pt>0.0){
	fHistoVector[iVar].Fill(failedmatch_Higgs_pt,histoweight);
	//fTreeVariables[iVar] = failedmatch_Higgs_pt;
	std::cout<<"failedmatch_Higgs_pt:   "<<failedmatch_Higgs_pt<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_Higgs_pt;
    }
    
    if(fVariables[iVar] == "failedmatch_Higgs_Eta"){
      if (failedmatch_Higgs_Eta>-4.0){
	fHistoVector[iVar].Fill(failedmatch_Higgs_Eta,histoweight);
	//fTreeVariables[iVar] = failedmatch_Higgs_pt;
	std::cout<<"failedmatch_Higgs_Eta:   "<<failedmatch_Higgs_Eta<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_Higgs_Eta;
    }
    
    if(fVariables[iVar] == "failedmatch_Higgs_Phi"){
      if (failedmatch_Higgs_Phi>-4.0){
	fHistoVector[iVar].Fill(failedmatch_Higgs_Phi,histoweight);
	//fTreeVariables[iVar] = failedmatch_Higgs_Phi;
	std::cout<<"failedmatch_Higgs_Phi:   "<<failedmatch_Higgs_Phi<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_Higgs_Phi;
    }
    
    if(fVariables[iVar] == "failedmatch_Higgs_E"){
      if (failedmatch_Higgs_E>0.0){
	fHistoVector[iVar].Fill(failedmatch_Higgs_E,histoweight);
	//fTreeVariables[iVar] = failedmatch_Higgs_E;
	std::cout<<"failedmatch_Higgs_E:   "<<failedmatch_Higgs_E<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_Higgs_E;
    }
    
    if(fVariables[iVar] == "failedmatch_Higgs_mass"){
      if (failedmatch_Higgs_mass>0.0){
	fHistoVector[iVar].Fill(failedmatch_Higgs_mass,histoweight);
	//fTreeVariables[iVar] = failedmatch_Higgs_mass;
	std::cout<<"failedmatch_Higgs_mass:   "<<failedmatch_Higgs_mass<<std::endl;
      }
      fTreeVariables[iVar] = failedmatch_Higgs_mass;
    }
    
    if(fVariables[iVar] == "allevents_Higgs_pt"){
      if (allevents_Higgs_pt>0.0){
	fHistoVector[iVar].Fill(allevents_Higgs_pt,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_pt;
	std::cout<<"allevents_Higgs_pt:   "<<allevents_Higgs_pt<<std::endl;
      }
      fTreeVariables[iVar] = allevents_Higgs_pt;
    }
    
    if(fVariables[iVar] == "allevents_Higgs_Eta"){
      if (allevents_Higgs_Eta>-4.0){
	fHistoVector[iVar].Fill(allevents_Higgs_Eta,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_pt;
	std::cout<<"allevents_Higgs_Eta:   "<<allevents_Higgs_Eta<<std::endl;
      }
      fTreeVariables[iVar] = allevents_Higgs_Eta;
    }
    
    if(fVariables[iVar] == "allevents_Higgs_Phi"){
      if (allevents_Higgs_Phi>-4.0){
	fHistoVector[iVar].Fill(allevents_Higgs_Phi,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_Phi;
	std::cout<<"allevents_Higgs_Phi:   "<<allevents_Higgs_Phi<<std::endl;
      }
      fTreeVariables[iVar] = allevents_Higgs_Phi;
    }
    
    if(fVariables[iVar] == "allevents_Higgs_E"){
      if (allevents_Higgs_E>0.0){
	fHistoVector[iVar].Fill(allevents_Higgs_E,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_E;
	std::cout<<"allevents_Higgs_E:   "<<allevents_Higgs_E<<std::endl;
      }
      fTreeVariables[iVar] = allevents_Higgs_E;
    }
    
    if(fVariables[iVar] == "allevents_Higgs_mass"){
      if (allevents_Higgs_mass>0.0){
	fHistoVector[iVar].Fill(allevents_Higgs_mass,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"allevents_Higgs_mass:   "<<allevents_Higgs_mass<<std::endl;
      }
      fTreeVariables[iVar] = allevents_Higgs_mass;
    }
    
    if(fVariables[iVar] == "Higgs_matched_b1b2_deltaR"){
      if (Higgs_matched_b1b2_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_matched_b1b2_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_matched_b1b2_deltaR:   "<<Higgs_matched_b1b2_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_matched_b1b2_deltaR;
    }

    if(fVariables[iVar] == "Higgs_failedmatch_b1b2_deltaR"){
      if (Higgs_failedmatch_b1b2_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_failedmatch_b1b2_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_failedmatch_b1b2_deltaR:   "<<Higgs_failedmatch_b1b2_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_failedmatch_b1b2_deltaR;
    }

    if(fVariables[iVar] == "Higgs_allevents_b1b2_deltaR"){
      if (Higgs_allevents_b1b2_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_allevents_b1b2_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_allevents_b1b2_deltaR:   "<<Higgs_allevents_b1b2_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_allevents_b1b2_deltaR;
    }

    if(fVariables[iVar] == "Higgs_deltaR"){
      if (Higgs_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_deltaR:  "<<Higgs_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_deltaR;
    }

    if(fVariables[iVar] == "Higgs_matched_b1b2_deltaR"){
      if (Higgs_matched_b1b2_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_matched_b1b2_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_matched_b1b2_deltaR:  "<<Higgs_matched_b1b2_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_matched_b1b2_deltaR;
    }

    if(fVariables[iVar] == "Higgs_failedmatch_b1b2_deltaR"){
      if (Higgs_failedmatch_b1b2_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_failedmatch_b1b2_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_failedmatch_b1b2_deltaR:  "<<Higgs_failedmatch_b1b2_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_failedmatch_b1b2_deltaR;
    }

    if(fVariables[iVar] == "Higgs_allevents_b1b2_deltaR"){
      if (Higgs_allevents_b1b2_deltaR>0.0){
	fHistoVector[iVar].Fill(Higgs_allevents_b1b2_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Higgs_allevents_b1b2_deltaR:  "<<Higgs_allevents_b1b2_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Higgs_allevents_b1b2_deltaR;
    }

    if(fVariables[iVar] == "Top_H_deltaR"){
      if (Top_H_deltaR>0.0){
	fHistoVector[iVar].Fill(Top_H_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Top_H_deltaR:   "<<Top_H_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Top_H_deltaR;
    }

     if(fVariables[iVar] == "Top_L_deltaR"){
      if (Top_H_deltaR>0.0){
	fHistoVector[iVar].Fill(Top_L_deltaR,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"Top_L_deltaR:   "<<Top_L_deltaR<<std::endl;
      }
      fTreeVariables[iVar] = Top_L_deltaR;
    }
    
    if(fVariables[iVar] == "EResolutionhadTop"){
      if (EnergyResolutionhadTop>0.0){
	fHistoVector[iVar].Fill(EnergyResolutionhadTop,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"EnergyResolutionhadTop:   "<<EnergyResolutionhadTop<<std::endl;
      }
      fTreeVariables[iVar] = EnergyResolutionhadTop;
    }

    if(fVariables[iVar] == "EResolutionlepTop"){
      if (EnergyResolutionlepTop>0.0){
	fHistoVector[iVar].Fill(EnergyResolutionlepTop,histoweight);
	//fTreeVariables[iVar] = allevents_Higgs_mass;
	std::cout<<"EnergyResolutionlepTop:   "<<EnergyResolutionlepTop<<std::endl;
      }
      fTreeVariables[iVar] = EnergyResolutionlepTop;
    }
    

    
  }

  //DELETEME
  //}


}

