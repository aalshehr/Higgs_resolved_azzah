#ifndef HistoCreator_H_
#define HistoCreator_H_

#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/EventExtras.h"
#include "PlotFactoryBoosted/Enums.h"
#include "PlotFactoryBoosted/ConfigClass.h"

#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include <utility>
#include <vector>
#include <string>

using namespace std;

class HistoCreator{

 public:

  HistoCreator(std::string, std::string, std::string, std::shared_ptr<ConfigClass>, std::string);
  virtual ~HistoCreator();

  void FillCPDistributions();

  void FillCutflowChallenge(EventExtras* iEntryExtrasInfo);
  bool FillParticleLevelCutFlow(bool FillHist, float LumiWeight);

  void FillWjetsHistos(EventExtras* iEntryExtrasInfo);
  void FillCoreHistos(EventExtras* iEntryExtrasInfo);
  void FillCoreBoostedHistos(EventExtras* iEntryExtrasInfo, string fSampleType, string fTreeName);
  void FillBoostedTruthHistos(EventExtras* iEntryExtrasInfo, string fSampleType, string fTreeName);
  void FillSmallJetsAllHistos(EventExtras* iEntryExtrasInfo);
  void FillSmallJetsAdditionalHistos(EventExtras* iEntryExtrasInfo);
  void FillMVAResolvedHistos(EventExtras* iEntryExtrasInfo);
  void FillMVABoostedHistos(EventExtras* iEntryExtrasInfo);
  void FillTTHExtraHistos(EventExtras* iEntryExtrasInfo);
  void FillTTBarRecoHistos(EventExtras* iEntryExtrasInfo);
  void FillTTBarExtras(EventExtras* iEntryExtrasInfo);
  void FillTTBarMigrationMatrices(EventExtras* iEntryExtrasInfo, bool matchParticleLevel);
  void FillMatchedTruthHistos(EventExtras *iEntryExtrasInfo);
  void FillUncutTruthHistos(EventExtras* iEntryExtrasInfo);
  void FillPartonLevelHistos(EventExtras* iEntryExtrasInfo);
  void FillParticleLevelHistos(EventExtras* iEntryExtrasInfo);
  void FillWeights(EventExtras* iEntryExtrasInfo);
  void SetScaleFactorType(std::string type){fSFType = type;};

  bool InitializeHistograms(std::string InputFileName, std::string InputFileDir);

  float CalculateLumiWeight();
 
  float GetSystWeights(std::string);

  std::string m_ttbbNLONormFileName;
  std::string m_ttbbNLOShapeFileName;
  std::string m_ttbarHFFracFileName;


 private:

  std::vector<std::string> m_ttbarHFweights;

  std::shared_ptr<ConfigClass> gConfig;
  
  FlatTreeReader *fFlatTree;
  FlatTreeReader *fTruthTree;

  std::pair<float, int> fSampleInfo;

  float m_BTagCut,m_TopMass,m_Eff,m_Rejection;
  bool  m_Integrate,m_TopMassFix;
 
  std::vector<TH1F>                       fHistoVector;
  std::vector<std::string>                fHistoName;
  std::vector<std::string>                fVariables;
  std::vector<std::string>                fVariables_vec;
  std::vector<std::string>                fWeights;
  std::vector<double>                     fTreeVariables;
  std::vector< std::vector<float> >       fTreeVectorVariables;
  std::vector<double>                     fTreeWeights;
  std::vector<Float_t>                    fVar;
  std::vector<Float_t>                    fVar_vec;
  std::vector<TH2D>                       fCutflowHistoVector;
  std::vector<std::string>                fCutflowName;
  TH2D fNtupleCutflowHisto_e;
  TH2D fNtupleCutflowHisto_mu;

  //2D plots
  std::vector<TH2F>        fHistoVector_2D;
  std::vector<Float_t>     fVar_2D;
  std::vector<std::string> fHistoName_2D;
  std::vector<std::string> fVariables_2D;

  std::vector<std::string> fSetList;

  // Total Number of events read in
  int fTotalEventsRead;

  //
  int fEventSel;

  string fInputFile;
  string fOutputFile;
  string fOutputTreeFile;
  string fLeptonType;
  string fChannel;
  string fSampleType;
  string fTreeName;
  string fSampleList;
  string fAnalysisType;

  int   fDSID;
  float fLumi;
  float fTotalEventsMC;

  float fSampleXSection;
  
  float missingEtCut;
  float triangularCut;

  bool  fSplitHF;
  bool  fMVATreesForResolved;

  string fTTbarHF;

  string fUser;

  string fSFType;

  bool doTruth;

  BINS fBTagBin;
  BINS fJetBin;
  BINS fLargeJetBin;
  BINS fTopTagBin; 
  BINS fHiggsTagBin; 
  BINS fVetoBTagBin;
  BINS fVetoJetBin;
  BINS fVetoLargeJetBin;
  BINS fVetoTopTagBin; 
  BINS fVetoHiggsTagBin; 

  bool fReweightTTbb;


};

#endif
