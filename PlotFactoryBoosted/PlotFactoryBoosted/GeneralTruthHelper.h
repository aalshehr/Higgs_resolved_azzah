#ifndef GeneralTruthHelper_HH
#define GeneralTruthHelper_HH


#include <iostream>

namespace TTHbbTruth{

  enum TTHbbTruthParticleLabel{

    higgs=0,
    hadTop=1,
    lepTop=2,
    leadingTop=3,
    subleadingTop=4,
    vectorBoson=5, /// not from top or Higgs
    BSMHiggs=6,
    ChargedHiggs=7,
    bFromCollide=8,
    childFromChargedHiggs=9,

    //// particles from Higgs and Top
    childFromHiggs=10,
    leadingChildFromHiggs=11,
    subleadingChildFromHiggs=12,
    childFromLepTop=13,
    childFromHadTop=14,
    childFromLeadingTop=15,
    childFromSubleadingTop=16,
    directChildFromTop=17,
    leadingAFromHiggs=18,
    subleadingAFromHiggs=19,

    childFromTop=28,
    childFromAntiTop=29,

    //// particules from Z/W/A from Higgs/Top
    leadingChildFromWTop=20,
    subleadingChildFromWTop=21,
    childFromWHiggs=22,
    childFromZHiggs=23,
    leadingChildFromLeadingA=24,
    subleadingChildFromLeadingA=25,
    leadingChildFromSubleadingA=26,
    subleadingChildFromSubleadingA=27,

    /// particles from vector bosons (not from top/higgs decay)
    childFromVectorBoson=30
   
  };

  enum TTHbbTruthHiggsDecay{

    Hbb=0,
    HWW=1,
    HZZ=2,
    Hgaga=3,
    Htt=4,
    Hgg=5,
    Hcc=6,
    Hmumu=7,
    Hother=8

  };

  enum JetTruthMatchDefinition{JetMatchLeadingBHiggs=0,  
			       JetMatchSubLeadingBHiggs=1,
			       JetMatchBLepTop=2,
			       JetMatchBHadTop=3,
			       JetMatchLeadingJetW=4,
			       JetMatchSubLeadingJetW=5,

			       JetMatchLeadingChildFromLeadingA=6,
			       JetMatchSubleadingChildFromLeadingA=7,
			       JetMatchLeadingChildFromSubleadingA=8,
			       JetMatchSubleadingChildFromSubleadingA=9,

			       JetMatchAssociatedB=10,
			       JetMatchBChargedHiggs=11,
			       JetMatchBTop=12,
			       JetMatchBAntiTop=13,
			       JetMatchJetWplus=14,
			       JetMatchJetWminus=15,

  };



  bool isHiggs(int pdg, int info);
  bool isFromHiggs(int info);
  bool isLeadingFromHiggs(int info);
  bool isSubleadingFromHiggs(int info);
  bool isFromWFromHiggs(int info);
  bool isFromZFromHiggs(int info);

  bool isHadTop(int pdg, int info);
  bool isLepTop(int pdg, int info);
  bool isLeadingTop(int pdg, int info);
  bool isSubLeadingTop(int pdg, int info);
  bool isFromHadTop(int info);  /// takes into account t->w->xx
  bool isFromLepTop(int info);
  bool isFromLeadingTop(int info);
  bool isFromSubLeadingTop(int info);
  bool isFromTop(int info);
  bool isFromAntiTop(int info);
  bool isDirectlyFromTop(int info);
  bool isLeadingFromWTop(int info);
  bool isSubleadingFromWTop(int info);
  
  bool isVectorBoson(int pdg, int info); /// not from top/H decay
  bool isFromVectorBoson(int info); /// not from top/H decay

  bool isBSMHiggs(int info);
  bool isChargedHiggs(int info);
  bool isFromChargedHiggs(int info);
  bool isAssociatedBwithChargedHiggs(int info);
  bool isLeadingAFromHiggs(int info);
  bool isSubleadingAFromHiggs(int info);
  bool isLeadingChildFromLeadingA(int info);
  bool isSubleadingChildFromLeadingA(int info);
  bool isLeadingChildFromSubleadingA(int info);
  bool isSubleadingChildFromSubleadingA(int info);


}






#endif
