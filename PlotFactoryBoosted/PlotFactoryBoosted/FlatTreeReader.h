//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Aug  5 17:02:02 2016 by ROOT version 6.04/16
// from TTree nominal_Loose/tree
// found on file: /ptmp/mpp/knand/AnalysisTTH_02-04-16-01/Downloads/user.tpelzer.mc15_13TeV.410000.PoPy6_ttbar_lep.e3698s2608r7725p2669.TTHbbL-2-4-16-1.1l_sys.v1_out.root/user.tpelzer.9078765._000001.out.root
//////////////////////////////////////////////////////////

#ifndef FlatTreeReader_h
#define FlatTreeReader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include "TLorentzVector.h"

using namespace std;

class FlatTreeReader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         PS_HLT_mu24;
   Float_t         PS_HLT_e24_lhmedium_nod0_L1EM18VH;
   Float_t         weight_mm_ejets;
   Float_t         weight_mm_mujets;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config8;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config7;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config6;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config4;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config5;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config3;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config2;
   Float_t         fakesMM_weight_ejets_2015_Loose_test2015config1;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config7;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config8;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config6;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config5;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config4;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config3;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config2;
   Float_t         fakesMM_weight_ejets_2016_Loose_test2016config1;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config8;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config7;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config6;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config4;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config5;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config3;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config2;
   Float_t         fakesMM_weight_mujets_2015_Loose_test2015config1;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config7;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config8;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config6;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config5;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config4;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config3;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config2;
   Float_t         fakesMM_weight_mujets_2016_Loose_test2016config1;
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_bTagSF_60;
   Float_t         weight_bTagSF_70;
   Float_t         weight_bTagSF_77;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Trigger;
   Float_t         weight_indiv_SF_EL_Trigger_UP;
   Float_t         weight_indiv_SF_EL_Trigger_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_UP;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_UP;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_60_eigenvars_B_up;
   vector<float>   *weight_bTagSF_60_eigenvars_C_up;
   vector<float>   *weight_bTagSF_60_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_60_eigenvars_B_down;
   vector<float>   *weight_bTagSF_60_eigenvars_C_down;
   vector<float>   *weight_bTagSF_60_eigenvars_Light_down;
   Float_t         weight_bTagSF_60_extrapolation_up;
   Float_t         weight_bTagSF_60_extrapolation_down;
   Float_t         weight_bTagSF_60_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_60_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_70_extrapolation_up;
   Float_t         weight_bTagSF_70_extrapolation_down;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_77_extrapolation_up;
   Float_t         weight_bTagSF_77_extrapolation_down;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_isTight;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_typebkg;
   vector<int>     *el_true_originbkg;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<char>    *mu_isTight;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_jvt;
   vector<int>     *jet_truthflav;
   vector<char>    *jet_isbtagged_60;
   vector<char>    *jet_isbtagged_70;
   vector<char>    *jet_isbtagged_77;
   vector<float>   *ljet_pt;
   vector<float>   *ljet_eta;
   vector<float>   *ljet_phi;
   vector<float>   *ljet_e;
   vector<float>   *ljet_m;
   vector<float>   *ljet_sd12;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           boosted_ejets_2015;
   Int_t           boosted_ejets_2016;
   Int_t           boosted_mujets_2015;
   Int_t           boosted_mujets_2016;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           boosted_ejets_2015_Loose;
   Int_t           boosted_ejets_2016_Loose;
   Int_t           boosted_mujets_2015_Loose;
   Int_t           boosted_mujets_2016_Loose;
   Int_t           ejets_2015_Loose;
   Int_t           ejets_2016_Loose;
   Int_t           mujets_2015_Loose;
   Int_t           mujets_2016_Loose;
   Int_t           ee_2015;
   Int_t           ee_2016;
   Int_t           mumu_2015;
   Int_t           mumu_2016;
   Int_t           emu_2015;
   Int_t           emu_2016;
   Char_t          HLT_mu24_ivarmedium;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_e60_lhmedium_nod0;
   Char_t          HLT_e140_lhloose_nod0;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_e24_lhtight_nod0_ivarloose;
   Char_t          HLT_e120_lhloose;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
   vector<char>    *el_trigMatch_HLT_e24_lhtight_nod0_ivarloose;
   vector<char>    *el_trigMatch_HLT_e120_lhloose;
   vector<char>    *mu_trigMatch_HLT_mu24_ivarmedium;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
   Int_t           TopHeavyFlavorFilterFlag;
   ULong64_t       prwHash;
   vector<int>     *el_true_pdg;
   vector<float>   *el_true_pt;
   vector<float>   *el_true_eta;
   vector<int>     *mu_true_pdg;
   vector<float>   *mu_true_pt;
   vector<float>   *mu_true_eta;
   vector<int>     *jet_truthPartonLabel;
   Int_t           nJets;
   Int_t           nBTags;
   Int_t           nPrimaryVtx;
   Int_t           nElectrons;
   Int_t           nMuons;
   Int_t           nHFJets;
   Float_t         HT_jets;
   Float_t         HT_all;
   Float_t         Centrality_all;
   Float_t         Mbb_MindR_77;
   Float_t         dRbb_MaxPt_77;
   Float_t         Mjj_MaxPt;
   Float_t         pT_jet5;
   Float_t         H1_all;
   Float_t         dRbb_avg_77;
   Float_t         Mbj_MaxPt_77;
   Float_t         dRlepbb_MindR_77;
   Float_t         Muu_MindR;
   Float_t         Aplan_bjets;
   Float_t         dRuu_MindR;
   Float_t         Mjjj_MaxPt;   
   Int_t           nJets_Pt40;
   Float_t         Mbj_MindR;
   Float_t         Mjj_MindR;
   Float_t         pTuu_MindR;
   Float_t         Mbb_MaxM;
   Float_t         Mbj_Wmass;
   Float_t         dRbj_Wmass;
   Int_t           tauVetoFlag;
   Int_t           isTrueSL;
   Int_t           isTrueDIL;
   Float_t         dEtajj_MaxdEta;
   Float_t         MHiggs;
   Float_t         dRHl_MindR;
   Int_t           nHiggsbb30_77;
   Float_t         Mjj_MinM;
   Float_t         dRHl_MaxdR;
   Float_t         Mjj_HiggsMass;
   Float_t         dRlj_MindR;
   Float_t         H4_all;
   Float_t         pT_jet3;
   Float_t         dRbb_MaxM;
   Float_t         Aplanarity_jets;
   //   Int_t           nJets_Pt40;
   Float_t         Mbb_MaxPt;
   Int_t           nLjets;
   Float_t         HhadT_nJets;
   Float_t         HhadT_nLjets;
   Float_t         FirstLjetPt;
   Float_t         SecondLjetPt;
   Float_t         FirstLjetM;
   Float_t         SecondLjetM;
   Float_t         HT_ljets;
   Int_t           nLjet_m100;
   Int_t           nLjet_m50;
   Int_t           nJetOutsideLjet;
   Int_t           nBjetOutsideLjet;
   Float_t         dRbb_min;
   Float_t         dRjj_min;
   Float_t         HiggsbbM;
   Float_t         HiggsjjM;
   Float_t         klfitter_bestPerm_topHad_pt;
   Float_t         klfitter_bestPerm_topHad_eta;
   Float_t         klfitter_bestPerm_topHad_phi;
   Float_t         klfitter_bestPerm_topHad_E;
   Float_t         klfitter_bestPerm_topLep_pt;
   Float_t         klfitter_bestPerm_topLep_eta;
   Float_t         klfitter_bestPerm_topLep_phi;
   Float_t         klfitter_bestPerm_topLep_E;
   Float_t         klfitter_bestPerm_ttbar_pt;
   Float_t         klfitter_bestPerm_ttbar_eta;
   Float_t         klfitter_bestPerm_ttbar_phi;
   Float_t         klfitter_bestPerm_ttbar_E;
   //Float_t         matched_Higgs_pt;
   vector<float>   *ljet_sd23;
   vector<float>   *ljet_tau21;
   vector<float>   *ljet_tau32;
   vector<float>   *ljet_tau21_wta;
   vector<float>   *ljet_tau32_wta;
   vector<float>   *ljet_D2;
   vector<float>   *ljet_C2;
   vector<float>   *klfitter_model_Higgs_b1_pt;
   vector<float>   *klfitter_model_Higgs_b1_eta;
   vector<float>   *klfitter_model_Higgs_b1_phi;
   vector<float>   *klfitter_model_Higgs_b1_E;
   vector<unsigned int>   *klfitter_model_Higgs_b1_jetIndex;
   vector<float>   *klfitter_model_Higgs_b2_pt;
   vector<float>   *klfitter_model_Higgs_b2_eta;
   vector<float>   *klfitter_model_Higgs_b2_phi;
   vector<float>   *klfitter_model_Higgs_b2_E;
   vector<unsigned int>   *klfitter_model_Higgs_b2_jetIndex;
   vector<char>    *ljet_topTag;
   vector<char>    *ljet_bosonTag;
   vector<char>    *ljet_topTag_loose;
   vector<char>    *ljet_bosonTag_loose;
   Int_t           ljet_topTagN;
   Int_t           ljet_topTagN_loose;
   Int_t           ljet_bosonTagN;
   Int_t           ljet_bosonTagN_loose;
   vector<int>     *ljet_truthmatch;
   vector<int>     *jet_truthmatch;
   vector<float>   *truth_jet_pt;
   vector<float>   *truth_jet_eta;
   vector<float>   *truth_jet_phi;
   vector<float>   *truth_jet_m;
   vector<float>   *truth_pt;
   vector<float>   *truth_eta;
   vector<float>   *truth_phi;
   vector<float>   *truth_m;
   vector<int>     *truth_pdgid;
   vector<int>     *truth_status;
   vector<int>     *truth_tthbb_info;
   Char_t          truth_nHiggs;
   Char_t          truth_nTop;
   Char_t          truth_nLepTop;
   Char_t          truth_nVectorBoson;
   Float_t         truth_ttbar_pt;
   Float_t         truth_top_pt;
   Float_t         truth_higgs_eta;
   Float_t         truth_tbar_pt;
   Char_t          truth_HDecay;
   Bool_t          truth_top_dilep_filter;
   Float_t         semilepMVAreco_higgs_mass;
   Float_t         semilepMVAreco_bbhiggs_dR;
   Float_t         semilepMVAreco_higgsbleptop_mass;
   Float_t         semilepMVAreco_higgsleptop_dR;
   Float_t         semilepMVAreco_higgslep_dR;
   Float_t         semilepMVAreco_leptophadtop_dR;
   Float_t         semilepMVAreco_higgsq1hadW_mass;
   Float_t         semilepMVAreco_hadWb1Higgs_mass;
   Float_t         semilepMVAreco_b1higgsbhadtop_dR;
   Float_t         semilepMVAreco_higgsbleptop_withH_dR;
   Float_t         semilepMVAreco_higgsttbar_withH_dR;
   Float_t         semilepMVAreco_higgsbhadtop_withH_dR;
   Float_t         semilepMVAreco_leptophadtop_withH_dR;
   Float_t         semilepMVAreco_ttH_Ht_withH;
   Float_t         semilepMVAreco_BDT_output;
   Float_t         semilepMVAreco_BDT_withH_output;
   Int_t           semilepMVAreco_BDT_output_truthMatchPattern;
   Int_t           semilepMVAreco_BDT_withH_output_truthMatchPattern;
   Int_t           semilepMVAreco_Ncombinations;
   Int_t           semilepMVAreco_nuApprox_recoBDT;
   Int_t           semilepMVAreco_nuApprox_recoBDT_withH;
   vector<int>     *jet_semilepMVAreco_recoBDT_cand;
   vector<int>     *jet_semilepMVAreco_recoBDT_withH_cand;
   Float_t         semilepMVAreco_BDT_output_6jsplit;
   Float_t         semilepMVAreco_BDT_withH_output_6jsplit;
   Int_t           semilepMVAreco_nuApprox_recoBDT_6jsplit;
   Int_t           semilepMVAreco_nuApprox_recoBDT_withH_6jsplit;
   vector<int>     *jet_semilepMVAreco_recoBDT_cand_6jsplit;
   vector<int>     *jet_semilepMVAreco_recoBDT_withH_cand_6jsplit;
   Float_t         ClassifBDTOutput_basic;
   Float_t         ClassifBDTOutput_withReco_basic;
   Float_t         ClassifBDTOutput_6jsplit;
   Float_t         ClassifBDTOutput_withReco_6jsplit;
   Float_t         Zjets_Systematic_ckkw15;
   Float_t         Zjets_Systematic_ckkw30;
   Float_t         Zjets_Systematic_fac025;
   Float_t         Zjets_Systematic_fac4;
   Float_t         Zjets_Systematic_renorm025;
   Float_t         Zjets_Systematic_renorm4;
   Float_t         Zjets_Systematic_qsf025;
   Float_t         Zjets_Systematic_qsf4;
   Float_t         NBFricoNN_ljets;
   Float_t         NBFricoNN_dil;
   Float_t         ClassifHPLUS_Semilep_HF_BDT200_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT225_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT250_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT275_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT300_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT350_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT400_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT500_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT1000_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT2000_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT200_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT225_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT250_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT275_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT300_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT350_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT400_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT500_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT600_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT700_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT800_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT900_Output;
   vector<int>     *el_LHMedium;
   vector<int>     *el_LHTight;
   vector<char>    *el_isoGradient;
   vector<char>    *el_isoGradientLoose;
   vector<char>    *el_isoTight;
   vector<char>    *el_isoLoose;
   vector<char>    *el_isoLooseTrackOnly;
   vector<char>    *el_isoFixedCutTight;
   vector<char>    *el_isoFixedCutTightTrackOnly;
   vector<char>    *el_isoFixedCutLoose;
   vector<char>    *mu_Tight;
   vector<char>    *mu_Medium;
   vector<char>    *mu_isoGradient;
   vector<char>    *mu_isoGradientLoose;
   vector<char>    *mu_isoTight;
   vector<char>    *mu_isoLoose;
   vector<char>    *mu_isoLooseTrackOnly;
   vector<char>    *mu_isoFixedCutTightTrackOnly;
   vector<char>    *mu_isoFixedCutLoose;
   Int_t           HF_Classification;
   Int_t           HF_SimpleClassification;
   Float_t         q1_pt;
   Float_t         q1_eta;
   Float_t         q1_phi;
   Float_t         q1_m;
   Float_t         q2_pt;
   Float_t         q2_eta;
   Float_t         q2_phi;
   Float_t         q2_m;
   Float_t         qq_pt;
   Float_t         qq_ht;
   Float_t         qq_dr;
   Float_t         qq_m;
   Int_t           nTruthJets15;
   Int_t           nTruthJets20;
   Int_t           nTruthJets25;
   Int_t           nTruthJets25W;
   Int_t           nTruthJets50;
   Float_t         weight_ttbb_Norm;
   Float_t         weight_ttbb_CSS_KIN;
   Float_t         weight_ttbb_NNPDF;
   Float_t         weight_ttbb_MSTW;
   Float_t         weight_ttbb_Q_CMMPS;
   Float_t         weight_ttbb_glosoft;
   Float_t         weight_ttbb_defaultX05;
   Float_t         weight_ttbb_defaultX2;
   Float_t         weight_ttbb_MPIup;
   Float_t         weight_ttbb_MPIdown;
   Float_t         weight_ttbb_MPIfactor;
   Float_t         weight_ttbb_aMcAtNloHpp;
   Float_t         weight_ttbb_aMcAtNloPy8;
   Float_t         weight_ttbar_FracRw;
   Float_t         ttHF_mva_discriminant;

   // List of branches
   TBranch        *b_klfitter_model_Higgs_b1_pt;  //!
   TBranch        *b_klfitter_model_Higgs_b1_eta;  //!
   TBranch        *b_klfitter_model_Higgs_b1_phi;  //!
   TBranch        *b_klfitter_model_Higgs_b1_E;  //!
   TBranch        *b_klfitter_model_Higgs_b1_jetIndex;  //!
   TBranch        *b_klfitter_model_Higgs_b2_pt;  //!
   TBranch        *b_klfitter_model_Higgs_b2_eta;  //!
   TBranch        *b_klfitter_model_Higgs_b2_phi;  //!
   TBranch        *b_klfitter_model_Higgs_b2_E;  //!
   TBranch        *b_klfitter_model_Higgs_b2_jetIndex; //!

   TBranch        *b_klfitter_bestPerm_topHad_pt; //!
   TBranch        *b_klfitter_bestPerm_topHad_eta; //!
   TBranch        *b_klfitter_bestPerm_topHad_phi; //!
   TBranch        *b_klfitter_bestPerm_topHad_E; //!
   TBranch        *b_klfitter_bestPerm_topLep_pt; //!
   TBranch        *b_klfitter_bestPerm_topLep_eta; //!
   TBranch        *b_klfitter_bestPerm_topLep_phi; //!
   TBranch        *b_klfitter_bestPerm_topLep_E; //!
   TBranch        *b_klfitter_bestPerm_ttbar_pt; //!
   TBranch        *b_klfitter_bestPerm_ttbar_eta; //!
   TBranch        *b_klfitter_bestPerm_ttbar_phi; //!
   TBranch        *b_klfitter_bestPerm_ttbar_E; //!
   //TBranch        *b_matched_Higgs_pt; //!
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config8;   //!                                                                                      
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config7;   //!                                                                                       
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config6;   //!                                                                                        
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config4;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config5;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config3;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config2;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2015_Loose_test2015config1;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config7;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config8;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config6;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config5;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config4;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config3;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config2;   //!                                                                                         
   TBranch        *b_fakesMM_weight_ejets_2016_Loose_test2016config1;   //!                                                                                         
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config8;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config7;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config6;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config4;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config5;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config3;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config2;   //!
   TBranch        *b_fakesMM_weight_mujets_2015_Loose_test2015config1;   //!
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config8;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config7;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config6;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config4;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config5;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config3;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config2;   //!                                                                                        
   TBranch        *b_fakesMM_weight_mujets_2016_Loose_test2016config1;   //!   


   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_bTagSF_60;   //!
   TBranch        *b_weight_bTagSF_70;   //!
   TBranch        *b_weight_bTagSF_77;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_isTight;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_typebkg;   //!
   TBranch        *b_el_true_originbkg;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_isbtagged_60;   //!
   TBranch        *b_jet_isbtagged_70;   //!
   TBranch        *b_jet_isbtagged_77;   //!
   TBranch        *b_ljet_pt;   //!
   TBranch        *b_ljet_eta;   //!
   TBranch        *b_ljet_phi;   //!
   TBranch        *b_ljet_e;   //!
   TBranch        *b_ljet_m;   //!
   TBranch        *b_ljet_sd12;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_boosted_ejets_2015;   //!
   TBranch        *b_boosted_ejets_2016;   //!
   TBranch        *b_boosted_mujets_2015;   //!
   TBranch        *b_boosted_mujets_2016;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_boosted_ejets_2015_Loose;   //!                                                                                                               
   TBranch        *b_boosted_ejets_2016_Loose;   //!                                                                                                               
   TBranch        *b_boosted_mujets_2015_Loose;   //!                                                                                                             
   TBranch        *b_boosted_mujets_2016_Loose;   //!                                                                                                               
   TBranch        *b_ejets_2015_Loose;   //!                                                                                                                     
   TBranch        *b_ejets_2016_Loose;   //!                                                                                                                  
   TBranch        *b_mujets_2015_Loose;   //!                                                                                                                     
   TBranch        *b_mujets_2016_Loose;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2016;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2016;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2016;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_trigMatch_HLT_e120_lhloose;   //!
   TBranch        *b_mu_trigMatch_HLT_mu24_ivarmedium;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_TopHeavyFlavorFilterFlag;   //!
   TBranch        *b_prwHash;   //!
   TBranch        *b_el_true_pdg;   //!
   TBranch        *b_el_true_pt;   //!
   TBranch        *b_el_true_eta;   //!
   TBranch        *b_mu_true_pdg;   //!
   TBranch        *b_mu_true_pt;   //!
   TBranch        *b_mu_true_eta;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_nBTags;   //!
   TBranch        *b_nPrimaryVtx;   //!
   TBranch        *b_nElectrons;   //!
   TBranch        *b_nMuons;   //!
   TBranch        *b_nHFJets;   //!
   TBranch        *b_HT_jets;   //!
   TBranch        *b_HT_all;   //!
   TBranch        *b_Centrality_all;   //!
   TBranch        *b_Mbb_MindR_77;   //!
   TBranch        *b_dRbb_MaxPt_77;   //!
   TBranch        *b_Mjj_MaxPt;   //!
   TBranch        *b_pT_jet5;   //!
   TBranch        *b_H1_all;   //!
   TBranch        *b_dRbb_avg_77;   //!
   TBranch        *b_Mbj_MaxPt_77;   //!
   TBranch        *b_dRlepbb_MindR_77;   //!
   TBranch        *b_Muu_MindR;   //!
   TBranch        *b_Aplan_bjets;   //!
   TBranch        *b_dRuu_MindR;   //!
   TBranch        *b_Mjjj_MaxPt;   //!
   TBranch        *b_nJets_Pt40;   //!
   TBranch        *b_Mbj_MindR;   //!
   TBranch        *b_Mjj_MindR;   //!
   TBranch        *b_pTuu_MindR;   //!
   TBranch        *b_Mbb_MaxM;   //!
   TBranch        *b_Mbj_Wmass;   //!
   TBranch        *b_dRbj_Wmass;   //!
   TBranch        *b_tauVetoFlag;   //!
   TBranch        *b_isTrueSL;   //!
   TBranch        *b_isTrueDIL;   //!
   TBranch        *b_dEtajj_MaxdEta;   //!
   TBranch        *b_MHiggs;   //!
   TBranch        *b_dRHl_MindR;   //!
   TBranch        *b_nHiggsbb30_77;   //!
   TBranch        *b_Mjj_MinM;   //!
   TBranch        *b_dRHl_MaxdR;   //!
   TBranch        *b_Mjj_HiggsMass;   //!
   TBranch        *b_dRlj_MindR;   //!
   TBranch        *b_H4_all;   //!
   TBranch        *b_pT_jet3;   //!
   TBranch        *b_dRbb_MaxM;   //!
   TBranch        *b_Aplanarity_jets;   //!
   //   TBranch        *b_nJets_Pt40;   //!
   TBranch        *b_Mbb_MaxPt;   //!
   TBranch        *b_nLjets;   //!
   TBranch        *b_HhadT_nJets;   //!
   TBranch        *b_HhadT_nLjets;   //!
   TBranch        *b_FirstLjetPt;   //!
   TBranch        *b_SecondLjetPt;   //!
   TBranch        *b_FirstLjetM;   //!
   TBranch        *b_SecondLjetM;   //!
   TBranch        *b_HT_ljets;   //!
   TBranch        *b_nLjet_m100;   //!
   TBranch        *b_nLjet_m50;   //!
   TBranch        *b_nJetOutsideLjet;   //!
   TBranch        *b_nBjetOutsideLjet;   //!
   TBranch        *b_dRbb_min;   //!
   TBranch        *b_dRjj_min;   //!
   TBranch        *b_HiggsbbM;   //!
   TBranch        *b_HiggsjjM;   //!
   TBranch        *b_ljet_sd23;   //!
   TBranch        *b_ljet_tau21;   //!
   TBranch        *b_ljet_tau32;   //!
   TBranch        *b_ljet_tau21_wta;   //!
   TBranch        *b_ljet_tau32_wta;   //!
   TBranch        *b_ljet_D2;   //!
   TBranch        *b_ljet_C2;   //!
   TBranch        *b_ljet_topTag;   //!
   TBranch        *b_ljet_bosonTag;   //!
   TBranch        *b_ljet_topTag_loose;   //!
   TBranch        *b_ljet_bosonTag_loose;   //!
   TBranch        *b_ljet_topTagN;   //!
   TBranch        *b_ljet_topTagN_loose;   //!
   TBranch        *b_ljet_bosonTagN;   //!
   TBranch        *b_ljet_bosonTagN_loose;   //!
   TBranch        *b_ljet_truthmatch;   //!
   TBranch        *b_jet_truthmatch;   //!
   TBranch        *b_truth_jet_pt;   //!
   TBranch        *b_truth_jet_eta;   //!
   TBranch        *b_truth_jet_phi;   //!
   TBranch        *b_truth_jet_m;   //!
   TBranch        *b_truth_pt;   //!
   TBranch        *b_truth_eta;   //!
   TBranch        *b_truth_phi;   //!
   TBranch        *b_truth_m;   //!
   TBranch        *b_truth_pdgid;   //!
   TBranch        *b_truth_status;   //!
   TBranch        *b_truth_tthbb_info;   //!
   TBranch        *b_truth_nHiggs;   //!
   TBranch        *b_truth_nTop;   //!
   TBranch        *b_truth_nLepTop;   //!
   TBranch        *b_truth_nVectorBoson;   //!
   TBranch        *b_truth_ttbar_pt;   //!
   TBranch        *b_truth_top_pt;   //!
   TBranch        *b_truth_higgs_eta;   //!
   TBranch        *b_truth_tbar_pt;   //!
   TBranch        *b_truth_HDecay;   //!
   TBranch        *b_truth_top_dilep_filter;   //!
   TBranch        *b_semilepMVAreco_higgs_mass;   //!
   TBranch        *b_semilepMVAreco_bbhiggs_dR;   //!
   TBranch        *b_semilepMVAreco_higgsbleptop_mass;   //!
   TBranch        *b_semilepMVAreco_higgsleptop_dR;   //!
   TBranch        *b_semilepMVAreco_higgslep_dR;   //!
   TBranch        *b_semilepMVAreco_leptophadtop_dR;   //!
   TBranch        *b_semilepMVAreco_higgsq1hadW_mass;   //!
   TBranch        *b_semilepMVAreco_hadWb1Higgs_mass;   //!
   TBranch        *b_semilepMVAreco_b1higgsbhadtop_dR;   //!
   TBranch        *b_semilepMVAreco_higgsbleptop_withH_dR;   //!
   TBranch        *b_semilepMVAreco_higgsttbar_withH_dR;   //!
   TBranch        *b_semilepMVAreco_higgsbhadtop_withH_dR;   //!
   TBranch        *b_semilepMVAreco_leptophadtop_withH_dR;   //!
   TBranch        *b_semilepMVAreco_ttH_Ht_withH;   //!
   TBranch        *b_semilepMVAreco_BDT_output;   //!
   TBranch        *b_semilepMVAreco_BDT_withH_output;   //!
   TBranch        *b_semilepMVAreco_BDT_output_truthMatchPattern;   //!
   TBranch        *b_semilepMVAreco_BDT_withH_output_truthMatchPattern;   //!
   TBranch        *b_semilepMVAreco_Ncombinations;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT_withH;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_cand;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_withH_cand;   //!
   TBranch        *b_semilepMVAreco_BDT_output_6jsplit;   //!
   TBranch        *b_semilepMVAreco_BDT_withH_output_6jsplit;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT_6jsplit;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT_withH_6jsplit;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_cand_6jsplit;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_withH_cand_6jsplit;   //!
   TBranch        *b_ClassifBDTOutput_basic;   //!
   TBranch        *b_ClassifBDTOutput_withReco_basic;   //!
   TBranch        *b_ClassifBDTOutput_6jsplit;   //!
   TBranch        *b_ClassifBDTOutput_withReco_6jsplit;   //!
   TBranch        *b_Zjets_Systematic_ckkw15;   //!
   TBranch        *b_Zjets_Systematic_ckkw30;   //!
   TBranch        *b_Zjets_Systematic_fac025;   //!
   TBranch        *b_Zjets_Systematic_fac4;   //!
   TBranch        *b_Zjets_Systematic_renorm025;   //!
   TBranch        *b_Zjets_Systematic_renorm4;   //!
   TBranch        *b_Zjets_Systematic_qsf025;   //!
   TBranch        *b_Zjets_Systematic_qsf4;   //!
   TBranch        *b_NBFricoNN_ljets;   //!
   TBranch        *b_NBFricoNN_dil;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT200_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT225_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT250_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT275_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT300_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT350_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT400_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT500_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT1000_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT2000_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT200_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT225_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT250_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT275_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT300_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT350_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT400_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT500_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT600_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT700_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT800_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT900_Output;   //!
   TBranch        *b_el_LHMedium;   //!
   TBranch        *b_el_LHTight;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_isoGradientLoose;   //!
   TBranch        *b_el_isoTight;   //!
   TBranch        *b_el_isoLoose;   //!
   TBranch        *b_el_isoLooseTrackOnly;   //!
   TBranch        *b_el_isoFixedCutTight;   //!
   TBranch        *b_el_isoFixedCutTightTrackOnly;   //!
   TBranch        *b_el_isoFixedCutLoose;   //!
   TBranch        *b_mu_Tight;   //!
   TBranch        *b_mu_Medium;   //!
   TBranch        *b_mu_isoGradient;   //!
   TBranch        *b_mu_isoGradientLoose;   //!
   TBranch        *b_mu_isoTight;   //!
   TBranch        *b_mu_isoLoose;   //!
   TBranch        *b_mu_isoLooseTrackOnly;   //!
   TBranch        *b_mu_isoFixedCutTightTrackOnly;   //!
   TBranch        *b_mu_isoFixedCutLoose;   //!
   TBranch        *b_HF_Classification;   //!
   TBranch        *b_HF_SimpleClassification;   //!
   TBranch        *b_q1_pt;   //!
   TBranch        *b_q1_eta;   //!
   TBranch        *b_q1_phi;   //!
   TBranch        *b_q1_m;   //!
   TBranch        *b_q2_pt;   //!
   TBranch        *b_q2_eta;   //!
   TBranch        *b_q2_phi;   //!
   TBranch        *b_q2_m;   //!
   TBranch        *b_qq_pt;   //!
   TBranch        *b_qq_ht;   //!
   TBranch        *b_qq_dr;   //!
   TBranch        *b_qq_m;   //!
   TBranch        *b_nTruthJets15;   //!
   TBranch        *b_nTruthJets20;   //!
   TBranch        *b_nTruthJets25;   //!
   TBranch        *b_nTruthJets25W;   //!
   TBranch        *b_nTruthJets50;   //!
   TBranch        *b_weight_ttbb_Norm;   //!
   TBranch        *b_weight_ttbb_CSS_KIN;   //!
   TBranch        *b_weight_ttbb_NNPDF;   //!
   TBranch        *b_weight_ttbb_MSTW;   //!
   TBranch        *b_weight_ttbb_Q_CMMPS;   //!
   TBranch        *b_weight_ttbb_glosoft;   //!
   TBranch        *b_weight_ttbb_defaultX05;   //!
   TBranch        *b_weight_ttbb_defaultX2;   //!
   TBranch        *b_weight_ttbb_MPIup;   //!
   TBranch        *b_weight_ttbb_MPIdown;   //!
   TBranch        *b_weight_ttbb_MPIfactor;   //!
   TBranch        *b_weight_ttbb_aMcAtNloHpp;   //!
   TBranch        *b_weight_ttbb_aMcAtNloPy8;   //!
   TBranch        *b_weight_ttbar_FracRw;   //!
   TBranch        *b_ttHF_mva_discriminant;   //!

   FlatTreeReader(TTree *tree=0);
   virtual ~FlatTreeReader();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef FlatTreeReader_cxx
FlatTreeReader::FlatTreeReader(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/ptmp/mpp/knand/AnalysisTTH_02-04-16-01/Downloads/user.tpelzer.mc15_13TeV.410000.PoPy6_ttbar_lep.e3698s2608r7725p2669.TTHbbL-2-4-16-1.1l_sys.v1_out.root/user.tpelzer.9078765._000001.out.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/ptmp/mpp/knand/AnalysisTTH_02-04-16-01/Downloads/user.tpelzer.mc15_13TeV.410000.PoPy6_ttbar_lep.e3698s2608r7725p2669.TTHbbL-2-4-16-1.1l_sys.v1_out.root/user.tpelzer.9078765._000001.out.root");
      }
      f->GetObject("nominal_Loose",tree);

   }
   Init(tree);
}

FlatTreeReader::~FlatTreeReader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t FlatTreeReader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t FlatTreeReader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void FlatTreeReader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
  klfitter_model_Higgs_b1_pt = 0;
  klfitter_model_Higgs_b1_eta = 0;
  klfitter_model_Higgs_b1_phi = 0;
  klfitter_model_Higgs_b1_E = 0;
  klfitter_model_Higgs_b1_jetIndex = 0;
  klfitter_model_Higgs_b2_pt = 0;
  klfitter_model_Higgs_b2_eta = 0;
  klfitter_model_Higgs_b2_phi = 0;
  klfitter_model_Higgs_b2_E = 0;
  klfitter_model_Higgs_b2_jetIndex = 0;
  klfitter_bestPerm_topHad_pt = 0;
  klfitter_bestPerm_topHad_eta = 0;
  klfitter_bestPerm_topHad_phi = 0;
  klfitter_bestPerm_topHad_E = 0;
  klfitter_bestPerm_topLep_pt = 0;
  klfitter_bestPerm_topLep_eta = 0;
  klfitter_bestPerm_topLep_phi = 0;
  klfitter_bestPerm_topLep_E = 0;
  klfitter_bestPerm_ttbar_pt = 0;
  klfitter_bestPerm_ttbar_eta = 0;
  klfitter_bestPerm_ttbar_phi = 0;
  klfitter_bestPerm_ttbar_E = 0;
  //matched_Higgs_pt = 0;
  mc_generator_weights = 0;
   weight_bTagSF_60_eigenvars_B_up = 0;
   weight_bTagSF_60_eigenvars_C_up = 0;
   weight_bTagSF_60_eigenvars_Light_up = 0;
   weight_bTagSF_60_eigenvars_B_down = 0;
   weight_bTagSF_60_eigenvars_C_down = 0;
   weight_bTagSF_60_eigenvars_Light_down = 0;
   weight_bTagSF_70_eigenvars_B_up = 0;
   weight_bTagSF_70_eigenvars_C_up = 0;
   weight_bTagSF_70_eigenvars_Light_up = 0;
   weight_bTagSF_70_eigenvars_B_down = 0;
   weight_bTagSF_70_eigenvars_C_down = 0;
   weight_bTagSF_70_eigenvars_Light_down = 0;
   weight_bTagSF_77_eigenvars_B_up = 0;
   weight_bTagSF_77_eigenvars_C_up = 0;
   weight_bTagSF_77_eigenvars_Light_up = 0;
   weight_bTagSF_77_eigenvars_B_down = 0;
   weight_bTagSF_77_eigenvars_C_down = 0;
   weight_bTagSF_77_eigenvars_Light_down = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_isTight = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_typebkg = 0;
   el_true_originbkg = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_isTight = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c10 = 0;
   jet_mv2c20 = 0;
   jet_jvt = 0;
   jet_truthflav = 0;
   jet_isbtagged_60 = 0;
   jet_isbtagged_70 = 0;
   jet_isbtagged_77 = 0;
   ljet_pt = 0;
   ljet_eta = 0;
   ljet_phi = 0;
   ljet_e = 0;
   ljet_m = 0;
   ljet_sd12 = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
   el_trigMatch_HLT_e140_lhloose_nod0 = 0;
   el_trigMatch_HLT_e60_lhmedium = 0;
   el_trigMatch_HLT_e24_lhmedium_L1EM20VH = 0;
   el_trigMatch_HLT_e24_lhtight_nod0_ivarloose = 0;
   el_trigMatch_HLT_e120_lhloose = 0;
   mu_trigMatch_HLT_mu24_ivarmedium = 0;
   mu_trigMatch_HLT_mu50 = 0;
   mu_trigMatch_HLT_mu20_iloose_L1MU15 = 0;
   el_true_pdg = 0;
   el_true_pt = 0;
   el_true_eta = 0;
   mu_true_pdg = 0;
   mu_true_pt = 0;
   mu_true_eta = 0;
   jet_truthPartonLabel = 0;
   ljet_sd23 = 0;
   ljet_tau21 = 0;
   ljet_tau32 = 0;
   ljet_tau21_wta = 0;
   ljet_tau32_wta = 0;
   ljet_D2 = 0;
   ljet_C2 = 0;
   ljet_topTag = 0;
   ljet_bosonTag = 0;
   ljet_topTag_loose = 0;
   ljet_bosonTag_loose = 0;
   ljet_truthmatch = 0;
   jet_truthmatch = 0;
   truth_jet_pt = 0;
   truth_jet_eta = 0;
   truth_jet_phi = 0;
   truth_jet_m = 0;
   truth_pt = 0;
   truth_eta = 0;
   truth_phi = 0;
   truth_m = 0;
   truth_pdgid = 0;
   truth_status = 0;
   truth_tthbb_info = 0;
   jet_semilepMVAreco_recoBDT_cand = 0;
   jet_semilepMVAreco_recoBDT_withH_cand = 0;
   jet_semilepMVAreco_recoBDT_cand_6jsplit = 0;
   jet_semilepMVAreco_recoBDT_withH_cand_6jsplit = 0;
   el_LHMedium = 0;
   el_LHTight = 0;
   el_isoGradient = 0;
   el_isoGradientLoose = 0;
   el_isoTight = 0;
   el_isoLoose = 0;
   el_isoLooseTrackOnly = 0;
   el_isoFixedCutTight = 0;
   el_isoFixedCutTightTrackOnly = 0;
   el_isoFixedCutLoose = 0;
   mu_Tight = 0;
   mu_Medium = 0;
   mu_isoGradient = 0;
   mu_isoGradientLoose = 0;
   mu_isoTight = 0;
   mu_isoLoose = 0;
   mu_isoLooseTrackOnly = 0;
   mu_isoFixedCutTightTrackOnly = 0;
   mu_isoFixedCutLoose = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   // if (fChain->GetListOfBranches()->Contains("matched_Higgs_pt")) if (fChain->GetListOfBranches()->Contains("matched_Higgs_pt")) fChain->SetBranchAddress("matched_Higgs_pt", &matched_Higgs_pt, &b_matched_Higgs_pt);
if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_pt")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_pt")) fChain->SetBranchAddress("klfitter_model_Higgs_b1_pt", &klfitter_model_Higgs_b1_pt, &b_klfitter_model_Higgs_b1_pt);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_eta")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_eta")) fChain->SetBranchAddress("klfitter_model_Higgs_b1_eta", &klfitter_model_Higgs_b1_eta, &b_klfitter_model_Higgs_b1_eta);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_phi")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_phi")) fChain->SetBranchAddress("klfitter_model_Higgs_b1_phi", &klfitter_model_Higgs_b1_phi, &b_klfitter_model_Higgs_b1_phi);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_E")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_E")) fChain->SetBranchAddress("klfitter_model_Higgs_b1_E", &klfitter_model_Higgs_b1_E, &b_klfitter_model_Higgs_b1_E);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_jetIndex")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b1_jetIndex")) fChain->SetBranchAddress("klfitter_model_Higgs_b1_jetIndex", &klfitter_model_Higgs_b1_jetIndex, &b_klfitter_model_Higgs_b1_jetIndex); 
   if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_pt")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_pt")) fChain->SetBranchAddress("klfitter_model_Higgs_b2_pt", &klfitter_model_Higgs_b2_pt, &b_klfitter_model_Higgs_b2_pt);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_eta")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_eta")) fChain->SetBranchAddress("klfitter_model_Higgs_b2_eta", &klfitter_model_Higgs_b2_eta, &b_klfitter_model_Higgs_b2_eta);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_phi")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_phi")) fChain->SetBranchAddress("klfitter_model_Higgs_b2_phi", &klfitter_model_Higgs_b2_phi, &b_klfitter_model_Higgs_b2_phi);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_E")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_E")) fChain->SetBranchAddress("klfitter_model_Higgs_b2_E", &klfitter_model_Higgs_b2_E, &b_klfitter_model_Higgs_b2_E);
 if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_jetIndex")) if (fChain->GetListOfBranches()->Contains("klfitter_model_Higgs_b2_jetIndex")) fChain->SetBranchAddress("klfitter_model_Higgs_b2_jetIndex", &klfitter_model_Higgs_b2_jetIndex, &b_klfitter_model_Higgs_b2_jetIndex);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_pt")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_pt")) fChain->SetBranchAddress("klfitter_bestPerm_topHad_pt", &klfitter_bestPerm_topHad_pt, &b_klfitter_bestPerm_topHad_pt);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_eta")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_eta")) fChain->SetBranchAddress("klfitter_bestPerm_topHad_eta", &klfitter_bestPerm_topHad_eta, &b_klfitter_bestPerm_topHad_eta);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_phi")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_phi")) fChain->SetBranchAddress("klfitter_bestPerm_topHad_phi", &klfitter_bestPerm_topHad_phi, &b_klfitter_bestPerm_topHad_phi);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_E")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topHad_E")) fChain->SetBranchAddress("klfitter_bestPerm_topHad_E", &klfitter_bestPerm_topHad_E, &b_klfitter_bestPerm_topHad_E);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_pt")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_pt")) fChain->SetBranchAddress("klfitter_bestPerm_topLep_pt", &klfitter_bestPerm_topLep_pt, &b_klfitter_bestPerm_topLep_pt);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_eta")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_eta")) fChain->SetBranchAddress("klfitter_bestPerm_topLep_eta", &klfitter_bestPerm_topLep_eta, &b_klfitter_bestPerm_topLep_eta);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_phi")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_phi")) fChain->SetBranchAddress("klfitter_bestPerm_topLep_phi", &klfitter_bestPerm_topLep_phi, &b_klfitter_bestPerm_topLep_phi);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_E")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_topLep_E")) fChain->SetBranchAddress("klfitter_bestPerm_topLep_E", &klfitter_bestPerm_topLep_E, &b_klfitter_bestPerm_topLep_E);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_pt")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_pt")) fChain->SetBranchAddress("klfitter_bestPerm_ttbar_pt", &klfitter_bestPerm_ttbar_pt, &b_klfitter_bestPerm_ttbar_pt);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_eta")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_eta")) fChain->SetBranchAddress("klfitter_bestPerm_ttbar_eta", &klfitter_bestPerm_ttbar_eta, &b_klfitter_bestPerm_ttbar_eta);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_phi")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_phi")) fChain->SetBranchAddress("klfitter_bestPerm_ttbar_phi", &klfitter_bestPerm_ttbar_phi, &b_klfitter_bestPerm_ttbar_phi);
 if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_E")) if (fChain->GetListOfBranches()->Contains("klfitter_bestPerm_ttbar_E")) fChain->SetBranchAddress("klfitter_bestPerm_ttbar_E", &klfitter_bestPerm_ttbar_E, &b_klfitter_bestPerm_ttbar_E);

   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config8")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config8")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config8", &fakesMM_weight_ejets_2015_Loose_test2015config8, &b_fakesMM_weight_ejets_2015_Loose_test2015config8);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config7")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config7")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config7", &fakesMM_weight_ejets_2015_Loose_test2015config7, &b_fakesMM_weight_ejets_2015_Loose_test2015config7);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config6")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config6")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config6", &fakesMM_weight_ejets_2015_Loose_test2015config6, &b_fakesMM_weight_ejets_2015_Loose_test2015config6);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config4")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config4")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config4", &fakesMM_weight_ejets_2015_Loose_test2015config4, &b_fakesMM_weight_ejets_2015_Loose_test2015config4);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config5")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config5")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config5", &fakesMM_weight_ejets_2015_Loose_test2015config5, &b_fakesMM_weight_ejets_2015_Loose_test2015config5);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config3")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config3")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config3", &fakesMM_weight_ejets_2015_Loose_test2015config3, &b_fakesMM_weight_ejets_2015_Loose_test2015config3);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config2")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config2")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config2", &fakesMM_weight_ejets_2015_Loose_test2015config2, &b_fakesMM_weight_ejets_2015_Loose_test2015config2);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config1")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2015_Loose_test2015config1")) fChain->SetBranchAddress("fakesMM_weight_ejets_2015_Loose_test2015config1", &fakesMM_weight_ejets_2015_Loose_test2015config1, &b_fakesMM_weight_ejets_2015_Loose_test2015config1);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config7")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config7")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config7", &fakesMM_weight_ejets_2016_Loose_test2016config7, &b_fakesMM_weight_ejets_2016_Loose_test2016config7);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config8")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config8")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config8", &fakesMM_weight_ejets_2016_Loose_test2016config8, &b_fakesMM_weight_ejets_2016_Loose_test2016config8);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config6")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config6")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config6", &fakesMM_weight_ejets_2016_Loose_test2016config6, &b_fakesMM_weight_ejets_2016_Loose_test2016config6);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config5")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config5")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config5", &fakesMM_weight_ejets_2016_Loose_test2016config5, &b_fakesMM_weight_ejets_2016_Loose_test2016config5);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config4")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config4")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config4", &fakesMM_weight_ejets_2016_Loose_test2016config4, &b_fakesMM_weight_ejets_2016_Loose_test2016config4);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config3")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config3")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config3", &fakesMM_weight_ejets_2016_Loose_test2016config3, &b_fakesMM_weight_ejets_2016_Loose_test2016config3);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config2")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config2")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config2", &fakesMM_weight_ejets_2016_Loose_test2016config2, &b_fakesMM_weight_ejets_2016_Loose_test2016config2);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config1")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_ejets_2016_Loose_test2016config1")) fChain->SetBranchAddress("fakesMM_weight_ejets_2016_Loose_test2016config1", &fakesMM_weight_ejets_2016_Loose_test2016config1, &b_fakesMM_weight_ejets_2016_Loose_test2016config1);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config8")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config8")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config8", &fakesMM_weight_mujets_2015_Loose_test2015config8, &b_fakesMM_weight_mujets_2015_Loose_test2015config8);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config7")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config7")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config7", &fakesMM_weight_mujets_2015_Loose_test2015config7, &b_fakesMM_weight_mujets_2015_Loose_test2015config7);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config6")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config6")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config6", &fakesMM_weight_mujets_2015_Loose_test2015config6, &b_fakesMM_weight_mujets_2015_Loose_test2015config6);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config4")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config4")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config4", &fakesMM_weight_mujets_2015_Loose_test2015config4, &b_fakesMM_weight_mujets_2015_Loose_test2015config4);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config5")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config5")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config5", &fakesMM_weight_mujets_2015_Loose_test2015config5, &b_fakesMM_weight_mujets_2015_Loose_test2015config5);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config3")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config3")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config3", &fakesMM_weight_mujets_2015_Loose_test2015config3, &b_fakesMM_weight_mujets_2015_Loose_test2015config3);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config2")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config2")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config2", &fakesMM_weight_mujets_2015_Loose_test2015config2, &b_fakesMM_weight_mujets_2015_Loose_test2015config2);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config1")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2015_Loose_test2015config1")) fChain->SetBranchAddress("fakesMM_weight_mujets_2015_Loose_test2015config1", &fakesMM_weight_mujets_2015_Loose_test2015config1, &b_fakesMM_weight_mujets_2015_Loose_test2015config1);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config7")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config7")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config7", &fakesMM_weight_mujets_2016_Loose_test2016config7, &b_fakesMM_weight_mujets_2016_Loose_test2016config7);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config8")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config8")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config8", &fakesMM_weight_mujets_2016_Loose_test2016config8, &b_fakesMM_weight_mujets_2016_Loose_test2016config8);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config6")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config6")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config6", &fakesMM_weight_mujets_2016_Loose_test2016config6, &b_fakesMM_weight_mujets_2016_Loose_test2016config6);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config5")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config5")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config5", &fakesMM_weight_mujets_2016_Loose_test2016config5, &b_fakesMM_weight_mujets_2016_Loose_test2016config5);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config4")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config4")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config4", &fakesMM_weight_mujets_2016_Loose_test2016config4, &b_fakesMM_weight_mujets_2016_Loose_test2016config4);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config3")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config3")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config3", &fakesMM_weight_mujets_2016_Loose_test2016config3, &b_fakesMM_weight_mujets_2016_Loose_test2016config3);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config2")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config2")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config2", &fakesMM_weight_mujets_2016_Loose_test2016config2, &b_fakesMM_weight_mujets_2016_Loose_test2016config2);
   if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config1")) if (fChain->GetListOfBranches()->Contains("fakesMM_weight_mujets_2016_Loose_test2016config1")) fChain->SetBranchAddress("fakesMM_weight_mujets_2016_Loose_test2016config1", &fakesMM_weight_mujets_2016_Loose_test2016config1, &b_fakesMM_weight_mujets_2016_Loose_test2016config1);

   if (fChain->GetListOfBranches()->Contains("mc_generator_weights")) if (fChain->GetListOfBranches()->Contains("mc_generator_weights")) fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   if (fChain->GetListOfBranches()->Contains("weight_mc")) if (fChain->GetListOfBranches()->Contains("weight_mc")) fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   if (fChain->GetListOfBranches()->Contains("weight_pileup")) if (fChain->GetListOfBranches()->Contains("weight_pileup")) fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF")) fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60")) fChain->SetBranchAddress("weight_bTagSF_60", &weight_bTagSF_60, &b_weight_bTagSF_60);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70")) fChain->SetBranchAddress("weight_bTagSF_70", &weight_bTagSF_70, &b_weight_bTagSF_70);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77")) fChain->SetBranchAddress("weight_bTagSF_77", &weight_bTagSF_77, &b_weight_bTagSF_77);
   if (fChain->GetListOfBranches()->Contains("weight_jvt")) if (fChain->GetListOfBranches()->Contains("weight_jvt")) fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   if (fChain->GetListOfBranches()->Contains("weight_pileup_UP")) if (fChain->GetListOfBranches()->Contains("weight_pileup_UP")) fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   if (fChain->GetListOfBranches()->Contains("weight_pileup_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_pileup_DOWN")) fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Trigger_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Trigger_UP")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Trigger_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Trigger_DOWN")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Reco_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Reco_UP")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Reco_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Reco_DOWN")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_ID_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_ID_UP")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_ID_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_ID_DOWN")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Isol_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Isol_UP")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Isol_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_EL_SF_Isol_DOWN")) fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_STAT_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_STAT_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_SYST_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Trigger_SYST_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_STAT_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_STAT_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_SYST_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_Isol_SYST_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_STAT_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_STAT_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_SYST_UP")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_leptonSF_MU_SF_TTVA_SYST_DOWN")) fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Trigger")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Trigger")) fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger", &weight_indiv_SF_EL_Trigger, &b_weight_indiv_SF_EL_Trigger);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Trigger_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Trigger_UP")) fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_UP", &weight_indiv_SF_EL_Trigger_UP, &b_weight_indiv_SF_EL_Trigger_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Trigger_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Trigger_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_DOWN", &weight_indiv_SF_EL_Trigger_DOWN, &b_weight_indiv_SF_EL_Trigger_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Reco")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Reco")) fChain->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Reco_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Reco_UP")) fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Reco_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Reco_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_ID")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_ID")) fChain->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_ID_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_ID_UP")) fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_ID_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_ID_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Isol")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Isol")) fChain->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Isol_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Isol_UP")) fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Isol_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_EL_Isol_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger")) fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger", &weight_indiv_SF_MU_Trigger, &b_weight_indiv_SF_MU_Trigger);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_STAT_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_UP", &weight_indiv_SF_MU_Trigger_STAT_UP, &b_weight_indiv_SF_MU_Trigger_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_STAT_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_DOWN", &weight_indiv_SF_MU_Trigger_STAT_DOWN, &b_weight_indiv_SF_MU_Trigger_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_SYST_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_UP", &weight_indiv_SF_MU_Trigger_SYST_UP, &b_weight_indiv_SF_MU_Trigger_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Trigger_SYST_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_DOWN", &weight_indiv_SF_MU_Trigger_SYST_DOWN, &b_weight_indiv_SF_MU_Trigger_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_LOWPT_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_LOWPT_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_LOWPT_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_LOWPT_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol")) fChain->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_STAT_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_STAT_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_SYST_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_Isol_SYST_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA")) fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_STAT_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_STAT_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_STAT_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_STAT_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_SYST_UP")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_SYST_UP")) fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
   if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_SYST_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_indiv_SF_MU_TTVA_SYST_DOWN")) fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_jvt_UP")) if (fChain->GetListOfBranches()->Contains("weight_jvt_UP")) fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   if (fChain->GetListOfBranches()->Contains("weight_jvt_DOWN")) if (fChain->GetListOfBranches()->Contains("weight_jvt_DOWN")) fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_B_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_B_up")) fChain->SetBranchAddress("weight_bTagSF_60_eigenvars_B_up", &weight_bTagSF_60_eigenvars_B_up, &b_weight_bTagSF_60_eigenvars_B_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_C_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_C_up")) fChain->SetBranchAddress("weight_bTagSF_60_eigenvars_C_up", &weight_bTagSF_60_eigenvars_C_up, &b_weight_bTagSF_60_eigenvars_C_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_Light_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_Light_up")) fChain->SetBranchAddress("weight_bTagSF_60_eigenvars_Light_up", &weight_bTagSF_60_eigenvars_Light_up, &b_weight_bTagSF_60_eigenvars_Light_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_B_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_B_down")) fChain->SetBranchAddress("weight_bTagSF_60_eigenvars_B_down", &weight_bTagSF_60_eigenvars_B_down, &b_weight_bTagSF_60_eigenvars_B_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_C_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_C_down")) fChain->SetBranchAddress("weight_bTagSF_60_eigenvars_C_down", &weight_bTagSF_60_eigenvars_C_down, &b_weight_bTagSF_60_eigenvars_C_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_Light_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_eigenvars_Light_down")) fChain->SetBranchAddress("weight_bTagSF_60_eigenvars_Light_down", &weight_bTagSF_60_eigenvars_Light_down, &b_weight_bTagSF_60_eigenvars_Light_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_up")) fChain->SetBranchAddress("weight_bTagSF_60_extrapolation_up", &weight_bTagSF_60_extrapolation_up, &b_weight_bTagSF_60_extrapolation_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_down")) fChain->SetBranchAddress("weight_bTagSF_60_extrapolation_down", &weight_bTagSF_60_extrapolation_down, &b_weight_bTagSF_60_extrapolation_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_from_charm_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_from_charm_up")) fChain->SetBranchAddress("weight_bTagSF_60_extrapolation_from_charm_up", &weight_bTagSF_60_extrapolation_from_charm_up, &b_weight_bTagSF_60_extrapolation_from_charm_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_from_charm_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_60_extrapolation_from_charm_down")) fChain->SetBranchAddress("weight_bTagSF_60_extrapolation_from_charm_down", &weight_bTagSF_60_extrapolation_from_charm_down, &b_weight_bTagSF_60_extrapolation_from_charm_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_B_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_B_up")) fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_B_up", &weight_bTagSF_70_eigenvars_B_up, &b_weight_bTagSF_70_eigenvars_B_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_C_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_C_up")) fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_C_up", &weight_bTagSF_70_eigenvars_C_up, &b_weight_bTagSF_70_eigenvars_C_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_Light_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_Light_up")) fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_Light_up", &weight_bTagSF_70_eigenvars_Light_up, &b_weight_bTagSF_70_eigenvars_Light_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_B_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_B_down")) fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_B_down", &weight_bTagSF_70_eigenvars_B_down, &b_weight_bTagSF_70_eigenvars_B_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_C_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_C_down")) fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_C_down", &weight_bTagSF_70_eigenvars_C_down, &b_weight_bTagSF_70_eigenvars_C_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_Light_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_eigenvars_Light_down")) fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_Light_down", &weight_bTagSF_70_eigenvars_Light_down, &b_weight_bTagSF_70_eigenvars_Light_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_up")) fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_up", &weight_bTagSF_70_extrapolation_up, &b_weight_bTagSF_70_extrapolation_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_down")) fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_down", &weight_bTagSF_70_extrapolation_down, &b_weight_bTagSF_70_extrapolation_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_from_charm_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_from_charm_up")) fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_from_charm_up", &weight_bTagSF_70_extrapolation_from_charm_up, &b_weight_bTagSF_70_extrapolation_from_charm_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_from_charm_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_70_extrapolation_from_charm_down")) fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_from_charm_down", &weight_bTagSF_70_extrapolation_from_charm_down, &b_weight_bTagSF_70_extrapolation_from_charm_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_B_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_B_up")) fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_B_up", &weight_bTagSF_77_eigenvars_B_up, &b_weight_bTagSF_77_eigenvars_B_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_C_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_C_up")) fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_C_up", &weight_bTagSF_77_eigenvars_C_up, &b_weight_bTagSF_77_eigenvars_C_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_Light_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_Light_up")) fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_Light_up", &weight_bTagSF_77_eigenvars_Light_up, &b_weight_bTagSF_77_eigenvars_Light_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_B_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_B_down")) fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_B_down", &weight_bTagSF_77_eigenvars_B_down, &b_weight_bTagSF_77_eigenvars_B_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_C_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_C_down")) fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_C_down", &weight_bTagSF_77_eigenvars_C_down, &b_weight_bTagSF_77_eigenvars_C_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_Light_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_eigenvars_Light_down")) fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_Light_down", &weight_bTagSF_77_eigenvars_Light_down, &b_weight_bTagSF_77_eigenvars_Light_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_up")) fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_up", &weight_bTagSF_77_extrapolation_up, &b_weight_bTagSF_77_extrapolation_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_down")) fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_down", &weight_bTagSF_77_extrapolation_down, &b_weight_bTagSF_77_extrapolation_down);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_from_charm_up")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_from_charm_up")) fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_from_charm_up", &weight_bTagSF_77_extrapolation_from_charm_up, &b_weight_bTagSF_77_extrapolation_from_charm_up);
   if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_from_charm_down")) if (fChain->GetListOfBranches()->Contains("weight_bTagSF_77_extrapolation_from_charm_down")) fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_from_charm_down", &weight_bTagSF_77_extrapolation_from_charm_down, &b_weight_bTagSF_77_extrapolation_from_charm_down);
   if (fChain->GetListOfBranches()->Contains("eventNumber")) if (fChain->GetListOfBranches()->Contains("eventNumber")) fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   if (fChain->GetListOfBranches()->Contains("runNumber")) if (fChain->GetListOfBranches()->Contains("runNumber")) fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   if (fChain->GetListOfBranches()->Contains("randomRunNumber")) if (fChain->GetListOfBranches()->Contains("randomRunNumber")) fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   if (fChain->GetListOfBranches()->Contains("mcChannelNumber")) if (fChain->GetListOfBranches()->Contains("mcChannelNumber")) fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   if (fChain->GetListOfBranches()->Contains("mu")) if (fChain->GetListOfBranches()->Contains("mu")) fChain->SetBranchAddress("mu", &mu, &b_mu);
   if (fChain->GetListOfBranches()->Contains("backgroundFlags")) if (fChain->GetListOfBranches()->Contains("backgroundFlags")) fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   if (fChain->GetListOfBranches()->Contains("el_pt")) if (fChain->GetListOfBranches()->Contains("el_pt")) fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   if (fChain->GetListOfBranches()->Contains("el_eta")) if (fChain->GetListOfBranches()->Contains("el_eta")) fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   if (fChain->GetListOfBranches()->Contains("el_cl_eta")) if (fChain->GetListOfBranches()->Contains("el_cl_eta")) fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   if (fChain->GetListOfBranches()->Contains("el_phi")) if (fChain->GetListOfBranches()->Contains("el_phi")) fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   if (fChain->GetListOfBranches()->Contains("el_e")) if (fChain->GetListOfBranches()->Contains("el_e")) fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   if (fChain->GetListOfBranches()->Contains("el_charge")) if (fChain->GetListOfBranches()->Contains("el_charge")) fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   if (fChain->GetListOfBranches()->Contains("el_topoetcone20")) if (fChain->GetListOfBranches()->Contains("el_topoetcone20")) fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   if (fChain->GetListOfBranches()->Contains("el_ptvarcone20")) if (fChain->GetListOfBranches()->Contains("el_ptvarcone20")) fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   if (fChain->GetListOfBranches()->Contains("el_isTight")) if (fChain->GetListOfBranches()->Contains("el_isTight")) fChain->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
   if (fChain->GetListOfBranches()->Contains("el_d0sig")) if (fChain->GetListOfBranches()->Contains("el_d0sig")) fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   if (fChain->GetListOfBranches()->Contains("el_delta_z0_sintheta")) if (fChain->GetListOfBranches()->Contains("el_delta_z0_sintheta")) fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   if (fChain->GetListOfBranches()->Contains("el_true_type")) if (fChain->GetListOfBranches()->Contains("el_true_type")) fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   if (fChain->GetListOfBranches()->Contains("el_true_origin")) if (fChain->GetListOfBranches()->Contains("el_true_origin")) fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   if (fChain->GetListOfBranches()->Contains("el_true_typebkg")) if (fChain->GetListOfBranches()->Contains("el_true_typebkg")) fChain->SetBranchAddress("el_true_typebkg", &el_true_typebkg, &b_el_true_typebkg);
   if (fChain->GetListOfBranches()->Contains("el_true_originbkg")) if (fChain->GetListOfBranches()->Contains("el_true_originbkg")) fChain->SetBranchAddress("el_true_originbkg", &el_true_originbkg, &b_el_true_originbkg);
   if (fChain->GetListOfBranches()->Contains("mu_pt")) if (fChain->GetListOfBranches()->Contains("mu_pt")) fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   if (fChain->GetListOfBranches()->Contains("mu_eta")) if (fChain->GetListOfBranches()->Contains("mu_eta")) fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   if (fChain->GetListOfBranches()->Contains("mu_phi")) if (fChain->GetListOfBranches()->Contains("mu_phi")) fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   if (fChain->GetListOfBranches()->Contains("mu_e")) if (fChain->GetListOfBranches()->Contains("mu_e")) fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   if (fChain->GetListOfBranches()->Contains("mu_charge")) if (fChain->GetListOfBranches()->Contains("mu_charge")) fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   if (fChain->GetListOfBranches()->Contains("mu_topoetcone20")) if (fChain->GetListOfBranches()->Contains("mu_topoetcone20")) fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   if (fChain->GetListOfBranches()->Contains("mu_ptvarcone30")) if (fChain->GetListOfBranches()->Contains("mu_ptvarcone30")) fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   if (fChain->GetListOfBranches()->Contains("mu_isTight")) if (fChain->GetListOfBranches()->Contains("mu_isTight")) fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
   if (fChain->GetListOfBranches()->Contains("mu_d0sig")) if (fChain->GetListOfBranches()->Contains("mu_d0sig")) fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   if (fChain->GetListOfBranches()->Contains("mu_delta_z0_sintheta")) if (fChain->GetListOfBranches()->Contains("mu_delta_z0_sintheta")) fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   if (fChain->GetListOfBranches()->Contains("mu_true_type")) if (fChain->GetListOfBranches()->Contains("mu_true_type")) fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   if (fChain->GetListOfBranches()->Contains("mu_true_origin")) if (fChain->GetListOfBranches()->Contains("mu_true_origin")) fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   if (fChain->GetListOfBranches()->Contains("jet_pt")) if (fChain->GetListOfBranches()->Contains("jet_pt")) fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   if (fChain->GetListOfBranches()->Contains("jet_eta")) if (fChain->GetListOfBranches()->Contains("jet_eta")) fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   if (fChain->GetListOfBranches()->Contains("jet_phi")) if (fChain->GetListOfBranches()->Contains("jet_phi")) fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   if (fChain->GetListOfBranches()->Contains("jet_e")) if (fChain->GetListOfBranches()->Contains("jet_e")) fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   if (fChain->GetListOfBranches()->Contains("jet_mv2c10")) if (fChain->GetListOfBranches()->Contains("jet_mv2c10")) fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   if (fChain->GetListOfBranches()->Contains("jet_mv2c20")) if (fChain->GetListOfBranches()->Contains("jet_mv2c20")) fChain->SetBranchAddress("jet_mv2c20", &jet_mv2c20, &b_jet_mv2c20);
   if (fChain->GetListOfBranches()->Contains("jet_jvt")) if (fChain->GetListOfBranches()->Contains("jet_jvt")) fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   if (fChain->GetListOfBranches()->Contains("jet_truthflav")) if (fChain->GetListOfBranches()->Contains("jet_truthflav")) fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   if (fChain->GetListOfBranches()->Contains("jet_isbtagged_60")) if (fChain->GetListOfBranches()->Contains("jet_isbtagged_60")) fChain->SetBranchAddress("jet_isbtagged_60", &jet_isbtagged_60, &b_jet_isbtagged_60);
   if (fChain->GetListOfBranches()->Contains("jet_isbtagged_70")) if (fChain->GetListOfBranches()->Contains("jet_isbtagged_70")) fChain->SetBranchAddress("jet_isbtagged_70", &jet_isbtagged_70, &b_jet_isbtagged_70);
   if (fChain->GetListOfBranches()->Contains("jet_isbtagged_77")) if (fChain->GetListOfBranches()->Contains("jet_isbtagged_77")) fChain->SetBranchAddress("jet_isbtagged_77", &jet_isbtagged_77, &b_jet_isbtagged_77);
   if (fChain->GetListOfBranches()->Contains("ljet_pt")) if (fChain->GetListOfBranches()->Contains("ljet_pt")) fChain->SetBranchAddress("ljet_pt", &ljet_pt, &b_ljet_pt);
   if (fChain->GetListOfBranches()->Contains("ljet_eta")) if (fChain->GetListOfBranches()->Contains("ljet_eta")) fChain->SetBranchAddress("ljet_eta", &ljet_eta, &b_ljet_eta);
   if (fChain->GetListOfBranches()->Contains("ljet_phi")) if (fChain->GetListOfBranches()->Contains("ljet_phi")) fChain->SetBranchAddress("ljet_phi", &ljet_phi, &b_ljet_phi);
   if (fChain->GetListOfBranches()->Contains("ljet_e")) if (fChain->GetListOfBranches()->Contains("ljet_e")) fChain->SetBranchAddress("ljet_e", &ljet_e, &b_ljet_e);
   if (fChain->GetListOfBranches()->Contains("ljet_m")) if (fChain->GetListOfBranches()->Contains("ljet_m")) fChain->SetBranchAddress("ljet_m", &ljet_m, &b_ljet_m);
   if (fChain->GetListOfBranches()->Contains("ljet_sd12")) if (fChain->GetListOfBranches()->Contains("ljet_sd12")) fChain->SetBranchAddress("ljet_sd12", &ljet_sd12, &b_ljet_sd12);
   if (fChain->GetListOfBranches()->Contains("met_met")) if (fChain->GetListOfBranches()->Contains("met_met")) fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   if (fChain->GetListOfBranches()->Contains("met_phi")) if (fChain->GetListOfBranches()->Contains("met_phi")) fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   if (fChain->GetListOfBranches()->Contains("boosted_ejets_2015")) if (fChain->GetListOfBranches()->Contains("boosted_ejets_2015")) fChain->SetBranchAddress("boosted_ejets_2015", &boosted_ejets_2015, &b_boosted_ejets_2015);
   if (fChain->GetListOfBranches()->Contains("boosted_ejets_2016")) if (fChain->GetListOfBranches()->Contains("boosted_ejets_2016")) fChain->SetBranchAddress("boosted_ejets_2016", &boosted_ejets_2016, &b_boosted_ejets_2016);
   if (fChain->GetListOfBranches()->Contains("boosted_mujets_2015")) if (fChain->GetListOfBranches()->Contains("boosted_mujets_2015")) fChain->SetBranchAddress("boosted_mujets_2015", &boosted_mujets_2015, &b_boosted_mujets_2015);
   if (fChain->GetListOfBranches()->Contains("boosted_mujets_2016")) if (fChain->GetListOfBranches()->Contains("boosted_mujets_2016")) fChain->SetBranchAddress("boosted_mujets_2016", &boosted_mujets_2016, &b_boosted_mujets_2016);
   if (fChain->GetListOfBranches()->Contains("ejets_2015")) if (fChain->GetListOfBranches()->Contains("ejets_2015")) fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
   if (fChain->GetListOfBranches()->Contains("ejets_2016")) if (fChain->GetListOfBranches()->Contains("ejets_2016")) fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
   if (fChain->GetListOfBranches()->Contains("mujets_2015")) if (fChain->GetListOfBranches()->Contains("mujets_2015")) fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
   if (fChain->GetListOfBranches()->Contains("mujets_2016")) if (fChain->GetListOfBranches()->Contains("mujets_2016")) fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);

   if (fChain->GetListOfBranches()->Contains("boosted_ejets_2015_Loose")) if (fChain->GetListOfBranches()->Contains("boosted_ejets_2015_Loose")) fChain->SetBranchAddress("boosted_ejets_2015_Loose", &boosted_ejets_2015_Loose, &b_boosted_ejets_2015_Loose);
   if (fChain->GetListOfBranches()->Contains("boosted_ejets_2016_Loose")) if (fChain->GetListOfBranches()->Contains("boosted_ejets_2016_Loose")) fChain->SetBranchAddress("boosted_ejets_2016_Loose", &boosted_ejets_2016_Loose, &b_boosted_ejets_2016_Loose);
   if (fChain->GetListOfBranches()->Contains("boosted_mujets_2015_Loose")) if (fChain->GetListOfBranches()->Contains("boosted_mujets_2015_Loose")) fChain->SetBranchAddress("boosted_mujets_2015_Loose", &boosted_mujets_2015_Loose, &b_boosted_mujets_2015_Loose);
   if (fChain->GetListOfBranches()->Contains("boosted_mujets_2016_Loose")) if (fChain->GetListOfBranches()->Contains("boosted_mujets_2016_Loose")) fChain->SetBranchAddress("boosted_mujets_2016_Loose", &boosted_mujets_2016_Loose, &b_boosted_mujets_2016_Loose);
   if (fChain->GetListOfBranches()->Contains("ejets_2015_Loose")) if (fChain->GetListOfBranches()->Contains("ejets_2015_Loose")) fChain->SetBranchAddress("ejets_2015_Loose", &ejets_2015_Loose, &b_ejets_2015_Loose);
   if (fChain->GetListOfBranches()->Contains("ejets_2016_Loose")) if (fChain->GetListOfBranches()->Contains("ejets_2016_Loose")) fChain->SetBranchAddress("ejets_2016_Loose", &ejets_2016_Loose, &b_ejets_2016_Loose);
   if (fChain->GetListOfBranches()->Contains("mujets_2015_Loose")) if (fChain->GetListOfBranches()->Contains("mujets_2015_Loose")) fChain->SetBranchAddress("mujets_2015_Loose", &mujets_2015_Loose, &b_mujets_2015_Loose);
   if (fChain->GetListOfBranches()->Contains("mujets_2016_Loose")) if (fChain->GetListOfBranches()->Contains("mujets_2016_Loose")) fChain->SetBranchAddress("mujets_2016_Loose", &mujets_2016_Loose, &b_mujets_2016_Loose);


   if (fChain->GetListOfBranches()->Contains("ee_2015")) if (fChain->GetListOfBranches()->Contains("ee_2015")) fChain->SetBranchAddress("ee_2015", &ee_2015, &b_ee_2015);
   if (fChain->GetListOfBranches()->Contains("ee_2016")) if (fChain->GetListOfBranches()->Contains("ee_2016")) fChain->SetBranchAddress("ee_2016", &ee_2016, &b_ee_2016);
   if (fChain->GetListOfBranches()->Contains("mumu_2015")) if (fChain->GetListOfBranches()->Contains("mumu_2015")) fChain->SetBranchAddress("mumu_2015", &mumu_2015, &b_mumu_2015);
   if (fChain->GetListOfBranches()->Contains("mumu_2016")) if (fChain->GetListOfBranches()->Contains("mumu_2016")) fChain->SetBranchAddress("mumu_2016", &mumu_2016, &b_mumu_2016);
   if (fChain->GetListOfBranches()->Contains("emu_2015")) if (fChain->GetListOfBranches()->Contains("emu_2015")) fChain->SetBranchAddress("emu_2015", &emu_2015, &b_emu_2015);
   if (fChain->GetListOfBranches()->Contains("emu_2016")) if (fChain->GetListOfBranches()->Contains("emu_2016")) fChain->SetBranchAddress("emu_2016", &emu_2016, &b_emu_2016);
   if (fChain->GetListOfBranches()->Contains("HLT_mu24_ivarmedium")) if (fChain->GetListOfBranches()->Contains("HLT_mu24_ivarmedium")) fChain->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   if (fChain->GetListOfBranches()->Contains("HLT_mu20_iloose_L1MU15")) if (fChain->GetListOfBranches()->Contains("HLT_mu20_iloose_L1MU15")) fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   if (fChain->GetListOfBranches()->Contains("HLT_e60_lhmedium_nod0")) if (fChain->GetListOfBranches()->Contains("HLT_e60_lhmedium_nod0")) fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   if (fChain->GetListOfBranches()->Contains("HLT_e140_lhloose_nod0")) if (fChain->GetListOfBranches()->Contains("HLT_e140_lhloose_nod0")) fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   if (fChain->GetListOfBranches()->Contains("HLT_mu50")) if (fChain->GetListOfBranches()->Contains("HLT_mu50")) fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   if (fChain->GetListOfBranches()->Contains("HLT_e60_lhmedium")) if (fChain->GetListOfBranches()->Contains("HLT_e60_lhmedium")) fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   if (fChain->GetListOfBranches()->Contains("HLT_e24_lhmedium_L1EM20VH")) if (fChain->GetListOfBranches()->Contains("HLT_e24_lhmedium_L1EM20VH")) fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   if (fChain->GetListOfBranches()->Contains("HLT_e24_lhtight_nod0_ivarloose")) if (fChain->GetListOfBranches()->Contains("HLT_e24_lhtight_nod0_ivarloose")) fChain->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   if (fChain->GetListOfBranches()->Contains("HLT_e120_lhloose")) if (fChain->GetListOfBranches()->Contains("HLT_e120_lhloose")) fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e60_lhmedium_nod0")) if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e60_lhmedium_nod0")) fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e140_lhloose_nod0")) if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e140_lhloose_nod0")) fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
   if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e60_lhmedium")) if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e60_lhmedium")) fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium, &b_el_trigMatch_HLT_e60_lhmedium);
   if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e24_lhmedium_L1EM20VH")) if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e24_lhmedium_L1EM20VH")) fChain->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH, &b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
   if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e24_lhtight_nod0_ivarloose")) if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e24_lhtight_nod0_ivarloose")) fChain->SetBranchAddress("el_trigMatch_HLT_e24_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e24_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e24_lhtight_nod0_ivarloose);
   if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e120_lhloose")) if (fChain->GetListOfBranches()->Contains("el_trigMatch_HLT_e120_lhloose")) fChain->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose, &b_el_trigMatch_HLT_e120_lhloose);
   if (fChain->GetListOfBranches()->Contains("mu_trigMatch_HLT_mu24_ivarmedium")) if (fChain->GetListOfBranches()->Contains("mu_trigMatch_HLT_mu24_ivarmedium")) fChain->SetBranchAddress("mu_trigMatch_HLT_mu24_ivarmedium", &mu_trigMatch_HLT_mu24_ivarmedium, &b_mu_trigMatch_HLT_mu24_ivarmedium);
   if (fChain->GetListOfBranches()->Contains("mu_trigMatch_HLT_mu50")) if (fChain->GetListOfBranches()->Contains("mu_trigMatch_HLT_mu50")) fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
   if (fChain->GetListOfBranches()->Contains("mu_trigMatch_HLT_mu20_iloose_L1MU15")) if (fChain->GetListOfBranches()->Contains("mu_trigMatch_HLT_mu20_iloose_L1MU15")) fChain->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15, &b_mu_trigMatch_HLT_mu20_iloose_L1MU15);
   if (fChain->GetListOfBranches()->Contains("TopHeavyFlavorFilterFlag")) if (fChain->GetListOfBranches()->Contains("TopHeavyFlavorFilterFlag")) fChain->SetBranchAddress("TopHeavyFlavorFilterFlag", &TopHeavyFlavorFilterFlag, &b_TopHeavyFlavorFilterFlag);
   if (fChain->GetListOfBranches()->Contains("prwHash")) if (fChain->GetListOfBranches()->Contains("prwHash")) fChain->SetBranchAddress("prwHash", &prwHash, &b_prwHash);
   if (fChain->GetListOfBranches()->Contains("el_true_pdg")) if (fChain->GetListOfBranches()->Contains("el_true_pdg")) fChain->SetBranchAddress("el_true_pdg", &el_true_pdg, &b_el_true_pdg);
   if (fChain->GetListOfBranches()->Contains("el_true_pt")) if (fChain->GetListOfBranches()->Contains("el_true_pt")) fChain->SetBranchAddress("el_true_pt", &el_true_pt, &b_el_true_pt);
   if (fChain->GetListOfBranches()->Contains("el_true_eta")) if (fChain->GetListOfBranches()->Contains("el_true_eta")) fChain->SetBranchAddress("el_true_eta", &el_true_eta, &b_el_true_eta);
   if (fChain->GetListOfBranches()->Contains("mu_true_pdg")) if (fChain->GetListOfBranches()->Contains("mu_true_pdg")) fChain->SetBranchAddress("mu_true_pdg", &mu_true_pdg, &b_mu_true_pdg);
   if (fChain->GetListOfBranches()->Contains("mu_true_pt")) if (fChain->GetListOfBranches()->Contains("mu_true_pt")) fChain->SetBranchAddress("mu_true_pt", &mu_true_pt, &b_mu_true_pt);
   if (fChain->GetListOfBranches()->Contains("mu_true_eta")) if (fChain->GetListOfBranches()->Contains("mu_true_eta")) fChain->SetBranchAddress("mu_true_eta", &mu_true_eta, &b_mu_true_eta);
   if (fChain->GetListOfBranches()->Contains("jet_truthPartonLabel")) if (fChain->GetListOfBranches()->Contains("jet_truthPartonLabel")) fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   if (fChain->GetListOfBranches()->Contains("nJets")) if (fChain->GetListOfBranches()->Contains("nJets")) fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   if (fChain->GetListOfBranches()->Contains("nBTags")) if (fChain->GetListOfBranches()->Contains("nBTags")) fChain->SetBranchAddress("nBTags", &nBTags, &b_nBTags);
   if (fChain->GetListOfBranches()->Contains("nPrimaryVtx")) if (fChain->GetListOfBranches()->Contains("nPrimaryVtx")) fChain->SetBranchAddress("nPrimaryVtx", &nPrimaryVtx, &b_nPrimaryVtx);
   if (fChain->GetListOfBranches()->Contains("nElectrons")) if (fChain->GetListOfBranches()->Contains("nElectrons")) fChain->SetBranchAddress("nElectrons", &nElectrons, &b_nElectrons);
   if (fChain->GetListOfBranches()->Contains("nMuons")) if (fChain->GetListOfBranches()->Contains("nMuons")) fChain->SetBranchAddress("nMuons", &nMuons, &b_nMuons);
   if (fChain->GetListOfBranches()->Contains("nHFJets")) if (fChain->GetListOfBranches()->Contains("nHFJets")) fChain->SetBranchAddress("nHFJets", &nHFJets, &b_nHFJets);
   if (fChain->GetListOfBranches()->Contains("HT_jets")) if (fChain->GetListOfBranches()->Contains("HT_jets")) fChain->SetBranchAddress("HT_jets", &HT_jets, &b_HT_jets);
   if (fChain->GetListOfBranches()->Contains("HT_all")) if (fChain->GetListOfBranches()->Contains("HT_all")) fChain->SetBranchAddress("HT_all", &HT_all, &b_HT_all);
   if (fChain->GetListOfBranches()->Contains("Centrality_all")) if (fChain->GetListOfBranches()->Contains("Centrality_all")) fChain->SetBranchAddress("Centrality_all", &Centrality_all, &b_Centrality_all);
   if (fChain->GetListOfBranches()->Contains("Mbb_MindR_77")) if (fChain->GetListOfBranches()->Contains("Mbb_MindR_77")) fChain->SetBranchAddress("Mbb_MindR_77", &Mbb_MindR_77, &b_Mbb_MindR_77);
   if (fChain->GetListOfBranches()->Contains("dRbb_MaxPt_77")) if (fChain->GetListOfBranches()->Contains("dRbb_MaxPt_77")) fChain->SetBranchAddress("dRbb_MaxPt_77", &dRbb_MaxPt_77, &b_dRbb_MaxPt_77);
   if (fChain->GetListOfBranches()->Contains("Mjj_MaxPt")) if (fChain->GetListOfBranches()->Contains("Mjj_MaxPt")) fChain->SetBranchAddress("Mjj_MaxPt", &Mjj_MaxPt, &b_Mjj_MaxPt);
   if (fChain->GetListOfBranches()->Contains("pT_jet5")) if (fChain->GetListOfBranches()->Contains("pT_jet5")) fChain->SetBranchAddress("pT_jet5", &pT_jet5, &b_pT_jet5);
   if (fChain->GetListOfBranches()->Contains("H1_all")) if (fChain->GetListOfBranches()->Contains("H1_all")) fChain->SetBranchAddress("H1_all", &H1_all, &b_H1_all);
   if (fChain->GetListOfBranches()->Contains("dRbb_avg_77")) if (fChain->GetListOfBranches()->Contains("dRbb_avg_77")) fChain->SetBranchAddress("dRbb_avg_77", &dRbb_avg_77, &b_dRbb_avg_77);
   if (fChain->GetListOfBranches()->Contains("Mbj_MaxPt_77")) if (fChain->GetListOfBranches()->Contains("Mbj_MaxPt_77")) fChain->SetBranchAddress("Mbj_MaxPt_77", &Mbj_MaxPt_77, &b_Mbj_MaxPt_77);
   if (fChain->GetListOfBranches()->Contains("dRlepbb_MindR_77")) if (fChain->GetListOfBranches()->Contains("dRlepbb_MindR_77")) fChain->SetBranchAddress("dRlepbb_MindR_77", &dRlepbb_MindR_77, &b_dRlepbb_MindR_77);
   if (fChain->GetListOfBranches()->Contains("Muu_MindR")) if (fChain->GetListOfBranches()->Contains("Muu_MindR")) fChain->SetBranchAddress("Muu_MindR", &Muu_MindR, &b_Muu_MindR);
   if (fChain->GetListOfBranches()->Contains("Aplan_bjets")) if (fChain->GetListOfBranches()->Contains("Aplan_bjets")) fChain->SetBranchAddress("Aplan_bjets", &Aplan_bjets, &b_Aplan_bjets);
   if (fChain->GetListOfBranches()->Contains("dRuu_MindR")) if (fChain->GetListOfBranches()->Contains("dRuu_MindR")) fChain->SetBranchAddress("dRuu_MindR", &dRuu_MindR, &b_dRuu_MindR);
   if (fChain->GetListOfBranches()->Contains("Mjjj_MaxPt")) if (fChain->GetListOfBranches()->Contains("Mjjj_MaxPt")) fChain->SetBranchAddress("Mjjj_MaxPt", &Mjjj_MaxPt, &b_Mjjj_MaxPt);
   if (fChain->GetListOfBranches()->Contains("nJets_Pt40")) if (fChain->GetListOfBranches()->Contains("nJets_Pt40")) fChain->SetBranchAddress("nJets_Pt40", &nJets_Pt40, &b_nJets_Pt40);
   if (fChain->GetListOfBranches()->Contains("Mbj_MindR")) if (fChain->GetListOfBranches()->Contains("Mbj_MindR")) fChain->SetBranchAddress("Mbj_MindR", &Mbj_MindR, &b_Mbj_MindR);
   if (fChain->GetListOfBranches()->Contains("Mjj_MindR")) if (fChain->GetListOfBranches()->Contains("Mjj_MindR")) fChain->SetBranchAddress("Mjj_MindR", &Mjj_MindR, &b_Mjj_MindR);
   if (fChain->GetListOfBranches()->Contains("pTuu_MindR")) if (fChain->GetListOfBranches()->Contains("pTuu_MindR")) fChain->SetBranchAddress("pTuu_MindR", &pTuu_MindR, &b_pTuu_MindR);
   if (fChain->GetListOfBranches()->Contains("Mbb_MaxM")) if (fChain->GetListOfBranches()->Contains("Mbb_MaxM")) fChain->SetBranchAddress("Mbb_MaxM", &Mbb_MaxM, &b_Mbb_MaxM);
   if (fChain->GetListOfBranches()->Contains("Mbj_Wmass")) if (fChain->GetListOfBranches()->Contains("Mbj_Wmass")) fChain->SetBranchAddress("Mbj_Wmass", &Mbj_Wmass, &b_Mbj_Wmass);
   if (fChain->GetListOfBranches()->Contains("dRbj_Wmass")) if (fChain->GetListOfBranches()->Contains("dRbj_Wmass")) fChain->SetBranchAddress("dRbj_Wmass", &dRbj_Wmass, &b_dRbj_Wmass);
   if (fChain->GetListOfBranches()->Contains("tauVetoFlag")) if (fChain->GetListOfBranches()->Contains("tauVetoFlag")) fChain->SetBranchAddress("tauVetoFlag", &tauVetoFlag, &b_tauVetoFlag);
   if (fChain->GetListOfBranches()->Contains("isTrueSL")) if (fChain->GetListOfBranches()->Contains("isTrueSL")) fChain->SetBranchAddress("isTrueSL", &isTrueSL, &b_isTrueSL);
   if (fChain->GetListOfBranches()->Contains("isTrueDIL")) if (fChain->GetListOfBranches()->Contains("isTrueDIL")) fChain->SetBranchAddress("isTrueDIL", &isTrueDIL, &b_isTrueDIL);
   if (fChain->GetListOfBranches()->Contains("dEtajj_MaxdEta")) if (fChain->GetListOfBranches()->Contains("dEtajj_MaxdEta")) fChain->SetBranchAddress("dEtajj_MaxdEta", &dEtajj_MaxdEta, &b_dEtajj_MaxdEta);
   if (fChain->GetListOfBranches()->Contains("MHiggs")) if (fChain->GetListOfBranches()->Contains("MHiggs")) fChain->SetBranchAddress("MHiggs", &MHiggs, &b_MHiggs);
   if (fChain->GetListOfBranches()->Contains("dRHl_MindR")) if (fChain->GetListOfBranches()->Contains("dRHl_MindR")) fChain->SetBranchAddress("dRHl_MindR", &dRHl_MindR, &b_dRHl_MindR);
   if (fChain->GetListOfBranches()->Contains("nHiggsbb30_77")) if (fChain->GetListOfBranches()->Contains("nHiggsbb30_77")) fChain->SetBranchAddress("nHiggsbb30_77", &nHiggsbb30_77, &b_nHiggsbb30_77);
   if (fChain->GetListOfBranches()->Contains("Mjj_MinM")) if (fChain->GetListOfBranches()->Contains("Mjj_MinM")) fChain->SetBranchAddress("Mjj_MinM", &Mjj_MinM, &b_Mjj_MinM);
   if (fChain->GetListOfBranches()->Contains("dRHl_MaxdR")) if (fChain->GetListOfBranches()->Contains("dRHl_MaxdR")) fChain->SetBranchAddress("dRHl_MaxdR", &dRHl_MaxdR, &b_dRHl_MaxdR);
   if (fChain->GetListOfBranches()->Contains("Mjj_HiggsMass")) if (fChain->GetListOfBranches()->Contains("Mjj_HiggsMass")) fChain->SetBranchAddress("Mjj_HiggsMass", &Mjj_HiggsMass, &b_Mjj_HiggsMass);
   if (fChain->GetListOfBranches()->Contains("dRlj_MindR")) if (fChain->GetListOfBranches()->Contains("dRlj_MindR")) fChain->SetBranchAddress("dRlj_MindR", &dRlj_MindR, &b_dRlj_MindR);
   if (fChain->GetListOfBranches()->Contains("H4_all")) if (fChain->GetListOfBranches()->Contains("H4_all")) fChain->SetBranchAddress("H4_all", &H4_all, &b_H4_all);
   if (fChain->GetListOfBranches()->Contains("pT_jet3")) if (fChain->GetListOfBranches()->Contains("pT_jet3")) fChain->SetBranchAddress("pT_jet3", &pT_jet3, &b_pT_jet3);
   if (fChain->GetListOfBranches()->Contains("dRbb_MaxM")) if (fChain->GetListOfBranches()->Contains("dRbb_MaxM")) fChain->SetBranchAddress("dRbb_MaxM", &dRbb_MaxM, &b_dRbb_MaxM);
   if (fChain->GetListOfBranches()->Contains("Aplanarity_jets")) if (fChain->GetListOfBranches()->Contains("Aplanarity_jets")) fChain->SetBranchAddress("Aplanarity_jets", &Aplanarity_jets, &b_Aplanarity_jets);
//    if (fChain->GetListOfBranches()->Contains("nJets_Pt40")) if (fChain->GetListOfBranches()->Contains("nJets_Pt40")) fChain->SetBranchAddress("nJets_Pt40", &nJets_Pt40, &b_nJets_Pt40);
   if (fChain->GetListOfBranches()->Contains("Mbb_MaxPt")) if (fChain->GetListOfBranches()->Contains("Mbb_MaxPt")) fChain->SetBranchAddress("Mbb_MaxPt", &Mbb_MaxPt, &b_Mbb_MaxPt);
   if (fChain->GetListOfBranches()->Contains("nLjets")) if (fChain->GetListOfBranches()->Contains("nLjets")) fChain->SetBranchAddress("nLjets", &nLjets, &b_nLjets);
   if (fChain->GetListOfBranches()->Contains("HhadT_nJets")) if (fChain->GetListOfBranches()->Contains("HhadT_nJets")) fChain->SetBranchAddress("HhadT_nJets", &HhadT_nJets, &b_HhadT_nJets);
   if (fChain->GetListOfBranches()->Contains("HhadT_nLjets")) if (fChain->GetListOfBranches()->Contains("HhadT_nLjets")) fChain->SetBranchAddress("HhadT_nLjets", &HhadT_nLjets, &b_HhadT_nLjets);
   if (fChain->GetListOfBranches()->Contains("FirstLjetPt")) if (fChain->GetListOfBranches()->Contains("FirstLjetPt")) fChain->SetBranchAddress("FirstLjetPt", &FirstLjetPt, &b_FirstLjetPt);
   if (fChain->GetListOfBranches()->Contains("SecondLjetPt")) if (fChain->GetListOfBranches()->Contains("SecondLjetPt")) fChain->SetBranchAddress("SecondLjetPt", &SecondLjetPt, &b_SecondLjetPt);
   if (fChain->GetListOfBranches()->Contains("FirstLjetM")) if (fChain->GetListOfBranches()->Contains("FirstLjetM")) fChain->SetBranchAddress("FirstLjetM", &FirstLjetM, &b_FirstLjetM);
   if (fChain->GetListOfBranches()->Contains("SecondLjetM")) if (fChain->GetListOfBranches()->Contains("SecondLjetM")) fChain->SetBranchAddress("SecondLjetM", &SecondLjetM, &b_SecondLjetM);
   if (fChain->GetListOfBranches()->Contains("HT_ljets")) if (fChain->GetListOfBranches()->Contains("HT_ljets")) fChain->SetBranchAddress("HT_ljets", &HT_ljets, &b_HT_ljets);
   if (fChain->GetListOfBranches()->Contains("nLjet_m100")) if (fChain->GetListOfBranches()->Contains("nLjet_m100")) fChain->SetBranchAddress("nLjet_m100", &nLjet_m100, &b_nLjet_m100);
   if (fChain->GetListOfBranches()->Contains("nLjet_m50")) if (fChain->GetListOfBranches()->Contains("nLjet_m50")) fChain->SetBranchAddress("nLjet_m50", &nLjet_m50, &b_nLjet_m50);
   if (fChain->GetListOfBranches()->Contains("nJetOutsideLjet")) if (fChain->GetListOfBranches()->Contains("nJetOutsideLjet")) fChain->SetBranchAddress("nJetOutsideLjet", &nJetOutsideLjet, &b_nJetOutsideLjet);
   if (fChain->GetListOfBranches()->Contains("nBjetOutsideLjet")) if (fChain->GetListOfBranches()->Contains("nBjetOutsideLjet")) fChain->SetBranchAddress("nBjetOutsideLjet", &nBjetOutsideLjet, &b_nBjetOutsideLjet);
   if (fChain->GetListOfBranches()->Contains("dRbb_min")) if (fChain->GetListOfBranches()->Contains("dRbb_min")) fChain->SetBranchAddress("dRbb_min", &dRbb_min, &b_dRbb_min);
   if (fChain->GetListOfBranches()->Contains("dRjj_min")) if (fChain->GetListOfBranches()->Contains("dRjj_min")) fChain->SetBranchAddress("dRjj_min", &dRjj_min, &b_dRjj_min);
   if (fChain->GetListOfBranches()->Contains("HiggsbbM")) if (fChain->GetListOfBranches()->Contains("HiggsbbM")) fChain->SetBranchAddress("HiggsbbM", &HiggsbbM, &b_HiggsbbM);
   if (fChain->GetListOfBranches()->Contains("HiggsjjM")) if (fChain->GetListOfBranches()->Contains("HiggsjjM")) fChain->SetBranchAddress("HiggsjjM", &HiggsjjM, &b_HiggsjjM);
   if (fChain->GetListOfBranches()->Contains("ljet_sd23")) if (fChain->GetListOfBranches()->Contains("ljet_sd23")) fChain->SetBranchAddress("ljet_sd23", &ljet_sd23, &b_ljet_sd23);
   if (fChain->GetListOfBranches()->Contains("ljet_tau21")) if (fChain->GetListOfBranches()->Contains("ljet_tau21")) fChain->SetBranchAddress("ljet_tau21", &ljet_tau21, &b_ljet_tau21);
   if (fChain->GetListOfBranches()->Contains("ljet_tau32")) if (fChain->GetListOfBranches()->Contains("ljet_tau32")) fChain->SetBranchAddress("ljet_tau32", &ljet_tau32, &b_ljet_tau32);
   if (fChain->GetListOfBranches()->Contains("ljet_tau21_wta")) if (fChain->GetListOfBranches()->Contains("ljet_tau21_wta")) fChain->SetBranchAddress("ljet_tau21_wta", &ljet_tau21_wta, &b_ljet_tau21_wta);
   if (fChain->GetListOfBranches()->Contains("ljet_tau32_wta")) if (fChain->GetListOfBranches()->Contains("ljet_tau32_wta")) fChain->SetBranchAddress("ljet_tau32_wta", &ljet_tau32_wta, &b_ljet_tau32_wta);
   if (fChain->GetListOfBranches()->Contains("ljet_D2")) if (fChain->GetListOfBranches()->Contains("ljet_D2")) fChain->SetBranchAddress("ljet_D2", &ljet_D2, &b_ljet_D2);
   if (fChain->GetListOfBranches()->Contains("ljet_C2")) if (fChain->GetListOfBranches()->Contains("ljet_C2")) fChain->SetBranchAddress("ljet_C2", &ljet_C2, &b_ljet_C2);
   if (fChain->GetListOfBranches()->Contains("ljet_topTag")) if (fChain->GetListOfBranches()->Contains("ljet_topTag")) fChain->SetBranchAddress("ljet_topTag", &ljet_topTag, &b_ljet_topTag);
   if (fChain->GetListOfBranches()->Contains("ljet_bosonTag")) if (fChain->GetListOfBranches()->Contains("ljet_bosonTag")) fChain->SetBranchAddress("ljet_bosonTag", &ljet_bosonTag, &b_ljet_bosonTag);
   if (fChain->GetListOfBranches()->Contains("ljet_topTag_loose")) if (fChain->GetListOfBranches()->Contains("ljet_topTag_loose")) fChain->SetBranchAddress("ljet_topTag_loose", &ljet_topTag_loose, &b_ljet_topTag_loose);
   if (fChain->GetListOfBranches()->Contains("ljet_bosonTag_loose")) if (fChain->GetListOfBranches()->Contains("ljet_bosonTag_loose")) fChain->SetBranchAddress("ljet_bosonTag_loose", &ljet_bosonTag_loose, &b_ljet_bosonTag_loose);
   if (fChain->GetListOfBranches()->Contains("ljet_topTagN")) if (fChain->GetListOfBranches()->Contains("ljet_topTagN")) fChain->SetBranchAddress("ljet_topTagN", &ljet_topTagN, &b_ljet_topTagN);
   if (fChain->GetListOfBranches()->Contains("ljet_topTagN_loose")) if (fChain->GetListOfBranches()->Contains("ljet_topTagN_loose")) fChain->SetBranchAddress("ljet_topTagN_loose", &ljet_topTagN_loose, &b_ljet_topTagN_loose);
   if (fChain->GetListOfBranches()->Contains("ljet_bosonTagN")) if (fChain->GetListOfBranches()->Contains("ljet_bosonTagN")) fChain->SetBranchAddress("ljet_bosonTagN", &ljet_bosonTagN, &b_ljet_bosonTagN);
   if (fChain->GetListOfBranches()->Contains("ljet_bosonTagN_loose")) if (fChain->GetListOfBranches()->Contains("ljet_bosonTagN_loose")) fChain->SetBranchAddress("ljet_bosonTagN_loose", &ljet_bosonTagN_loose, &b_ljet_bosonTagN_loose);
   if (fChain->GetListOfBranches()->Contains("ljet_truthmatch")) if (fChain->GetListOfBranches()->Contains("ljet_truthmatch")) fChain->SetBranchAddress("ljet_truthmatch", &ljet_truthmatch, &b_ljet_truthmatch);
   if (fChain->GetListOfBranches()->Contains("jet_truthmatch")) if (fChain->GetListOfBranches()->Contains("jet_truthmatch")) fChain->SetBranchAddress("jet_truthmatch", &jet_truthmatch, &b_jet_truthmatch);
   if (fChain->GetListOfBranches()->Contains("truth_jet_pt")) if (fChain->GetListOfBranches()->Contains("truth_jet_pt")) fChain->SetBranchAddress("truth_jet_pt", &truth_jet_pt, &b_truth_jet_pt);
   if (fChain->GetListOfBranches()->Contains("truth_jet_eta")) if (fChain->GetListOfBranches()->Contains("truth_jet_eta")) fChain->SetBranchAddress("truth_jet_eta", &truth_jet_eta, &b_truth_jet_eta);
   if (fChain->GetListOfBranches()->Contains("truth_jet_phi")) if (fChain->GetListOfBranches()->Contains("truth_jet_phi")) fChain->SetBranchAddress("truth_jet_phi", &truth_jet_phi, &b_truth_jet_phi);
   if (fChain->GetListOfBranches()->Contains("truth_jet_m")) if (fChain->GetListOfBranches()->Contains("truth_jet_m")) fChain->SetBranchAddress("truth_jet_m", &truth_jet_m, &b_truth_jet_m);
   if (fChain->GetListOfBranches()->Contains("truth_pt")) if (fChain->GetListOfBranches()->Contains("truth_pt")) fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
   if (fChain->GetListOfBranches()->Contains("truth_eta")) if (fChain->GetListOfBranches()->Contains("truth_eta")) fChain->SetBranchAddress("truth_eta", &truth_eta, &b_truth_eta);
   if (fChain->GetListOfBranches()->Contains("truth_phi")) if (fChain->GetListOfBranches()->Contains("truth_phi")) fChain->SetBranchAddress("truth_phi", &truth_phi, &b_truth_phi);
   if (fChain->GetListOfBranches()->Contains("truth_m")) if (fChain->GetListOfBranches()->Contains("truth_m")) fChain->SetBranchAddress("truth_m", &truth_m, &b_truth_m);
   if (fChain->GetListOfBranches()->Contains("truth_pdgid")) if (fChain->GetListOfBranches()->Contains("truth_pdgid")) fChain->SetBranchAddress("truth_pdgid", &truth_pdgid, &b_truth_pdgid);
   if (fChain->GetListOfBranches()->Contains("truth_status")) if (fChain->GetListOfBranches()->Contains("truth_status")) fChain->SetBranchAddress("truth_status", &truth_status, &b_truth_status);
   if (fChain->GetListOfBranches()->Contains("truth_tthbb_info")) if (fChain->GetListOfBranches()->Contains("truth_tthbb_info")) fChain->SetBranchAddress("truth_tthbb_info", &truth_tthbb_info, &b_truth_tthbb_info);
   if (fChain->GetListOfBranches()->Contains("truth_nHiggs")) if (fChain->GetListOfBranches()->Contains("truth_nHiggs")) fChain->SetBranchAddress("truth_nHiggs", &truth_nHiggs, &b_truth_nHiggs);
   if (fChain->GetListOfBranches()->Contains("truth_nTop")) if (fChain->GetListOfBranches()->Contains("truth_nTop")) fChain->SetBranchAddress("truth_nTop", &truth_nTop, &b_truth_nTop);
   if (fChain->GetListOfBranches()->Contains("truth_nLepTop")) if (fChain->GetListOfBranches()->Contains("truth_nLepTop")) fChain->SetBranchAddress("truth_nLepTop", &truth_nLepTop, &b_truth_nLepTop);
   if (fChain->GetListOfBranches()->Contains("truth_nVectorBoson")) if (fChain->GetListOfBranches()->Contains("truth_nVectorBoson")) fChain->SetBranchAddress("truth_nVectorBoson", &truth_nVectorBoson, &b_truth_nVectorBoson);
   if (fChain->GetListOfBranches()->Contains("truth_ttbar_pt")) if (fChain->GetListOfBranches()->Contains("truth_ttbar_pt")) fChain->SetBranchAddress("truth_ttbar_pt", &truth_ttbar_pt, &b_truth_ttbar_pt);
   if (fChain->GetListOfBranches()->Contains("truth_top_pt")) if (fChain->GetListOfBranches()->Contains("truth_top_pt")) fChain->SetBranchAddress("truth_top_pt", &truth_top_pt, &b_truth_top_pt);
   if (fChain->GetListOfBranches()->Contains("truth_higgs_eta")) if (fChain->GetListOfBranches()->Contains("truth_higgs_eta")) fChain->SetBranchAddress("truth_higgs_eta", &truth_higgs_eta, &b_truth_higgs_eta);
   if (fChain->GetListOfBranches()->Contains("truth_tbar_pt")) if (fChain->GetListOfBranches()->Contains("truth_tbar_pt")) fChain->SetBranchAddress("truth_tbar_pt", &truth_tbar_pt, &b_truth_tbar_pt);
   if (fChain->GetListOfBranches()->Contains("truth_HDecay")) if (fChain->GetListOfBranches()->Contains("truth_HDecay")) fChain->SetBranchAddress("truth_HDecay", &truth_HDecay, &b_truth_HDecay);
   if (fChain->GetListOfBranches()->Contains("truth_top_dilep_filter")) if (fChain->GetListOfBranches()->Contains("truth_top_dilep_filter")) fChain->SetBranchAddress("truth_top_dilep_filter", &truth_top_dilep_filter, &b_truth_top_dilep_filter);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgs_mass")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgs_mass")) fChain->SetBranchAddress("semilepMVAreco_higgs_mass", &semilepMVAreco_higgs_mass, &b_semilepMVAreco_higgs_mass);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_bbhiggs_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_bbhiggs_dR")) fChain->SetBranchAddress("semilepMVAreco_bbhiggs_dR", &semilepMVAreco_bbhiggs_dR, &b_semilepMVAreco_bbhiggs_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsbleptop_mass")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsbleptop_mass")) fChain->SetBranchAddress("semilepMVAreco_higgsbleptop_mass", &semilepMVAreco_higgsbleptop_mass, &b_semilepMVAreco_higgsbleptop_mass);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsleptop_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsleptop_dR")) fChain->SetBranchAddress("semilepMVAreco_higgsleptop_dR", &semilepMVAreco_higgsleptop_dR, &b_semilepMVAreco_higgsleptop_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgslep_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgslep_dR")) fChain->SetBranchAddress("semilepMVAreco_higgslep_dR", &semilepMVAreco_higgslep_dR, &b_semilepMVAreco_higgslep_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_leptophadtop_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_leptophadtop_dR")) fChain->SetBranchAddress("semilepMVAreco_leptophadtop_dR", &semilepMVAreco_leptophadtop_dR, &b_semilepMVAreco_leptophadtop_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsq1hadW_mass")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsq1hadW_mass")) fChain->SetBranchAddress("semilepMVAreco_higgsq1hadW_mass", &semilepMVAreco_higgsq1hadW_mass, &b_semilepMVAreco_higgsq1hadW_mass);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_hadWb1Higgs_mass")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_hadWb1Higgs_mass")) fChain->SetBranchAddress("semilepMVAreco_hadWb1Higgs_mass", &semilepMVAreco_hadWb1Higgs_mass, &b_semilepMVAreco_hadWb1Higgs_mass);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_b1higgsbhadtop_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_b1higgsbhadtop_dR")) fChain->SetBranchAddress("semilepMVAreco_b1higgsbhadtop_dR", &semilepMVAreco_b1higgsbhadtop_dR, &b_semilepMVAreco_b1higgsbhadtop_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsbleptop_withH_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsbleptop_withH_dR")) fChain->SetBranchAddress("semilepMVAreco_higgsbleptop_withH_dR", &semilepMVAreco_higgsbleptop_withH_dR, &b_semilepMVAreco_higgsbleptop_withH_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsttbar_withH_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsttbar_withH_dR")) fChain->SetBranchAddress("semilepMVAreco_higgsttbar_withH_dR", &semilepMVAreco_higgsttbar_withH_dR, &b_semilepMVAreco_higgsttbar_withH_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsbhadtop_withH_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_higgsbhadtop_withH_dR")) fChain->SetBranchAddress("semilepMVAreco_higgsbhadtop_withH_dR", &semilepMVAreco_higgsbhadtop_withH_dR, &b_semilepMVAreco_higgsbhadtop_withH_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_leptophadtop_withH_dR")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_leptophadtop_withH_dR")) fChain->SetBranchAddress("semilepMVAreco_leptophadtop_withH_dR", &semilepMVAreco_leptophadtop_withH_dR, &b_semilepMVAreco_leptophadtop_withH_dR);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_ttH_Ht_withH")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_ttH_Ht_withH")) fChain->SetBranchAddress("semilepMVAreco_ttH_Ht_withH", &semilepMVAreco_ttH_Ht_withH, &b_semilepMVAreco_ttH_Ht_withH);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_output")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_output")) fChain->SetBranchAddress("semilepMVAreco_BDT_output", &semilepMVAreco_BDT_output, &b_semilepMVAreco_BDT_output);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_withH_output")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_withH_output")) fChain->SetBranchAddress("semilepMVAreco_BDT_withH_output", &semilepMVAreco_BDT_withH_output, &b_semilepMVAreco_BDT_withH_output);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_output_truthMatchPattern")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_output_truthMatchPattern")) fChain->SetBranchAddress("semilepMVAreco_BDT_output_truthMatchPattern", &semilepMVAreco_BDT_output_truthMatchPattern, &b_semilepMVAreco_BDT_output_truthMatchPattern);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_withH_output_truthMatchPattern")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_withH_output_truthMatchPattern")) fChain->SetBranchAddress("semilepMVAreco_BDT_withH_output_truthMatchPattern", &semilepMVAreco_BDT_withH_output_truthMatchPattern, &b_semilepMVAreco_BDT_withH_output_truthMatchPattern);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_Ncombinations")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_Ncombinations")) fChain->SetBranchAddress("semilepMVAreco_Ncombinations", &semilepMVAreco_Ncombinations, &b_semilepMVAreco_Ncombinations);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT")) fChain->SetBranchAddress("semilepMVAreco_nuApprox_recoBDT", &semilepMVAreco_nuApprox_recoBDT, &b_semilepMVAreco_nuApprox_recoBDT);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT_withH")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT_withH")) fChain->SetBranchAddress("semilepMVAreco_nuApprox_recoBDT_withH", &semilepMVAreco_nuApprox_recoBDT_withH, &b_semilepMVAreco_nuApprox_recoBDT_withH);
   if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_cand")) if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_cand")) fChain->SetBranchAddress("jet_semilepMVAreco_recoBDT_cand", &jet_semilepMVAreco_recoBDT_cand, &b_jet_semilepMVAreco_recoBDT_cand);
   if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_withH_cand")) if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_withH_cand")) fChain->SetBranchAddress("jet_semilepMVAreco_recoBDT_withH_cand", &jet_semilepMVAreco_recoBDT_withH_cand, &b_jet_semilepMVAreco_recoBDT_withH_cand);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_output_6jsplit")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_output_6jsplit")) fChain->SetBranchAddress("semilepMVAreco_BDT_output_6jsplit", &semilepMVAreco_BDT_output_6jsplit, &b_semilepMVAreco_BDT_output_6jsplit);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_withH_output_6jsplit")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_BDT_withH_output_6jsplit")) fChain->SetBranchAddress("semilepMVAreco_BDT_withH_output_6jsplit", &semilepMVAreco_BDT_withH_output_6jsplit, &b_semilepMVAreco_BDT_withH_output_6jsplit);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT_6jsplit")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT_6jsplit")) fChain->SetBranchAddress("semilepMVAreco_nuApprox_recoBDT_6jsplit", &semilepMVAreco_nuApprox_recoBDT_6jsplit, &b_semilepMVAreco_nuApprox_recoBDT_6jsplit);
   if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT_withH_6jsplit")) if (fChain->GetListOfBranches()->Contains("semilepMVAreco_nuApprox_recoBDT_withH_6jsplit")) fChain->SetBranchAddress("semilepMVAreco_nuApprox_recoBDT_withH_6jsplit", &semilepMVAreco_nuApprox_recoBDT_withH_6jsplit, &b_semilepMVAreco_nuApprox_recoBDT_withH_6jsplit);
   if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_cand_6jsplit")) if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_cand_6jsplit")) fChain->SetBranchAddress("jet_semilepMVAreco_recoBDT_cand_6jsplit", &jet_semilepMVAreco_recoBDT_cand_6jsplit, &b_jet_semilepMVAreco_recoBDT_cand_6jsplit);
   if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_withH_cand_6jsplit")) if (fChain->GetListOfBranches()->Contains("jet_semilepMVAreco_recoBDT_withH_cand_6jsplit")) fChain->SetBranchAddress("jet_semilepMVAreco_recoBDT_withH_cand_6jsplit", &jet_semilepMVAreco_recoBDT_withH_cand_6jsplit, &b_jet_semilepMVAreco_recoBDT_withH_cand_6jsplit);
   if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_basic")) if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_basic")) fChain->SetBranchAddress("ClassifBDTOutput_basic", &ClassifBDTOutput_basic, &b_ClassifBDTOutput_basic);
   if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_withReco_basic")) if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_withReco_basic")) fChain->SetBranchAddress("ClassifBDTOutput_withReco_basic", &ClassifBDTOutput_withReco_basic, &b_ClassifBDTOutput_withReco_basic);
   if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_6jsplit")) if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_6jsplit")) fChain->SetBranchAddress("ClassifBDTOutput_6jsplit", &ClassifBDTOutput_6jsplit, &b_ClassifBDTOutput_6jsplit);
   if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_withReco_6jsplit")) if (fChain->GetListOfBranches()->Contains("ClassifBDTOutput_withReco_6jsplit")) fChain->SetBranchAddress("ClassifBDTOutput_withReco_6jsplit", &ClassifBDTOutput_withReco_6jsplit, &b_ClassifBDTOutput_withReco_6jsplit);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_ckkw15")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_ckkw15")) fChain->SetBranchAddress("Zjets_Systematic_ckkw15", &Zjets_Systematic_ckkw15, &b_Zjets_Systematic_ckkw15);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_ckkw30")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_ckkw30")) fChain->SetBranchAddress("Zjets_Systematic_ckkw30", &Zjets_Systematic_ckkw30, &b_Zjets_Systematic_ckkw30);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_fac025")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_fac025")) fChain->SetBranchAddress("Zjets_Systematic_fac025", &Zjets_Systematic_fac025, &b_Zjets_Systematic_fac025);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_fac4")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_fac4")) fChain->SetBranchAddress("Zjets_Systematic_fac4", &Zjets_Systematic_fac4, &b_Zjets_Systematic_fac4);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_renorm025")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_renorm025")) fChain->SetBranchAddress("Zjets_Systematic_renorm025", &Zjets_Systematic_renorm025, &b_Zjets_Systematic_renorm025);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_renorm4")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_renorm4")) fChain->SetBranchAddress("Zjets_Systematic_renorm4", &Zjets_Systematic_renorm4, &b_Zjets_Systematic_renorm4);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_qsf025")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_qsf025")) fChain->SetBranchAddress("Zjets_Systematic_qsf025", &Zjets_Systematic_qsf025, &b_Zjets_Systematic_qsf025);
   if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_qsf4")) if (fChain->GetListOfBranches()->Contains("Zjets_Systematic_qsf4")) fChain->SetBranchAddress("Zjets_Systematic_qsf4", &Zjets_Systematic_qsf4, &b_Zjets_Systematic_qsf4);
   if (fChain->GetListOfBranches()->Contains("NBFricoNN_ljets")) if (fChain->GetListOfBranches()->Contains("NBFricoNN_ljets")) fChain->SetBranchAddress("NBFricoNN_ljets", &NBFricoNN_ljets, &b_NBFricoNN_ljets);
   if (fChain->GetListOfBranches()->Contains("NBFricoNN_dil")) if (fChain->GetListOfBranches()->Contains("NBFricoNN_dil")) fChain->SetBranchAddress("NBFricoNN_dil", &NBFricoNN_dil, &b_NBFricoNN_dil);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT200_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT200_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT200_Output", &ClassifHPLUS_Semilep_HF_BDT200_Output, &b_ClassifHPLUS_Semilep_HF_BDT200_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT225_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT225_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT225_Output", &ClassifHPLUS_Semilep_HF_BDT225_Output, &b_ClassifHPLUS_Semilep_HF_BDT225_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT250_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT250_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT250_Output", &ClassifHPLUS_Semilep_HF_BDT250_Output, &b_ClassifHPLUS_Semilep_HF_BDT250_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT275_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT275_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT275_Output", &ClassifHPLUS_Semilep_HF_BDT275_Output, &b_ClassifHPLUS_Semilep_HF_BDT275_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT300_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT300_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT300_Output", &ClassifHPLUS_Semilep_HF_BDT300_Output, &b_ClassifHPLUS_Semilep_HF_BDT300_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT350_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT350_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT350_Output", &ClassifHPLUS_Semilep_HF_BDT350_Output, &b_ClassifHPLUS_Semilep_HF_BDT350_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT400_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT400_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT400_Output", &ClassifHPLUS_Semilep_HF_BDT400_Output, &b_ClassifHPLUS_Semilep_HF_BDT400_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT500_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_HF_BDT500_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_HF_BDT500_Output", &ClassifHPLUS_Semilep_HF_BDT500_Output, &b_ClassifHPLUS_Semilep_HF_BDT500_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT1000_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT1000_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT1000_Output", &ClassifHPLUS_Semilep_INC_BDT1000_Output, &b_ClassifHPLUS_Semilep_INC_BDT1000_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT2000_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT2000_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT2000_Output", &ClassifHPLUS_Semilep_INC_BDT2000_Output, &b_ClassifHPLUS_Semilep_INC_BDT2000_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT200_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT200_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT200_Output", &ClassifHPLUS_Semilep_INC_BDT200_Output, &b_ClassifHPLUS_Semilep_INC_BDT200_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT225_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT225_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT225_Output", &ClassifHPLUS_Semilep_INC_BDT225_Output, &b_ClassifHPLUS_Semilep_INC_BDT225_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT250_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT250_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT250_Output", &ClassifHPLUS_Semilep_INC_BDT250_Output, &b_ClassifHPLUS_Semilep_INC_BDT250_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT275_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT275_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT275_Output", &ClassifHPLUS_Semilep_INC_BDT275_Output, &b_ClassifHPLUS_Semilep_INC_BDT275_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT300_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT300_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT300_Output", &ClassifHPLUS_Semilep_INC_BDT300_Output, &b_ClassifHPLUS_Semilep_INC_BDT300_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT350_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT350_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT350_Output", &ClassifHPLUS_Semilep_INC_BDT350_Output, &b_ClassifHPLUS_Semilep_INC_BDT350_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT400_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT400_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT400_Output", &ClassifHPLUS_Semilep_INC_BDT400_Output, &b_ClassifHPLUS_Semilep_INC_BDT400_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT500_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT500_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT500_Output", &ClassifHPLUS_Semilep_INC_BDT500_Output, &b_ClassifHPLUS_Semilep_INC_BDT500_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT600_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT600_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT600_Output", &ClassifHPLUS_Semilep_INC_BDT600_Output, &b_ClassifHPLUS_Semilep_INC_BDT600_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT700_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT700_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT700_Output", &ClassifHPLUS_Semilep_INC_BDT700_Output, &b_ClassifHPLUS_Semilep_INC_BDT700_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT800_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT800_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT800_Output", &ClassifHPLUS_Semilep_INC_BDT800_Output, &b_ClassifHPLUS_Semilep_INC_BDT800_Output);
   if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT900_Output")) if (fChain->GetListOfBranches()->Contains("ClassifHPLUS_Semilep_INC_BDT900_Output")) fChain->SetBranchAddress("ClassifHPLUS_Semilep_INC_BDT900_Output", &ClassifHPLUS_Semilep_INC_BDT900_Output, &b_ClassifHPLUS_Semilep_INC_BDT900_Output);
   if (fChain->GetListOfBranches()->Contains("el_LHMedium")) if (fChain->GetListOfBranches()->Contains("el_LHMedium")) fChain->SetBranchAddress("el_LHMedium", &el_LHMedium, &b_el_LHMedium);
   if (fChain->GetListOfBranches()->Contains("el_LHTight")) if (fChain->GetListOfBranches()->Contains("el_LHTight")) fChain->SetBranchAddress("el_LHTight", &el_LHTight, &b_el_LHTight);
   if (fChain->GetListOfBranches()->Contains("el_isoGradient")) if (fChain->GetListOfBranches()->Contains("el_isoGradient")) fChain->SetBranchAddress("el_isoGradient", &el_isoGradient, &b_el_isoGradient);
   if (fChain->GetListOfBranches()->Contains("el_isoGradientLoose")) if (fChain->GetListOfBranches()->Contains("el_isoGradientLoose")) fChain->SetBranchAddress("el_isoGradientLoose", &el_isoGradientLoose, &b_el_isoGradientLoose);
   if (fChain->GetListOfBranches()->Contains("el_isoTight")) if (fChain->GetListOfBranches()->Contains("el_isoTight")) fChain->SetBranchAddress("el_isoTight", &el_isoTight, &b_el_isoTight);
   if (fChain->GetListOfBranches()->Contains("el_isoLoose")) if (fChain->GetListOfBranches()->Contains("el_isoLoose")) fChain->SetBranchAddress("el_isoLoose", &el_isoLoose, &b_el_isoLoose);
   if (fChain->GetListOfBranches()->Contains("el_isoLooseTrackOnly")) if (fChain->GetListOfBranches()->Contains("el_isoLooseTrackOnly")) fChain->SetBranchAddress("el_isoLooseTrackOnly", &el_isoLooseTrackOnly, &b_el_isoLooseTrackOnly);
   if (fChain->GetListOfBranches()->Contains("el_isoFixedCutTight")) if (fChain->GetListOfBranches()->Contains("el_isoFixedCutTight")) fChain->SetBranchAddress("el_isoFixedCutTight", &el_isoFixedCutTight, &b_el_isoFixedCutTight);
   if (fChain->GetListOfBranches()->Contains("el_isoFixedCutTightTrackOnly")) if (fChain->GetListOfBranches()->Contains("el_isoFixedCutTightTrackOnly")) fChain->SetBranchAddress("el_isoFixedCutTightTrackOnly", &el_isoFixedCutTightTrackOnly, &b_el_isoFixedCutTightTrackOnly);
   if (fChain->GetListOfBranches()->Contains("el_isoFixedCutLoose")) if (fChain->GetListOfBranches()->Contains("el_isoFixedCutLoose")) fChain->SetBranchAddress("el_isoFixedCutLoose", &el_isoFixedCutLoose, &b_el_isoFixedCutLoose);
   if (fChain->GetListOfBranches()->Contains("mu_Tight")) if (fChain->GetListOfBranches()->Contains("mu_Tight")) fChain->SetBranchAddress("mu_Tight", &mu_Tight, &b_mu_Tight);
   if (fChain->GetListOfBranches()->Contains("mu_Medium")) if (fChain->GetListOfBranches()->Contains("mu_Medium")) fChain->SetBranchAddress("mu_Medium", &mu_Medium, &b_mu_Medium);
   if (fChain->GetListOfBranches()->Contains("mu_isoGradient")) if (fChain->GetListOfBranches()->Contains("mu_isoGradient")) fChain->SetBranchAddress("mu_isoGradient", &mu_isoGradient, &b_mu_isoGradient);
   if (fChain->GetListOfBranches()->Contains("mu_isoGradientLoose")) if (fChain->GetListOfBranches()->Contains("mu_isoGradientLoose")) fChain->SetBranchAddress("mu_isoGradientLoose", &mu_isoGradientLoose, &b_mu_isoGradientLoose);
   if (fChain->GetListOfBranches()->Contains("mu_isoTight")) if (fChain->GetListOfBranches()->Contains("mu_isoTight")) fChain->SetBranchAddress("mu_isoTight", &mu_isoTight, &b_mu_isoTight);
   if (fChain->GetListOfBranches()->Contains("mu_isoLoose")) if (fChain->GetListOfBranches()->Contains("mu_isoLoose")) fChain->SetBranchAddress("mu_isoLoose", &mu_isoLoose, &b_mu_isoLoose);
   if (fChain->GetListOfBranches()->Contains("mu_isoLooseTrackOnly")) if (fChain->GetListOfBranches()->Contains("mu_isoLooseTrackOnly")) fChain->SetBranchAddress("mu_isoLooseTrackOnly", &mu_isoLooseTrackOnly, &b_mu_isoLooseTrackOnly);
   if (fChain->GetListOfBranches()->Contains("mu_isoFixedCutTightTrackOnly")) if (fChain->GetListOfBranches()->Contains("mu_isoFixedCutTightTrackOnly")) fChain->SetBranchAddress("mu_isoFixedCutTightTrackOnly", &mu_isoFixedCutTightTrackOnly, &b_mu_isoFixedCutTightTrackOnly);
   if (fChain->GetListOfBranches()->Contains("mu_isoFixedCutLoose")) if (fChain->GetListOfBranches()->Contains("mu_isoFixedCutLoose")) fChain->SetBranchAddress("mu_isoFixedCutLoose", &mu_isoFixedCutLoose, &b_mu_isoFixedCutLoose);
   if (fChain->GetListOfBranches()->Contains("HF_Classification")) if (fChain->GetListOfBranches()->Contains("HF_Classification")) fChain->SetBranchAddress("HF_Classification", &HF_Classification, &b_HF_Classification);
   if (fChain->GetListOfBranches()->Contains("HF_SimpleClassification")) if (fChain->GetListOfBranches()->Contains("HF_SimpleClassification")) fChain->SetBranchAddress("HF_SimpleClassification", &HF_SimpleClassification, &b_HF_SimpleClassification);
   if (fChain->GetListOfBranches()->Contains("q1_pt")) if (fChain->GetListOfBranches()->Contains("q1_pt")) fChain->SetBranchAddress("q1_pt", &q1_pt, &b_q1_pt);
   if (fChain->GetListOfBranches()->Contains("q1_eta")) if (fChain->GetListOfBranches()->Contains("q1_eta")) fChain->SetBranchAddress("q1_eta", &q1_eta, &b_q1_eta);
   if (fChain->GetListOfBranches()->Contains("q1_phi")) if (fChain->GetListOfBranches()->Contains("q1_phi")) fChain->SetBranchAddress("q1_phi", &q1_phi, &b_q1_phi);
   if (fChain->GetListOfBranches()->Contains("q1_m")) if (fChain->GetListOfBranches()->Contains("q1_m")) fChain->SetBranchAddress("q1_m", &q1_m, &b_q1_m);
   if (fChain->GetListOfBranches()->Contains("q2_pt")) if (fChain->GetListOfBranches()->Contains("q2_pt")) fChain->SetBranchAddress("q2_pt", &q2_pt, &b_q2_pt);
   if (fChain->GetListOfBranches()->Contains("q2_eta")) if (fChain->GetListOfBranches()->Contains("q2_eta")) fChain->SetBranchAddress("q2_eta", &q2_eta, &b_q2_eta);
   if (fChain->GetListOfBranches()->Contains("q2_phi")) if (fChain->GetListOfBranches()->Contains("q2_phi")) fChain->SetBranchAddress("q2_phi", &q2_phi, &b_q2_phi);
   if (fChain->GetListOfBranches()->Contains("q2_m")) if (fChain->GetListOfBranches()->Contains("q2_m")) fChain->SetBranchAddress("q2_m", &q2_m, &b_q2_m);
   if (fChain->GetListOfBranches()->Contains("qq_pt")) if (fChain->GetListOfBranches()->Contains("qq_pt")) fChain->SetBranchAddress("qq_pt", &qq_pt, &b_qq_pt);
   if (fChain->GetListOfBranches()->Contains("qq_ht")) if (fChain->GetListOfBranches()->Contains("qq_ht")) fChain->SetBranchAddress("qq_ht", &qq_ht, &b_qq_ht);
   if (fChain->GetListOfBranches()->Contains("qq_dr")) if (fChain->GetListOfBranches()->Contains("qq_dr")) fChain->SetBranchAddress("qq_dr", &qq_dr, &b_qq_dr);
   if (fChain->GetListOfBranches()->Contains("qq_m")) if (fChain->GetListOfBranches()->Contains("qq_m")) fChain->SetBranchAddress("qq_m", &qq_m, &b_qq_m);
   if (fChain->GetListOfBranches()->Contains("nTruthJets15")) if (fChain->GetListOfBranches()->Contains("nTruthJets15")) fChain->SetBranchAddress("nTruthJets15", &nTruthJets15, &b_nTruthJets15);
   if (fChain->GetListOfBranches()->Contains("nTruthJets20")) if (fChain->GetListOfBranches()->Contains("nTruthJets20")) fChain->SetBranchAddress("nTruthJets20", &nTruthJets20, &b_nTruthJets20);
   if (fChain->GetListOfBranches()->Contains("nTruthJets25")) if (fChain->GetListOfBranches()->Contains("nTruthJets25")) fChain->SetBranchAddress("nTruthJets25", &nTruthJets25, &b_nTruthJets25);
   if (fChain->GetListOfBranches()->Contains("nTruthJets25W")) if (fChain->GetListOfBranches()->Contains("nTruthJets25W")) fChain->SetBranchAddress("nTruthJets25W", &nTruthJets25W, &b_nTruthJets25W);
   if (fChain->GetListOfBranches()->Contains("nTruthJets50")) if (fChain->GetListOfBranches()->Contains("nTruthJets50")) fChain->SetBranchAddress("nTruthJets50", &nTruthJets50, &b_nTruthJets50);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_Norm")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_Norm")) fChain->SetBranchAddress("weight_ttbb_Norm", &weight_ttbb_Norm, &b_weight_ttbb_Norm);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_CSS_KIN")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_CSS_KIN")) fChain->SetBranchAddress("weight_ttbb_CSS_KIN", &weight_ttbb_CSS_KIN, &b_weight_ttbb_CSS_KIN);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_NNPDF")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_NNPDF")) fChain->SetBranchAddress("weight_ttbb_NNPDF", &weight_ttbb_NNPDF, &b_weight_ttbb_NNPDF);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_MSTW")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_MSTW")) fChain->SetBranchAddress("weight_ttbb_MSTW", &weight_ttbb_MSTW, &b_weight_ttbb_MSTW);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_Q_CMMPS")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_Q_CMMPS")) fChain->SetBranchAddress("weight_ttbb_Q_CMMPS", &weight_ttbb_Q_CMMPS, &b_weight_ttbb_Q_CMMPS);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_glosoft")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_glosoft")) fChain->SetBranchAddress("weight_ttbb_glosoft", &weight_ttbb_glosoft, &b_weight_ttbb_glosoft);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_defaultX05")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_defaultX05")) fChain->SetBranchAddress("weight_ttbb_defaultX05", &weight_ttbb_defaultX05, &b_weight_ttbb_defaultX05);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_defaultX2")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_defaultX2")) fChain->SetBranchAddress("weight_ttbb_defaultX2", &weight_ttbb_defaultX2, &b_weight_ttbb_defaultX2);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_MPIup")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_MPIup")) fChain->SetBranchAddress("weight_ttbb_MPIup", &weight_ttbb_MPIup, &b_weight_ttbb_MPIup);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_MPIdown")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_MPIdown")) fChain->SetBranchAddress("weight_ttbb_MPIdown", &weight_ttbb_MPIdown, &b_weight_ttbb_MPIdown);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_MPIfactor")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_MPIfactor")) fChain->SetBranchAddress("weight_ttbb_MPIfactor", &weight_ttbb_MPIfactor, &b_weight_ttbb_MPIfactor);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_aMcAtNloHpp")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_aMcAtNloHpp")) fChain->SetBranchAddress("weight_ttbb_aMcAtNloHpp", &weight_ttbb_aMcAtNloHpp, &b_weight_ttbb_aMcAtNloHpp);
   if (fChain->GetListOfBranches()->Contains("weight_ttbb_aMcAtNloPy8")) if (fChain->GetListOfBranches()->Contains("weight_ttbb_aMcAtNloPy8")) fChain->SetBranchAddress("weight_ttbb_aMcAtNloPy8", &weight_ttbb_aMcAtNloPy8, &b_weight_ttbb_aMcAtNloPy8);
   if (fChain->GetListOfBranches()->Contains("weight_ttbar_FracRw")) if (fChain->GetListOfBranches()->Contains("weight_ttbar_FracRw")) fChain->SetBranchAddress("weight_ttbar_FracRw", &weight_ttbar_FracRw, &b_weight_ttbar_FracRw);
   if (fChain->GetListOfBranches()->Contains("ttHF_mva_discriminant")) if (fChain->GetListOfBranches()->Contains("ttHF_mva_discriminant")) fChain->SetBranchAddress("ttHF_mva_discriminant", &ttHF_mva_discriminant, &b_ttHF_mva_discriminant);
   Notify();
}

Bool_t FlatTreeReader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void FlatTreeReader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t FlatTreeReader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef FlatTreeReader_cxx
