#ifndef EventExtras_H_
#define EventExtras_H_

#include "PlotFactoryBoosted/FlatTreeReader.h"
#include "PlotFactoryBoosted/ConfigClass.h"
#include "PlotFactoryBoosted/Enums.h"

#include "TLorentzVector.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <vector>

class EventExtras{

 public:

  EventExtras();
  EventExtras(int EventIndex,FlatTreeReader *FlatTree, string Analysis, std::shared_ptr<ConfigClass> Config);
  virtual ~EventExtras();

  void SetEventIndex(int EventIndex){m_eventIndex=EventIndex;};

  void SetNLargeJetsGood(int NLargeJets){m_nLargeJets=NLargeJets;};
  void SetNTopTags(int TopTags){m_nTopTags=TopTags;};

  void SetNJetsAll(int NJetsAll){m_nJets_all=NJetsAll;};
  void SetNBTagsAll(int NBTagsAll){m_nBTags_all=NBTagsAll;};

  void SetEntryWeight(float EntryWeight){m_entryWeight=EntryWeight;};
  void SetEntryLumiWeight(float EntryLumiWeight){m_entryLumiWeight=EntryLumiWeight;};
  void SetPassTTBarXSec(bool passselection){m_passTTBarXSec = passselection;};

  int GetEventIndex(){ return m_eventIndex;};

  int GetNLargeJets(){ return m_nLargeJets;};
  int GetNTopTags(){ return m_nTopTags;};
  int GetNHiggsTags(){ return m_nHiggsTags;};

  int GetNJetsAll(){ return m_nJets_all;};
  int GetNBTagsAll(){ return m_nBTags_all;};
  int GetNAntiTagsAll(){ return m_nAntiTags_all;};

  int GetNJetsAdditional(){ return m_nJets_additional;};
  int GetNBTagsAdditional(){ return m_nTags_additional;};
  int GetNAntiTagsAdditional(){ return m_nAntiTags_additional;};

  int GetNJetsOutAllT(){ return m_nJets_outallT;};
  int GetNBTagsOutAllT(){ return m_nTags_outallT;};
  int GetNAntiTagsOutAllT(){ return m_nAntiTags_outallT;};

  int GetNJetsInTop(){ return m_nJets_inTop;};
  int GetNBTagsInTop(){ return m_nTags_inTop;};
  int GetNAntiTagsInTop(){ return m_nAntiTags_inTop;};

  float GetDR_l_jet(){ return m_dR_l_jet; };
  float GetEntryWeight(){ return m_entryWeight;};
  float GetEntryLumiWeight(){ return m_entryLumiWeight;};
  float GetMissingEtCut(){return m_missingEtCut;};
  float GetTriangularCut(){return m_triangularCut;};
  float GetLeptonPtCut(){return leadleppt;};
  float GetBTagCut(){return btagdisc;};
  std::vector<TLorentzVector> GetGoodLJets(){ return m_GoodLJets;};
  std::vector<int> GetGoodLJetIndex(){ return m_GoodLJetIndex;};
  std::vector<TLorentzVector> GetTopTaggedJets(){ return m_TopTagJets;};
  std::vector<int> GetTopTagJetIndex(){ return m_TopTagJetIndex;};
  std::vector<TLorentzVector> GetHiggsTaggedJets(){ return m_HiggsTagJets;};
  std::vector<int> GetHiggsTagJetIndex(){ return m_HiggsTagJetIndex;};

  std::vector<TLorentzVector> GetGoodSmallJets(){ return m_GoodSmallJets;};
  std::vector<TLorentzVector> GetGoodSmallTags(){ return m_GoodSmallBTags;};
  std::vector<TLorentzVector> GetGoodSmallAntiTags(){ return m_GoodSmallAntiTags;};

  std::vector<TLorentzVector> GetAdditionalSmallJets(){ return m_AdditionalJets;};
  std::vector<TLorentzVector> GetAdditionalSmallTags(){ return m_AdditionalBTags;};
  std::vector<TLorentzVector> GetAdditionalSmallAntiTags(){ return m_AdditionalAntiTags;};

  std::vector<TLorentzVector> GetAdditionalSmallJetsOutAllT(){ return m_OutAllTJets;};
  std::vector<TLorentzVector> GetAdditionalSmallTagsOutAllT(){ return m_OutAllTBTags;};
  std::vector<TLorentzVector> GetAdditionalSmallAntiTagsOutAllT(){ return m_OutAllTAntiTags;};

  int GetNMu() {return m_mu_n;};
  int GetNEl() {return m_el_n;};
  bool IsMu() {return m_isMu;};
  bool IsEl() {return m_isEl;};
  bool GetPassTTBarXSec() {return m_passTTBarXSec;};
  TLorentzVector GetGoodLepton() { return m_GoodLepton;} ;
  TLorentzVector GetNeutrino() { return m_nu;} ;
  TLorentzVector GetWBoson()   { return m_Wboson;} ;
  TLorentzVector GetLepTop()   { return m_leptop;} ;
  TLorentzVector GetHadTop()   { return m_hadtop;} ;
  TLorentzVector GetTTBar()    { return m_ttbar;} ;

  int GetGoodLeptonCharge(){return m_GoodLeptonCharge;};
  char GetGoodLeptonIsTight(){return m_LeptonIsTight;};

 private:

  char m_LeptonIsTight;
  int m_GoodLeptonCharge;

  int m_eventIndex;

  int m_nLargeJets;
  int m_nTopTags;
  int m_nHiggsTags;

  int m_nJets_all;
  int m_nBTags_all;
  int m_nAntiTags_all;

  int m_nJets_additional;
  int m_nTags_additional;
  int m_nAntiTags_additional;

  int m_nJets_outallT;
  int m_nTags_outallT;
  int m_nAntiTags_outallT;

  int m_nJets_inTop;
  int m_nTags_inTop;
  int m_nAntiTags_inTop;

  int m_nJets_inHiggs;
  int m_nTags_inHiggs;
  int m_nAntiTags_inHiggs;
  
  int m_mu_n;
  int m_el_n;
  bool m_isMu;
  bool m_isEl;
  bool m_passTTBarXSec;
  float m_entryWeight;
  float m_entryLumiWeight;
  float m_dR_l_jet;

  FlatTreeReader *m_flattree;

  std::vector<TLorentzVector> m_GoodLJets;
  std::vector<int> m_GoodLJetIndex;
  std::vector<TLorentzVector> m_TopTagJets;
  std::vector<int> m_TopTagJetIndex;
  std::vector<TLorentzVector> m_HiggsTagJets;
  std::vector<int> m_HiggsTagJetIndex;

  std::vector<TLorentzVector> m_GoodSmallJets;
  std::vector<TLorentzVector> m_GoodSmallBTags;
  std::vector<TLorentzVector> m_GoodSmallAntiTags;

  std::vector<TLorentzVector> m_AdditionalJets;
  std::vector<TLorentzVector> m_AdditionalBTags;
  std::vector<TLorentzVector> m_AdditionalAntiTags;

  std::vector<TLorentzVector> m_OutAllTJets;
  std::vector<TLorentzVector> m_OutAllTBTags;
  std::vector<TLorentzVector> m_OutAllTAntiTags;

  std::vector<TLorentzVector> m_goodLeptons;

  TLorentzVector m_GoodLepton;
  TLorentzVector m_nu;
  TLorentzVector m_Wboson;
  TLorentzVector m_leptop;
  TLorentzVector m_hadtop;
  TLorentzVector m_ttbar;

  float min_largejetpt;
  float min_Toppt;
  float min_Higgspt;
  float matchingdR;
  float max_largejetpt = 1500.0;
  float largejetmass   =   50.0;
  float largejeteta    =    2.0;
  float btagdisc;
  float btagdisc_loose;
  float leadleppt;
  float m_missingEtCut;
  float m_triangularCut;
  int loose_b_tagging;
  BINS nbInTop;
  BINS nNonbInTop;
  BINS nbInHiggs;

};

#endif
