#ifndef ControlPlots_H_
#define ControlPlots_H_

#include "PlotFactoryBoosted/ConfigClass.h"

#include "TH1D.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2D.h"
#include "TColor.h"
#include <map>
#include <vector>
#include <string>

class ControlPlots{

 public:

  ControlPlots(std::shared_ptr<ConfigClass>);
  virtual ~ControlPlots();

  void AddTTbarHFile(std::string file) {fTTbarHList.push_back(file);};
  void AddTTbarZFile(std::string file) {fTTbarZList.push_back(file);};
  void AddTTbarWFile(std::string file) {fTTbarWList.push_back(file);};
  void AddOtherTTbarVFile(std::string file) {fOtherTTbarVList.push_back(file);};
  void AddTTbbFile(std::string file) {fTTBBList.push_back(file);};
  void AddTTccFile(std::string file) {fTTCCList.push_back(file);};
  void AddTTLightFile(std::string file) {fTTLightList.push_back(file);};
  void AddTTbarFile(std::string file)    {fTTbarList.push_back(file);};
  void AddTTbarFile2(std::string file)   {fTTbarList2.push_back(file);};
  void AddWjetsFile(std::string file)    {fWjetsList.push_back(file);};
  void AddZjetsFile(std::string file)    {fZjetsList.push_back(file);};
  void AddDibosonFile(std::string file)  {fDibosonList.push_back(file);};
  void AddSingleTopFile(std::string file){fSingleTopList.push_back(file);};
  void AddDataFile(std::string file)     {fDataList.push_back(file);};
  void AddQCDFile(std::string file)      {fQCDList.push_back(file);};
  void createHTML(std::string); 

  void AddSystematicFile(std::string file){fSystList.push_back(file);};

  void PlotSeparation(TH1D, TH1D, std::string, std::string);
  void PlotErrorBand(bool plot){fPlotErrorBand = plot;}; 

  //void SetMETCut(int met){fMETCut = met;};
  //void SetHTCut(int ht){fHTCut = ht;};
  void SetChargeFlag(std::string flag){fChargeFlag = flag;};
  void SetHFFlag(bool flag){fUseHF = flag;};
  void SetLeptonType(std::string LeptonType){fLeptonType = LeptonType;};
  void SetJetBinLabel(std::string);
  void SetVetoBtagLabel(std::string);
  void SetVetoJetBinLabel(std::string);
  void SetBtagLabel(std::string);
  void SetChannelLabel(std::string);
  void SetLargeJetLabel(std::string);
  void SetTopTagLabel(std::string);
  void SetHiggsTagLabel(std::string);
  void SetLumiLabel(std::string);
  void SetLumi(float lumi){fLumi = lumi;};
  void SetOutputFile(std::string output){fOutputFile = output;};
  void SetOutputFolder(std::string output){fOutputFolder = output;};
  void SetSummaryFile(std::string file){fSummaryFile = file;};
  void SetSystematicType(std::string type){fSystType = type;};
  void WriteToSummaryFile(bool flag){DoWriteToSummaryFile = flag;};
  void WriteTree();

  void SetCombinedFlag(bool flag){fCombined = flag;};

  // parameters: histo_name, x_axis_label, OutputFolder+"/"+histo_name+"_"+LeptonType
  void MakePlots(std::string, std::string, std::string, bool, bool, bool,int);
  void DrawMatrix(std::string, std::string, std::string, bool);
  void DrawFinalTruthPlot(std::string, std::string, std::string, bool);
  void DrawFinalPlot(std::string, std::string, std::string, bool, bool, TH1D);
  void PlotOverlap(TH1D, TH1D, std::string, std::string);
  void DeleteFiles();

  void WriteEventYield(bool);

  TH1D *AddHistos(std::string, std::vector<std::string>, std::string SystName="nominal", bool DoReplace=false);
  TH2D *AddHistos2D(std::string, std::vector<std::string>);

  std::string GetXAxisLabel(std::string);
  std::string GetYAxisLabel(std::string);

  bool pairCompare(const std::pair<std::string, float>& firstElem, const std::pair<std::string, float>& secondElem);

 protected:

  std::vector<std::pair<float, std::string> > fSeparationList;

  std::shared_ptr<ConfigClass> gConfig;

  std::map<std::string,std::string> _htmls;

  std::vector<std::string> fTTbarList, fTTbarList2, fWjetsList, fZjetsList, fSingleTopList, fDibosonList, fQCDList, fDataList, fTTbarHList, fTTbarHFList, fTTbarZList, fTTbarWList, fOtherTTbarVList, fSystList;
  std::vector<TFile*>       fWjetsFileList, fZjetsFileList, fSingleTopFileList, fDibosonFileList, fQCDFileList, fDataFileList;
  std::vector<TFile*>       fTTbarFileList, fTTbarFileList2, fTTbarHFileList, fTTbarZFileList, fTTbarWFileList, fOtherTTbarVFileList, fTTbarHFFileList;

  TH1D fTTbarHist, fTTLightHist, fTTBBHist, fTTCCHist, fTTbarHist2, fWjetsHist, fZjetsHist, fSingleTopHist, fDibosonHist, fDataHist, fQCDHist, fTTbarHHist, fTTbarHFHist, fTTbarZHist, fTTbarWHist, fOtherTTbarVHist;
  TH1D *fTTbarSum, *fTTLightSum, *fTTBBSum, *fTTCCSum, *fTTbarSum2,  *fWjetsSum,  *fZjetsSum,  *fSingleTopSum,  *fDibosonSum,  *fDataSum,  *fQCDSum, *fTTbarHSum, *fTTbarHFSum, *fTTbarWSum, *fTTbarZSum, *fOtherTTbarVSum;
  TH1D *fTTbarSumClone, *fTTLightSumClone, *fTTBBSumClone, *fTTCCSumClone, *fTTbarSumClone2,  *fTTbarWSumClone,  *fTTbarZSumClone,  *fOtherTTbarVSumClone,  *fWjetsSumClone,  *fZjetsSumClone,  *fSingleTopSumClone,  *fDibosonSumClone,  *fDataSumClone,  *fQCDSumClone;
  TH2D *fTTbarSum2D, *fTTbarSum22D,  *fWjetsSum2D,  *fZjetsSum2D,  *fSingleTopSum2D,  *fDibosonSum2D,  *fDataSum2D,  *fQCDSum2D, *fTTbarHSum2D, *fTTbarHFSum2D, *fTTbarWSum2D, *fTTbarZSum2D, *fOtherTTbarVSum2D, *fTTLightSum2D, *fTTBBSum2D, *fTTCCSum2D;


  TH1D fTTbarHelp, fTTbarHelp2, fWjetsHelp, fZjetsHelp, fSingleTopHelp, fDibosonHelp, fDataHelp, fQCDHelp, fTTbarHHelp, fTTbarHFHelp, fTTbarWHelp, fTTbarZHelp, fOtherTTbarVHelp;

  TH1D *fWjetsOut, *fQCDOut;

  TH1D *fTotalSum, *fTotalSum2, *fTotalSumTTH, *fRemBkg;

  std::vector<std::string> fTTBBList, fTTCCList, fTTLightList;

  std::vector<TH1D> fAllProc;
  std::vector<TH2D> fAllProc2D;
  std::vector<std::string> fAllProcNames;

  float fQCDScale; // take 2015 QCD estimate and scale to lumi used

  bool fCombined;
  bool fUseHF;

  std::string fLeptonType;
  std::string fLargeJetLabel;
  std::string fChannelLabel;
  std::string fTopTagLabel;
  std::string fHiggsTagLabel;
  std::string fJetBinLabel;
  std::string fBtagLabel;
  std::string fJetBin;
  std::string fBTagBin;
  std::string fVetoJetBin;
  std::string fVetoBTagBin;
  std::string fLumiLabel;
  std::string fOutputFile;
  std::string fOutputFolder;
  std::string fHTMLLabel;
  std::string fSystType;
  std::string fSummaryFile;
  std::string fChargeFlag;
  std::string fAnalysisType;
  std::string fDataLabel;

  /* int fMETCut; */
  /* int fHTCut; */

  bool DoWriteToSummaryFile;
  bool fPlotErrorBand;
   
  TFile *histo_file;

  float fWjetsNorm,     fQCDNorm,     fRemBkgNorm,     fZjetsNorm, fTTbarNorm, fTTbarNorm2, fTTbarHNorm, fTTbarHFNorm, fTTbarWNorm, fTTbarZNorm, fOtherTTbarVNorm;
  float fSingleTopNorm, fDibosonNorm, fPseudoDataNorm, fDataNorm,  fLumi;

  TTree *fOutputTree;



};

#endif
