/**
 * Andrea Knue (aknue@cern.ch):
 * 
 * 
 */

#include <iostream>

#include "PlotFactoryBoosted/ControlPlots.h"
#include "PlotFactoryBoosted/ConfigClass.h"

#include "TSystem.h"
#include "TRandom3.h"
#include "TH1F.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <set>
#include <iomanip>
#include <string>

using namespace std;

int main(int argc, char *argv[]){

         if(argc != 16){

	   std::cout << "\t" << std::endl;
	   std::cout << "ERROR: Number of input variables is wrong!!!   ===> " << argc << std::endl;
	   std::cout << "\t" << std::endl;
	   
	   return 1;
	   
	 }
	 else{
	   
	 }
	 
	 std::string InputFolder    = argv[1];
	 std::string OutputFolder   = argv[2];
	 std::string JetBin         = argv[3];
	 std::string BTagBin        = argv[4];
	 std::string LargeRBin      = argv[5];
	 std::string TopTagBin      = argv[6];
	 std::string HiggsTagBin    = argv[7];
	 std::string VetoJetBin     = argv[8];
	 std::string VetoBTagBin    = argv[9];
	 std::string VetoLargeRBin  = argv[10];
	 std::string VetoTopTagBin  = argv[11];
	 std::string VetoHiggsTagBin  = argv[12];
	 std::string ConfigFileName = argv[13];
	 int DoErrorBands           = TString(argv[14]).Atoi();
	 std::string DataLumi       = argv[15];
	 
	 std::shared_ptr<ConfigClass> fConfig(new ConfigClass());
	 
	 fConfig -> readSettingsFromConfig(ConfigFileName.c_str());
	 fConfig -> SetJetBin(TranslateBinToEnum(JetBin));
	 fConfig -> SetNumberOfBTags(TranslateBinToEnum(BTagBin));
	 fConfig -> SetLargeJetBin(TranslateBinToEnum(LargeRBin));
	 fConfig -> SetTopTagBin(TranslateBinToEnum(TopTagBin));
	 fConfig -> SetHiggsTagBin(TranslateBinToEnum(HiggsTagBin));
	 fConfig -> SetVetoJetBin(TranslateBinToEnum(VetoJetBin));
	 fConfig -> SetVetoNumberOfBTags(TranslateBinToEnum(VetoBTagBin));
	 fConfig -> SetVetoLargeJetBin(TranslateBinToEnum(VetoLargeRBin));
	 fConfig -> SetVetoTopTagBin(TranslateBinToEnum(VetoTopTagBin));
	 fConfig -> SetVetoHiggsTagBin(TranslateBinToEnum(VetoHiggsTagBin));
	 
	 std::string Channel      = fConfig -> GetChannel();
	 std::string AnalysisType = fConfig -> GetAnalysisType();
	 bool        UseHF        = fConfig -> GetUseHFSplitting();
	 float       Lumi         = fConfig -> GetLumi();
	 std::string LeptonType   = fConfig -> GetChannel();
	 
	 ControlPlots *fNewCP = new ControlPlots(fConfig);
	 
	 fNewCP -> PlotErrorBand(DoErrorBands);
	 
	 if(!UseHF)
	   fNewCP -> AddTTbarFile(InputFolder+"/Merged_ttbar.root");
	 else{
	     
	   fNewCP -> AddTTLightFile(InputFolder+"/Merged_ttlight_PP8.root");
	   fNewCP -> AddTTbbFile(InputFolder+"/Merged_ttbb_PP8.root");
	   fNewCP -> AddTTccFile(InputFolder+"/Merged_ttcc_PP8.root");
	 }

	 fNewCP -> AddTTbarWFile(InputFolder+"/Merged_ttW.root");
	 fNewCP -> AddTTbarZFile(InputFolder+"/Merged_ttZ.root");
	 fNewCP -> AddOtherTTbarVFile(InputFolder+"/Merged_OtherttV.root");
	 fNewCP -> AddWjetsFile(InputFolder+"/Merged_wjets.root");
	 fNewCP -> AddZjetsFile(InputFolder+"/Merged_zjets.root");
	 fNewCP -> AddDibosonFile(InputFolder+"/Merged_diboson.root");
	 fNewCP -> AddSingleTopFile(InputFolder+"/Merged_singleTop.root");

	 

	 fNewCP -> AddQCDFile(InputFolder+"/Merged_qcd_2015.root");

	 if(DataLumi == "2015"){
	   fNewCP -> AddDataFile(InputFolder+"/Merged_data_2015.root");
	 }
	 else if(DataLumi == "2016"){
	   fNewCP -> AddDataFile(InputFolder+"/Merged_data_2016.root");
	 }
	 else if(DataLumi == "2015+2016"){
	   fNewCP -> AddDataFile(InputFolder+"/Merged_data_2015.root");
	   fNewCP -> AddDataFile(InputFolder+"/Merged_data_2016.root");
	 }

	 if(DoErrorBands){

	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_ttbar.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_ttZ.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_ttW.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_OtherttV.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_wjets.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_zjets.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_diboson.root");
	    fNewCP -> AddSystematicFile(InputFolder+"/Merged_singleTop.root");

	 }


	 //if(AnalysisType == "ttH")
	 //fNewCP -> AddTTbarHFile(InputFolder+"/Merged_ttH.root");
	 
	 fNewCP -> AddTTbarHFile(InputFolder+"/Merged_ttH_Py8.root");
	 	 
	 fNewCP -> SetJetBinLabel(JetBin);
	 fNewCP -> SetVetoJetBinLabel(VetoJetBin);
         fNewCP -> SetChannelLabel(LargeRBin);
	 fNewCP -> SetBtagLabel(BTagBin);
	 fNewCP -> SetVetoBtagLabel(VetoBTagBin);
	 fNewCP -> SetLargeJetLabel(LargeRBin);
	 fNewCP -> SetTopTagLabel(TopTagBin);
	 fNewCP -> SetHiggsTagLabel(HiggsTagBin);
	 fNewCP -> SetOutputFolder(OutputFolder);

	 fNewCP -> SetCombinedFlag(false);

	 string pathToConf         = gSystem->Getenv("ROOTCOREBIN");
	 std::string InputFileName = pathToConf+"/data/PlotFactoryBoosted/"+fConfig->GetConfigFileVariables();
	 std::string InputFileDir  = pathToConf+"/data/PlotFactoryBoosted/";

	 ifstream InputListFile;
	 InputListFile.open(InputFileName.c_str());

	 int Counter = 0;

	 string fileline;
	 string line;
	 while (InputListFile.good()) {

	   getline(InputListFile, fileline);
	   std::istringstream input_fileline(fileline);

	   string setname;
	   string setindex;
	   input_fileline >> setname;
	   input_fileline >> setindex;

	   int   setindex_int = TString(setindex).Atoi();

	   if(setindex_int == 0 && setname!="CoreHistos") continue;

	   string setfilename = InputFileDir+setname+".txt";

	   ifstream InputFile;
	   InputFile.open(setfilename.c_str());

	   while (InputFile.good()) {
	     
	     getline(InputFile, line);
	     
	     std::istringstream input_line(line);
	     string name;
	     string flag;

	     input_line >> name;
	     input_line >> flag;
	     int flagInt = TString(flag).Atoi();

	     if(flagInt == 1 || flagInt == 2 ){
	       
	       if(Counter == 0)
		 fNewCP -> MakePlots("hist_"+name, name,  OutputFolder+"/"+name+"_"+LeptonType,        true, true, true, flagInt);
	       else
		 fNewCP -> MakePlots("hist_"+name, name,  OutputFolder+"/"+name+"_"+LeptonType,        true, true, false, flagInt);
	       
	       Counter++;
	       
	     }
	     
	   }
	 }

	 delete fNewCP;

	 return 0;

}
