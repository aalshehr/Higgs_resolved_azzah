/**
 * Andrea Knue (aknue@cern.ch):
 * 
 * 
 */

#include <iostream>

#include "PlotFactoryBoosted/HistoCreator.h"
#include "PlotFactoryBoosted/StatusLogbook.h"
#include "PlotFactoryBoosted/Enums.h"
#include "PlotFactoryBoosted/ConfigClass.h"

#include "TRandom3.h"
#include "TSystem.h"
#include "TH1F.h"

#include <sys/time.h>
typedef unsigned long long timestamp_t;

static timestamp_t
get_timestamp ()
{
  struct timeval now;
  gettimeofday (&now, NULL);
  return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}


int main(int argc, char *argv[]){


 timestamp_t t0 = get_timestamp();
  

  if(argc < 18){

    WriteErrorStatus("MakePlots", "Number of input variables is wrong!!!");

    return 1;

  }
  else{

    for(int i = 1; i < argc; ++i)
      WriteParameterStatus("MakePlots", argv[i]);

  }

  std::string InputFileList  = argv[1];
  std::string OutputFile     = argv[2];
  std::string OutputTreeFile = argv[3];
  std::string SampleType     = argv[4];
  std::string JetBin         = argv[5];
  std::string BTagBin        = argv[6];
  std::string LargeRBin      = argv[7];
  std::string TopTagBin      = argv[8];
  std::string HiggsTagBin    = argv[9];
  std::string VetoJetBin     = argv[10];
  std::string VetoBTagBin    = argv[11];
  std::string VetoLargeRBin  = argv[12];
  std::string VetoTopTagBin  = argv[13];
  std::string VetoHiggsTagBin  = argv[14];
  std::string HFFlag         = argv[15];
  std::string ConfigFileName = argv[16];
  std::string TreeName       = argv[17];

  std::string ScaleFactor    = "nominal";

  if(argc == 19){
    ScaleFactor = argv[18];
    TreeName    = "nominal";
  }
  std::cout<<"AzzahMakePlots ----------------------------------1"<<std::endl;
  std::shared_ptr<ConfigClass> fConfig(new ConfigClass());
  
  fConfig -> readSettingsFromConfig(ConfigFileName.c_str());

  fConfig -> SetHFType(HFFlag);
  fConfig -> SetJetBin(TranslateBinToEnum(JetBin));
  fConfig -> SetNumberOfBTags(TranslateBinToEnum(BTagBin));
  fConfig -> SetLargeJetBin(TranslateBinToEnum(LargeRBin));
  fConfig -> SetTopTagBin(TranslateBinToEnum(TopTagBin));
  fConfig -> SetHiggsTagBin(TranslateBinToEnum(HiggsTagBin));
  fConfig -> SetVetoJetBin(TranslateBinToEnum(VetoJetBin));
  fConfig -> SetVetoNumberOfBTags(TranslateBinToEnum(VetoBTagBin));
  fConfig -> SetVetoLargeJetBin(TranslateBinToEnum(VetoLargeRBin));
  fConfig -> SetVetoTopTagBin(TranslateBinToEnum(VetoTopTagBin));
  fConfig -> SetVetoHiggsTagBin(TranslateBinToEnum(VetoHiggsTagBin));
  fConfig -> SetInputFile(InputFileList);
  fConfig -> SetOutputFile(OutputFile);
  fConfig -> SetOutputTreeFile(OutputTreeFile);
  fConfig -> SetSampleType(SampleType);
  std::cout<<"AzzahMakePlots ----------------------------------2"<<std::endl;
  std::string Channel      = fConfig -> GetChannel();
  std::string AnalysisType = fConfig -> GetAnalysisType();

  std::cout << "Channel = " << Channel.c_str() << std::endl;

  std::cout << "SampleType = " << SampleType.c_str() << std::endl;

  /*if (SampleType.find("QCD") != std::string::npos){
    if (OutputFile.find("_ejets.root") != std::string::npos && Channel.find("boosted") != std::string::npos)
      Channel = "boosted_ejets";
    else if (OutputFile.find("_mujets.root") != std::string::npos && Channel.find("boosted") != std::string::npos)
      Channel = "boosted_mujets";
    
      }*/
  std::cout<<"AzzahMakePlots ----------------------------------2-1"<<std::endl;
  
  HistoCreator *fNewHisto = new HistoCreator(InputFileList, Channel, AnalysisType, fConfig, TreeName);
  std::cout<<"AzzahMakePlots ----------------------------------2-1-2"<<std::endl;
  WriteInfoStatus("MakePlots", "Check if analysis is properly blinded...");

  //  fNewHisto -> CheckAnalysisBlinding();
  std::cout<<"AzzahMakePlots ----------------------------------2-2"<<std::endl;
  string pathToConf       = gSystem->Getenv("ROOTCOREBIN");
  //  std::string ConfigFile  = pathToConf+"/data/PlotFactoryBoosted/InputVariablesList_"+JetBin+"_"+BTagBin+".txt";

  std::string ConfigFile    = pathToConf+"/data/PlotFactoryBoosted/"+fConfig->GetConfigFileVariables();
  std::string ConfigFileDir = pathToConf+"/data/PlotFactoryBoosted/";
  std::cout<<"AzzahMakePlots ----------------------------------3"<<std::endl;
  fNewHisto -> SetScaleFactorType(ScaleFactor);
  fNewHisto -> InitializeHistograms(ConfigFile, ConfigFileDir);

  WriteInfoStatus("MakePlots", "Fill CP Distributions...");

  fNewHisto -> FillCPDistributions();

  delete fNewHisto;

  timestamp_t t1 = get_timestamp();

  double secs = (t1 - t0) / 1000000.0L;
  std::cout<<"AzzahMakePlots ----------------------------------4"<<std::endl;
  std::cout << "TIME TO MAKE PLOTS: " << secs << std::endl;

  return 0;

}
